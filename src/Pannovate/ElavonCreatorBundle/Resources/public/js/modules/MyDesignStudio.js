CLASS.define({

    name: 'Modules.MyDesignStudio',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        this.FORM_SUBMITTED_EVENT = 'Events.MyDesignStudio.ShippingAddress.formSubmitted';
        this.FORM_CANCELED_EVENT = 'Events.MyDesignStudio.ShippingAddress.formCanceled';

        var that = this;
        
        var cardDesignModel;
        var cardDesignOrderModel;
        var countryModel;
        var shippingAddressModel;

        var myDesignStudioController;
        
        var allDesignsContainer;
        var mainTab;
        var existingShippingAddressesList;
        var shippingAddressFormController;
        var shippingAddressForm;

        var countrySelectElement;

        var deleteCardDesignPopup;
        var deleteShippingAddressPopup;

        var recentOrdersList;

        this.setAllDesignsContainer = function(cnt){
        	allDesignsContainer = cnt;
        };

        this.getAllDesignContainer = function(){
        	return allDesignsContainer;
        };

        this.setMainTab = function(tab){
            mainTab = tab;
        };

        this.getMainTab = function(){
            return mainTab;
        };

        this.setExistingShippingAddressesList = function(list) {
            existingShippingAddressesList = list;
        };

        this.getExistingShippingAddressesList = function() {
            return existingShippingAddressesList;
        };

        this.setShippingAddressForm = function(form) {
            shippingAddressForm = form;
        };

        this.getShippingAddressForm = function() {
            return shippingAddressForm;
        };

        this.setDeleteCardDesignPopup = function(popup) {
            deleteCardDesignPopup = popup;
        };

        this.getDeleteCardDesignPopup = function() {
            return deleteCardDesignPopup;
        };

        this.setDeleteShippingAddressPopup = function(popup) {
            shippingAddressPopup = popup;
        };

        this.getDeleteShippingAddressPopup = function() {
            return shippingAddressPopup;
        };

        this.setCountrySelectElement = function(select) {
            countrySelectElement = select;
        };

        this.getCountrySelectElement = function() {
            return countrySelectElement;
        };

        this.setRecentOrdersList = function(list) {
            recentOrdersList = list;
        };

        this.getRecentOrdersList = function() {
            return recentOrdersList;
        };

        this.getModuleName = function() {
            return 'MyDesignStudio';
        };

        /*-------------------------------------------
         *           MODELS get/set
         *------------------------------------------*/

        this.setCardDesignModel = function(dsm) {
            cardDesignModel = dsm;
        };

        this.getCardDesignModel = function() {
            return cardDesignModel;
        };

        this.setCardDesignOrderModel = function(dsm) {
            cardDesignOrderModel = dsm;
        };

        this.getCardDesignOrderModel = function() {
            return cardDesignOrderModel;
        };

        this.setCountryModel = function(model) {
            countryModel = model;
        };

        this.getCountryModel = function() {
            return countryModel;
        };

        this.setShippingAddressModel = function(model) {
            shippingAddressModel = model;
        };

        this.getShippingAddressModel = function() {
            return shippingAddressModel;
        };


        /*----------------  END OF MODELS get/set  ------------------- */



        /*-------------------------------------------
         *           CONTROLLERS get/set
         *------------------------------------------*/

        this.setMyDesignStudioController = function(designStudioController) {
            myDesignStudioController = designStudioController;
        };

        this.getMyDesignStudioController = function() {
            return myDesignStudioController;
        };

        this.setShippingAddressFormController = function(form) {
            shippingAddressFormController = form;
        };

        this.getShippingAddressFormController = function() {
            return shippingAddressFormController;
        };

        /*----------------  END OF CONTROLLERS get/set  ------------------- */
    },

    prototypeMethods: {

    	initModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'initModule');

            /*------- DEFINE MODELS --------*/
            this.setCardDesignModel(this.getApp().getModel('Model.CardDesign'));
            this.setCardDesignOrderModel(this.getApp().getModel('Model.CardDesignOrder'));
            this.setCountryModel(this.getApp().getModel('Model.Country'));
            this.setShippingAddressModel(this.getApp().getModel('Model.ShippingAddress', [this.getCountryModel()]));

            /*------- INITIALIZE CONTROLLERS --------*/
            this.setMyDesignStudioController(this.getApp().getController('Controllers.MyDesignStudio', [this]));
            this.setShippingAddressFormController(this.getApp().getController('Controllers.Form', [this, this.getShippingAddressModel()]));

            this.getShippingAddressFormController().setFormEvents({
                FORM_SUBMITTED_EVENT: this.FORM_SUBMITTED_EVENT,
                FORM_CANCELED_EVENT: this.FORM_CANCELED_EVENT
            });
        },

        startModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getMyDesignStudioController());
            this.getApp().startController(this.getShippingAddressFormController());
        },

        setControls: function() {

            this.setAllDesignsContainer(this.getModuleElement().find('#card-designs-list'));

            var tab = this.getModuleElement().find("#design-studio-main-tab").tab({
                contentHolder: "#main-tab-content-holder",
                defaultTab: 1,
                onTabSelected: this.getMyDesignStudioController().mainTabTabSelectedHandler
            });

            this.setMainTab(tab);

            this.setRecentOrdersList(this.getModuleElement().find('#recent-orders-collapse').collapse());

            var list = this.getModuleElement().find('#existing-shipping-addresses-list').collapse();
            this.setExistingShippingAddressesList(list);

            this.setShippingAddressForm(this.getModuleElement().find('#shipping-address-form'));
            this.getShippingAddressFormController().setFormContainer(this.getShippingAddressForm());

            var selectElement = this.getShippingAddressForm().find('select[name="country"]');

            var countrySelectBoxIt = selectElement.selectBoxIt({
                // Uses the jQuery 'fadeIn' effect when opening the drop down
                showEffect: "fadeIn",

                // Sets the jQuery 'fadeIn' effect speed to 400 milleseconds
                showEffectSpeed: 400,

                // Uses the jQuery 'fadeOut' effect when closing the drop down
                hideEffect: "fadeOut",

                // Sets the jQuery 'fadeOut' effect speed to 400 milleseconds
                hideEffectSpeed: 400
            });
            this.setCountrySelectElement(selectElement);

            var deleteDesignPopupOptions = {
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Do you really want to delete design?',
                    okLabel: 'Yes',
                    cancelLabel: 'No'
                },

                onFormSubmitted: this.getMyDesignStudioController().deleteCardDesignPopupSubmittedHandler
            };

            this.setDeleteCardDesignPopup(this.getModuleElement().find("#delete-card-design-popup").panpopup(deleteDesignPopupOptions));

            var deleteShippingPopupOptions = {
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Do you really want to delete this address?',
                    okLabel: 'Yes',
                    cancelLabel: 'No'
                },

                onFormSubmitted: this.getMyDesignStudioController().deleteShippingAddressPopupSubmittedHandler
            };

            this.setDeleteShippingAddressPopup(this.getModuleElement().find("#delete-shipping-address-popup").panpopup(deleteShippingPopupOptions));
        }
    },

    extendPrototypeFrom: 'Base.Module'
});
