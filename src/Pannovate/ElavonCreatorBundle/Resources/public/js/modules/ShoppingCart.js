CLASS.define({

    name: 'Modules.ShoppingCart',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        var shoppingCartItemsList;
        var shoppingCartController;
        var removeShoppingCartItemPopup;

        var cartSubTotalContainer;
        var cartShippingChargeTotalContainer;
        var cartTaxTotalContainer;
        var orderTotalList;

        var shoppingCartModel;
        var taxModel;

        this.setShoppingCartController = function(controller) {
            shoppingCartController = controller;
        };

        this.getShoppingCartController = function() {
            return shoppingCartController;
        };

        this.setRemoveShoppingCartItemPopup = function(popup) {
            removeShoppingCartItemPopup = popup;
        };

        this.getRemoveShoppingCartItemPopup = function() {
            return removeShoppingCartItemPopup;
        };

        this.setShoppingCartItemsList = function(list) {
            shoppingCartItemsList = list;
        };

        this.getShoppingCartItemsList = function() {
            return shoppingCartItemsList;
        };

        this.setOrderTotalList = function(list) {
            orderTotalList = list;
        };

        this.getOrderTotalList = function() {
            return orderTotalList;
        };

        /*-------------------------------------------
         *           MODELS get/set
         *------------------------------------------*/

        this.setShoppingCartModel = function(model) {
            shoppingCartModel = model;
        };

        this.getShoppingCartModel = function() {
            return shoppingCartModel;
        };

        this.getModuleName = function() {
            return 'ShoppingCart';
        };
    },

    prototypeMethods: {

        initModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'initModule');

            this.setShoppingCartModel(this.getApp().getModel('Model.ShoppingCart'));

            this.setShoppingCartController(this.getApp().getController('Controllers.ShoppingCart', [this]));

            this.getApp().getCartHeaderContainer().hide();
        },

        showModule: function() {
            this.callSuper('Base.Module', 'showModule');
            this.getApp().getCartHeaderContainer().hide();
        },

        hideModule: function() {
            this.callSuper('Base.Module', 'hideModule');
            this.getApp().getCartHeaderContainer().show();
        },

        startModule: function() {
            
            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getShoppingCartController());

            if(this.isObject(this.getModuleData())) {
                this.addShoppingCartItem(this.getModuleData(), true);
                this.setModuleData(null);
            }
        },

        reinitializeModule: function(data, permissions) {

            this.callSuper('Base.Module', 'reinitializeModule', [data, permissions]);

            if(this.isObject(data)) {
                this.addShoppingCartItem(data, true);
                this.setModuleData(null);
            }
        },

        setControls: function() {

            this.setShoppingCartItemsList(this.getModuleElement().find('#shopping-cart-items-list'));
            this.setOrderTotalList(this.getModuleElement().find('#shopping-cart-total-list'));

            var deleteShoppingCartItemPopupOptions = {
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Do you really want to remove design from shopping cart?',
                    okLabel: 'Yes',
                    cancelLabel: 'No'
                },

                onFormSubmitted: this.getShoppingCartController().removeShoppingCartItemPopupSubmittedHandler
            };

            this.setRemoveShoppingCartItemPopup(this.getModuleElement().find("#remove-shopping-cart-item-popup").panpopup(deleteShoppingCartItemPopupOptions));
        },

        addShoppingCartItem: function(itemData, updateQuantity) {

            var shoppingCartItem = this.getApp().getShoppingCartItemModel().getLoadedDataRowByProperty('id', itemData.id);
            var templateExists = this.getShoppingCartItemsList().find('.shopping-cart-item[data-id="'+itemData.id+'"]').length;

            if(templateExists && shoppingCartItem && updateQuantity === true) {
                this.getApp().getShoppingCartItemModel().updateItemQuantity(shoppingCartItem.id, shoppingCartItem.quantity + 1);
            }
            else {

                var templateId = 'shopping-cart-item-template';

                if(itemData.cardDesign.type === 'Promo') {
                    templateId = 'shopping-cart-denomination-item-template';
                }

                var compiledItem = this.getApp().compileTemplate(templateId, itemData);

                this.getShoppingCartItemsList().append(compiledItem);
            }
        }
    },

    extendPrototypeFrom: 'Base.Module'
});
