CLASS.define({

    name: 'Modules.CardDesigner',

    definition: function(app, permissions) {

        this.extendFromClass('Base.Module', [app, permissions]);
        this.extendFromClass('Modules.Controls.CardDesigner');

        var that = this;

        var cardDesignModel;
        var cardDesignFrontSideModel;
        var cardDesignBackSideModel;
        var facebookModel;
        var flickrModel;
        var creatorGalleryModel;

        var cardDesignerController;
        var importFacebookPhotosController;
        var importFlickrPhotosController;
        var backgroundImageEffectsController;
        var cardDesignerImageGallery;

        var cardFrontSideElements = {};
        var cardBackSideElements = {};

        this.setSaveCardProgressInterval = function(pi) {
            saveCardProgressInterval = pi;
        };

        this.getSaveCardProgressInterval = function() {
            return saveCardProgressInterval;
        };

        this.getSaveCardProgressInterval = function() {
            return saveCardProgressInterval;
        };

        this.setWindowTopOffset = function(topOffset) {
            windowTopOffset = topOffset;
        };

        this.getWindowTopOffset = function() {
            return windowTopOffset;
        };

        this.getModuleName = function() {
            return 'CardDesigner';
        };

        /*-------------------------------------------
         *           MODELS get/set
         *------------------------------------------*/

        this.setCardDesignModel = function(dm) {
            cardDesignModel = dm;
        };

        this.getCardDesignModel = function() {
            return cardDesignModel;
        };

        this.setFacebookModel = function(fbm) {
            facebookModel = fbm;
        };

        this.getFacebookModel = function() {
            return facebookModel;
        };

        this.setFlickrModel = function(fcm) {
            flickrModel = fcm;
        };

        this.getFlickrModel = function() {
            return flickrModel;
        };

        this.setCreatorGalleryModel = function(model) {
            creatorGalleryModel = model;
        };

        this.getCreatorGalleryModel = function() {
            return creatorGalleryModel;
        };

        /*----------------  END OF MODELS get/set  ------------------- */


        /*-------------------------------------------
         *           CONTROLLERS get/set
         *------------------------------------------*/

        this.setImportFacebookPhotosController = function(fbController) {
            importFacebookPhotosController = fbController;
        };

        this.getImportFacebookPhotosController = function(ev) {
            return importFacebookPhotosController;
        };

        this.setCardDesignerController = function(designController) {
            cardDesignerController = designController;
        };

        this.getCardDesignerController = function() {
            return cardDesignerController;
        };

        this.setFlickrImportPhotosController = function(flickrController) {
            importFlickrPhotosController = flickrController;
        };

        this.getFlickrImportPhotosController = function() {
            return importFlickrPhotosController;
        };

        this.setBackgroundImageEffectsController = function(imageController) {
            backgroundImageEffectsController = imageController;
        };

        this.getBackgroundImageEffectsController = function() {
            return backgroundImageEffectsController;
        };

        this.setCardDesignerImageGalleryController = function(controller) {
            cardDesignerImageGallery = controller;
        };

        this.getCardDesignerImageGalleryController = function() {
            return cardDesignerImageGallery;
        };

        /*----------------  END OF CONTROLLERS get/set  ------------------- */


        /*-------------------------------------------
         *           EVENT HANDLERS
         *------------------------------------------*/

        this.windowScrollEventHandler = function(){

            if(that.getModuleElement().is(':visible') === false) {
                return;
            }

            that.setWindowTopOffset($(window).scrollTop());


            if( windowTopOffset > 168){ // 168 is fixed header height

                that.getModuleElement().find('.creator-page-top').css({
                    'position' : 'fixed',
                    'top' : '0',
                    'z-index': '2'
                });
                that.getModuleElement().find('.page-top-message').css({
                    'width' : '100%'
                });
                that.getModuleElement().find('.page-top-message-inner-wrapper').css({
                    'width' : '1010px',
                    'padding' : '0px 20px'
                });
                that.getModuleElement().find('.creator-page-wrapper').css({
                    'padding-top' : '135px'
                });
            }
            else{
                that.getModuleElement().find('.creator-page-top').css({
                    'position' : 'static',
                    'top' : '0'
                });
                that.getModuleElement().find('.page-top-message').css({
                    'width' : '1010px'
                });
                that.getModuleElement().find('.page-top-message-inner-wrapper').css({
                    'width' : 'auto',
                    'padding' : '0px'
                });
                that.getModuleElement().find('.creator-page-wrapper').css({
                    'padding-top' : '0'
                });
            }

            //Show on scroll Responsive designer nav
            if( windowTopOffset > 100){
                that.getModuleElement().find('.responsive-controles').addClass('opened-controles');
            }
            else{
                that.getModuleElement().find('.responsive-controles').removeClass('opened-controles');
                
                $('.card-designer-background-tools').removeClass('opened');
                $('.card-designer-tools-options').removeClass('opened');
            }
        };

        /*----------------  END OF EVENT HANDLERS  ------------------- */
    },

    prototypeMethods: {

        initModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'initModule');

            this.setWindowTopOffset($(window).scrollTop());

            /*------- DEFINE MODELS --------*/
            this.setCardDesignModel(this.getApp().getModel('Model.CardDesign'));
            this.setFacebookModel(this.getApp().getModel('Model.Facebook'));
            this.setFlickrModel(this.getApp().getModel('Model.Flickr'));
            
            /*------- INITIALIZE CONTROLLERS --------*/
            this.setImportFacebookPhotosController(this.getApp().getController('Controllers.FacebookImportPhotos', [this]));
            this.setFlickrImportPhotosController(this.getApp().getController('Controllers.FlickrImportPhotos', [this]));
            this.setBackgroundImageEffectsController(this.getApp().getController('Controllers.BackgroundImageEffects', [this]));
            this.setCardDesignerImageGalleryController(this.getApp().getController('Controllers.CardDesignerImageGallery', [this]));
        },

        reinitializeModule: function(data, permissions) {
            this.callSuper('Base.Module', 'reinitializeModule', [data, permissions]);
        },

        startModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getImportFacebookPhotosController());
            this.getApp().startController(this.getFlickrImportPhotosController());
            this.getApp().startController(this.getBackgroundImageEffectsController());

        },

        setControls: function() {

            this.callSuper('Base.Module', 'setControls');

            this.setCardFrontSideSwitchButton(this.getModuleElement().find("#card-front-side-switch-button"));
            this.setCardBackSideSwitchButton(this.getModuleElement().find("#card-back-side-switch-button"));
            this.setImportPhotosFromFacebookButton(this.getModuleElement().find("#import-from-facebook-button"));
            this.setCreateNewDesignButton(this.getModuleElement().find("#create-new-design-button"));
            this.setSaveAsDesignButton(this.getModuleElement().find("#save-as-design-button"));
            this.setCardPriceInfoContainer(this.getModuleElement().find(".amount-price-info"));
            this.setCardDesignLoader(this.getModuleElement().find("#card-design-loader"));

            var facebookPopupOptions = {
                triggerButton: this.getImportPhotosFromFacebookButton(),
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Import from Facebook',
                    loadMoreButonValue: 'Load more...'
                }
            };

            var facebookPopup = this.getModuleElement().find("#facebook-import-photo-popup").panpopup(facebookPopupOptions);
            this.setImportFromFacebookPopup(facebookPopup);

            this.setImportFromFacebookContainer(this.getImportFromFacebookPopup().find("#facebook-content-all-holder"));
            this.setLoadMoreButton(this.getImportFromFacebookPopup().find("#facebook-load-more-button"));
            this.setFacebookAlbumsContainer(this.getImportFromFacebookPopup().find("#facebook-albums-container"));
            this.setFacebookAlbumPhotosContainer(this.getImportFromFacebookPopup().find("#facebook-album-photos-container"));
            this.setPhotosLoader(this.getImportFromFacebookPopup().find("#facebook-photos-loader"));
            this.setDesignElementsCloseButtonsContainer(this.getModuleElement().find("#fabric-element-close-buttons-container"));
            this.setSaveCardDesignButton(this.getModuleElement().find("#save-card-design-button"));
        },

        addEvents: function() {
            $(window).on('scroll', this.windowScrollEventHandler);
        },

        removeEvents: function() {
            $(window).off('scroll', this.windowScrollEventHandler);
        },

        startCreator: function(designData) {

            if(!designData) {
                this.getCardDesignLoader().addClass('display-none');
                this.getApp().showNotification('error', 'Invalid Card design data.');
                return;
            }

            this.getCardDesignModel().getFrontCardSideModel().resetCardDesignElements();
            this.getCardDesignModel().getBackCardSideModel().resetCardDesignElements();

            this.getCardDesignModel().setCurrentCardDesign(designData);
            this.getCardDesignModel().getFrontCardSideModel().setCurrentCardDesign(designData);
            this.getCardDesignModel().getBackCardSideModel().setCurrentCardDesign(designData);

            if(designData.frontDesignImageData) {

                var jsonFront;

                if(this.isString(designData.frontDesignImageData)) {
                    jsonFront = JSON.parse(designData.frontDesignImageData);
                }
                else if(this.isObject(designData.frontDesignImageData)){
                    jsonFront = designData.frontDesignImageData;
                }
                else {
                    console.log("Invalid front side image data");
                }
                
                this.loadDesignFromJson('front', jsonFront);
            }
            else {
                this.getCardFrontSideDesignEditor().clear();
                this.getCardFrontSideDesignEditor().setBackgroundColor('rgb(255,255,255)');
            }


            if(designData.backDesignImageData) {

                var jsonBack;

                if(this.isString(designData.backDesignImageData)) {
                    jsonBack = JSON.parse(designData.backDesignImageData);
                }
                else if(this.isObject(designData.backDesignImageData)){
                    jsonBack = designData.backDesignImageData;
                }
                else {
                    console.log("Invalid front side image data");
                }

                this.loadDesignFromJson('back', jsonBack);
            }
            else {
                this.getCardBackSideDesignEditor().clear();
                this.getCardBackSideDesignEditor().setBackgroundColor('rgb(255,255,255)');
            }

            if(!designData.frontDesignImageData && !designData.backDesignImageData) {
                this.getCardDesignLoader().addClass('display-none');
            }
        },

        loadDesignFromJson: function(cardSide, jsonData) {

            var designEditor;

            if(cardSide === 'front') {
                designEditor = this.getCardFrontSideDesignEditor();
            }
            else {
                designEditor = this.getCardBackSideDesignEditor();
            }

            // set default params to all objects
            for(var i in jsonData.objects) {

                if(jsonData.objects[i].elementType === 'fixed-item') {
                    this.extendFixedElementParams(jsonData.objects[i]);
                }
                else {
                    this.extendElementDefaultControlParams(jsonData.objects[i]);
                }
            }

            designEditor.loadFromJSON(
                jsonData,
                this.getCardDesignerController().designEditorLoadFromJsonCallback.bind(this.getCardDesignerController(), designEditor, cardSide),
                this.getCardDesignerController().designElementLoadedCallback.bind(this.getCardDesignerController(), cardSide)
            );
        },

        extendElementDefaultControlParams: function(element) {
            var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectDefaultParams');
            $.extend(element, fabricObjectParams);
        },

        extendFixedElementParams: function(element) {
            var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectFixedParams');
            $.extend(element, fabricObjectParams);
        },

        getCurrentCardSide: function() {

            if($(this.getCardFrontSideDesignEditor().wrapperEl).is(':visible')) {
                return 'front';
            }
            else {
                return 'back';
            }
        },

        getCardEditorFromSide: function(cardSide) {
            if(cardSide === 'front') {
                return this.getCardFrontSideDesignEditor();
            }
            else {
                return this.getCardBackSideDesignEditor();
            }
        },

        createDesignElementCloseButton: function(element, cardSide) {

            var btn = $('<button />', {
                id: element.id,
                class: 'design-element-close-button card-side-' + cardSide,
                text: 'X'
            });

            this.getDesignElementsCloseButtonsContainer().append(btn);

            return btn;
        },
    },

    extendPrototypeFrom: 'Base.Module'
});
