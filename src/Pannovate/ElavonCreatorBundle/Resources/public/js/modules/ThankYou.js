CLASS.define({

    name: 'Modules.ThankYou',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        this.getModuleName = function() {
            return 'ThankYou';
        };
    },

    prototypeMethods: {},

    extendPrototypeFrom: 'Base.Module'
});
