CLASS.define({

    name: 'Modules.Controls.CardDesigner',

    definition: function() {

        var cardDesignLoader;
        var cardFrontSideDesignEditor;
        var cardBackSideDesignEditor;

        var windowTopOffset;
        var saveCardProgressInterval;

        var cardFrontSideSwitchButton;
        var cardBackSideSwitchButton;

        var importFromFacebookContainer;
        var importPhotosFromFacebookButton;
        var importFromFacebookPopup;
        var albumsContainer;
        var albumPhotosContainer;
        var loadMoreButton;
        var loadUserPhotosButton;
        var photosLoader;

        var createNewDesignButton;
        var saveCardDesignButton;
        var saveAsDesignButton;

        var backgroundImageEffectsButton;
        var designElementsCloseButtonsContainer;

        this.setSaveCardDesignButton = function(button) {
            saveCardDesignButton = button;
        };

        this.getSaveCardDesignButton = function() {
            return saveCardDesignButton;
        };

        this.setCreateNewDesignButton = function(button) {
            createNewDesignButton = button;
        };

        this.getCreateNewDesignButton = function() {
            return createNewDesignButton;
        };

        this.setSaveAsDesignButton = function(button) {
            saveAsDesignButton = button;
        };

        this.getSaveAsDesignButton = function() {
            return saveAsDesignButton;
        };

        this.setCardFrontSideSwitchButton = function(button) {
            cardFrontSideSwitchButton = button;
        };

        this.getCardFrontSideSwitchButton = function() {
            return cardFrontSideSwitchButton;
        };

        this.setCardBackSideSwitchButton = function(button) {
            cardBackSideSwitchButton = button;
        };

        this.getCardBackSideSwitchButton = function() {
            return cardBackSideSwitchButton;
        };

        this.setCardFrontSideDesignEditor = function(editor) {
            cardFrontSideDesignEditor = editor;
        };

        this.getCardFrontSideDesignEditor = function() {
            return cardFrontSideDesignEditor;
        };

        this.setCardBackSideDesignEditor = function(editor) {
            cardBackSideDesignEditor = editor;
        };

        this.getCardBackSideDesignEditor = function() {
            return cardBackSideDesignEditor;
        };

        this.setImportFromFacebookContainer = function(pc) {
            importFromFacebookContainer = pc;
        };

        this.getImportFromFacebookContainer = function() {
            return importFromFacebookContainer;
        };

        this.setImportPhotosFromFacebookButton = function(fbButton) {
            importPhotosFromFacebookButton = fbButton;
        };

        this.getImportPhotosFromFacebookButton = function() {
            return importPhotosFromFacebookButton;
        };

        this.setFacebookAlbumsContainer = function(cnt) {
            albumsContainer = cnt;
        };

        this.getFacebookAlbumsContainer = function() {
            return albumsContainer;
        };

        this.setFacebookAlbumPhotosContainer = function(cnt) {
            albumPhotosContainer = cnt;
        };

        this.getFacebookAlbumPhotosContainer = function() {
            return albumPhotosContainer;
        };

        this.setLoadMoreButton = function(lmb) {
            loadMoreButton = lmb;
        };

        this.getLoadMoreButton = function(lmb) {
            return loadMoreButton;
        };

        this.setLoadUserPhotosButton = function(lupb) {
            loadUserPhotosButton = lupb;
        };

        this.getLoadUserPhotosButton = function() {
            return loadUserPhotosButton;
        };

        this.setPhotosLoader = function(loader) {
            photosLoader = loader;
        };

        this.getPhotosLoader = function() {
            return photosLoader;
        };

        this.setDesignElementsCloseButtonsContainer = function(container) {
            designElementsCloseButtonsContainer = container;
        };

        this.getDesignElementsCloseButtonsContainer = function() {
            return designElementsCloseButtonsContainer;
        };

        this.setImportFromFacebookPopup = function(popup) {
            importFromFacebookPopup = popup;
        };

        this.getImportFromFacebookPopup = function() {
            return importFromFacebookPopup;
        };

        this.setBackgroundImageEffectsButton = function(button) {
            backgroundImageEffectsButton = button;
        };

        this.getBackgroundImageEffectsButton = function() {
            return backgroundImageEffectsButton;

        };

        this.setCardDesignLoader = function(loader) {
            cardDesignLoader = loader;
        };

        this.getCardDesignLoader = function() {
            return cardDesignLoader;
        };
    }
});
