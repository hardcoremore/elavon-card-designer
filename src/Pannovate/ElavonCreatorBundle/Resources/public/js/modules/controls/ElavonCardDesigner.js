CLASS.define({

    name: 'Modules.Controls.ElavonCardDesigner',

    definition: function() {

        var newDesignPopup;
        var saveAsDesignPopup;
        var backgroundImageToolButton;
        var backgroundColorToolButton;
        var backgroundImageSlider;
        var importPhotosFromFlickrButton;
        var creatorGalleryButton;
        var flickrUsernamePopup;
        var flickrUserPhotosPopup;
        var importUserPhotosFromFlickrButton;
        var flickrPhotosContainer;
        var flickrImportPhotosLoader;
        var flickrLoadMoreButton;
        var flickrContentAllHolder;
        var effectsButtonContainer;
        var cardDesignerImageGalleryController;
        var cardDesignerImageGalleryPopup;
        var galleryAlbumsContainer;
        var galleryPhotosContainer;
        var imageGalleryItemModel;
        var imageGalleryCategoryModel;
        var backgroundImageCanvasEditor;
        var recentCardDesignsList;
        var colorPickerButton;
        var colorPickerHolder;

        var logoImageContainer;
        var imageRulesCheckbox;
        var addToShoppingCartButton;

        var cardPriceInfoContainer;
        var numberOfCardsInput;

        this.setNumberOfCardsInput = function(input) {
            numberOfCardsInput = input;
        };

        this.getNumberOfCardsInput = function() {
            return numberOfCardsInput;
        };

        this.setCardPriceInfoContainer = function(container) {
            cardPriceInfoContainer = container;
        };

        this.getCardPriceInfoContainer = function() {
            return cardPriceInfoContainer;
        };

        this.setImageGalleryItemModel = function(model) {
            imageGalleryItemModel = model;
        };

        this.getImageGalleryItemModel = function() {
            return imageGalleryItemModel;
        };
 
        this.setImageGalleryCategoryModel = function(model) {
            imageGalleryCategoryModel = model;
        };

        this.getImageGalleryCategoryModel = function() {
            return imageGalleryCategoryModel;
        };

        this.setNewDesignPopup = function(popup) {
            newDesignPopup = popup;
        };

        this.getNewDesignPopup = function() {
            return newDesignPopup;
        };

        this.setSaveAsDesignPopup = function(popup) {
            saveAsDesignPopup = popup;
        };

        this.getSaveAsDesignPopup = function() {
            return saveAsDesignPopup;
        };

        this.setBackgroundImageToolButton = function(button) {
            backgroundImageToolButton = button;
        };

        this.getBackgroundImageToolButton = function() {
            return backgroundImageToolButton;
        };

        this.setBackgroundColorToolButton = function(button) {
            backgroundColorToolButton = button;
        };

        this.getBackgroundColorToolButton = function() {
            return backgroundColorToolButton;
        };

        this.setBackgroundImageSlider = function(slider) {
            backgroundImageSlider = slider;
        };

        this.getBackgroundImageSlider = function() {
            return backgroundImageSlider;
        };

        this.setImportPhotosFromFlickrButton = function(flickrButton) {
            importPhotosFromFlickrButton = flickrButton;
        };

        this.getImportPhotosFromFlickrButton = function() {
            return importPhotosFromFlickrButton;
        };

        this.setFlickrUsernamePopup = function(popup) {
            flickrUsernamePopup = popup;
        };

        this.getFlickrUsernamePopup = function() {
            return flickrUsernamePopup;
        };

        this.setFlickrUserPhotosPopup = function(popup) {
            flickrUserPhotosPopup = popup;
        };

        this.getFlickrUserPhotosPopup = function() {
            return flickrUserPhotosPopup;
        };

        this.setBackgroundImageEffectsPopup = function(popup) {
            backgroundImageEffectsPopup = popup;
        };

        this.getBackgroundImageEffectsPopup = function() {
            return backgroundImageEffectsPopup;
        };

        this.setFlickrPhotosContainer = function(fpc) {
            flickrPhotosContainer = fpc;
        };

        this.getFlickrPhotosContainer = function() {
            return flickrPhotosContainer;
        };

        this.setFlickrImportPhotosLoader = function(fipl) {
            flickrImportPhotosLoader = fipl;
        };

        this.getFlickrImportPhotosLoader = function() {
            return flickrImportPhotosLoader;
        };

        this.setFlickrLoadMoreButton = function(flmb) {
            flickrLoadMoreButton = flmb;
        };

        this.getFlickrLoadMoreButton = function() {
            return flickrLoadMoreButton;
        };

        this.setFlickrContentAllHolder = function(fcah) {
            flickrContentAllHolder = fcah;
        };

        this.getFlickrContentAllHolder = function() {
            return flickrContentAllHolder;
        };

        this.setEffectsButtonContainer = function(ebContainer) {
            effectsButtonContainer = ebContainer;
        };

        this.getEffectsButtonContainer = function() {
            return effectsButtonContainer;
        };

        this.setBackgroundImageCanvasEditor = function(canvas) {
            backgroundImageCanvasEditor = canvas;
        };

        this.getBackgroundImageCanvasEditor = function() {
            return backgroundImageCanvasEditor;
        };

        this.setApplyEffectButton = function(button) {
            applyEffectsButton = button;
        };

        this.getApplyEffectButton = function() {
            return applyEffectsButton;
        };

        this.setImageEffectsBackButton = function(button) {
            imageEffectsBackButton = button;
        };

        this.getImageEffectsBackButton = function() {
            return imageEffectsBackButton;
        };

        this.setCreatorGalleryButton = function(button) {
            creatorGalleryButton = button;
        };

        this.getCreatorGalleryButton = function() {
            return creatorGalleryButton;
        };

        this.setRecentCardDesignsList = function(button) {
            recentCardDesignsList = button;
        };

        this.getRecentCardDesignsList = function() {
            return recentCardDesignsList;
        };

        this.setBackgroundColorPicker = function(colorPicker) {
            backgroundColorPicker = colorPicker;
        };

        this.getBackgroundColorPicker = function() {
            return backgroundColorPicker;
        };

        this.setColorPickerHolder = function(holder) {
            colorPickerHolder = holder;
        };

        this.getColorPickerHolder = function() {
            return colorPickerHolder;
        };

        this.setLogoImageContainer = function(lic) {
            logoImageContainer = lic;
        };

        this.getLogoImageContainer = function() {
            return logoImageContainer;
        };

        this.setImageRulesCheckbox = function(checkBox) {
            imageRulesCheckbox = checkBox;
        };

        this.getImageRulesCheckbox = function() {
            return imageRulesCheckbox;
        };

        this.setAddToShoppingCartButton = function(button) {
            addToShoppingCartButton = button;
        };

        this.getAddToShoppingCartButton = function() {
            return addToShoppingCartButton;
        };

        this.setCardDesignerImageGalleryController = function(controller) {
            cardDesignerImageGalleryController = controller;
        };

        this.getCardDesignerImageGalleryController = function() {
            return cardDesignerImageGalleryController;
        };

        this.setCardDesignerImageGalleryPopup = function(popup) {
            cardDesignerImageGalleryPopup = popup;
        }

        this.getCardDesignerImageGalleryPopup = function(popup) {
            return cardDesignerImageGalleryPopup;
        }

        this.setGalleryAlbumsContainer = function(container) {
            galleryAlbumsContainer = container;
        };

        this.getGalleryAlbumsContainer = function() {
            return galleryAlbumsContainer;
        };

        this.setGalleryPhotosContainer = function(container) {
            galleryPhotosContainer = container;
        };

        this.getGalleryPhotosContainer = function() {
            return galleryPhotosContainer;
        };
    }
});