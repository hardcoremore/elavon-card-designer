CLASS.define({

    name: 'Modules.ElavonCardDesigner',

    definition: function(app, permissions) {

        this.extendFromClass('Modules.CardDesigner', [app, permissions]);
        this.extendFromClass('Modules.Controls.ElavonCardDesigner');

        var that = this;
        var currentSideModel;

        this.setCurrentSideModel = function(model) {
            currentSideModel = model;
        };

        this.getCurrentSideModel = function() {
            return currentSideModel;
        };

        this.getModuleName = function() {
            return 'ElavonCardDesigner';
        };

        this.newDesignPopupSubmittedHandler = function(ev, data) {

            that.getCardDesignModel().create(
                {
                    name: data.formData.name,
                    type: data.formData.type
                },
                {
                    scope:that
                }
            );

            that.setModuleData(data.formData.type);

            that.getNewDesignPopup().panpopup('close');
        };

        this.saveAsDesignPopupSubmittedHandler = function(ev, data) {

            var design = that.getCardDesignModel().getCurrentCardDesign();

            that.getCardDesignModel().create(
                {
                    name: data.formData.name,
                    type: that.getCardDesignModel().getCurrentCardDesign().type
                },
                {
                    scope:'save-as'
                }
            );
        };
    },

    prototypeMethods: {

        initModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Modules.CardDesigner', 'initModule');

            var frontSideModel = this.getApp().getModel('Model.ElavonCardDesignSide', null, false);
                frontSideModel.setCardSide(frontSideModel.CARD_SIDE_FRONT);

            this.getCardDesignModel().setFrontCardSideModel(frontSideModel);

            var backSideModel = this.getApp().getModel('Model.ElavonCardDesignSide', null, false);
                backSideModel.setCardSide(backSideModel.CARD_SIDE_BACK);

            this.getCardDesignModel().setBackCardSideModel(backSideModel);

            this.setImageGalleryItemModel(this.getApp().getModel('Model.ImageGalleryItem'));
            this.setImageGalleryCategoryModel(this.getApp().getModel('Model.ImageGalleryCategory'));

            this.setCardDesignerController(this.getApp().getController('Controllers.ElavonCardDesigner', [this]));

            this.setCardDesignerImageGalleryController(this.getApp().getController('Controllers.CardDesignerImageGallery', [this]));
        },

        startModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Modules.CardDesigner', 'startModule');

            /*------- INITIALIZE CONTROLLERS --------*/
            this.getApp().startController(this.getCardDesignerController());
            this.getApp().startController(this.getCardDesignerImageGalleryController());
        },

        setControls: function() {

            this.callSuper('Modules.CardDesigner', 'setControls');

            this.setBackgroundColorToolButton(this.getModuleElement().find("#background-color-tool-button"));
            this.setBackgroundImageToolButton(this.getModuleElement().find("#background-image-tool-button"));
            this.setImportPhotosFromFlickrButton(this.getModuleElement().find("#import-from-flickr-button"));
            this.setCreatorGalleryButton(this.getModuleElement().find("#import-from-gallery-button"));
            this.setBackgroundImageEffectsButton(this.getModuleElement().find(".icon-14"));
            this.setNumberOfCardsInput(this.getModuleElement().find("#card-designer-number-of-cards-input"));

            this.setCardFrontSideDesignEditor(new fabric.Canvas("card-front-side-design-editor"));
            this.setCardBackSideDesignEditor(new fabric.Canvas("card-back-side-design-editor"));

            var cardTypeOptions = [];
            var programs = this.getApp().getUserModel().getCurrentUser().programs;

            for(var i = 0, len = programs.length; i < len; i++) {
                cardTypeOptions.push({value: programs[i], label: programs[i]});
                if(programs[i].toLowerCase() === this.getModuleData()) {
                    cardTypeOptions[i].selected = 'selected';
                }
            }

            var newCardDesignPopupOptions = {
                triggerButton: [this.getCreateNewDesignButton()],
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Create new design',
                    okLabel: 'Ok',
                    cancelLabel: 'Cancel',
                    inputs:[
                        {
                            type: 'text',
                            name: 'name',
                            value: 'My Design',
                            placeHolder: 'Design name'
                        },
                        {
                            type: 'select',
                            cardType: 'type',
                            placeHolder: 'Card type',
                            options: cardTypeOptions,
                        }
                    ]
                },

                onFormSubmitted: this.newDesignPopupSubmittedHandler
            };

            var newDesignPopup = this.getModuleElement().find("#new-card-design-popup").panpopup(newCardDesignPopupOptions);
            this.setNewDesignPopup(newDesignPopup);

            var saveAsDesignPopupOptions = {
                triggerButton: this.getSaveAsDesignButton(),
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Save new design',
                    okLabel: 'Ok',
                    cancelLabel: 'Cancel',
                    inputs:[
                        {
                            type: 'text',
                            name: 'name',
                            value: 'My Design',
                            placeHolder: 'Design name'
                        }
                    ]
                },

                onFormSubmitted: this.saveAsDesignPopupSubmittedHandler
            };

            var flickrUsernamePopupOptions = {
                triggerButton: this.getImportPhotosFromFlickrButton(),
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Import from Flickr',
                    okLabel: 'Get images',
                    cancelLabel: 'Cancel',
                    inputs:[
                        {
                            type: 'text',
                            name: 'username',
                            value: '',
                            placeHolder: 'Flickr username'
                        }
                    ]
                },

                onFormSubmitted: this.getFlickrImportPhotosController().flickrUsernamePopupSubmitterHandler
            };

            var flickerUserPhotosPopupOptions = {
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Flicker user photos'
                }
            };

            var backgroundImageEffectsPopupOptions = {
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Background image effects',
                }
            };

            var cardDesignerImageGalleryPopupOptions = {
                triggerButton: this.getCreatorGalleryButton(),
                compileTemplateFunction: this.getApp().compileTemplate,
                onPopupOpen: this.getCardDesignerImageGalleryController().imageGalleryPopupOpen,
                compileTemplateData: {
                    title: 'Card designer image gallery',
                }
            };

            var saveAsDesignPopup = this.getModuleElement().find("#save-as-card-design-popup").panpopup(saveAsDesignPopupOptions);
            this.setSaveAsDesignPopup(saveAsDesignPopup);

            var flickrUsernamePopup = this.getModuleElement().find("#flickr-username-popup").panpopup(flickrUsernamePopupOptions);
            this.setFlickrUsernamePopup(flickrUsernamePopup);

            var flickerUserPhotosPopup = this.getModuleElement().find("#flickr-user-photos-popup").panpopup(flickerUserPhotosPopupOptions);
            this.setFlickrUserPhotosPopup(flickerUserPhotosPopup);

            var sliderElement = this.getModuleElement().find('#background-image-slider');

            var imageEffectsPopup = this.getModuleElement().find("#image-effects-popup").panpopup(backgroundImageEffectsPopupOptions);
            this.setBackgroundImageEffectsPopup(imageEffectsPopup);

            var cardDesignerImageGallery = this.getModuleElement().find("#gallery-photos-popup").panpopup(cardDesignerImageGalleryPopupOptions);
            this.setCardDesignerImageGalleryPopup(cardDesignerImageGallery);

            this.setBackgroundImageSlider($(sliderElement).ionRangeSlider({
                min: 0.1,
                max: 3,
                step: 0.001,
                type: 'single',
            }));

            this.setFlickrPhotosContainer(this.getFlickrUserPhotosPopup().find("#flickr-photos-container"));
            this.setFlickrImportPhotosLoader(this.getFlickrUserPhotosPopup().find("#flickr-import-photos-loader"));
            this.setFlickrLoadMoreButton(this.getFlickrUserPhotosPopup().find("#flickr-load-more-button"));
            this.setFlickrContentAllHolder(this.getFlickrUserPhotosPopup().find("#flickr-content-all-holder"));

            this.setEffectsButtonContainer(this.getBackgroundImageEffectsPopup().find("#effect-buttons-container"));
            this.setApplyEffectButton(this.getBackgroundImageEffectsPopup().find("#apply-effect-button"));
            this.setImageEffectsBackButton(this.getBackgroundImageEffectsPopup().find("#back-button-image-effects"));

            this.setRecentCardDesignsList(this.getModuleElement().find("#recent-card-designs-list"));
            this.setLogoImageContainer(this.getModuleElement().find('.logo-tools-pannel'));

            this.setGalleryAlbumsContainer(this.getCardDesignerImageGalleryPopup().find('#gallery-albums-container'));
            this.setGalleryPhotosContainer(this.getCardDesignerImageGalleryPopup().find('#gallery-photos-container'));

            var backgroundColorPicker =  $('#background-image-colorpicker').ColorPicker({
                onSubmit: this.getCardDesignerController().colorPickerButtonClickHandler
            }).bind('keyup', function () {
                $(this).ColorPickerSetColor(this.value);
            });

            this.setBackgroundColorPicker(backgroundColorPicker);

            this.setAddToShoppingCartButton(this.getModuleElement().find("#add-to-cart-button"));
            this.setImageRulesCheckbox(this.getModuleElement().find("#image-rules-checkbox"));
        },

        reinitializeModule: function(data, permissions) {

            this.callSuper('Modules.CardDesigner', 'reinitializeModule', [data, permissions]);

            if(this.isObject(data)) {
                this.startCreator(data);
            }
        },

        startCreator: function(designData) {

            this.getCardDesignModel().getFrontCardSideModel().setBackgroundImage(null);
            this.getCardDesignModel().getFrontCardSideModel().setBackgroundColor(null);
            this.getCardDesignModel().getFrontCardSideModel().setLogoImage(null);

            this.getCardDesignModel().getBackCardSideModel().setBackgroundImage(null);
            this.getCardDesignModel().getBackCardSideModel().setBackgroundColor(null);
            this.getCardDesignModel().getBackCardSideModel().setLogoImage(null);

            this.callSuper('Modules.CardDesigner', 'startCreator', [designData]);

            var currencyId = this.getApp().getSelectCurrencyList().find('.active-currency').data('id');

            this.updateCardPriceInfo(this.getNumberOfCardsInput().val(), currencyId);

            var frontImageData = designData.frontDesignImageData;
            var backImageData = designData.backDesignImageData;

            if(frontImageData === null || (this.isString(frontImageData) && frontImageData.length < 32)) {

                this.getCardDesignerController().loadFrontSideFixedElements();
                this.getCardDesignerController().loadFrontSideNonPrintingElements();
                this.getCardDesignerController().changeCardSide('front');
            }

            if(backImageData === null || (this.isString(backImageData) && backImageData.length < 32)) {
                this.getCardDesignerController().loadBackSideFixedElements();
                this.getCardDesignerController().loadBackSideNonPrintingElements();
            }
        },

        updateCardPriceInfo: function(numberOfCards, currencyId) {

            var usersCurrencyId = this.getApp().getUserModel().getCurrentUser().currency.id;

            if(typeof currencyId === 'undefined' || currencyId === null) {
                currencyId = usersCurrencyId;
            }

            var cardPricingData = this.getApp().getCardPricingModel().getCardPricingForNumberOfCards(numberOfCards, currencyId);
            var currentDesignType = this.getCardDesignModel().getCurrentCardDesign().type.toString().toLowerCase();

            var calculatedPrice = cardPricingData[currentDesignType] * numberOfCards;
            var totalPriceElement = this.getCardPriceInfoContainer().find('span[data-price="total"]');
                totalPriceElement.text(calculatedPrice.toFixed(2));

            var currencySymbol = this.getApp().getSelectCurrencyList().find('.active-currency').data('symbol');

            this.getCardPriceInfoContainer().find('span[data-currency=""]').text(currencySymbol);
            this.getCardPriceInfoContainer().find('span[data-price="per-card"]').text(cardPricingData[currentDesignType]);
        },
    },

    extendPrototypeFrom: 'Modules.CardDesigner'
});
