CLASS.define({

    name: 'Modules.Checkout',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        this.SHIPPING_ADDRESS_FORM_SUBMITTED_EVENT = 'Events.Checkout.ShippingAddress.formSubmitted';
        this.SHIPPING_ADDRESS_FORM_CANCELED_EVENT = 'Events.Checkout.ShippingAddress.formCanceled';

        this.BILLING_ADDRESS_FORM_SUBMITTED_EVENT = 'Events.Checkout.BillingAddress.formSubmitted';

        var that = this;

        var proceedToPaymentPopup;
        var checkoutModel;
        var paymentCloseButton;
        var checkoutForm;

        var countryModel;
        var shippingAddressModel;
        var billingAddressModel;
        var cardDesingOrderModel;

        var shippingCountrySelect;
        var billingCountrySelect;

        var existingShippingAddressesList;
     	var shippingAddressFormController;
        var shippingAddressForm;
        var checkoutItemsList;

        var billingAddressFormController;
        var billingAddressForm;
        var proceedToPaymentButton;
        var sameAsBillingAddressCheckBox;

        var billingAddressSelect;
        var shippingAddressSelect;
        var shippingAddressPopup;

        var checkoutController;
        var shoppingCartController;
        var shoppingCartModel;

        var orderTotalList;

        this.setExistingShippingAddressesList = function(list) {
            existingShippingAddressesList = list;
        };

        this.getExistingShippingAddressesList = function() {
            return existingShippingAddressesList;
        };

        this.setShippingAddressForm = function(form) {
            shippingAddressForm = form;
        };

        this.getShippingAddressForm = function() {
            return shippingAddressForm;
        };

        this.setBillingAddressForm = function(form) {
            billingAddressForm = form;
        };

        this.getBillingAddressForm = function() {
            return billingAddressForm;
        };

        this.setProceedToPaymentButton = function(button) {
            proceedToPaymentButton = button;
        };

        this.getProceedToPaymentButton = function() {
            return proceedToPaymentButton;
        };

        this.setCheckoutItemsList = function(list) {
            checkoutItemsList = list;
        };

        this.getCheckoutItemsList = function() {
            return checkoutItemsList;
        };

        this.setBillingAddressSelect = function(select) {
			billingAddressSelect = select;
        };

        this.getBillingAddressSelect = function() {
			return billingAddressSelect;
        };

        this.setShippingAddressSelect = function(select) {
            shippingAddressSelect = select;
        };

        this.getShippingAddressSelect = function() {
            return shippingAddressSelect;
        };

        this.setShippingCountrySelect = function(select) {
            shippingCountrySelect = select;
        };

        this.getShippingCountrySelect = function() {
            return shippingCountrySelect;
        };

        this.setBillingCountrySelect = function(select) {
            billingCountrySelect = select;
        };

        this.getBillingCountrySelect = function() {
            return billingCountrySelect;
        };

        this.setOrderTotalList = function(list) {
            orderTotalList = list;
        };

        this.getOrderTotalList = function() {
            return orderTotalList;
        };

        this.setSameAsBillingAddressCheckBox = function(checkBox) {
            sameAsBillingAddressCheckBox = checkBox;
        };

        this.getSameAsBillingAddressCheckBox = function() {
            return sameAsBillingAddressCheckBox;
        };

        this.setProceedToPaymentPopup = function(popup) {
            proceedToPaymentPopup = popup;
        };
        this.getProceedToPaymentPopup = function() {
            return proceedToPaymentPopup;
        };

        this.setPaymentCloseButton = function(button) {
            paymentCloseButton = button;
        };
        this.getPaymentCloseButton = function() {
            return paymentCloseButton;
        };

        this.setCheckoutForm = function(form) {
            checkoutForm = form;
        };
        this.getCheckoutForm = function() {
            return checkoutForm;
        };

        this.getModuleName = function() {
            return 'Checkout';
        };

        /*-------------------------------------------
         *           MODELS get/set
         *------------------------------------------*/

        this.setCountryModel = function(model) {
            countryModel = model;
        };

        this.getCountryModel = function() {
            return countryModel;
        };

        this.setShippingAddressModel = function(model) {
            shippingAddressModel = model;
        };

        this.getShippingAddressModel = function() {
            return shippingAddressModel;
        };

        this.setBillingAddressModel = function(model) {
            billingAddressModel = model;
        };

        this.getBillingAddressModel = function() {
            return billingAddressModel;
        };

        this.setCardDesignOrderModel = function(model) {
            cardDesingOrderModel = model;
        };

        this.getCardDesignOrderModel = function() {
            return cardDesingOrderModel;
        };

        this.setShoppingCartModel = function(model) {
            shoppingCartModel = model;
        };

        this.getShoppingCartModel = function() {
            return shoppingCartModel;
        };

        this.setCheckoutModel = function(model) {
            checkoutModel = model;
        };
        this.getCheckoutModel = function() {
            return checkoutModel;
        };

        /*----------------  END OF MODELS get/set  ------------------- */



        /*-------------------------------------------
         *           CONTROLLERS get/set
         *------------------------------------------*/

        this.setCheckoutController = function(controller) {
            checkoutController = controller;
        };

        this.getCheckoutController = function() {
            return checkoutController;
        };

        this.setShippingAddressFormController = function(form) {
            shippingAddressFormController = form;
        };

        this.getShippingAddressFormController = function() {
            return shippingAddressFormController;
        };

         this.setBillingAddressFormController = function(form) {
            billingAddressFormController = form;
        };

        this.getBillingAddressFormController = function() {
            return billingAddressFormController;
        };

        this.setShoppingCartController = function(controller) {
            shoppingCartController = controller;
        };

        this.getShoppingCartController = function() {
            return shoppingCartController;
        };

        /*----------------  END OF CONTROLLERS get/set  ------------------- */
    },

    prototypeMethods: {

    	initModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'initModule');

            /*------- DEFINE MODELS --------*/
            this.setCountryModel(this.getApp().getModel('Model.Country'));

            this.setShippingAddressModel(this.getApp().getModel('Model.ShippingAddress', [this.getCountryModel()]));
            this.setBillingAddressModel(this.getApp().getModel('Model.BillingAddress', [this.getCountryModel()]));
            this.setCardDesignOrderModel(this.getApp().getModel('Model.CardDesignOrder', [this]));
            this.setShoppingCartModel(this.getApp().getModel('Model.ShoppingCart', [this]));
            this.setCheckoutModel(this.getApp().getModel('Model.Checkout'));

            /*------- INITIALIZE CONTROLLERS --------*/
            this.setCheckoutController(this.getApp().getController('Controllers.Checkout', [this]));
            this.setShoppingCartController(this.getApp().getController('Controllers.ShoppingCart', [this]));
            this.setShippingAddressFormController(this.getApp().getController('Controllers.Form', [this, this.getShippingAddressModel()]));
            this.setBillingAddressFormController(this.getApp().getController('Controllers.Form', [this, this.getBillingAddressModel()]));

            this.getShippingAddressFormController().setFormEvents({
                FORM_SUBMITTED_EVENT: this.SHIPPING_ADDRESS_FORM_SUBMITTED_EVENT,
                FORM_CANCELED_EVENT: this.SHIPPING_ADDRESS_FORM_CANCELED_EVENT,
            });

            this.getBillingAddressFormController().setFormEvents({
                FORM_SUBMITTED_EVENT: this.BILLING_ADDRESS_FORM_SUBMITTED_EVENT,
            });

            this.getApp().getCartHeaderContainer().hide();
        },

        showModule: function() {
            this.callSuper('Base.Module', 'showModule');
            this.getApp().getCartHeaderContainer().hide();
        },

        hideModule: function() {
            this.callSuper('Base.Module', 'hideModule');
            this.getApp().getCartHeaderContainer().show();
        },

        startModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getCheckoutController());
            this.getApp().startController(this.getShippingAddressFormController());
            this.getApp().startController(this.getBillingAddressFormController());
        },

        setControls: function() {

            this.getModuleElement().find('#recent-orders-collapse').collapse();

            this.setSameAsBillingAddressCheckBox(this.getModuleElement().find('#same-as-billing-checkbox'));

            this.setOrderTotalList(this.getModuleElement().find('#shopping-cart-total-list'));

            this.setBillingAddressForm(this.getModuleElement().find('#billing-address-form'));
            this.setShippingAddressForm(this.getModuleElement().find('#shipping-address-form'));

            this.setProceedToPaymentButton(this.getModuleElement().find('#proceed-to-payment-button'));

            this.getBillingAddressFormController().setFormContainer(this.getBillingAddressForm());
            this.getShippingAddressFormController().setFormContainer(this.getShippingAddressForm());


            var shippingList = this.getModuleElement().find('#existing-shipping-addresses-list');
            this.setExistingShippingAddressesList(shippingList);

            this.setCheckoutItemsList(this.getModuleElement().find("#checkout-items-list"));
            
            var shippingSelectElement = this.getModuleElement().find('#existing-shipping-address-select');
            this.setShippingAddressSelect(this.createSelectBoxiIt(shippingSelectElement));

            var billingSelectElement = this.getModuleElement().find('#existing-billing-address-select');
            this.setBillingAddressSelect(this.createSelectBoxiIt(billingSelectElement));

            var countryShippingSelectElement = this.getShippingAddressFormController().getFormElementByName("country");
            this.setShippingCountrySelect(this.createSelectBoxiIt(countryShippingSelectElement));

            var countryBillingSelectElement = this.getBillingAddressFormController().getFormElementByName("country");
            this.setBillingCountrySelect(this.createSelectBoxiIt(countryBillingSelectElement));

            var proceedToPaymentPopupOptions = {
                compileTemplateFunction: this.getApp().compileTemplate,
                onPopupClose: this.getCheckoutController().paymentPopupCloseCallback,
                compileTemplateData: {
                    title: 'Last step to checkout',
                }
            };

            var proceedToPaymentPopup = this.getModuleElement().find("#proceed-to-payment-popup").panpopup(proceedToPaymentPopupOptions);
            this.setProceedToPaymentPopup(proceedToPaymentPopup);

            this.setPaymentCloseButton(this.getModuleElement().find("#payment-popup-close-button"));
            this.setCheckoutForm(this.getModuleElement().find("#checkout"));
        },

        createSelectBoxiIt: function(element) {
			var selectBoxIt = element.selectBoxIt({
                // Uses the jQuery 'fadeIn' effect when opening the drop down
                showEffect: "fadeIn",

                // Sets the jQuery 'fadeIn' effect speed to 400 milleseconds
                showEffectSpeed: 400,

                // Uses the jQuery 'fadeOut' effect when closing the drop down
                hideEffect: "fadeOut",

                // Sets the jQuery 'fadeOut' effect speed to 400 milleseconds
                hideEffectSpeed: 400
            });

            return selectBoxIt;
        }
    },

    extendPrototypeFrom: 'Base.Module'
});
