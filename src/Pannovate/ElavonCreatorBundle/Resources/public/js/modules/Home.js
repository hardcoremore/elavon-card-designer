CLASS.define({

    name: 'Modules.Home',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        this.getModuleName = function() {
            return 'Home';
        };
    },

    prototypeMethods: {

        initModule: function() {
        	
            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'initModule');


            var userPrograms = this.getApp().getUserModel().getCurrentUser().programs;

            var loyaltyCard = this.getModuleElement().find('span[data-module-data="loyalty"]');
            var promoCard = this.getModuleElement().find('span[data-module-data="promo"]');
            var giftCard = this.getModuleElement().find('span[data-module-data="gift"]');

            for(var i = 0, len = userPrograms.length; i < len; i++) {
				if(userPrograms[i].toLowerCase() === 'loyalty') {
					loyaltyCard.removeClass('inactive');
					loyaltyCard.siblings('img').addClass('inactive');
				}

				if(userPrograms[i].toLowerCase() === 'promo') {
					promoCard.removeClass('inactive');
					promoCard.siblings('img').addClass('inactive');
				}

				if(userPrograms[i].toLowerCase() === 'gift') {
					giftCard.removeClass('inactive');
					giftCard.siblings('img').addClass('inactive');
				}
    		}
    	}
    },

    extendPrototypeFrom: 'Base.Module'
});
