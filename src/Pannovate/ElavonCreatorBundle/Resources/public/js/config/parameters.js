CLASS.createNamespace('Globals.Parameters', {

    env: 'prod',

    appName: 'Elavon Creator',
    defaultModuleNamespace: 'Modules',
    defaultModule: 'Home',
    cardDesignerModuleClassname: 'Modules.ElavonCardDesigner',

    errorNotificationDuration: false, // false is infinite
    successNotificationDuration: 5000, // 5 seconds
    warningNotificationDuration: 10000, // 10 seconds
    informationNotificationDuration: 5000, // 5 seconds

    fileUploadProgressInterval: 800, // 0.8 second

    fabricExportMultiply: 5,

    pageParameterProperties: {
        rowsPerPage:  "rowsPerPage" ,
        pageNumber:  "pageNumber",
        sortColumnName:  "sortColumnName",
        sortColumnOrder: "sortColumnOrder"
    },

    fabricOjbectDefaultParams: {
        borderColor: '#C2F0FF',
        cornerColor: 'black',
        cornerSize: 14,
        transparentCorners: true
    },

    fabricOjbectFixedParams: {
        lockMovementX: true,
        lockMovementY: true,
        hasBorders: false,
        selectable: false,
        hasControls: false,
        evented: false,
    },

    fabricGlobalSerializationPropertyName: ['id', 'elementType'],
    fabricImageSerializationPropertyName: ['backgroundActive', 'originalImageWidth', 'originalImageHeight', 'userBackgroundPosition'],

    monthNames: ['Јануар', 'Фебруар', 'Март', 'Април', 'Мај', 'Јун', 'Јул', 'Август', 'Септембар', 'Октобар', 'Новембар', 'Децембар'],

    errorCodes: {
        invalidResponseFormat: 1400
    },

    errorMessages: {
        invalidResponseFormatMsg: "Read Error occured. Invalid response format. Please contact support.",

        /****** VALIDATION ********/
        fieldIsRequired: "This field is required.",
        fieldInvalidFormat: "Value is not in valid format.",
        fieldLengthError: "Maximum or minimun characters exceeded.",
        fieldRangeError: "Value is greater or lower than allowed."
    },

    frontLoyaltyLogoLight: '/images/designer/ukpr-logo-light.png',
    frontLoyaltyLogoDark: '/images/designer/ukpr-logo-dark.png',

    frontPromoLogoLight: '/images/designer/light-promo.png',
    frontPromoLogoDark: '/images/designer/dark-promo.png',

    frontGiftLogoLight: '/images/designer/light-gift.png',
    frontGiftLogoDark: '/images/designer/dark-gift.png',

    cardDisclaimerTextLoyalty: 'This card is the property of and has been issued by the retailer shown on the front of the card. ' +
                               'This card can be used to earn rewards at issuing retailer in accordance with the terms and conditions of the Loyalty ' +
                               'Programme available to view at www.fanfareloyalty.com/mydbasite. Use of this card evidences acceptance of the terms and ' +
                               'conditions of the Loyalty Programme. This card has no cash value and is not refundable or redeemable for cash. ' +
                               'This is not a cheque guarantee, credit or debit card. This card is not transferable. Valid in Country of Issue only. ' +
                               'To register and view your points balance and rewards visit www.fanfareloyalty.com/mydbasite.',

    cardDisclaimerTextPromo: 'This card has been issued by the retailer shown on the front of the card. This is a single-use promotional card, ' +
                             'not a gift card, and will not carry a future balance. This is not a cheque guarantee, credit or debit card. Card ' +
                             'may only be used for purchase of goods or services offered for sale by the issuing retailer and is not redeemable ' +
                             'for cash. Card cannot be replaced if lost or stolen. Valid in Country of Issue only.',

    cardDisclaimerTextGift: 'This gift card has been issued by the retailer shown on the front of the card. Card has no value until activated. ' +
                            'This card can be redeemed as payment for goods or services from the issuing retailer only. Not redeemable or ' +
                            'refundable for cash. Please protect this card like cash. Cannot be replaced if lost or stolen. Valid in Country ' +
                            'of Issue only. Void where prohibited. For your gift card balance, please visit fanfareloyalty.com/balance or contact ' +
                            'the issuing retailer.'
});
