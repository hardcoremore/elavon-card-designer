/*** START APP WHEN DOCUMENT IS READY ***/
$(document).ready(function(){

    var params = CLASS.getNamespaceValue('Globals.Parameters');
    
    var config = CLASS.getInstance('Base.Config', [params], false);

    var app = CLASS.getInstance('App.ElavonClient', [config]);
        app.setLocalStorage(CLASS.getInstance('Base.LocalStorage'));
        app.setRequest(CLASS.getInstance('Base.RESTRequest', [app]));
        app.init();
});