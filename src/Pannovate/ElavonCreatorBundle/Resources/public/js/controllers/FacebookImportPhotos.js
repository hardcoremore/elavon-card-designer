CLASS.define({

    name: 'Controllers.FacebookImportPhotos',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);

        var that = this;

        var currentAlbumId;

        var albumCoverPhotosLoadedCount = 0;
        var albumPhotosLoadedCount = 0;

        this.setCurrentAlbumId = function(albumId) {
            currentAlbumId = albumId;
        };

        this.getCurrentAlbumId = function() {
            return currentAlbumId;
        };

        this.setAlbumCoverPhotosLoadedCount = function(count) {
            albumCoverPhotosLoadedCount = count;
        };

        this.getAlbumCoverPhotosLoadedCount = function() {
            return albumCoverPhotosLoadedCount;
        };

        this.setAlbumPhotosLoadedCount = function(count) {
            albumPhotosLoadedCount = count;
        };

        this.getAlbumPhotosLoadedCount = function() {
            return albumPhotosLoadedCount;
        };
        

        /*** CLICK HANDLERS ****/
        this.importPhotosFromFacebookClickHandler = function(ev) {

            $("body").addClass('body-content-limit');

            if(that.isObject(that.getModule().getFacebookModel().getSessionData()) !== true) {

                that.getModule().getFacebookModel().login('public_profile, user_photos');
                that.getModule().getLoadMoreButton().hide();
                that.getModule().getPhotosLoader().hide();
            }
        };

        this.facebookControlClickHandler = function(ev) {
            
            var action = $(ev.target).data('action');

            switch(action) {

                case 'close':

                    that.getModule().getImportFromFacebookPopup().panpopup('close');

                    $("body").removeClass('body-content-limit');

                break;
                case 'refresh':
                break;
                case 'back':

                    if(that.getModule().getFacebookModel().getAlbumsPaging().next) {
                        that.getModule().getLoadMoreButton().show();
                    }
                    else {
                        that.getModule().getLoadMoreButton().hide();
                    }

                    that.getModule().getFacebookAlbumPhotosContainer().hide();
                    that.getModule().getFacebookAlbumsContainer().show();

                    that.getModule().getImportFromFacebookContainer().find('.facebook-back').hide();

                break;
            }
        };

        this.loadMoreItemsFromFacebookClickHandler = function(ev) {

            //that.getModule().getPhotosLoader().show();

            if(that.getModule().getFacebookAlbumsContainer().is(':visible')) {
                that.setAlbumCoverPhotosLoadedCount(0);
                that.getModule().getFacebookModel().loadAlbums(that.getModule().getFacebookModel().getAlbumsPaging().next);
            }
            else {
                that.setAlbumPhotosLoadedCount(0);
                that.getModule().getFacebookModel().loadAlbumPhotos(that.getCurrentAlbumId(), that.getModule().getFacebookModel().getPhotosPaging().next);
            }
        };

        this.facebookAlbumClickEventHandler = function(ev) {

            var albumId = $(ev.currentTarget).data('id');

            if(that.getCurrentAlbumId() === albumId) {

                that.getModule().getFacebookAlbumsContainer().hide();
                that.getModule().getFacebookAlbumPhotosContainer().show();
                that.getModule().getImportFromFacebookContainer().find('.facebook-back').show();

                if(that.getModule().getFacebookModel().getPhotosPaging()) {
                    that.getModule().getLoadMoreButton().show();
                }
                else {
                    that.getModule().getLoadMoreButton().hide();
                }
            }
            else {

                that.getModule().getFacebookAlbumPhotosContainer().empty();

                that.setCurrentAlbumId(albumId);
                that.setAlbumPhotosLoadedCount(0);

                that.getModule().getFacebookAlbumsContainer().hide();
                //that.getModule().getPhotosLoader().show();

                that.getModule().getFacebookModel().loadAlbumPhotos(that.getCurrentAlbumId());

                that.getModule().getImportFromFacebookContainer().find('.facebook-back').show();
            }
        };

        this.facebookPhotoClickedEventHandler = function(ev) {
            
            var photoUrl = $(ev.currentTarget).find('.facebook-image-item').first().attr('src');

            that.getModule().getFacebookModel().setSelectedPhoto(photoUrl);

            that.getModule().getImportFromFacebookPopup().panpopup('close');

            $("body").removeClass('body-content-limit');
        };
        /*** END OF CLICK HANDLERS ****/

        /*** FACEBOOK MODEL HANDLERS ***/
        this.facebookLoginCompleteHandler = function(ev) {
            that.getModule().getFacebookModel().loadAlbums();
        };


        this.facebookAlbumsLoadCompleteHandler = function(ev) {

            var albums = ev.albums;

            if(that.getModule().getFacebookModel().getAlbumsPaging().next) {
                that.getModule().getLoadMoreButton().show();
            }
            else {
                that.getModule().getLoadMoreButton().hide();
            }

            $.each(albums, function(index){

                var html = that.getApp().compileTemplate(
                    "facebook-album-item-template",
                    {
                        name:this.name,
                        albumId: index,
                    }
                );
                
                var item = $(html);
                    that.getModule().getFacebookAlbumsContainer().append(item);

                that.getModule().getFacebookModel().loadAlbumCoverPhoto(index);
            });

            that.getModule().getFacebookAlbumPhotosContainer().hide();
        };

        this.facebookAlbumCoverPhotoLoadCompleteEventHandler = function(ev) {

            $("#facebook-album-" + ev.albumId).on('load',that.albumCoverImageLoadedEventHandler);
            $("#facebook-album-" + ev.albumId).attr('src', ev.response.data.url).parent().show();
        };

        this.albumCoverImageLoadedEventHandler = function(ev){

            that.setAlbumCoverPhotosLoadedCount(that.getAlbumCoverPhotosLoadedCount() + 1);

            if(that.getAlbumCoverPhotosLoadedCount() === that.getModule().getFacebookModel().getCurrentAlbums().length - 1){
                //that.getModule().getPhotosLoader().hide();
                that.showAlbums();
            }
        };


        this.facebookAlbumPhotosLoadComplete = function(ev) {

            that.getModule().getFacebookAlbumPhotosContainer().show();

            if(that.getModule().getFacebookModel().getPhotosPaging().next) {
                that.getModule().getLoadMoreButton().show();
            }
            else {
                that.getModule().getLoadMoreButton().hide();
            }

            $.each(ev.photos, function(index){

                var html = that.getApp().compileTemplate(
                    "facebook-photo-item-template",
                    {
                        name: this.name,
                        photoId: this.id,
                    }
                );

                var item = $(html);
                that.getModule().getFacebookAlbumPhotosContainer().append(item);

                that.getModule().getFacebookModel().loadPhotoById(this.id);
            });
        };

        this.facebookPhotoLoadCompleteEventHandler = function(ev) {

            $("#facebook-photo-" + ev.photoId).on('load',that.photoLoadedEventHandler);
            $("#facebook-photo-" + ev.photoId).attr('src', ev.response.images[0].source).parent().show();
        };   

        this.photoLoadedEventHandler = function(ev) {

            that.setAlbumPhotosLoadedCount(that.getAlbumPhotosLoadedCount() + 1);

            if(that.getAlbumPhotosLoadedCount() === that.getModule().getFacebookModel().getCurrentAlbumPhotos().length - 1){

                //that.getModule().getPhotosLoader().hide();
                that.showAlbumPhotos();                
            }
        };
        /*** END OF FACEBOOK MODEL HANDLERS ****/
    },

    prototypeMethods: {

        addEvents: function() {

            $(document).on(this.getModule().getFacebookModel().events.LOGIN_COMPLETE, this.facebookLoginCompleteHandler);
            $(document).on(this.getModule().getFacebookModel().events.ALBUMS_LOAD_COMPLETE, this.facebookAlbumsLoadCompleteHandler);
            $(document).on(this.getModule().getFacebookModel().events.ALBUM_COVER_PHOTO_LOAD_COMPLETE, this.facebookAlbumCoverPhotoLoadCompleteEventHandler);
            $(document).on(this.getModule().getFacebookModel().events.ALBUM_PHOTOS_LOAD_COMPLETE, this.facebookAlbumPhotosLoadComplete);
            $(document).on(this.getModule().getFacebookModel().events.PHOTO_LOAD_COMPLETE, this.facebookPhotoLoadCompleteEventHandler);

            this.getModule().getImportFromFacebookContainer().on('click', '.facebook-import-control', this.facebookControlClickHandler);
            this.getModule().getImportPhotosFromFacebookButton().on('click', this.importPhotosFromFacebookClickHandler);
            this.getModule().getLoadMoreButton().on('click', this.loadMoreItemsFromFacebookClickHandler);
            this.getModule().getFacebookAlbumsContainer().on('click', '.facebook-album-item', this.facebookAlbumClickEventHandler);
            this.getModule().getFacebookAlbumPhotosContainer().on('click', '.facebook-photo-item', this.facebookPhotoClickedEventHandler);
        },

        removeEvents: function() {

            $(document).off(this.getModule().getFacebookModel().events.LOGIN_COMPLETE, this.facebookLoginCompleteHandler);
            $(document).off(this.getModule().getFacebookModel().events.ALBUMS_LOAD_COMPLETE, this.facebookAlbumsLoadCompleteHandler);
            $(document).off(this.getModule().getFacebookModel().events.ALBUM_COVER_PHOTO_LOAD_COMPLETE, this.facebookAlbumCoverPhotoLoadCompleteEventHandler);
            $(document).off(this.getModule().getFacebookModel().events.ALBUM_PHOTOS_LOAD_COMPLETE, this.facebookAlbumPhotosLoadComplete);
            $(document).off(this.getModule().getFacebookModel().events.PHOTO_LOAD_COMPLETE, this.facebookPhotoLoadCompleteEventHandler);

            this.getModule().getImportFromFacebookContainer().off('click', '.facebook-import-control', this.facebookControlClickHandler);
            this.getModule().getImportPhotosFromFacebookButton().off('click', this.importPhotosFromFacebookClickHandler);
            this.getModule().getLoadMoreButton().off('click', this.loadMoreItemsFromFacebookClickHandler);
            this.getModule().getFacebookAlbumsContainer().off('click', '.facebook-album-item', this.facebookAlbumClickEventHandler);
            this.getModule().getFacebookAlbumPhotosContainer().off('click', '.facebook-photo-item', this.facebookPhotoClickedEventHandler);

        },

        showAlbums: function() {
            this.getModule().getFacebookAlbumsContainer().show();  
        },

        showAlbumPhotos: function() {
            this.getModule().getFacebookAlbumPhotosContainer().show();
        }
    },

    extendPrototypeFrom: 'Base.Controller'
});
