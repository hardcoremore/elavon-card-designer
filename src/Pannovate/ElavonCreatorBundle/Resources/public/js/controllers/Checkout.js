CLASS.define({

	name: 'Controllers.Checkout',

	definition: function(app, module) {

		this.extendFromClass('Base.Controller', [app, module]);

		var that = this;
        var selectedShippingAddressId;
        var updatedRow;

		this.readCountriesForSelectCompleteHandler = function(ev) {

            that.getModule()
                .getBillingAddressFormController()
                .populateRelationField('country', ev.responseData);
            
            that.getModule().getBillingCountrySelect().selectBoxIt('refresh');

            that.getModule()
                .getShippingAddressFormController()
                .populateRelationField('country', ev.responseData);

            that.getModule().getShippingCountrySelect().selectBoxIt('refresh');

            that.getModule().getBillingAddressModel().readAll({scope:that});
            that.getModule().getShippingAddressModel().readAll({scope:that});
		};

		this.readCountriesForSelectErrorHandler = function(ev) {
			that.getApp().showNotification('error', 'Countries could not be loaded from server. Editing Shipping Addresses is disabled');
		};

        this.billingAddressReadAllCompleteEventHandler = function(ev) {

            var lastIndex = ev.responseData.length;

            if(ev.responseData.length !== 0) {

                var lastAddress = that.getModule().getBillingAddressModel().getLoadedDataRowByProperty('id', ev.responseData[lastIndex - 1].id);

                $.each(ev.responseData, function(){
                    that.addBillingAddressSelectOption(this);
                });

                that.getModule().getBillingAddressSelect().val(ev.responseData[lastIndex - 1].id);
                that.getModule().getBillingAddressSelect().selectBoxIt('refresh');
                that.getModule().getBillingAddressFormController().populateForm(lastAddress);
                that.getModule().getBillingAddressFormController().getFormElementByName("country").selectBoxIt('refresh');
            }
        };

        this.shippingAddressReadAllCompleteEventHandler = function(ev) {
            
            var lastIndex = ev.responseData.length;
            
            if(ev.responseData.length !== 0) {

                var lastAddress = that.getModule().getShippingAddressModel().getLoadedDataRowByProperty('id', ev.responseData[lastIndex - 1].id);
                var lastAddressTemplate = that.getApp().compileTemplate('existing-shipping-address-template', lastAddress);

                $.each(ev.responseData, function(){
                    that.addShippingAddressSelectOption(this);
                });

                that.getModule().getShippingAddressSelect().val(ev.responseData[lastIndex - 1].id);
                that.getModule().getShippingAddressSelect().selectBoxIt('refresh');
                that.getModule().getExistingShippingAddressesList().empty().append(lastAddressTemplate);
            }
            else {
                that.getModule().getExistingShippingAddressesList().hide();
            }
        };

        this.shippingAddressReadAllErrorHandler = function(ev) {
            that.getApp().showNotification('error', 'Loading existing shipping addresses failed :(');
        };

        this.shippingAddressFormSubmittedEventHandler = function(ev) {
            that.getModule().getShippingAddressModel().create(ev.formData, {scope:this});
        };

        this.shippingAddressCreateCompleteHandler = function(ev) {

            that.getModule().getShippingAddressFormController().resetForm();

            var shippingAddressHtml = that.getApp().compileTemplate('existing-shipping-address-template', ev.responseData);
            that.getModule().getExistingShippingAddressesList().empty().append(shippingAddressHtml);

            that.addShippingAddressSelectOption(ev.responseData);
            that.getModule().getShippingAddressSelect().val(ev.responseData.id);
            that.getModule().getShippingAddressSelect().selectBoxIt('refresh');

            if(that.getModule().getModuleElement().is(':visible')) {
                that.getApp().showNotification('success', 'You have successfully created shipping address.');
            }

            that.patchShoppingCartTotals();

            that.getModule().getModuleElement().find('.checkout-shipping-address .checkout-address-form').hide();
            that.getModule().getModuleElement().find('.checkout-shipping-options').show();
            that.getModule().getExistingShippingAddressesList().show();
        };

        this.shippingAddressCreateErrorHandler = function(ev) {
            console.log(ev.responseData);
        };

        this.shippingAddressValidationErrorHandler = function(ev) {
            that.getModule().getShippingAddressFormController().displayFormErrors(ev.validationErrors);
        };

        this.shippingAddressUpdateCompleteHandle = function(ev) {

            var shippingAddressHtml = that.getApp().compileTemplate('shipping-address-item-template', updatedRow);
            var collapseItem = that.getModule().getExistingShippingAddressesList().find('.collapse-item[data-id="'+selectedShippingAddressId+'"]');
            var collapseItemIndex = collapseItem.index();

            collapseItem.remove();

            that.getModule().getExistingShippingAddressesList().find('.collapse-item:nth-child('+collapseItemIndex+')').after(shippingAddressHtml);

            that.getApp().showNotification('success', 'You have successfully updated shipping address.');
        };

        this.selectExistingBillingAddresHandler = function(ev) {

            var selectedAddess = that.getModule().getBillingAddressModel().getLoadedDataRowByProperty('id', that.getModule().getBillingAddressSelect().val());
            that.getModule().getBillingAddressFormController().populateForm(selectedAddess);

            that.getModule().getBillingAddressFormController().getFormElementByName("country").selectBoxIt('refresh');

            that.patchShoppingCartTotals();
        };

        this.selectExistingShippingAddresHandler = function(ev) {

            var existingAddressContainer = that.getModule().getExistingShippingAddressesList();
            var selectedAddess = that.getModule().getShippingAddressModel().getLoadedDataRowByProperty('id', that.getModule().getShippingAddressSelect().val());

            existingAddressContainer.empty();

            that.patchShoppingCartTotals();

            var html = that.getApp().compileTemplate('existing-shipping-address-template', selectedAddess);
            existingAddressContainer.append(html);
        };

        this.billingAddressFormSubmittedEventHandler = function(ev) {

            if(ev.scope === 'create') {
                that.getModule().getBillingAddressModel().create(ev.formData, {scope:this});
            }
            else if(ev.scope === 'update') {

                that.getModule().getBillingAddressModel().update(
                    that.getModule().getBillingAddressSelect().val(),
                    ev.formData,
                    {scope:this}
                );
            }
            else if(ev.scope === 'delete') {
                that.getModule().getBillingAddressModel().delete(that.getModule().getBillingAddressSelect().val(),{scope:this});
            }
        };

        this.billingAddressCreateCompleteHandler = function(ev) {

            that.getApp().showNotification('success', 'You have successfully created new billing address.');

            that.addBillingAddressSelectOption(ev.responseData);
            that.patchShoppingCartTotals();

            that.getModule().getBillingAddressSelect().val(ev.responseData.id);
            that.getModule().getBillingAddressSelect().selectBoxIt('refresh');

            that.getApp().getUserModel().patch(ev.responseData.user.id, {
                firstName: ev.responseData.firstName,
                lastName: ev.responseData.lastName,
                email: ev.responseData.email,
                country: ev.responseData.country.id,
                city: ev.responseData.city
            },
            {scope:that, showLoader:false});
        };

        this.billingAddressCreateErrorHandler = function(ev) {
            console.log(ev.responseData);
        };

        this.billingAddressValidationErrorHandler = function(ev) {
            that.getModule().getBillingAddressFormController().displayFormErrors(ev.validationErrors);
        };

        this.billingAddressUpdateCompleteHandler = function(ev) {

            var selectedBillingAddressId =  that.getModule().getBillingAddressSelect().val();
            var selectedAddress = that.getModule().getBillingAddressModel().getLoadedDataRowByProperty('id', that.getModule().getBillingAddressSelect().val());
            var billingAddressSelectOptions = $(that.getModule().getBillingAddressSelect()[0]).find('option[value="'+selectedBillingAddressId+'"]');
                billingAddressSelectOptions.text(selectedAddress.address + ', ' + selectedAddress.city);

            that.patchShoppingCartTotals();

            that.getModule().getBillingAddressSelect().selectBoxIt('refresh');
            that.getApp().showNotification('success', 'You have successfully updated billing address.');
        };

        this.readAllForShoppingCartCompleteEventHandler = function(ev) {

            if(that.isArray(ev.responseData) && ev.responseData.length > 0) {
                for(var i = 0, len = ev.responseData.length; i < len; i++) {
                    that.addCheckoutItem(ev.responseData[i]);
                }
            }
            else {
                that.getApp().getModuleController().loadModule('Modules.Home');
            }           
        };

        this.readAllForShoppingCartErrorEventHandler = function(ev) {

        };

        this.shoppingCartReadCompleteEventHandler = function(ev) {

            var shoppingCart = that.getApp().getUserModel().getCurrentUser().shoppingCart;
            shoppingCart = ev.responseData;

            that.updateCartTotals(shoppingCart);
        };

        this.shoppingCartModelPatchCompleteEventHandler = function(ev) {
            that.updateCartTotals(ev.responseData);
        };

        this.shoppingCartItemCreateCompleteEventHandler = function(ev) {
            that.addCheckoutItem(ev.responseData);
        };

        this.shoppingCartItemDeleteCompleteEventHandler = function(ev) {
            var checkoutItem = that.getModule().getCheckoutItemsList().find('li[data-id="'+ev.responseData+'"]');
            checkoutItem.remove();
        };

        this.shoppingCartItemDeleteErrorEventHandler = function(ev) {

        };

        this.shoppingCartItemUpdatedEventHandler = function(ev) {
            
            var checkoutItem = that.getModule().getCheckoutItemsList().find('li[data-id="'+ev.shoppingCartItem.id+'"]');
            
            var itemListIndex = checkoutItem.index();
            
            checkoutItem.remove();

            var compiledItem = that.getApp().compileTemplate('checkout-cart-item-template', ev.shoppingCartItem);
            that.getModule().getCheckoutItemsList().find('.checkout-table-row:nth-child('+itemListIndex+')').after(compiledItem);
        };

        this.shippingAddressDeleteCompleteHandler = function(ev) {
            var element = that.getModule().getExistingShippingAddressesList().find('option[value="'+ ev.responseData +'"]');
            element.remove();
        };

        this.proceedToPaymentButtonClickEventHandler = function(ev) {

            var currentOrders = that.getModule().getCardDesignOrderModel().getLoadedData();

            if(that.isArray(currentOrders) && currentOrders.length > 0 && currentOrders[0].status === 'not_payed') {
                that.getModule().getCheckoutModel().readPaymentParams(currentOrders[0].id, {scope: that});
                return;
            }

            var cardDesignOrderData = {
                shoppingCart: that.getApp().getUserModel().getCurrentUser().shoppingCart.id
            };

            var selectedBillingAddess = that.getModule()
                                            .getBillingAddressModel()
                                            .getLoadedDataRowByProperty('id', that.getModule().getBillingAddressSelect().val());

            if(selectedBillingAddess) {
                cardDesignOrderData['billingAddress'] = selectedBillingAddess.id;
            }

            if(that.getModule().getSameAsBillingAddressCheckBox().is(":checked")) {
                cardDesignOrderData['shippingSameAsBilling'] = true;
            }
            else {
                var selectedShippingAddess = that.getModule()
                                             .getShippingAddressModel()
                                             .getLoadedDataRowByProperty('id', that.getModule().getShippingAddressSelect().val());

                if(selectedShippingAddess) {
                    cardDesignOrderData['shippingAddress'] = selectedShippingAddess.id;
                }
            }

            that.getModule().getCardDesignOrderModel().create(cardDesignOrderData, {scope:that});
        };

        this.shippingSameAsBillingCheckBoxChangeHandler = function(ev) {
            that.patchShoppingCartTotals();
        };

        this.cardDesignOrderValidationErrorHandler = function(ev) {
            console.log(ev);
        };

        this.cardDesignOrderCreateCompleteHandler = function(ev) {
            that.getModule().getCheckoutModel().readPaymentParams(ev.responseData.id, {scope: that});
        };

        this.cardDesignOrderCreateErrorHandler = function(ev) {
            console.log(ev);
        };

        this.readPaymentParamsCompleteHandler = function(ev) {

            responseData = ev.responseData;

            $('#checkout').attr('action', responseData['worldNetUrl']);
            $('input[name=TERMINALID]').val(responseData['terminal']);
            $('input[name=ORDERID]').val(responseData['orderId']);
            $('input[name=CURRENCY]').val(responseData['currency']);
            $('input[name=AMOUNT]').val(responseData['amount']);
            $('input[name=DATETIME]').val(responseData['currentDate']);
            $('input[name=RECEIPTPAGEURL]').val(responseData['receiptPageUrl']);
            $('input[name=HASH]').val(responseData['hashString']);
            $('input[name=FF_ID]').val(responseData['ffId']);
            $('#checkout').attr('action', responseData['WORLDNETURL']);
            $('input[name=TERMINALID]').val(responseData['TERMINALID']);
            $('input[name=ORDERID]').val(responseData['ORDERID']);
            $('input[name=CURRENCY]').val(responseData['CURRENCY']);
            $('input[name=AMOUNT]').val(responseData['AMOUNT']);
            $('input[name=DATETIME]').val(responseData['DATETIME']);
            $('input[name=RECEIPTPAGEURL]').val(responseData['RECEIPTPAGEURL']);
            $('input[name=HASH]').val(responseData['HASH']);
            $('input[name=FFID]').val(responseData['FFID']);

            $('#checkout').submit();

            that.getModule().getProceedToPaymentPopup().panpopup('open');
        };

        this.getPaymentResponseParamsCompleteHandler = function(ev) {

        };

        this.getPaymentResponseParamsErrorHandler = function(ev) {

        };

        this.paymentPopupCloseCallback = function() {

            var paymentTankyouPage = $("#elavon-card-designer-payment-iframe").contents().find("#payment-thankyou-page");

            if(paymentTankyouPage.length === 1) {
                
                var orderId = paymentTankyouPage.data('order-id');
                var orderData = that.getModule().getCardDesignOrderModel().getLoadedDataRowByProperty('id', orderId);

                if(orderData) {
                    orderData.status = "payed";
                }

                that.getApp().getModuleController().loadModule('Modules.Home');
            }
        };
	},

	prototypeMethods: {

        addEvents: function() {

            $(document).on(this.getModule().getShippingAddressModel().events.CREATE_COMPLETE, this.shippingAddressCreateCompleteHandler);
            $(document).on(this.getModule().getShippingAddressModel().events.CREATE_ERROR, this.shippingAddressCreateErrorHandler);
            $(document).on(this.getModule().getShippingAddressModel().events.READ_ALL_COMPLETE, this.shippingAddressReadAllCompleteEventHandler);
            $(document).on(this.getModule().getShippingAddressModel().events.READ_ALL_ERROR, this.shippingAddressReadAllErrorHandler);
            $(document).on(this.getModule().getShippingAddressModel().events.UPDATE_COMPLETE, this.shippingAddressUpdateCompleteHandle);
            $(document).on(this.getModule().getShippingAddressModel().events.UPDATE_ERROR, this.shippingAddressUpdateErrorHandle);
            $(document).on(this.getModule().getShippingAddressModel().events.VALIDATION_ERROR, this.shippingAddressValidationErrorHandler);
            $(document).on(this.getModule().getShippingAddressModel().events.DELETE_COMPLETE, this.shippingAddressDeleteCompleteHandler);
            $(document).on(this.getModule().getShippingAddressModel().events.DELETE_ERROR, this.shippingAddressDeleteErrorHandler);

            $(document).on(this.getModule().getBillingAddressModel().events.CREATE_COMPLETE, this.billingAddressCreateCompleteHandler);
            $(document).on(this.getModule().getBillingAddressModel().events.CREATE_ERROR, this.billingAddressCreateErrorHandler);
            $(document).on(this.getModule().getBillingAddressModel().events.READ_ALL_COMPLETE, this.billingAddressReadAllCompleteEventHandler);
            $(document).on(this.getModule().getBillingAddressModel().events.READ_ALL_ERROR, this.billingAddressReadAllErrorEventHandler);
            $(document).on(this.getModule().getBillingAddressModel().events.UPDATE_COMPLETE, this.billingAddressUpdateCompleteHandler);
            $(document).on(this.getModule().getBillingAddressModel().events.UPDATE_ERROR, this.billingAddressUpdateErrorHandler);
            $(document).on(this.getModule().getBillingAddressModel().events.VALIDATION_ERROR, this.billingAddressValidationErrorHandler);

            $(document).on(this.getModule().getShoppingCartModel().events.PATCH_COMPLETE, this.shoppingCartModelPatchCompleteEventHandler);
            $(document).on(this.getModule().getShoppingCartModel().events.PATCH_ERROR, this.shoppingCartModelPatchErrorEventHandler);
            $(document).on(this.getModule().getShoppingCartModel().events.FIND_BY_ID_COMPLETE, this.shoppingCartReadCompleteEventHandler);
            $(document).on(this.getModule().getShoppingCartModel().events.FIND_BY_ID_ERROR, this.shoppingCartReadErrorEventHandler);

            $(document).on(this.getModule().getCountryModel().events.READ_FOR_SELECT_COMPLETE, this.readCountriesForSelectCompleteHandler);
            $(document).on(this.getModule().getCountryModel().events.READ_FOR_SELECT_ERROR, this.readCountriesForSelectErroreHandler);

            $(document).on(this.getModule().SHIPPING_ADDRESS_FORM_SUBMITTED_EVENT, this.shippingAddressFormSubmittedEventHandler);
            $(document).on(this.getModule().SHIPPING_ADDRESS_FORM_CANCELED_EVENT, this.cancelEditShippingAddressClickHandler);

            $(document).on(this.getModule().BILLING_ADDRESS_FORM_SUBMITTED_EVENT, this.billingAddressFormSubmittedEventHandler);

            var shoppingCartItemModel = this.getApp().getShoppingCartItemModel();

            $(document).on(shoppingCartItemModel.events.CREATE_COMPLETE, this.shoppingCartItemCreateCompleteEventHandler);

            $(document).on(shoppingCartItemModel.events.READ_ALL_FOR_SHOPPING_CART_COMPLETE, this.readAllForShoppingCartCompleteEventHandler);
            $(document).on(shoppingCartItemModel.events.READ_ALL_FOR_SHOPPING_CART_ERROR, this.readAllForShoppingCartErrorEventHandler);

            $(document).on(shoppingCartItemModel.events.DELETE_COMPLETE, this.shoppingCartItemDeleteCompleteEventHandler);
            $(document).on(shoppingCartItemModel.events.DELETE_ERROR, this.shoppingCartItemDeleteErrorEventHandler);

            $(document).on(shoppingCartItemModel.events.ITEM_UPDATED, this.shoppingCartItemUpdatedEventHandler);
    
            $(document).on(this.getModule().getCardDesignOrderModel().events.CREATE_COMPLETE, this.cardDesignOrderCreateCompleteHandler);
            $(document).on(this.getModule().getCardDesignOrderModel().events.CREATE_ERROR, this.cardDesignOrderCreateErrorHandler);
            $(document).on(this.getModule().getCardDesignOrderModel().events.VALIDATION_ERROR, this.cardDesignOrderValidationErrorHandler);

            $(document).on(this.getModule().getCheckoutModel().events.READ_PAYMENT_PARAMS_COMPLETE, this.readPaymentParamsCompleteHandler);
            $(document).on(this.getModule().getCheckoutModel().events.PAYMENT_RESPONSE_PARAMS_COMPLETE, this.getPaymentResponseParamsCompleteHandler);
            $(document).on(this.getModule().getCheckoutModel().events.PAYMENT_RESPONSE_PARAMS_ERROR, this.getPaymentResponseParamsErrorHandler);

            this.getModule().getSameAsBillingAddressCheckBox().on('change', this.shippingSameAsBillingCheckBoxChangeHandler);
            this.getModule().getBillingAddressSelect().on('change', this.selectExistingBillingAddresHandler);
            this.getModule().getShippingAddressSelect().on('change', this.selectExistingShippingAddresHandler);

            this.getModule().getProceedToPaymentButton().on('click', this.proceedToPaymentButtonClickEventHandler);
        },

        removeEvents: function() {

            $(document).off(this.getModule().getShippingAddressModel().events.CREATE_COMPLETE, this.shippingAddressCreateCompleteHandler);
            $(document).off(this.getModule().getShippingAddressModel().events.CREATE_ERROR, this.shippingAddressCreateErrorHandler);
            $(document).off(this.getModule().getShippingAddressModel().events.READ_ALL_COMPLETE, this.shippingAddressReadAllCompleteEventHandler);
            $(document).off(this.getModule().getShippingAddressModel().events.READ_ALL_ERROR, this.shippingAddressReadAllErrorHandler);
            $(document).off(this.getModule().getShippingAddressModel().events.VALIDATION_ERROR, this.shippingAddressValidationErrorHandler);

            $(document).off(this.getModule().getBillingAddressModel().events.CREATE_COMPLETE, this.billingAddressCreateCompleteHandler);
            $(document).off(this.getModule().getBillingAddressModel().events.CREATE_ERROR, this.billingAddressCreateErrorHandler);
            $(document).off(this.getModule().getBillingAddressModel().events.READ_ALL_COMPLETE, this.billingAddressReadAllCompleteEventHandler);
            $(document).off(this.getModule().getBillingAddressModel().events.READ_ALL_ERROR, this.billingAddressReadAllErrorEventHandler);
            $(document).off(this.getModule().getBillingAddressModel().events.UPDATE_COMPLETE, this.billingAddressUpdateCompleteHandler);
            $(document).off(this.getModule().getBillingAddressModel().events.UPDATE_ERROR, this.billingAddressUpdateErrorHandler);
            $(document).off(this.getModule().getBillingAddressModel().events.VALIDATION_ERROR, this.billingAddressValidationErrorHandler);
            $(document).off(this.getModule().getShippingAddressModel().events.DELETE_COMPLETE, this.shippingAddressDeleteCompleteHandler);
            $(document).off(this.getModule().getShippingAddressModel().events.DELETE_ERROR, this.shippingAddressDeleteErrorHandler);

            $(document).off(this.getModule().getShoppingCartModel().events.PATCH_COMPLETE, this.shoppingCartModelPatchCompleteEventHandler);
            $(document).off(this.getModule().getShoppingCartModel().events.PATCH_ERROR, this.shoppingCartModelPatchErrorEventHandler);

            $(document).off(this.getModule().getCountryModel().events.READ_FOR_SELECT_COMPLETE, this.readCountriesForSelectCompleteHandler);
            $(document).off(this.getModule().getCountryModel().events.READ_FOR_SELECT_ERROR, this.readCountriesForSelectErroreHandler);

            $(document).off(this.getModule().SHIPPING_ADDRESS_FORM_SUBMITTED_EVENT, this.shippingAddressFormSubmittedEventHandler);
            $(document).off(this.getModule().SHIPPING_ADDRESS_FORM_CANCELED_EVENT, this.cancelEditShippingAddressClickHandler);

            $(document).off(this.getModule().BILLING_ADDRESS_FORM_SUBMITTED_EVENT, this.billingAddressFormSubmittedEventHandler);

            var shoppingCartItemModel = this.getApp().getShoppingCartItemModel();

            $(document).off(shoppingCartItemModel.events.CREATE_COMPLETE, this.shoppingCartItemCreateCompleteEventHandler);

            $(document).off(shoppingCartItemModel.events.READ_ALL_FOR_SHOPPING_CART_COMPLETE, this.readAllForShoppingCartCompleteEventHandler);
            $(document).off(shoppingCartItemModel.events.READ_ALL_FOR_SHOPPING_CART_ERROR, this.readAllForShoppingCartErrorEventHandler);

            $(document).off(shoppingCartItemModel.events.DELETE_COMPLETE, this.shoppingCartItemDeleteCompleteEventHandler);
            $(document).off(shoppingCartItemModel.events.DELETE_ERROR, this.shoppingCartItemDeleteErrorEventHandler);

            $(document).off(shoppingCartItemModel.events.ITEM_UPDATED, this.shoppingCartItemUpdatedEventHandler);

            $(document).off(this.getModule().getCardDesignOrderModel().events.CREATE_COMPLETE, this.cardDesignOrderCreateCompleteHandler);
            $(document).off(this.getModule().getCardDesignOrderModel().events.CREATE_ERROR, this.cardDesignOrderCreateErrorHandler);
            $(document).off(this.getModule().getCardDesignOrderModel().events.VALIDATION_ERROR, this.cardDesignOrderValidationErrorHandler);

            $(document).off(this.getModule().getCheckoutModel().events.READ_PAYMENT_PARAMS_COMPLETE, this.readPaymentParamsCompleteHandler);
            $(document).off(this.getModule().getCheckoutModel().events.PAYMENT_RESPONSE_PARAMS_COMPLETE, this.getPaymentResponseParamsCompleteHandler);
            $(document).off(this.getModule().getCheckoutModel().events.PAYMENT_RESPONSE_PARAMS_ERROR, this.getPaymentResponseParamsErrorHandler);

            this.getModule().getSameAsBillingAddressCheckBox().off('change', this.shippingSameAsBillingCheckBoxChangeHandler);
            this.getModule().getBillingAddressSelect().off('change', this.selectExistingBillingAddresHandler);
            this.getModule().getShippingAddressSelect().off('change', this.selectExistingShippingAddresHandler);

            this.getModule().getProceedToPaymentButton().off('click', this.proceedToPaymentButtonClickEventHandler);
        },

        startController: function() {

            this.getModule().getCountryModel().readForSelect({scope:this});

            var loadedData = this.getApp().getShoppingCartItemModel().getLoadedData();
            var currentUser = this.getApp().getUserModel().getCurrentUser();

            if(loadedData && loadedData.length > 0) {
                for(var i = 0, len = loadedData.length; i < len; i++) {
                    this.addCheckoutItem(loadedData[i]);
                }
            }
            else {
                this.getApp().getShoppingCartItemModel().readAllForCurrentUser({scope:this});
            }

            var shoppingCartId = this.getApp().getUserModel().getCurrentUser().shoppingCart.id;
            this.getApp().getShoppingCartModel().findById(shoppingCartId, {scope:this, showLoader:false});
        },

        addShippingAddressSelectOption: function(item) {

            var option = $('<option/>', {
                text: item.address + ', ' + item.city,
                'value': item.id
            });

            this.getModule().getShippingAddressSelect().append(option);
            this.getModule().getShippingAddressSelect().selectBoxIt('refresh');
        },

        addBillingAddressSelectOption: function(item) {

            var option = $('<option/>', {
                text: item.address + ', ' + item.city,
                'value': item.id
            });

            this.getModule().getBillingAddressSelect().append(option);
            this.getModule().getBillingAddressSelect().selectBoxIt('refresh');
        },

        addCheckoutItem: function(data) {

            var compiledItem = this.getApp().compileTemplate('checkout-cart-item-template', data);
            this.getModule().getCheckoutItemsList().append(compiledItem);
        },

        updateCartTotals: function(shoppingCart) {

            if(shoppingCart.cartItemsTotalCount === 0) {
                shoppingCart.shippingCharge = 0;
                shoppingCart.taxCharge = 0;
                shoppingCart.cartPriceTotal = 0;
            }

            var usersCurrency = this.getApp().getUserModel().getCurrentUser().currency;

            var totalPriceTemplate = this.getApp().compileTemplate('checkout-item-total', {
                total: shoppingCart.cartPriceTotal,
                symbol: usersCurrency.symbol,
                charge: shoppingCart.shippingCharge,
                tax: shoppingCart.taxCharge,
                subtotal: shoppingCart.cartItemsTotalPrice
            });

            this.getModule().getOrderTotalList().empty();
            this.getModule().getOrderTotalList().append(totalPriceTemplate);
        },

        patchShoppingCartTotals: function() {

            var shoppingCartId = this.getApp().getUserModel().getCurrentUser().shoppingCart.id;
            var billingAddressId =  this.getModule().getBillingAddressSelect().val();
            var shippingAddressId = this.getModule().getShippingAddressSelect().val();
            var shippingSameAsBilling = this.getModule().getSameAsBillingAddressCheckBox();

            if(shippingSameAsBilling.is(":checked")) {
                shippingSameAsBilling = true;
            }
            else {
                shippingSameAsBilling = false;
            }

            console.log(shippingSameAsBilling);

            this.getApp().getShoppingCartModel().patch(shoppingCartId, {
                billingAddress: billingAddressId,
                shippingAddress: shippingAddressId,
                shippingSameAsBilling: shippingSameAsBilling
            },
            {scope:this, dataType: 'json'});
        }
    },

	extendPrototypeFrom: 'Base.Controller'
});