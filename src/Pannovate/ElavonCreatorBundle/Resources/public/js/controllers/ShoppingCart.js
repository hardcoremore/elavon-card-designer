CLASS.define({

	name: 'Controllers.ShoppingCart',

	definition: function(app, module) {

		this.extendFromClass('Base.Controller', [app, module]);

		var that = this;
        
        var deletingShoppingCartItemId;
        var quantityKeyupTimeout;

        this.shoppingCartItemCreateCompleteEventHandler = function (ev) {
            var shoppingCartId = that.getApp().getUserModel().getCurrentUser().shoppingCart.id;
            that.getModule().getShoppingCartModel().findById(shoppingCartId, {scope:this, showLoader:false});
        };

        this.readAllForShoppingCartCompleteEventHandler = function(ev) {
            
            $.each(ev.responseData, function() {
                that.getModule().addShoppingCartItem(this);
            });
        };

        this.readAllForShoppingCartErrorEventHandler = function(ev) {

        };

        this.shoppingCartItemUpdateCompleteEventHandler = function(ev) {
        };

        this.shoppingCartItemUpdateErrorEventHandler = function(ev) {

        };

        this.shoppingCartItemValidationErrorEventHandler = function(ev) {

        };

        this.removeShoppingCartItemPopupSubmittedHandler = function(ev) {

            that.getApp().getShoppingCartItemModel().delete(deletingShoppingCartItemId, {scope: that});

            that.getModule().getRemoveShoppingCartItemPopup().panpopup('close');


        };

        this.removeShoppingItemButtonClickHandler = function(ev) {

            var shoppingCartItemId = $(ev.currentTarget).closest('.shopping-cart-item').data('id');
            deletingShoppingCartItemId = shoppingCartItemId;

            that.getModule().getRemoveShoppingCartItemPopup().panpopup('open');
        };

        this.shoppingCartItemDeleteCompleteEventHandler = function() {
            that.getModule().getShoppingCartItemsList().find('.shopping-cart-item[data-id="'+deletingShoppingCartItemId+'"]').remove();

            var shoppingCartId = that.getApp().getUserModel().getCurrentUser().shoppingCart.id;
            that.getModule().getShoppingCartModel().findById(shoppingCartId, {scope:this, showLoader:false});
            that.getApp().showNotification('success', 'You have successfully removed card design from shopping cart');

            deletingShoppingCartItemId = null;
        };

        this.shoppingCartItemDeleteErrorEventHandler = function() {
            that.getApp().showNotification('error', 'Error ocurred while removing item from shopping cart :(');
        };

        this.shoppingCartItemQuantityInputKeyUpEventHandler = function(ev) {

            if(quantityKeyupTimeout) {
                clearTimeout(quantityKeyupTimeout);
            }

            var inputField = $(ev.currentTarget);
            var isNumber = !isNaN(parseInt(ev.key));
            var isBackSpace = ev.keyCode === 8;

            if(isNumber || isBackSpace) {
                quantityKeyupTimeout = setTimeout(function() {
                    quantityKeyupTimeout = null;
                    var shoppingCartItemId = $(ev.currentTarget).closest('.shopping-cart-item').data('id');
                    that.getApp().getShoppingCartItemModel().updateItemQuantity(shoppingCartItemId, inputField.val());    
                }, 300);
            }
        };

        this.shoppingCartReadCompleteEventHandler = function(ev) {

            var shoppingCart = that.getApp().getUserModel().getCurrentUser().shoppingCart;
            shoppingCart = ev.responseData;

            that.updateCartTotals(shoppingCart);
        };

        this.shoppingCartItemPatchCompleteEventHandler = function(ev) {
            console.log('patch');

            var shoppingCartId = that.getApp().getUserModel().getCurrentUser().shoppingCart.id;
            that.getModule().getShoppingCartModel().findById(shoppingCartId, {scope:this, showLoader:false});
        };
	},

	prototypeMethods: {

        addEvents: function() {

            var shoppingCartItemModel = this.getApp().getShoppingCartItemModel();

            $(document).on(shoppingCartItemModel.events.CREATE_COMPLETE, this.shoppingCartItemCreateCompleteEventHandler);

            $(document).on(shoppingCartItemModel.events.READ_ALL_FOR_SHOPPING_CART_COMPLETE, this.readAllForShoppingCartCompleteEventHandler);
            $(document).on(shoppingCartItemModel.events.READ_ALL_FOR_SHOPPING_CART_ERROR, this.readAllForShoppingCartErrorEventHandler);
            $(document).on(shoppingCartItemModel.events.UPDATE_COMPLETE, this.shoppingCartItemUpdateCompleteEventHandler);
            $(document).on(shoppingCartItemModel.events.UPDATE_ERROR, this.shoppingCartItemUpdateErrorEventHandler);
            $(document).on(shoppingCartItemModel.events.PATCH_COMPLETE, this.shoppingCartItemPatchCompleteEventHandler);
            $(document).on(shoppingCartItemModel.events.PATCH_ERROR, this.shoppingCartItemPatchErrorEventHandler);

            $(document).on(this.getModule().getShoppingCartModel().events.FIND_BY_ID_COMPLETE, this.shoppingCartReadCompleteEventHandler);
            $(document).on(this.getModule().getShoppingCartModel().events.FIND_BY_ID_ERROR, this.shoppingCartReadErrorEventHandler);

            $(document).on(shoppingCartItemModel.events.DELETE_COMPLETE, this.shoppingCartItemDeleteCompleteEventHandler);
            $(document).on(shoppingCartItemModel.events.DELETE_ERROR, this.shoppingCartItemDeleteErrorEventHandler);
            $(document).on(shoppingCartItemModel.events.VALIDATION_ERROR, this.shoppingCartItemValidationErrorEventHandler);

            this.getModule().getShoppingCartItemsList().on('click', '.remove-shopping-cart-item-button', this.removeShoppingItemButtonClickHandler);

            this.getModule().getShoppingCartItemsList().on('keydown', '.shopping-cart-item-quantity-input', this.onlyNumbersKeyDownHandler);
            this.getModule().getShoppingCartItemsList().on('keyup', '.shopping-cart-item-quantity-input', this.shoppingCartItemQuantityInputKeyUpEventHandler);
        },

        removeEvents: function() {

            var shoppingCartItemModel = this.getApp().getShoppingCartItemModel();

            $(document).off(shoppingCartItemModel.events.CREATE_COMPLETE, this.shoppingCartItemCreateCompleteEventHandler);
            $(document).off(shoppingCartItemModel.events.READ_ALL_FOR_SHOPPING_CART_COMPLETE, this.readAllForShoppingCartCompleteEventHandler);
            $(document).off(shoppingCartItemModel.events.READ_ALL_FOR_SHOPPING_CART_ERROR, this.readAllForShoppingCartErrorEventHandler);
            $(document).off(shoppingCartItemModel.events.UPDATE_COMPLETE, this.shoppingCartItemUpdateCompleteEventHandler);
            $(document).off(shoppingCartItemModel.events.UPDATE_ERROR, this.shoppingCartItemUpdateErrorEventHandler);
            $(document).off(shoppingCartItemModel.events.PATCH_COMPLETE, this.shoppingCartItemPatchCompleteEventHandler);
            $(document).off(shoppingCartItemModel.events.PATCH_ERROR, this.shoppingCartItemPatchErrorEventHandler);

            $(document).off(this.getModule().getShoppingCartModel().events.FIND_BY_ID_COMPLETE, this.shoppingCartReadCompleteEventHandler);
            $(document).off(this.getModule().getShoppingCartModel().events.FIND_BY_ID_ERROR, this.shoppingCartReadErrorEventHandler);

            $(document).off(shoppingCartItemModel.events.DELETE_COMPLETE, this.shoppingCartItemDeleteCompleteEventHandler);
            $(document).off(shoppingCartItemModel.events.DELETE_ERROR, this.shoppingCartItemDeleteErrorEventHandler);
            $(document).off(shoppingCartItemModel.events.VALIDATION_ERROR, this.shoppingCartItemValidationErrorEventHandler);

            this.getModule().getShoppingCartItemsList().off('click', '.remove-shopping-cart-item-button', this.removeShoppingItemButtonClickHandler);

            this.getModule().getShoppingCartItemsList().off('keydown', '.shopping-cart-item-quantity-input', this.offlyNumbersKeyDownHandler);
            this.getModule().getShoppingCartItemsList().on('keyup', '.shopping-cart-item-quantity-input', this.shoppingCartItemQuantityInputKeyUpEventHandler);
        },

        startController: function() {
            
            var currentUser = this.getApp().getUserModel().getCurrentUser();
            var cart = currentUser.shoppingCart;
            
            this.getModule().getModuleElement().find('.shopping-cart-currency-symbol').text(currentUser.currency.symbol);

            var loadedData = this.getApp().getShoppingCartItemModel().getLoadedData();

            if(loadedData === null) {
                this.getApp().getShoppingCartItemModel().readAllForCurrentUser({scope:this});
            }
            else {
                var that = this;
                $.each(loadedData, function() {
                    that.getModule().addShoppingCartItem(this);
                });
            }

            var shoppingCartId = this.getApp().getUserModel().getCurrentUser().shoppingCart.id;
            this.getModule().getShoppingCartModel().findById(shoppingCartId, {scope:this, showLoader:false});
        },

        updateCartTotals: function(shoppingCart) {

            if(shoppingCart.cartItemsTotalCount === 0) {
                shoppingCart.shippingCharge = 0;
                shoppingCart.taxCharge = 0;
                shoppingCart.cartPriceTotal = 0;
            }

            var usersCurrency = this.getApp().getUserModel().getCurrentUser().currency;

            var totalPriceTemplate = this.getApp().compileTemplate('shopping-cart-item-total', {
                total: shoppingCart.cartPriceTotal,
                symbol: usersCurrency.symbol,
                charge: shoppingCart.shippingCharge,
                tax: shoppingCart.taxCharge,
                subtotal: shoppingCart.cartItemsTotalPrice
            });

            this.getModule().getOrderTotalList().empty();
            this.getModule().getOrderTotalList().append(totalPriceTemplate);
        }
	},

	extendPrototypeFrom: 'Base.Controller'
});