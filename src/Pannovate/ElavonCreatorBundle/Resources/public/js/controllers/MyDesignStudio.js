CLASS.define({

	name: 'Controllers.MyDesignStudio',

	definition: function(app, module) {

		this.extendFromClass('Base.Controller', [app, module]);

		var that = this;
        var selectedShippingAddressId;
        var shippingAddressToBeDeletedId;
        var cardDesignToBeDeletedId;
        var updatedRow;

		this.readCountriesForSelectCompleteHandler = function(ev) {
            that.getModule()
                .getShippingAddressFormController()
                .populateRelationField('country', ev.responseData);

            that.getModule().getCountrySelectElement().selectBoxIt('refresh');
		};

		this.readCountriesForSelectErrorHandler = function(ev) {
			that.getApp().showNotification('error', 'Countries could not be loaded from server. Editing Shipping Addresses is disabled');
		};

		this.readAllDesignsCompleteEventHandler = function(ev) {
			
			$.each(ev.responseData, function(){
				var cardDesignsHtml = that.getApp().compileTemplate('preview-design-item-template', this);
                that.getModule().getAllDesignContainer().append(cardDesignsHtml);
			});
		};

		this.editCardDesignButtonClickedHandler = function(ev) {

			var designId = $(ev.currentTarget).data('id');
            var designData = that.getModule().getCardDesignModel().getLoadedDataRowByProperty('id', designId);

            var cardDesignerClassName = that.getApp().getConfig().getParameter('cardDesignerModuleClassname');

            that.getApp().getModuleController().loadModule(cardDesignerClassName, designData, true);
		};

        this.deleteCardDesignButtonClickHandler = function(ev) {
            cardDesignToBeDeletedId = $(ev.currentTarget).data('id');

            that.getModule().getDeleteCardDesignPopup().panpopup('open');
        };

        this.deleteCardDesignPopupSubmittedHandler = function(ev) {

            var element = that.getModule().getModuleElement().find('.card-design-item[data-id = ' + cardDesignToBeDeletedId + ']');

            that.getModule().getCardDesignModel().delete(cardDesignToBeDeletedId, {scope: that, designId: cardDesignToBeDeletedId});
            that.getModule().getDeleteCardDesignPopup().panpopup('close');

            element.remove();
        };

        this.shippingAddressReadAllCompleteHandler = function(ev) {

            $.each(ev.responseData, function(index){
                that.addShippingAddressItem(this);
            });
        };

        this.shippingAddressReadAllErrorHandler = function(ev) {
            that.getApp().showNotification('error', 'Loading existing shipping addresses failed :(');
        };

		this.mainTabTabSelectedHandler = function(ev, data) {

			if(data.selectedTab.indexOf('shipping-addresses-tab') !== -1) {

                var loadedShipiingAddresses = that.getModule().getShippingAddressModel().getLoadedData();
                if(!loadedShipiingAddresses) {
                    that.getModule().getShippingAddressModel().readAll({scope:that});
                }
                else {
                    that.getModule().getExistingShippingAddressesList().empty();

                    $.each(loadedShipiingAddresses, function(index){
                        that.addShippingAddressItem(this);
                    });
                }

                if(!that.getModule().getCountryModel().getSelectData()) {
                    that.getModule().getCountryModel().readForSelect({scope:that});
                }
			}
            else if(data.selectedTab.indexOf('my-designs-tab-content') !== -1) {

                var cardDesign = that.getModule().getCardDesignModel().getCurrentCardDesign();
            }

            else {
                var loadedRecentOrders = that.getModule().getCardDesignOrderModel().getLoadedData();
                if(!loadedRecentOrders) {
                    that.getModule().getCardDesignOrderModel().readAll({scope:that});
                }
                else {
                    that.getModule().getRecentOrdersList().empty();

                    $.each(loadedRecentOrders, function(index) {
                        that.addRecentOrderItem(this);
                    });
                }
            }
		};

        this.shippingAddressFormSubmittedEventHandler = function(ev) {

            if(ev.scope === 'create') {
                that.getModule().getShippingAddressModel().create(ev.formData, {scope:this});
            }
            else {
                updatedRow = ev.formData;
                that.getModule().getShippingAddressModel().update(selectedShippingAddressId, ev.formData, {scope:this});
            }
        };

        this.shippingAddressCreateCompleteHandle = function(ev) {

            var shippingAddressHtml = that.getApp().compileTemplate('shipping-address-item-template', ev.responseData);
            that.getModule().getExistingShippingAddressesList().append(shippingAddressHtml);
            

            if(that.getModule().getModuleElement().is(':visible')) {
                that.getModule().getShippingAddressFormController().resetForm();
                that.getApp().showNotification('success', 'You have successfully created shipping address.');
            }
        };

        this.shippingAddressCreateErrorHandle = function(ev) {
            console.log(ev.responseData);
        };

        this.shippingAddressValidationErrorHandle = function(ev) {
            that.getModule().getShippingAddressFormController().displayFormErrors(ev.validationErrors);
        };

        this.editShippingAddresClickHandler = function(ev) {

            selectedShippingAddressId = $(ev.currentTarget).data('id');

            var row = that.getModule().getShippingAddressModel().getLoadedDataRowByProperty('id', selectedShippingAddressId);

            if(row) {
                that.getModule().getShippingAddressFormController().populateForm(row);

                that.getModule().getShippingAddressForm().find('#create-shipping-address-button').hide();
                that.getModule().getShippingAddressForm().find('#update-shipping-address-button').show();
                that.getModule().getShippingAddressForm().find('#cancel-update-shipping-address-button').show();

                that.getModule().getShippingAddressFormController().getFormElementByName("country").selectBoxIt('refresh');
            }
        };

        this.cancelEditShippingAddressClickHandler = function(ev) {
            that.getModule().getShippingAddressForm().find('#create-shipping-address-button').show();
            that.getModule().getShippingAddressForm().find('#update-shipping-address-button').hide();
            that.getModule().getShippingAddressForm().find('#cancel-update-shipping-address-button').hide();
        };

        this.shippingAddressUpdateCompleteHandle = function(ev) {

            var shippingAddressHtml = that.getApp().compileTemplate('shipping-address-item-template', updatedRow);

            var collapseItem = that.getModule().getExistingShippingAddressesList().find('.collapse-item[data-id="'+selectedShippingAddressId+'"]');
            var collapseItemIndex = collapseItem.index();
            collapseItem.remove();

            that.getModule().getExistingShippingAddressesList().find('.collapse-item:nth-child('+collapseItemIndex+')').after(shippingAddressHtml);

            that.getApp().showNotification('success', 'You have successfully update shipping address.');
        };

        this.deleteShippingAddressClickHandler = function(ev) {
            shippingAddressToBeDeletedId = $(ev.currentTarget).data('id');

            that.getModule().getDeleteShippingAddressPopup().panpopup('open');
        };

        this.deleteShippingAddressPopupSubmittedHandler = function(ev) {

            var element = that.getModule().getExistingShippingAddressesList().find('.collapse-item[data-id="'+shippingAddressToBeDeletedId+'"]');
            
            that.getModule().getShippingAddressModel().delete(shippingAddressToBeDeletedId, {scope: that});
            that.getModule().getDeleteShippingAddressPopup().panpopup('close');

            element.remove();
        };

        this.shippingAddressDeleteCompleteHandler = function(ev) {
            that.getApp().showNotification('success', 'You have successfully deleted shipping address.');  
        };

        this.saveCardDesignCompleteHandler = function(ev) {

            var designExists = false;

            var designData = $.extend({}, ev.cardDesign, {
                imageCacheHash: new Date().getTime()
            });

            that.getModule().getAllDesignContainer().find('.card-design-item').each(function(){

                var cardDesignsHtml = that.getApp().compileTemplate('preview-design-item-template', designData);
                var element = $(this);    
                var index = element.index();

                if(element.data('id').toString() === ev.cardDesign.id.toString()) {

                    designExists = true;
                    that.getModule().getAllDesignContainer().find('.card-design-item:nth-child('+(index+1)+')').after(cardDesignsHtml);
                    element.remove();
                }
                
            });

            if(designExists === false) {
                var cardDesignsHtml = that.getApp().compileTemplate('preview-design-item-template', designData);
                that.getModule().getAllDesignContainer().append(cardDesignsHtml);
            }
        };

        this.readAllRecentOrdersCompleteEventHandler = function(ev) {
            $.each(ev.responseData, function(index){
                that.addRecentOrderItem(this);
            });
        };
	},

	prototypeMethods: {

        addEvents: function() {

            $(document).on(this.getModule().getShippingAddressModel().events.CREATE_COMPLETE, this.shippingAddressCreateCompleteHandle);
            $(document).on(this.getModule().getShippingAddressModel().events.CREATE_ERROR, this.shippingAddressCreateErrorHandle);
            $(document).on(this.getModule().getShippingAddressModel().events.READ_ALL_COMPLETE, this.shippingAddressReadAllCompleteHandler);
            $(document).on(this.getModule().getShippingAddressModel().events.READ_ALL_ERROR, this.shippingAddressReadAllErrorHandler);
            $(document).on(this.getModule().getShippingAddressModel().events.UPDATE_COMPLETE, this.shippingAddressUpdateCompleteHandle);
            $(document).on(this.getModule().getShippingAddressModel().events.UPDATE_ERROR, this.shippingAddressUpdateErrorHandle);
            $(document).on(this.getModule().getShippingAddressModel().events.DELETE_COMPLETE, this.shippingAddressDeleteCompleteHandler);
            $(document).on(this.getModule().getShippingAddressModel().events.DELETE_ERROR, this.shippingAddressDeleteErrorHandler);

            $(document).on(this.getModule().getShippingAddressModel().events.VALIDATION_ERROR, this.shippingAddressValidationErrorHandle);

            $(document).on(this.getModule().getCountryModel().events.READ_FOR_SELECT_COMPLETE, this.readCountriesForSelectCompleteHandler);
            $(document).on(this.getModule().getCountryModel().events.READ_FOR_SELECT_ERROR, this.readCountriesForSelectErroreHandler);

            $(document).on(this.getModule().getCardDesignModel().events.READ_ALL_COMPLETE, this.readAllDesignsCompleteEventHandler);
            $(document).on(this.getModule().getCardDesignModel().events.SAVE_CARD_DESIGN_COMPLETE, this.saveCardDesignCompleteHandler);

            $(document).on(this.getModule().getCardDesignOrderModel().events.READ_ALL_COMPLETE, this.readAllRecentOrdersCompleteEventHandler);

            $(document).on(this.getModule().FORM_SUBMITTED_EVENT, this.shippingAddressFormSubmittedEventHandler);
            $(document).on(this.getModule().FORM_CANCELED_EVENT, this.cancelEditShippingAddressClickHandler);

            this.getModule().getExistingShippingAddressesList().on('click', '.edit-shipping-address-button', this.editShippingAddresClickHandler);
            this.getModule().getExistingShippingAddressesList().on('click', '.delete-shipping-address-button', this.deleteShippingAddressClickHandler);
            this.getModule().getAllDesignContainer().on('click', '.edit-card-design-btn', this.editCardDesignButtonClickedHandler);
            this.getModule().getAllDesignContainer().on('click', '.delete-card-design-btn', this.deleteCardDesignButtonClickHandler);
        },

        removeEvents: function() {

            $(document).off(this.getModule().getShippingAddressModel().events.CREATE_COMPLETE, this.shippingAddressCreateCompleteHandle);
            $(document).off(this.getModule().getShippingAddressModel().events.CREATE_ERROR, this.shippingAddressCreateErrorHandle);
            $(document).off(this.getModule().getShippingAddressModel().events.READ_ALL_COMPLETE, this.shippingAddressReadAllCompleteHandler);
            $(document).off(this.getModule().getShippingAddressModel().events.READ_ALL_ERROR, this.shippingAddressReadAllErrorHandler);
            $(document).off(this.getModule().getShippingAddressModel().events.UPDATE_COMPLETE, this.shippingAddressUpdateCompleteHandle);
            $(document).off(this.getModule().getShippingAddressModel().events.UPDATE_ERROR, this.shippingAddressUpdateErrorHandle);
            $(document).off(this.getModule().getShippingAddressModel().events.DELETE_COMPLETE, this.shippingAddressDeleteCompleteHandler);
            $(document).off(this.getModule().getShippingAddressModel().events.DELETE_ERROR, this.shippingAddressDeleteErrorHandler);

            $(document).off(this.getModule().getShippingAddressModel().events.VALIDATION_ERROR, this.shippingAddressValidationErrorHandle);

            $(document).off(this.getModule().getCountryModel().events.READ_FOR_SELECT_COMPLETE, this.readCountriesForSelectCompleteHandler);
            $(document).off(this.getModule().getCountryModel().events.READ_FOR_SELECT_ERROR, this.readCountriesForSelectErroreHandler);

            $(document).off(this.getModule().getCardDesignModel().events.READ_ALL_COMPLETE, this.readAllDesignsCompleteEventHandler);
            $(document).off(this.getModule().getCardDesignModel().events.SAVE_CARD_DESIGN_COMPLETE, this.saveCardDesignCompleteHandler);

            $(document).off(this.getModule().getCardDesignOrderModel().events.READ_ALL_COMPLETE, this.readAllRecentOrdersCompleteEventHandler);

            $(document).off(this.getModule().FORM_SUBMITTED_EVENT, this.shippingAddressFormSubmittedEventHandler);
            $(document).off(this.getModule().FORM_CANCELED_EVENT, this.cancelEditShippingAddressClickHandler);

            this.getModule().getExistingShippingAddressesList().off('click', '.edit-shipping-address-button', this.editShippingAddresClickHandler);
            this.getModule().getExistingShippingAddressesList().off('click', '.delete-shipping-address-button', this.deleteShippingAddressClickHandler);
            this.getModule().getAllDesignContainer().off('click', '.edit-card-design-btn', this.editCardDesignButtonClickedHandler);
            this.getModule().getAllDesignContainer().off('click', '.delete-card-design-btn', this.deleteCardDesignButtonClickHandler);
        },

        startController: function() {
            this.getModule().getCardDesignModel().readAll({scope:this});
        },

        addShippingAddressItem: function(item) {
            var shippingAddressHtml = this.getApp().compileTemplate('shipping-address-item-template', item);
            this.getModule().getExistingShippingAddressesList().append(shippingAddressHtml);
        },

        addRecentOrderItem: function(item) {
            var recentOrdersHtml = this.getApp().compileTemplate('recent-orders-item-template', item);
            this.getModule().getRecentOrdersList().append(recentOrdersHtml);
        }
	},

	extendPrototypeFrom: 'Base.Controller'
});