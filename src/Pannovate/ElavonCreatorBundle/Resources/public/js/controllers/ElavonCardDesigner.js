CLASS.define({

    name: 'Controllers.ElavonCardDesigner',

    definition: function(app, module) {

        this.extendFromClass('Controllers.CardDesigner', [app, module]);
        this.extendFromClass('Controllers.EventHandlers.ElavonCardDesigner');

        var addToCartAfterSaving = false;

        this.setAddToCartAfterSaving = function(addToCart)  {
            addToCartAfterSaving = addToCart;
        };

        this.getAddToCartAfterSaving = function() {
            return addToCartAfterSaving;
        };
    },

    prototypeMethods: {

        init: function() {
            this.callSuper('Controllers.CardDesigner', 'init');
            this.getModule().getModuleElement().find('#background-color-pallete').show();
        },

        addEvents: function() {

            var moduleElement = this.getModule().getModuleElement();

            this.callSuper('Controllers.CardDesigner', 'addEvents');
            moduleElement.find('#delete-background-button').on('click', this.deleteBackgroundButtonClickedEventHandler);
            moduleElement.find('.input-position-box').on('change', this.elementInputFieldPositionChangeEventHandler);
            moduleElement.find('.canvas-container').on('dblclick', this.canvasContainerDoubleClickEventHandler);
            moduleElement.find('.number-of-cards').on('click', 'span', this.numberOfCardsControlClickedHandler);
            moduleElement.find('.number-of-cards').on('keydown', 'input', this.onlyNumbersKeyDownHandler);
            moduleElement.find('.number-of-cards').on('keyup', 'input', this.numberOfCardsControlKeyupHandler);

            this.getModule().getLogoImageContainer().on('click', this.cardDesignChooseLogoEventHandler);
            this.getModule().getDesignElementsCloseButtonsContainer().on('click', '.design-element-close-button', this.designElementCloseButtonClickHandler);
            this.getModule().getBackgroundImageSlider().on('change', this.backgroundImageSliderChangeHandler);
            this.getModule().getRecentCardDesignsList().on('click', '.card-design-menu-item', this.loadRecentCardDesignItemClickHandler);
            this.getModule().getCreatorGalleryButton().on('click', this.creatorGalleryButtonChickHandler);
            this.getModule().getAddToShoppingCartButton().on('click', this.addToCartButtonClickEventHandler);

            this.getApp().getSelectCurrencyList().on('click', this.changeCurrencyClickedHandler);

            $(document).on(this.getModule().getCardDesignModel().getFrontCardSideModel().events.BACKGROUND_IMAGE_FILTER_SELECTED, this.backgroundImageEffectFilterSelected);
            $(document).on(this.getModule().getCardDesignModel().events.READ_RECENT_DESIGNS_COMPLETE, this.recentDesignsLoadedCompleteHandler);
            $(document).on(this.getModule().getCardDesignModel().events.DELETE_COMPLETE, this.deleteCardDesignCompleteHandler);

            $(document).on(this.getModule().getImageGalleryItemModel().events.PHOTO_SELECTED, this.socialMediaPhotoSelectedEventHandler);

            var constants = this.getNamespaceValue('Static.Constants');
            $(document).on(constants.MODULE_SHOWN_EVENT, this.moduleShownEventHandler);
        },

        removeEvents: function() {

            var moduleElement = this.getModule().getModuleElement();

            this.callSuper('Controllers.CardDesigner', 'removeEvents');
            moduleElement.find('#delete-background-button').off('click', this.deleteBackgroundButtonClickedEventHandler);
            moduleElement.find('.input-position-box').off('change', this.elementInputFieldPositionChangeEventHandler);
            moduleElement.find('.canvas-container').off('dblclick', this.canvasContainerDoubleClickEventHandler);
            moduleElement.find('.number-of-cards').off('click', 'span', this.numberOfCardsControlClickedHandler);
            moduleElement.find('.number-of-cards').off('keydown', 'input', this.onlyNumbersKeyDownHandler);
            moduleElement.find('.number-of-cards').off('keyup', 'input', this.numberOfCardsControlKeyupHandler);

            this.getModule().getLogoImageContainer().off('click', this.cardDesignChooseLogoEventHandler);
            this.getModule().getDesignElementsCloseButtonsContainer().off('click', '.design-element-close-button', this.designElementCloseButtonClickHandler);
            this.getModule().getBackgroundImageSlider().off('change', this.backgroundImageSliderChangeHandler);
            this.getModule().getRecentCardDesignsList().off('click', '.card-design-menu-item', this.loadRecentCardDesignItemClickHandler);
            this.getModule().getCreatorGalleryButton().off('click', this.creatorGalleryButtonChickHandler);
            this.getModule().getAddToShoppingCartButton().off('click', this.addToCartButtonClickEventHandler);

            this.getApp().getSelectCurrencyList().off('click', this.changeCurrencyClickedHandler);

            $(document).off(this.getModule().getCardDesignModel().getFrontCardSideModel().events.BACKGROUND_IMAGE_FILTER_SELECTED, this.backgroundImageEffectFilterSelected);
            $(document).off(this.getModule().getCardDesignModel().events.READ_RECENT_DESIGNS_COMPLETE, this.recentDesignsLoadedCompleteHandler);
            $(document).off(this.getModule().getCardDesignModel().events.DELETE_COMPLETE, this.deleteCardDesignCompleteHandler);

            $(document).off(this.getModule().getImageGalleryItemModel().events.PHOTO_SELECTED, this.socialMediaPhotoSelectedEventHandler);

            var constants = this.getNamespaceValue('Static.Constants');
            $(document).off(constants.MODULE_SHOWN_EVENT, this.moduleShownEventHandler);
        },

        startController: function(ev) {

            this.callSuper('Controllers.CardDesigner', 'startController');

            this.getModule().getCardDesignModel().readRecentDesigns({scope:this, showLoader: false});
            
            var moduleData = this.getModule().getModuleData();

            this.getModule().getCardDesignLoader().removeClass('display-none');

            if(this.isString(moduleData)) {
                this.getModule().getCardDesignModel().readLastModified({scope:this, showLoader: false});
            }
            else if(this.isObject(moduleData)) {
                this.getModule().startCreator(moduleData);
            }
            else {
                this.getModule().getCardDesignModel().readLastModified({scope:this, showLoader: false});
            }

            var loadedShoppingCartItems = this.getApp().getShoppingCartItemModel().getLoadedData();

            if(loadedShoppingCartItems === null) {
                this.getApp().getShoppingCartItemModel().readAllForCurrentUser({scope:this, showLoader:false});
            }
        },


        showToolControls: function(toolName) {

            this.getModule().getModuleElement().find('.tools-pannel').removeClass('active-tools-pannel');

            switch(toolName) {
                case 'logo-image':
                    this.getModule().getModuleElement().find('.image-tools-pannel').addClass('active-tools-pannel');
                break;

                case 'text-field':
                    this.getModule().getModuleElement().find('.text-tools-pannel').addClass('active-tools-pannel');
                break;

                case 'logo-field':
                    this.getModule().getLogoImageContainer().addClass('active-tools-pannel');
                    this.loadFrontSideLogoImages();
                break;

                case 'background-image':
                    this.getModule().getModuleElement().find('.background-tools-pannel').addClass('active-tools-pannel');
                break;

                case 'disclaimer-text':
                    this.getModule().getModuleElement().find('.disclaimer-text-tool-pannel').addClass('active-tools-pannel');
                    this.getModule().getModuleElement().find('.disclaimer-text-tool-pannel').children('ul.color-pallete').show();
                break;

                case 'background-image-tools':

                    this.getModule().getModuleElement().find('.background-tools-pannel').addClass('active-tools-pannel');

                    this.getModule().getModuleElement().find('.background-color-tools').hide();
                    this.getModule().getModuleElement().find('.background-image-tools').show();

                    this.getModule().getBackgroundImageToolButton().children().first().addClass('checked-state');
                    this.getModule().getBackgroundColorToolButton().children().first().removeClass('checked-state');

                break;

                case 'background-color-tools':

                    this.getModule().getModuleElement().find('.background-tools-pannel').addClass('active-tools-pannel');

                    this.getModule().getModuleElement().find('.background-image-tools').hide();
                    this.getModule().getModuleElement().find('.background-color-tools').show();

                    this.getModule().getBackgroundImageToolButton().children().first().removeClass('checked-state');
                    this.getModule().getBackgroundColorToolButton().children().first().addClass('checked-state');

                break;

                case 'fixed-item':
                    this.getModule().getModuleElement().find('.background-tools-pannel').addClass('active-tools-pannel');
                break;
            }
        },

        changeCardSide: function(side) {

            this.callSuper('Controllers.CardDesigner', 'changeCardSide', [side]);

            var logoImageButton = this.getModule().getModuleElement().find('.tool-button[data-tool-type="logo-image"]');
            var logoFieldButton = this.getModule().getModuleElement().find('.tool-button[data-tool-type="logo-field"]');
            var disclaimerTextButton = this.getModule().getModuleElement().find('.tool-button[data-tool-type="disclaimer-text"]');

            if(side === 'front') {
                logoImageButton.show();
                logoFieldButton.show();
                disclaimerTextButton.hide();
            }
            else {
                logoImageButton.hide();
                logoFieldButton.hide();
                disclaimerTextButton.show();
            }

            if(this.getActiveElement()) {
                this.showToolControls(this.getActiveElement().elementType);
            }
            else {
                this.showToolControls('background-image');
            }

            if(this.getCardSideModelFromSide(side).getBackgroundImage()) {
                this.showToolControls('background-image-tools');
                this.updateBackgroundScaleSlider(this.getCardSideModelFromSide(side).getBackgroundImage().getScaleX());
            }
            else {
                this.showToolControls('background-color-tools');
            }
        },

        designEditorLoadFromJsonCallback: function(designEditor, cardSide) {

            designEditor.calcOffset();

            var objects = designEditor.getObjects();
            var element;

            for(var i = 0, len = objects.length; i < len; i++) {

                element = objects[i];

                this.initFabricElement(element);
                this.getCurrentCardDesignSideModel().addCardDesignElement(element, element.id);

                if(element.filters && element.filters.length > 0) {
                    element.applyFilters(designEditor.renderAll.bind(designEditor));
                }

                if(element.elementType === 'background-image') {

                    this.getCardSideModelFromSide(cardSide).setBackgroundImage(element);
                    element.hasControls = false;
                    element.backgroundActive = true;
                }
                else if(element.elementType === 'logo-image') {

                    this.getCardSideModelFromSide(cardSide).setLogoImage(element);
                    this.getModule().createDesignElementCloseButton(element, cardSide);
                    this.positionDesignElementCloseButton(element);
                }
                else if(element.elementType === 'fixed-item') {
                    this.getCardSideModelFromSide(cardSide).addFixedElement(element);
                }
                else if(element.elementType === 'text-field') {

                    this.getModule().createDesignElementCloseButton(element, cardSide);
                    this.positionDesignElementCloseButton(element);
                }
            }

            this.getModule().getDesignElementsCloseButtonsContainer().children().hide();

            if(cardSide === 'front') {

                this.loadFrontSideNonPrintingElements();
                this.getModule().getCardDesignModel().getFrontCardSideModel().setBackgroundColor(this.getModule().getCardFrontSideDesignEditor().backgroundColor);

                this.changeCardSide('front');
            }
            else {
                this.loadBackSideNonPrintingElements();
                this.getModule().getCardDesignModel().getBackCardSideModel().setBackgroundColor(this.getModule().getCardBackSideDesignEditor().backgroundColor);
            }

            if(cardSide === this.getCurrentCardDesignSideModel().getCardSide()) {

                var backgroundImage = this.getCurrentCardDesignSideModel().getBackgroundImage();

                if(backgroundImage) {
                    if(backgroundImage.backgroundActive) {
                        this.showToolControls('background-image-tools');
                    }
                    else {
                        this.showToolControls('color-image-tools');
                    }
                }
            }

            designEditor.deactivateAll().renderAll();

            var frontObjectsCount = this.getModule().getCardFrontSideDesignEditor().getObjects().length;
            var backObjectsCount = this.getModule().getCardBackSideDesignEditor().getObjects().length;

            if(frontObjectsCount > 0 && backObjectsCount > 0) {
                this.getModule().getCardDesignLoader().addClass('display-none');
            }
        },

        saveCardDesign: function(saveAsNew, addToCart) {

            var that = this;

            this.setAddToCartAfterSaving(addToCart);
            var nonPrintingElementsGroup = this.getModule().getCardDesignModel().getBackCardSideModel().getNonPrintingElementsGroup();

            var callback = function() {
                that.getModule().getCardBackSideDesignEditor().remove(nonPrintingElementsGroup);
                that.callSuper('Controllers.CardDesigner', 'saveCardDesign', [saveAsNew]);
            };

            this.hideNonPrintingElements(callback);
        },

        updateBackgroundScaleSlider: function(value) {

            var slider = this.getModule().getBackgroundImageSlider().data("ionRangeSlider");

            slider.update({
                from: value
            });
        },

        autoFitBackSideText: function(textField) {

            var textLength = textField.getText().length;

            if(textLength > 200) {
                this.getApp().showNotification('information', 'Maximum number of characters is 200');
                textField.setText(textField.getText().substring(0, 200));
            }

            if(textField.getHeight() > 125) {
                textField.setFontSize(16);
            }

            if(textField.getHeight() > 125) {
                textField.setFontSize(9);
            }

            if(textField.getHeight() > 125) {
                this.getApp().showNotification('information', 'Text out of limits. Please remove some text.');
            }
        },

        loadFrontSideLogoImages: function() {
            
            var logoDarkUrl, logoLightUrl;
            var logoName = this.getModule().getCardDesignModel().getCurrentCardDesign().type.toLowerCase();

            switch(logoName) {
                case 'loyalty':
                    logoDarkUrl = this.getApp().getConfig().getParameter('frontLoyaltyLogoDark');
                    logoLightUrl = this.getApp().getConfig().getParameter('frontLoyaltyLogoLight');
                break;

                case 'Promo':
                    logoDarkUrl = this.getApp().getConfig().getParameter('frontPromoLogoDark');
                    logoLightUrl = this.getApp().getConfig().getParameter('frontPromoLogoLight');
                break;

                case 'gift':
                    logoDarkUrl = this.getApp().getConfig().getParameter('frontGiftLogoDark');
                    logoLightUrl = this.getApp().getConfig().getParameter('frontGiftLogoLight');
                break;
            }

            html = this.getApp().compileTemplate(
                "front-side-logo-template",
                {
                    name:logoName,
                    logoLight: logoLightUrl,
                    logoDark: logoDarkUrl
                }
            );
            var item = $(html);
            this.getModule().getLogoImageContainer().empty();
            this.getModule().getLogoImageContainer().append(item);
        },

        getShopingCartItemDataForCreate: function() {

            var data = {};

            data.cardDesign = this.getModule().getCardDesignModel().getCurrentCardDesign().id;
            data.quantity = parseInt(this.getModule().getNumberOfCardsInput().val());
            data.pricePerCard = 1;

            return data;
        }
    },

    extendPrototypeFrom: ['Controllers.CardDesigner', CLASS.getNamespaceValue('Controllers.Fabric.ElavonCardDesigner')]
});
