CLASS.define({

    name: 'Controllers.EventHandlers.ElavonCardDesigner',

    definition: function() {

        var that = this;
        var cardDesignToBeLoaded;
        var createNewDesign;

        this.elementInputFieldsValuesEventHandler = function(element) {

            if(element.elementType === 'fixed-item') {
                return false;
            }

            var x = Math.round(element.get('left'));
            var y = Math.round(element.get('top'));
            var width = Math.round(element.getWidth());
            var height = Math.round(element.getHeight());

            that.getModule().getModuleElement().find('.input-position-box').children('input').each(function(index){
                var elem = $(this);

                if(elem.data('position')) {
                    if(elem.data('position') === 'top') {
                        elem.val(y);
                    }
                    else {
                        elem.val(x);
                    }
                }
                else if(elem.data('dimension') === 'width') {
                        elem.val(width);
                    }
                    else {
                        elem.val(height);
                    }
            });
        };

        this.elementInputFieldPositionChangeEventHandler = function(ev) {

            var value = parseInt($(ev.target).val());
            var element = that.getCurrentCardDesignEditor().getActiveObject();

            if(element === null) {
                return false;
            }

            if($(ev.target).data('position') === 'left') {
                element.setLeft(value);
            }
            if($(ev.target).data('position') === 'top') {
                element.setTop(value);
            }
            if($(ev.target).data('dimension') === 'width') {
                element.setScaleX(1);
                element.setWidth(value);
            }
            if($(ev.target).data('dimension') === 'height') {
                element.setScaleY(1);
                element.setHeight(value);
            }

            that.getCurrentCardDesignEditor().renderAll();
        };

        this.currentElementFontSizeClickHandler = function(ev) {

            if(ev.target.elementType === 'text-field') {

                var selectField = that.getModule().getModuleElement().find('select[data-operation-type="select-font-size"]');

                selectField.val(ev.target.fontSize);
                selectField.selectBoxIt('refresh');
            }
        };

        this.toolButtonClickHandler = function(ev) {
            var designEditor = that.getCurrentCardDesignEditor();
            var toolButton = $(ev.currentTarget);
            var toolType = toolButton.data('tool-type');
            var isChecked = $(ev.currentTarget).children('span').hasClass('checked-state');

            var currentBackgroundImage = that.getCurrentCardDesignSideModel().getBackgroundImage();
            var currentBackgroundColor = that.getCurrentCardDesignSideModel().backgroundColor;
            var currentDesign = that.getModule().getCardDesignModel().getCurrentCardDesign();

            var cardBackgroundImageUrl = that.getModule().getCurrentCardSide() === 'front' ? currentDesign.frontBackgroundImageUrl : currentDesign.backBackgroundImageUrl;

            that.showToolControls(toolType);

            if(toolType === 'text-field') {
                designEditor.calcOffset();
                that.addTextField('Double click to edit', 'text-field');
            }
            else if(toolType === 'logo-image') {

                if(that.getCurrentCardDesignSideModel().getLogoImage()) {
                    designEditor.setActiveObject(that.getCurrentCardDesignSideModel().getLogoImage());
                    that.getApp().showNotification('information', 'You can only have one image field.');
                }
            }
            else if(toolType === 'background-image-tools' && !isChecked) {

                designEditor.setBackgroundColor('');

                if(currentBackgroundImage) {
                    designEditor.add(currentBackgroundImage);
                    currentBackgroundImage.moveTo(0);
                }
                else if(cardBackgroundImageUrl) {
                    that.addImageObject(cardBackgroundImageUrl, 'background-image');
                }
            }
            else if(toolType === 'background-color-tools' && isChecked !== 'checked-state') {

                if(currentBackgroundImage) {

                    designEditor.remove(currentBackgroundImage);
                }

                if(that.getCurrentCardDesignSideModel().getBackgroundColor()) {
                    designEditor.setBackgroundColor(that.getCurrentCardDesignSideModel().getBackgroundColor());
                }
                else {
                    designEditor.setBackgroundColor('rgb(255,255,255)');
                }
            }

            designEditor.renderAll();
        };

        var superCardFrontSideElementSelectedEvent = this.cardFrontSideElementSelectedEvent;
        this.cardFrontSideElementSelectedEvent = function(ev) {

            superCardFrontSideElementSelectedEvent(ev);
            that.showToolControls(ev.target.elementType);
            that.elementInputFieldsValuesEventHandler(ev.target);

            that.positionDesignElementCloseButton(ev.target);

            that.currentElementFontSizeClickHandler(ev);
        };

        var superCardBackSideElementSelectedEvent = this.cardBackSideElementSelectedEvent;
        this.cardBackSideElementSelectedEvent = function(ev) {
            superCardBackSideElementSelectedEvent(ev);
            that.showToolControls(ev.target.elementType);
            that.elementInputFieldsValuesEventHandler(ev.target);

            that.currentElementFontSizeClickHandler(ev);
        };

        var superCardFrontSideElementRemovedEvent = this.cardFrontSideElementRemovedEvent;
        this.cardFrontSideElementRemovedEvent = function(ev) {
            superCardFrontSideElementRemovedEvent(ev);
            if(ev.target.elementType === 'logo-image') {
                that.getCurrentCardDesignSideModel().setLogoImage(null);
            }
        };

        var superCardBackSideElementRemovedEvent = this.cardBackSideElementRemovedEvent;
        this.cardBackSideElementRemovedEvent = function(ev) {
            superCardBackSideElementRemovedEvent(ev);
            if(ev.target.elementType === 'text-field') {
                that.getCurrentCardDesignSideModel().setTextField(null);
            }
        };

        var superColorSelectedEventHandler = this.colorSelectedEventHandler;
        this.colorSelectedEventHandler = function(ev) {

            superColorSelectedEventHandler(ev);

            if(that.getCurrentCardDesignSideModel().getBackgroundImage()) {
                that.getCurrentCardDesignSideModel().getBackgroundImage().backgroundActive = false;
            }
        };

        this.browseFileInputChangeEventHandler = function(ev) {

            var fileInput = $(ev.target);
            var file = ev.target.files[0];
            var fileUrl = URL.createObjectURL(file);

            that.getCurrentCardDesignSideModel().addCardDesignImageElementFile(fileUrl, file);

            if(fileInput.data('purpose') === 'background-image') {

                that.showToolControls('background-image-tools');
                that.setBackgroundImage(fileUrl);
                that.getModule().getBackgroundImageSlider().data("ionRangeSlider").update({
                    from: 0.1
                });

                that.setIsCardDesignChanged(true);
            }
            else if (fileInput.data('purpose') === 'logo-image') {
                that.setLogoImage(fileUrl);
                that.setIsCardDesignChanged(true);
            }
            else {
                that.throwException('File input purpose unknown.', 1500, 'controller');
            }
        };

        this.deleteBackgroundButtonClickedEventHandler = function(ev) {

            var background = that.getCurrentCardDesignSideModel().getBackgroundImage();

            if(background) {

                that.getCurrentCardDesignEditor().remove(background);
                that.getCurrentCardDesignSideModel().setBackgroundImage(null);
                that.getModule().getBackgroundImageSlider().data("ionRangeSlider").update({
                    from: 0.1
                });

                that.setIsCardDesignChanged(true);
            }
        };

        this.fabricImageLoadedEventHandler = function(ev) {};

        this.superControlButtonClickHandler = this.controlButtonClickHandler;

        this.controlButtonClickHandler = function(ev) {

            var operationType = $(ev.currentTarget).data('operation-type');
            var selectedItem = that.getActiveElement();

            switch(operationType) {

                case 'flip-horizontally':
                case 'flip-vertically':
                case 'rotate-left':
                case 'rotate-right':
                    that.getCurrentCardDesignEditor().forEachObject(function(obj){
                        if(obj.elementType === 'background-image') {
                            that.getCurrentCardDesignEditor().setActiveObject(obj);
                            that.superControlButtonClickHandler(ev);
                        }
                    });
                break;

                case 'import-facebook':
                case 'import-flickr':
                    that.showToolControls('background-image-tools');
                break;

                default:
                    that.superControlButtonClickHandler(ev);
                break;
            }
        };

        this.backgroundImageSliderChangeHandler = function(ev) {

            var scaleNumber = $(ev.target).val();
            var currentBackgroundImage = that.getCurrentCardDesignSideModel().getBackgroundImage();

            if(currentBackgroundImage) {

                currentBackgroundImage.userScaledBackground = true;
                currentBackgroundImage.setScaleX(scaleNumber);
                currentBackgroundImage.setScaleY(scaleNumber);

                if(currentBackgroundImage.userBackgroundPosition === false) {
                    that.centerBackgroundImage(currentBackgroundImage);
                }

                that.elementInputFieldsValuesEventHandler(currentBackgroundImage);

                that.getCurrentCardDesignEditor().renderAll();
            }
        };

        this.canvasContainerDoubleClickEventHandler = function(ev) {

            var backgroundImage = that.getCurrentCardDesignSideModel().getBackgroundImage();

            if(backgroundImage) {
                that.autoFitBackgroundImage(backgroundImage);
            }

            that.getCurrentCardDesignEditor().renderAll();
        };

        var superReadLastModifiedDesignCompleteHandler = this.readLastModifiedDesignCompleteHandler;
        this.readLastModifiedDesignCompleteHandler = function(ev) {
            superReadLastModifiedDesignCompleteHandler(ev);
        };

        var superCardFrontSideSelectionClearedEventHandler = this.cardFrontSideSelectionClearedEventHandler;
        this.cardFrontSideSelectionClearedEventHandler = function(ev) {
            superCardFrontSideSelectionClearedEventHandler(ev);
            that.showToolControls('background-image');
        };

        var superCardBackSideSelectionClearedEventHandler = this.cardBackSideSelectionClearedEventHandler;
        this.cardBackSideSelectionClearedEventHandler = function(ev) {
            superCardBackSideSelectionClearedEventHandler(ev);
            that.showToolControls('background-image');
        };

        var superDesignElementMovingEventHandler = this.designElementMovingEventHandler;
        this.designElementMovingEventHandler = function(ev) {

            var selectedElement = that.getCurrentCardDesignEditor().getActiveObject();
            var elementText;
            var boundingBox = that.getModule().getCardDesignModel().getBackCardSideModel().getElementsBoundingBox();

            if(selectedElement.elementType === 'text-field' && that.getCurrentCardDesignSideModel().getCardSide() === 'back') {

                elementText = selectedElement;

                var width =  elementText.getBoundingRectWidth();
                var height =  elementText.getBoundingRectHeight();
                var top = elementText.top;
                var bottom = top + height;
                var left = elementText.left;
                var right = left + width;

                var topBound = boundingBox.top;
                var bottomBound = topBound + boundingBox.height;
                var leftBound = boundingBox.left;
                var rightBound = leftBound + boundingBox.width;

                elementText.setLeft(Math.min(Math.max(left, leftBound), rightBound - elementText.getBoundingRectWidth()));
                elementText.setTop(Math.min(Math.max(top, topBound), bottomBound - elementText.getBoundingRectHeight()));
            }
            else if(selectedElement.elementType === 'background-image') {
                selectedElement.userBackgroundPosition = true;
            }

            superDesignElementMovingEventHandler(ev);

            that.elementInputFieldsValuesEventHandler(selectedElement);
        };

        var superCardDesignEditorObjectScalingEventHandler = this.cardDesignEditorObjectScalingEventHandler;
        this.cardDesignEditorObjectScalingEventHandler = function(ev) {
            superCardDesignEditorObjectScalingEventHandler(ev);

            var element = that.getCurrentCardDesignEditor().getActiveObject();
            that.elementInputFieldsValuesEventHandler(element);
        };

        this.socialMediaPhotoSelectedEventHandler = function(ev) {
            that.setBackgroundImage(ev.photo);
        };

        var superCardDesignCreateCompleteEventHandler = this.cardDesignCreateCompleteEventHandler;
        this.cardDesignCreateCompleteEventHandler = function(ev) {

            if(ev.scope === 'save-as') {

                var newDesignData = ev.responseData;
                var currentDesignData = that.getModule().getCardDesignModel().setCurrentCardDesign(ev.responseData);

                that.getModule().getCardDesignModel().getFrontCardSideModel().setCurrentCardDesign(ev.responseData);
                that.getModule().getCardDesignModel().getBackCardSideModel().setCurrentCardDesign(ev.responseData);

                that.saveCardDesign(true);
            }
            else {
                
                superCardDesignCreateCompleteEventHandler(ev);

                that.loadFrontSideFixedElements();
                that.loadBackSideFixedElements();

                that.loadFrontSideNonPrintingElements();
                that.loadBackSideNonPrintingElements();

                that.changeCardSide('front');

                that.getModule().getNewDesignPopup().panpopup('close');

                that.getModule().getCardDesignLoader().addClass('display-none');
            }
        };

        var superSaveCardSideDesignCompleteEventHandler = this.saveCardSideDesignCompleteEventHandler;
        this.saveCardSideDesignCompleteEventHandler = function(ev) {

            superSaveCardSideDesignCompleteEventHandler(ev);

            if(ev.scope.sideModel === that.getModule().getCardDesignModel().getBackCardSideModel()) {
                that.showNonPrintingElements();
            }
        };


        var superSaveCardDesignCompleteEventHandler = this.saveCardDesignCompleteEventHandler;
        this.saveCardDesignCompleteEventHandler = function(ev) {

            var recentDesigns = that.getModule().getCardDesignModel().getRecentDesigns();
            var recentDesignsList = that.getModule().getRecentCardDesignsList();
            var moreButtonExist = false;

            if(ev.scope.saveAsNew) {
                that.getModule().getSaveAsDesignPopup().panpopup('close');
                that.getApp().showNotification('success', 'You have successfully created new design');
            }
            else {
                that.getApp().showNotification('success', 'You have successfully saved your design');
            }

            recentDesignsList.children().each(function(){
                var element = $(this);

                if(element.data('id').toString() === ev.cardDesign.id.toString()) {
                    element.remove();
                }

                if(element.data('recent-index') === "") {
                    moreButtonExist = true;
                }
            });

            var liItem = $('<li/>', {
                'data-id': ev.cardDesign.id,
                'data-recent-index': recentDesigns.length,
                'class': 'card-design-menu-item'
            });
            var spanLabel = $('<span/>', {
                text: ev.cardDesign.name
            });

            liItem.append(spanLabel);
            recentDesignsList.prepend(liItem);

            if(recentDesignsList.children().length > 4){
                if(!moreButtonExist) {
                    moreButtonExist = true;
                    recentDesignsList.append('<li data-id="" data-recent-index=""><span data-module="MyDesignStudio">More..</span></li>');
                }
                recentDesignsList.children().last().prev().remove();
            }

            recentDesigns.push(ev.cardDesign);

            if(that.getAddToCartAfterSaving()) {

                var currentCardDesignId = that.getModule().getCardDesignModel().getCurrentCardDesign().id;
                var shoppingCartItem = that.getApp().getShoppingCartItemModel().findByDesignId(currentCardDesignId);

                if(shoppingCartItem) {
                    that.getApp().getModuleController().loadModule('Modules.ShoppingCart', shoppingCartItem, true);
                }
                else {
                    that.getApp().getShoppingCartItemModel().create(that.getShopingCartItemDataForCreate(), {scope:that});   
                }
            }

            superSaveCardDesignCompleteEventHandler(ev);
        };

        this.cardDesignChooseLogoEventHandler = function(ev) {

            var currentElement = $(ev.target).parent().data('choose-logo');
            var deleteButton = $(ev.target).find('span[data-operation-type="remove-logo"]');
            var logo;

            that.getCurrentCardDesignEditor().forEachObject(function(obj) {
                if(obj.id.indexOf('fixed-logo-front') !== -1) {
                    logo = obj;
                }
            });

            if(currentElement || deleteButton) {
                that.getCurrentCardDesignEditor().remove(logo);
            }

            switch(currentElement) {
                case 'loyalty-light':
                    that.addFrontSideFixedLogoField(that.getApp().getConfig().getParameter('frontLoyaltyLogoLight'));
                break;

                case 'loyalty-dark':
                    that.addFrontSideFixedLogoField(that.getApp().getConfig().getParameter('frontLoyaltyLogoDark'));
                break;

                case 'promo-light':
                    that.addFrontSideFixedLogoField(that.getApp().getConfig().getParameter('frontPromoLogoLight'));
                break;

                case 'promo-dark':
                    that.addFrontSideFixedLogoField(that.getApp().getConfig().getParameter('frontPromoLogoDark'));
                break;

                case 'gift-light':
                    that.addFrontSideFixedLogoField(that.getApp().getConfig().getParameter('frontGiftLogoLight'));
                break;

                case 'gift-dark':
                    that.addFrontSideFixedLogoField(that.getApp().getConfig().getParameter('frontGiftLogoDark'));
                break;
            }

            that.getCurrentCardDesignEditor().renderAll();
        };

        this.designElementCloseButtonClickHandler = function(ev) {

            var button = $(ev.target);
            that.removeDesignElement(button.attr('id'));
        };


        this.backgroundImageEffectFilterSelected = function(ev) {

            var obj = ev.sideModel.getBackgroundImage();
                obj.filters[0] = ev.filter;

            var applyFilterCallback = that.getCurrentCardDesignEditor().renderAll.bind(that.getCurrentCardDesignEditor());
                obj.applyFilters(applyFilterCallback);
        };

        this.loadRecentCardDesignItemClickHandler = function(ev) {

            var itemEl = $(ev.currentTarget);
            var id = itemEl.data('id');
            var recentIndex = itemEl.data('recent-index');
            var recentDesigns = that.getModule().getCardDesignModel().getRecentDesigns();

            that.getModule().startCreator(recentDesigns[recentIndex]);
        };

        this.recentDesignsLoadedCompleteHandler = function(ev) {

            var list = that.getModule().getModuleElement().find("#recent-card-designs-list");
            var liItem;
            var spanLabel;

            list.children().each(function(){
                $(this).remove();
            });

            for(var i in ev.responseData){

                liItem = $('<li/>', {
                    'data-id': ev.responseData[i].id,
                    'data-recent-index': i,
                    'class': 'card-design-menu-item'
                });

                spanLabel = $('<span/>', {
                    text: ev.responseData[i].name
                });

                liItem.append(spanLabel);

                that.getModule().getRecentCardDesignsList().append(liItem);
            }

            if(liItem) {
                list.append('<li data-id="" data-recent-index=""><span data-module="MyDesignStudio">More..</span></li>');
            }
        };

        this.colorPickerButtonClickHandler = function (hsb, hex, rgb, el) {

            $(el).val('#' + hex);
            $(el).ColorPickerHide();

            var constants = that.getNamespaceValue('Static.Constants');
            var e = $.Event(constants.COLOR_PALLETE_COLOR_CHANGED_EVENT, {
                color:'rgb('+rgb.r+','+rgb.g+','+rgb.b+')',
                'color-for': 'background'
            });

            $(document).trigger(e);
        };

        this.deleteCardDesignCompleteHandler = function(ev) {

            var recentDesignsList = that.getModule().getRecentCardDesignsList();
            var currentDesign = that.getModule().getCardDesignModel().getCurrentCardDesign();
            var recentDesigns = that.getModule().getCardDesignModel().getRecentDesigns();
            var isCurrentDesignDeleted = ev.designId.toString() === currentDesign.id.toString();

            recentDesignsList.empty();
            that.getModule().getCardDesignModel().readRecentDesigns({scope:that, showLoader:false});

            if(isCurrentDesignDeleted) {

                if(that.isArray(recentDesigns) && recentDesigns.length > 0) {
                    cardDesignToBeLoaded = recentDesigns[0];
                }
                else {
                    createNewDesign = true;
                }
            }
        };

        this.moduleShownEventHandler = function(ev) {

            if(ev.module.getModuleName() !== that.getModule().getModuleName()) {
                return;
            }

            if(cardDesignToBeLoaded) {
                that.getModule().startCreator(cardDesignToBeLoaded);
                cardDesignToBeLoaded = null;
            }
            else if(createNewDesign) {

                that.getModule().getCardFrontSideDesignEditor().clear();
                that.getModule().getCardBackSideDesignEditor().clear();

                that.getModule().getCardFrontSideDesignEditor().setBackgroundColor('rgb(255,255,255)');
                that.getModule().getCardBackSideDesignEditor().setBackgroundColor('rgb(255,255,255)');

                that.getModule().getCardFrontSideDesignEditor().renderAll();
                that.getModule().getCardBackSideDesignEditor().renderAll();


                that.getModule().getNewDesignPopup().panpopup('open');
            }
        };

        this.readLastModifiedDesignErrorHandler = function(ev) {
            that.getModule().getCardDesignLoader().addClass('display-none');
            that.getModule().getNewDesignPopup().panpopup('open');
        };

        this.addToCartButtonClickEventHandler = function(ev) {

            if(that.getModule().getImageRulesCheckbox().is(':checked') === false) {
                that.getApp().showNotification('warning', "You have to comply with our image rules before proceeding.");
                return;
            }

            if(that.getIsCardDesignChanged()) {
                that.saveCardDesign(false, true);
            }
            else {

                var currentCardDesignId = that.getModule().getCardDesignModel().getCurrentCardDesign().id;
                var shoppingCartItem = that.getApp().getShoppingCartItemModel().findByDesignId(currentCardDesignId);

                if(shoppingCartItem) {
                    that.getApp().getModuleController().loadModule('Modules.ShoppingCart', shoppingCartItem, true);
                }
                else {
                    that.getApp().getShoppingCartItemModel().create(that.getShopingCartItemDataForCreate(), {scope:that});   
                }
            }
        };

        this.numberOfCardsControlClickedHandler = function(ev) {

            var button = $(ev.target).text();
            var inputField = $(ev.currentTarget).siblings('input');
            var inputFieldValue = $(ev.currentTarget).siblings('input').val();

            if(button === '+'){
                inputFieldValue++;
            }
            else if(inputFieldValue > 0) {

                inputFieldValue--;
            }

            inputField.val(inputFieldValue);

            that.getModule().updateCardPriceInfo(inputFieldValue);
        };

        this.numberOfCardsControlKeyupHandler = function(ev) {

            var inputField = $(ev.currentTarget);
            var isNumber = !isNaN(parseInt(ev.key));
            var isBackSpace = ev.keyCode === 8;

            if(isNumber || isBackSpace) {
                that.getModule().updateCardPriceInfo(inputField.val());
            }
        };

        this.changeCurrencyClickedHandler = function(ev) {
            var clickedCurrency = $(ev.target).parent();
            var currencySymbol = $(ev.target).parent().data('symbol');

            var inputFieldValue = that.getModule().getNumberOfCardsInput().val();

            if(clickedCurrency.hasClass('active-currency')) {
                that.getModule().getCardPriceInfoContainer().find('span[data-currency=""]').text(currencySymbol);

                that.getModule().updateCardPriceInfo(inputFieldValue, clickedCurrency.data('id'));
            }
        };
    }
});
