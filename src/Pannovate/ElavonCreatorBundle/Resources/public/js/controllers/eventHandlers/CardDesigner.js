CLASS.define({

    name: 'Controllers.EventHandlers.CardDesigner',

    definition: function() {

        var that = this;

        this.controlButtonClickHandler = function(ev) {

            var operationType = $(ev.currentTarget).data('operation-type');
            var selectedItem = that.getActiveElement();

            var currentTextAlign, textAlign, rotation;

            switch(operationType) {

                case 'add-text-field':
                    that.addTextField('Double click to edit', 'text-field');
                break;

                case 'create-shadow':

                    if(selectedItem) {
                        selectedItem.setShadow({ color: 'rgba(0,0,0,0.3)', offsetX: 20, offsetY: 10 });
                        that.getCurrentCardDesignEditor().renderAll();
                    }

                break;

                case 'remove-element':

                    if(selectedItem) {
                        that.getCurrentCardDesignEditor().remove(selectedItem);
                        that.getCurrentCardDesignEditor().renderAll();
                    }

                break;

                case 'bring-forwards':
                    if(selectedItem) {
                        that.getCurrentCardDesignEditor().bringForward(selectedItem);
                    }
                break;

                case 'bring-to-front':
                    if(selectedItem) {
                        that.getCurrentCardDesignEditor().bringToFront(selectedItem);
                    }
                break;

                case 'send-backwards':
                    if(selectedItem) {
                        that.getCurrentCardDesignEditor().sendBackwards(selectedItem);
                    }
                break;

                case 'send-to-back':
                    if(selectedItem) {
                        that.getCurrentCardDesignEditor().sendToBack(selectedItem);
                    }
                break;

                case 'font-weight-bold':

                    var currentFontWeight = selectedItem.getFontWeight();
                    var fontWeight = currentFontWeight === 'normal' ? 'bold' : 'normal';

                    selectedItem.setFontWeight(fontWeight);
                    that.getCurrentCardDesignEditor().renderAll();

                break;

                case 'font-style-italic':

                    var currentFontStyle = selectedItem.getFontStyle();
                    var fontStyle = currentFontStyle === 'normal' ? 'italic' : 'normal';

                    selectedItem.setFontStyle(fontStyle);
                    that.getCurrentCardDesignEditor().renderAll();

                break;

                case 'font-align-left':
                    currentTextAlign = selectedItem.getTextAlign();
                    textAlign = currentTextAlign === 'center' || currentTextAlign === 'right' ? 'left' : currentTextAlign;

                    selectedItem.setTextAlign(textAlign);
                    that.getCurrentCardDesignEditor().renderAll();
                break;

                case 'font-align-center':
                    currentTextAlign = selectedItem.getTextAlign();
                    textAlign = currentTextAlign === 'left' || currentTextAlign === 'right' ? 'center' : currentTextAlign;

                    selectedItem.setTextAlign(textAlign);
                    that.getCurrentCardDesignEditor().renderAll();
                break;

                case 'font-align-right':
                    currentTextAlign = selectedItem.getTextAlign();
                    textAlign = currentTextAlign === 'left' || currentTextAlign === 'center' ? 'right' : currentTextAlign;

                    selectedItem.setTextAlign(textAlign);
                    that.getCurrentCardDesignEditor().renderAll();
                break;

                case 'flip-horizontally':
                    if(selectedItem) {
                        var scaleX = selectedItem.getScaleX();
                        selectedItem.setScaleX(scaleX * -1);
                        that.getCurrentCardDesignEditor().renderAll();
                    }
                break;

                case 'flip-vertically':
                    if(selectedItem) {
                        var scaleY = selectedItem.getScaleY();
                        selectedItem.setScaleY(scaleY * -1);
                        that.getCurrentCardDesignEditor().renderAll();
                    }
                break;

                case 'rotate-left':
                    if(selectedItem) {
                        rotation = selectedItem.getAngle();
                        selectedItem.setAngle(rotation + 5);
                        that.getCurrentCardDesignEditor().renderAll();
                    }
                break;

                case 'rotate-right':
                    if(selectedItem) {
                        rotation = selectedItem.getAngle();
                        selectedItem.setAngle(rotation - 5);
                        that.getCurrentCardDesignEditor().renderAll();
                    }
                break;
            }
        };

        this.browseFileInputChangeEventHandler = function(ev) {

            var file = ev.target.files[0];

            var fileUrl = URL.createObjectURL(file);

            that.getCurrentCardDesignSideModel().addCardDesignImageElementFile(fileUrl, file);

            that.addImageObject(fileUrl, 'background-image');
        };

        this.cardSideButtonClickHandler = function(ev) {
            var currentSide = that.getModule().getCurrentCardSide();
            that.getDesignEditorFromSide(currentSide).deactivateAll().renderAll();
            that.changeCardSide($(ev.currentTarget).data('card-side'));
        };

        this.colorPalleteToolButtonClickHandler = function(ev) {

            var colorPallete = $(ev.currentTarget).parent().find('.color-pallete');

            if(colorPallete.is(':visible')) {
                colorPallete.hide();
            }
            else {
                colorPallete.show();
            }
        };

        this.colorSelectedEventHandler = function(ev) {

            var colorButton = $(ev.currentTarget);
            var colorPallete = $(ev.currentTarget).parent().parent().find('.color-pallete');
            var color = colorButton.css('background-color');

            if(colorPallete.data('auto-hide') !== false) {
                colorPallete.hide();
            }

            var constants = that.getNamespaceValue('Static.Constants');

            var e = $.Event(constants.COLOR_PALLETE_COLOR_CHANGED_EVENT, {color:color, 'color-for': colorPallete.data('color-for')});
            $(document).trigger(e);
        };

        this.designElementMovingEventHandler = function(ev) {
            that.positionDesignElementCloseButton(that.getCurrentCardDesignEditor().getActiveObject());
            that.setIsCardDesignChanged(true);
        };

        this.cardDesignEditorObjectScalingEventHandler = function(ev) {
            var scaledObject = ev.target;
            that.positionDesignElementCloseButton(scaledObject);
        };

        this.cardDesignEditorObjectRotatingEventHandler = function(ev) {
            var scaledObject = ev.target;
            that.positionDesignElementCloseButton(scaledObject);
        };

        this.selectInputChangeEventHandler = function(ev) {

            var select = $(ev.currentTarget);
            var operationType = select.data('operation-type');

            switch(operationType) {
                case 'select-font-size':
                    that.changeTextFieldFontSize(that.getActiveElement(), select.val());
                break;

                case 'select-font':
                    that.changeTextFieldFont(that.getActiveElement(), select.val());
                break;
            }
        };

        this.colorPalleteColorChangedHandler = function(ev) {

            that.setIsCardDesignChanged(true);

            if(ev['color-for'] === 'background') {

                var currentBackgroundImage = that.getCurrentCardDesignSideModel().getBackgroundImage();

                if(currentBackgroundImage) {
                    that.getCurrentCardDesignEditor().remove(currentBackgroundImage);
                }

                that.getCurrentCardDesignSideModel().setBackgroundColor(ev.color);
                that.getCurrentCardDesignSideModel().setBackgroundImage(null);

                that.getCurrentCardDesignEditor().setBackgroundColor(ev.color);
                that.getCurrentCardDesignEditor().renderAll();
            }
            else if(ev['color-for'] === 'disclaimer-text-field') {
                var fixedElements = that.getModule().getCardDesignModel().getBackCardSideModel().getFixedElements();

                for(var i in fixedElements) {
                    if(fixedElements[i].id.indexOf('disclaimer-text') > -1 ) {
                        fixedElements[i].setColor(ev.color);
                    }
                }
                that.getCurrentCardDesignEditor().renderAll();
            }
            else {
                that.changeTextFieldColor(that.getActiveElement(), ev.color);
            }
        };

        this.fabricImageLoadedEventHandler = function(ev) {

        };

        this.cardFrontSideElementRemovedEvent = function(ev) {
            that.getModule().getModuleElement().find('#' + ev.target.id).remove();
        };

        this.cardBackSideElementRemovedEvent = function(ev) {
            that.getModule().getModuleElement().find('#' + ev.target.id).remove();
        };

        this.readLastModifiedDesignCompleteHandler = function(ev) {

            that.getModule().startCreator(ev.responseData);
        };

        this.readLastModifiedDesignErrorHandler = function(ev) {

            that.getModule().getCardDesignLoader().addClass('display-none');

            that.getModule().getCardDesignModel().create(
                {
                    name: 'My New Design',
                    type: that.getModule().getCardDesignModel().getCurrentCardDesign().type.toLowerCase()
                },
                {
                    scope:that
                }
            );
        };

        this.cardDesignCreateCompleteEventHandler = function(ev) {
            that.getModule().startCreator(ev.responseData);
        };

        this.saveCardDesignButtonClickHandler = function(ev) {
            that.saveCardDesign();
        };

        this.cardFrontSideElementSelectedEvent = function(ev) {

            if(that.getModule().getCardDesignModel().getFrontCardSideModel().getCurrentlySelectedElement()) {
                var previousSelectedElementId = that.getModule().getCardDesignModel().getFrontCardSideModel().getCurrentlySelectedElement().id;
                that.getModule().getDesignElementsCloseButtonsContainer().find("#" + previousSelectedElementId).hide();
            }

            that.getModule().getCardDesignModel().getFrontCardSideModel().setCurrentlySelectedElement(ev.target);
            that.getModule().getDesignElementsCloseButtonsContainer().find("#" + ev.target.id).show();

        };

        this.cardBackSideElementSelectedEvent = function(ev) {

            if(that.getModule().getCardDesignModel().getBackCardSideModel().getCurrentlySelectedElement()) {
                var previousSelectedElementId = that.getModule().getCardDesignModel().getBackCardSideModel().getCurrentlySelectedElement().id;
                that.getModule().getDesignElementsCloseButtonsContainer().find("#" + previousSelectedElementId).hide();
            }

            that.getModule().getCardDesignModel().getBackCardSideModel().setCurrentlySelectedElement(ev.target);
            that.getModule().getDesignElementsCloseButtonsContainer().find("#" + ev.target.id).show();
        };

        this.cardFrontSideSelectionClearedEventHandler = function(ev){
            that.getModule().getDesignElementsCloseButtonsContainer().children().hide();
        };

        this.cardBackSideSelectionClearedEventHandler = function(ev){
            that.getModule().getDesignElementsCloseButtonsContainer().children().hide();
        };

        this.saveCardSideDesignCompleteEventHandler = function(ev) {

            var cardDesign = that.getModule().getCardDesignModel().getCurrentCardDesign();

            if(ev.scope.sideModel === that.getModule().getCardDesignModel().getFrontCardSideModel()) {

                that.getApp().updateLoaderNotificationProgress(
                    that.getCardSaveProgressLoaderId(),
                    50
                );

                that.saveCardBackDesign();
            }
            else if(ev.scope.saveAsNew !== true) {

                var e = $.Event(
                    that.getModule().getCardDesignModel().events.SAVE_CARD_DESIGN_COMPLETE,
                    {
                        cardDesign: cardDesign,
                        scope: ev.scope
                    }
                );

                $(document).trigger(e);
            }
        };

        this.saveCardDesignCompleteEventHandler = function(ev) {
            that.stopSaveCardProgress();
            that.setIsCardDesignChanged(false);
        };

        this.readCardDesignImageUploadProgressCompleteHandler = function(ev) {

            var frontUploadItems = that.getModule().getCardDesignModel().getFrontCardSideModel().getSaveCardDesignUploadIds();
            var backUploadItems = that.getModule().getCardDesignModel().getBackCardSideModel().getSaveCardDesignUploadIds();

            if(ev.responseData.received) {

                var currentFilePercentUploaded = ev.responseData.received / ev.responseData.size * 100;
                var currentFilePercentUploaded = currentFilePercentUploaded / 2;

                var percentUploaded = 0;
                var uploadImages;
                var uploadImagesCount = 0;

                var itemsRemaining;

                if(ev.scope.getCardSide() === 'front') {
                    itemsRemaining = frontUploadItems.length;
                    uploadImages = that.getModule().getCardDesignModel().getFrontCardSideModel().getCardDesignUploadImages();
                }
                else {
                    itemsRemaining = backUploadItems.length;
                    uploadImages = that.getModule().getCardDesignModel().getBackCardSideModel().getCardDesignUploadImages();
                }

                uploadImagesCount = uploadImages.length + 1;

                currentFilePercentUploaded = currentFilePercentUploaded / uploadImagesCount;

                if(ev.scope.getCardDesignUploadStatus() === ev.scope.CARD_UPLOAD_STATUS_UPLOADING_DESIGN_ELEMENT) {
                    itemsRemaining += 1;
                }

                var currentFileRangePercent = (uploadImagesCount - itemsRemaining) * (50 / uploadImagesCount);

                if(ev.scope.getCardSide() === 'front') {
                    percentUploaded = currentFileRangePercent + currentFilePercentUploaded;
                }
                else {
                    percentUploaded = 50 + currentFileRangePercent + currentFilePercentUploaded;
                }

                that.getApp().updateLoaderNotificationProgress(
                    that.getCardSaveProgressLoaderId(),
                    percentUploaded
                );
            }
        };

        this.socialMediaPhotoSelectedEventHandler = function(ev) {
            that.addImageObject(ev.photo, 'background-image');
        };

        this.cardFrontSideTextChangedEventHandler = function(ev) {
        };

        this.cardBackSideTextChangedEventHandler = function(ev) {
            that.setIsCardDesignChanged(true);
            that.autoFitBackSideText(ev.target);
        };
    }
});
