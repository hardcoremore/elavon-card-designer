CLASS.define({

	name: 'Controllers.FlickrImportPhotos',

	definition: function(app, module) {

		this.extendFromClass('Base.Controller', [app, module]);

		var that = this;
		var imagesLoadedCount = 0;
		var flickrPhotosMasonry;

		this.flickrUsernamePopupSubmitterHandler = function(ev, data) {

			that.getModule().getFlickrModel().readUserId(data.formData.username, {
				scope: that
			});
        };

		this.flickrUserLoadedCompleteHandler = function(event) {

			var responseData = event.responseData;

			if(responseData.stat == 'fail') {

				that.getModule().getFlickrLoadMoreButton().off('click', that.flickrPhotoLoadMoreButtonClickHandler);
				that.getModule().getFlickrImportPhotosLoader().hide();
				that.getModule().getFlickrPhotosContainer().empty();
				module.getApp().showNotification('error', 'Flickr username does not exist');

			}
			else {

				that.getModule().getFlickrUsernamePopup().panpopup('close');

				var flickApiKey = that.getModule().getFlickrModel().getFlickrApiKey();

				that.loadUserImages(responseData.user.nsid);
				
				that.getModule().getFlickrPhotosContainer().empty();

				setTimeout(function(){
					that.getModule().getFlickrUserPhotosPopup().panpopup('open');	
				}, 360);
			}
		};

		this.loadUserImages = function(userId) {

			var data = {api_key: this.getModule().getFlickrModel().getFlickrApiKey(), user_id: userId, format: 'json', nojsoncallback: 1, per_page: 10, page: 1};

			that.getModule().getFlickrModel().getUserImages(data, {
				scope: this
			});
		};

		this.flickrPhotosLoadCompleteHandler = function(event) {

			photosNumber = event.responseData.photos.total;

			if(photosNumber != '0') {

				$.each(event.responseData.photos.photo, function(index){

					html = that.getApp().compileTemplate(
						"flickr-photo-item-template",
						{
							name:this.title,
							photoId: this.id,
							thumbImage: 'https://farm'+this.farm+'.staticflickr.com/'+this.server+'/'+this.id+'_'+this.secret+'_z.jpg',
							originalImage: 'https://farm'+this.farm+'.staticflickr.com/'+this.server+'/'+this.id+'_'+this.secret+'_z.jpg'
						}
					);
					var item = $(html);
					that.getModule().getFlickrPhotosContainer().append(item);
				});

				if(event.responseData.photos.page <  event.responseData.photos.pages)
				{
					nextPage = event.responseData.photos.page + 1;
					if(event.responseData.photos.page == 1)
					{
						that.getModule().getFlickrLoadMoreButton().on('click', that.flickrPhotoLoadMoreButtonClickHandler);
					}
				}
				else
				{
					that.getModule().getFlickrLoadMoreButton().off('click', that.flickrPhotoLoadMoreButtonClickHandler);
					that.getModule().getFlickrLoadMoreButton().hide();

				}

				that.getModule().getFlickrPhotosContainer().find(".flickr-image-item").on('load', that.flickrPhotoLoadedComplete);
			}
			else
			{
				that.getModule().getFlickrLoadMoreButton().off('click', that.flickrPhotoLoadMoreButtonClickHandler);
				that.getModule().getFlickrImportPhotosLoader().hide();
				that.getModule().getFlickrPhotosContainer().empty();
				module.getApp().showNotification('error', 'Flickr user does not have any pictures');
			}

		};

	/*Scrool handler*/

		//Scroll position
		var scrollElement  = $('.photo-list-wrapper'),
			scrollPosition = scrollElement.scrollTop();

		scrollElement.scroll(function(){
			//Update scroll position and return it to global var
			scrollPosition = scrollElement.scrollTop();
		});

		this.flickrPhotoLoadedComplete = function(event) {

			var photosInCurrentPage = 10;

			if(imagesLoadedCount === (photosInCurrentPage - 1)) {

				that.getModule().getFlickrPhotosContainer().show();

				if(flickrPhotosMasonry) {
					flickrPhotosMasonry.masonry('destroy');
				}

				flickrPhotosMasonry = that.getModule().getFlickrPhotosContainer().masonry({
					percentPosition : true,
					itemSelector: '.flickr-photo-item'
				});

				that.getModule().getFlickrImportPhotosLoader().hide();

			}
			else{
				imagesLoadedCount++;
			}
		};


		this.flickrPhotoLoadMoreButtonClickHandler = function(event) {

			var userNsid = that.getModule().getFlickrModel().getCurrentFlickrUser().nsid;
			var data = {api_key: that.getModule().getFlickrModel().getFlickrApiKey(), user_id: userNsid, format: 'json', nojsoncallback: 1, per_page: 10, page: nextPage};
			that.getModule().getFlickrImportPhotosLoader().show();
			that.getModule().getFlickrModel().getUserImages(data, {
				scope: this
			});
		};

		this.flickrPhotoSelectedHandler = function(event) {

			scrollElement.scrollTop(0);
			var photoUrl = $(event.target).attr('image-url');

			that.getModule().getFlickrModel().setSelectedPhoto(photoUrl);

			that.getModule().getFlickrUserPhotosPopup().panpopup('close');
			$('body').removeClass('body-content-limit');

			that.getModule().getFlickrLoadMoreButton().off('click', that.flickrPhotoLoadMoreButtonClickHandler);
			that.getModule().getFlickrImportPhotosLoader().hide();
			that.getModule().getFlickrPhotosContainer().empty();
		};

		this.flickrControlClickHandler = function(event) {

            var action = $(event.target).data('action');
            switch(action) {

                case 'close':

                	scrollElement.scrollTop(0);
					that.getModule().getFlickrUserPhotosPopup().panpopup('close');
					that.getModule().getFlickrLoadMoreButton().off('click', that.flickrPhotoLoadMoreButtonClickHandler);
					that.getModule().getFlickrImportPhotosLoader().hide();
					that.getModule().getFlickrPhotosContainer().empty();

                    $("body").removeClass('body-content-limit');
                break;
            }
        };

        this.flickrValidationErrorEventHandler = function(ev) {
        	console.log(ev);
        };
	},

	prototypeMethods: {

		init: function() {

			var flickerAPIKey = this.getApp().getConfig().getParameter('flickrAPIKey');
			this.getModule().getFlickrModel().setFlickrApiKey(flickerAPIKey);
		},

		addEvents: function() {

			$(document).on(this.getModule().getFlickrModel().events.READ_USER_ID_COMPLETE, this.flickrUserLoadedCompleteHandler);
			$(document).on(this.getModule().getFlickrModel().events.READ_USER_PHOTOS_COMPLETE, this.flickrPhotosLoadCompleteHandler);
			$(document).on(this.getModule().getFlickrModel().events.VALIDATION_ERROR, this.flickrValidationErrorEventHandler);

			this.getModule().getFlickrContentAllHolder().on('click', '.flickr-import-control', this.flickrControlClickHandler);
			this.getModule().getFlickrPhotosContainer().on('click', this.flickrPhotoSelectedHandler);
		},

		removeEvents: function() {

			$(document).off(this.getModule().getFlickrModel().events.READ_USER_ID_COMPLETE, this.flickrUserLoadedCompleteHandler);
			$(document).off(this.getModule().getFlickrModel().events.READ_USER_PHOTOS_COMPLETE, this.flickrPhotosLoadCompleteHandler);
			$(document).off(this.getModule().getFlickrModel().events.VALIDATION_ERROR, this.flickrValidationErrorEventHandler);

			this.getModule().getFlickrContentAllHolder().off('click', '.flickr-import-control', this.flickrControlClickHandler);
			this.getModule().getFlickrLoadMoreButton().off('click', this.flickrPhotoLoadMoreButtonClickHandler);
		},
	},

	extendPrototypeFrom: 'Base.Controller'
});
