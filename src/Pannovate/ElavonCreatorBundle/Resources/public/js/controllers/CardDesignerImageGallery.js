CLASS.define({

    name: 'Controllers.CardDesignerImageGallery',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);

        var that = this;
        var imagePreviewCanvas;
        var selectedAlbum;

        this.imageGalleryReadAllCategoriesCompleteHandle = function(ev) {
            for (i = 0; i < ev.responseData.length; i++){
                that.getModule().getGalleryAlbumsContainer().append('<span id="'+ev.responseData[i].id+'" class="span-btn green-btn effect-button" data-effect="original">'+ev.responseData[i].name+'</span>');
            }
            that.getModule().getImageGalleryCategoryModel().getImagesByCategory(ev.responseData[0].id, {scope:this});
        };

        this.addImageItemToCardDesignGallery = function(name, id, fileUrl) {

            html = that.getApp().compileTemplate(
                "album-photo-item-template",
                {
                    name: name,
                    photoId: id,
                    originalImage: fileUrl
                }
            );
            var item = $(html);
            that.getModule().getGalleryPhotosContainer().append(item);
        };

        this.imagesReadCompleteEventHandler = function(ev) {

            photosNumber = ev.responseData.length;

            that.getModule().getGalleryPhotosContainer().empty();
            $.each(ev.responseData, function(index){

                that.addImageItemToCardDesignGallery(this.name, this.id, this.fileUrl);
            });
        };

        this.imageGalleryPopupOpen = function(ev) {

            if(!that.getModule().getImageGalleryCategoryModel().getLoadedData()) {
                that.getModule().getImageGalleryCategoryModel().readAll({scope: that});
            }
        }

        this.cardDesigGalleryPhotoSelectedHandler = function(ev) {

            var photoUrl = $(ev.target).attr('src');

            that.getModule().getImageGalleryItemModel().setSelectedPhoto(photoUrl);
            that.getModule().getCardDesignerImageGalleryPopup().panpopup('close');
        };

        this.imageCategoryClickHandler = function(ev) {
            that.getModule().getImageGalleryCategoryModel().getImagesByCategory(ev.target.id, {scope:this});
        };
    },

    prototypeMethods: {

        addEvents: function() {

            var moduleElement = this.getModule().getModuleElement();

            $(document).on(this.getModule().getImageGalleryCategoryModel().events.READ_ALL_COMPLETE, this.imageGalleryReadAllCategoriesCompleteHandle);
            $(document).on(this.getModule().getImageGalleryCategoryModel().events.READ_COMPLETE, this.imagesReadCompleteEventHandler);

            this.getModule().getCreatorGalleryButton().on('click', this.imageGalleryButtonClickHandler);
            this.getModule().getGalleryPhotosContainer().on('click', this.cardDesigGalleryPhotoSelectedHandler);
            this.getModule().getGalleryAlbumsContainer().on('click', this.imageCategoryClickHandler);
        },

        removeEvents: function() {

            $(document).off(this.getModule().getImageGalleryCategoryModel().events.READ_ALL_COMPLETE, this.imageGalleryReadAllCategoriesCompleteHandle);
            $(document).off(this.getModule().getImageGalleryCategoryModel().events.READ_COMPLETE, this.imagesReadCompleteEventHandler);

            this.getModule().getCreatorGalleryButton().off('click', this.imageGalleryButtonClickHandler);
            this.getModule().getGalleryPhotosContainer().off('click', this.cardDesigGalleryPhotoSelectedHandler);
            this.getModule().getGalleryAlbumsContainer().off('click', this.imageCategoryClickHandler);
        }
    },

    extendPrototypeFrom: 'Base.Controller'
});
