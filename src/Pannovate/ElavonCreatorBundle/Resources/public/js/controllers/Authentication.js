CLASS.define({
    
    name: 'Controllers.Authentication',

    definition: function(app) {

        this.extendFromClass('Base.Controller', [app]);
        this.AUTHENTICATION_COMPETE_EVENT = "Events.Controllers.Authentication.authenticationComplete";

        var that = this;

        this.loadCurrentUserCompleteHandler = function(ev) {

            var userModel = that.getApp().getUserModel();
            var fanfareId = userModel.getFanfareId();

            if(ev.responseData.username.toString() === fanfareId.toString())
            {
                var e = $.Event(that.AUTHENTICATION_COMPETE_EVENT, {user: ev.responseData});
                $(document).trigger(e);
            }
            else
            {
                userModel.logout({scope:that});
            }
        };

        this.loadCurrentUserErrorHandler = function(ev) {

            if(ev.error.code === 403) {

                var fanfareId = that.getApp().getUserModel().getFanfareId();

                that.getApp().getUserModel().login(
                    fanfareId,
                    fanfareId,
                    {scope:that}
                );
            }
            else {
                that.throwException('Failed to load user from session', 1502, 'model');
            }
        };

        this.loginCompleteEventHandler = function(ev) {
            var e = $.Event(that.AUTHENTICATION_COMPETE_EVENT, {user: ev.responseData});
            $(document).trigger(e);
        };

        this.loginErrorEventHandler = function(ev) {

            if(ev.error.code === 400) {
                that.getApp().getUserModel().create(
                    {
                        username: that.getApp().getUserModel().getFanfareId(),
                        password: that.getApp().getUserModel().getFanfareId(),
                        isActive: true
                    },
                    {scope:that}
                );
            }
            else{
                that.getApp().throwException("Error ocurred while trying to log in user.", 1500, 'kernel');
            }
        };

        this.userCreateCompleteEventHandler = function() {

            var fanfareId = that.getApp().getUserModel().getFanfareId();

            that.getApp().getUserModel().login(
                fanfareId,
                fanfareId,
                {scope:that}
            );
        };

        this.userCreateErrorEventHandler = function() {
            that.getApp().throwException("Error ocurred while registering user.", 1500, 'kernel');
        };

        this.logoutCompleteEventHandler = function(ev) {

            var fanfareId = that.getApp().getUserModel().getFanfareId();

            that.getApp().getUserModel().login(
                fanfareId,
                fanfareId,
                {scope:that}
            );
        };

        this.logoutErrorEventHandler = function(ev) {
            console.log("LOGOUT ERROR HANDLER");  
        };

    },

    prototypeMethods: {

        init: function() {

            var userModel = this.getApp().getUserModel();

            $(document).on(userModel.events.LOAD_CURRENT_USER_COMPLETE, this.loadCurrentUserCompleteHandler);
            $(document).on(userModel.events.LOAD_CURRENT_USER_ERROR, this.loadCurrentUserErrorHandler);

            $(document).on(userModel.events.LOGIN_COMPLETE, this.loginCompleteEventHandler);
            $(document).on(userModel.events.LOGIN_ERROR, this.loginErrorEventHandler);

            $(document).on(userModel.events.CREATE_COMPLETE, this.userCreateCompleteEventHandler);
            $(document).on(userModel.events.CREATE_ERROR, this.userCreateErrorEventHandler);

            $(document).on(userModel.events.LOGOUT_COMPLETE, this.logoutCompleteEventHandler);
            $(document).on(userModel.events.LOGOUT_ERROR, this.logoutErrorEventHandler);

            userModel.loadCurrentUser({scope: this});
        }
    }
});