CLASS.define({

    name: 'Controllers.FacebookImportPhotosMasonry',

    definition: function(app, module) {

        this.extendFromClass('Controllers.FacebookImportPhotos', [app, module]);

        var that = this;

        var facebookAlbumsMasonry;
        var facebookPhotosMasonry;

        this.setAlbumsMasonry = function(pm) {
            facebookAlbumsMasonry = pm;
        };

        this.getAlbumsMasonry = function() {
            return facebookAlbumsMasonry;
        };

        this.setPhotosMasonry = function(pm) {
            facebookPhotosMasonry = pm;
        };

        this.getPhotosMasonry = function(am) {
            return facebookPhotosMasonry;
        };

        this.facebookAlbumsLoadCompleteHandler = function(ev) {

            var albums = ev.albums;

            if(facebookModel.getAlbumsPaging().next) {
                that.getModule().getLoadMoreButton().show();
            }
            else {
                that.getModule().getLoadMoreButton().hide();
            }

            $.each(albums, function(index){

                var html = that.getApp().compileTemplate(
                    "facebook-album-item-template",
                    {
                        name:this.name,
                        albumId: index,
                    }
                );
                
                var item = $(html);
                that.getAlbumsMasonry().append(item[0]).masonry('appended', item[0]);
                item.hide();

                that.getFacebookModel().loadAlbumCoverPhoto(index);
            });

        };

        this.facebookAlbumPhotosLoadComplete = function(ev) {


            if(that.getFacebookModel().getPhotosPaging().next) {
                that.getModule().getLoadMoreButton().show();
            }
            else {
                that.getModule().getLoadMoreButton().hide();
            }

            $.each(ev.photos, function(index){

                var html = that.getApp().compileTemplate(
                    "facebook-photo-item-template",
                    {
                        name: this.name,
                        photoId: this.id,
                    }
                );

                var item = $(html);
                that.getPhotosMasonry().append(item[0]).masonry('appended', item[0]);
                item.hide();

                that.getFacebookModel().loadPhotoById(this.id);
            });
        };

        /*** END OF FACEBOOK MODEL HANDLERS ****/
    },

    prototypeMethods: {

        init: function() {

            this.callSuper('Controllers.FacebookImportPhotos', 'init');

            var albumsMasonry = this.getModule().getFacebookAlbumsContainer().masonry({
                percentPosition : true,
                itemSelector: '.facebook-album-item'
            });

            this.setAlbumsMasonry(albumsMasonry);

            var photosMasonry = this.getModule().getFacebookAlbumPhotosContainer().masonry({
                percentPosition : true,
                itemSelector: '.facebook-photo-item'
            });

            this.setPhotosMasonry(photosMasonry);
        },

        showAlbums: function() {
            this.getAlbumsMasonry().masonry('layout');
        },

        showAlbumPhotos: function() {
            this.getPhotosMasonry().masonry('layout');
        }
    },

    extendPrototypeFrom: 'Controllers.FacebookImportPhotos'
});
