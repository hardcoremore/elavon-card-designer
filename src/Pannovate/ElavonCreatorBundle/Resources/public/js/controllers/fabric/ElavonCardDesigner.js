CLASS.createNamespace('Controllers.Fabric.ElavonCardDesigner', {

    addFrontSideFixedOverlay: function() {
        var that = this;

        var options = options || {};
        var elementId = this.generateElementId('fixed-ovarlay-front');
        var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectFixedParams');

        var params = $.extend(options, fabricObjectParams, {
            id: elementId,
            elementType: 'fixed-item',
            left: 0,
            top: 0,
            width: 476,
            height: 300
        });

        var logo = new fabric.Image.fromURL('/images/designer/border.png', function(img) {
            img.set(params);

            img.perPixelTargetFind = false;
            img.targetFindTolerance = 8;

            that.getModule().getCardFrontSideDesignEditor().add(img);

            that.getModule().getCardDesignModel().getFrontCardSideModel().addCardDesignElement(img);
            that.getModule().getCardDesignModel().getFrontCardSideModel().addFixedElement(img);
            that.extendObjectsSerializationFunction(img);

            that.getModule().getCardFrontSideDesignEditor().setActiveObject(img);
        });

    },

    addFrontSideFixedLogoField: function(logoUrl) {

        var that = this;
        var logoOptions;
        var options = {
            top: 210,
            left: 40,
            width: 437,
            height: 60
        };

        if(logoUrl.indexOf('promo') !== -1) {
            logoOptions = {
                top: 200,
                left: 20,
                width: 250,
                height: 40
            };
        }
        else if(logoUrl.indexOf('gift') !== -1) {
            logoOptions = {
                top: 220,
                left: 210,
                width: 220,
                height: 50
            };
        }

        var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectFixedParams');
        var elementId = this.generateElementId('fixed-logo-front');

        var params = $.extend(options, fabricObjectParams, logoOptions, {
            id: elementId,
            elementType: 'fixed-item',
        });

        var logoField = new fabric.Image.fromURL(logoUrl, function(img) {
            img.set(params);

            img.perPixelTargetFind = false;
            img.targetFindTolerance = 8;

            that.getModule().getCardFrontSideDesignEditor().add(img);

            that.getModule().getCardDesignModel().getFrontCardSideModel().addCardDesignElement(img);
            that.getModule().getCardDesignModel().getFrontCardSideModel().addFixedElement(img);
            that.extendObjectsSerializationFunction(img);
        });

        that.setIsCardDesignChanged(true);
    },

    loadFrontSideFixedElements: function() {

        var cardType = this.getModule().getCardDesignModel().getCurrentCardDesign().type.toLowerCase();
        
        switch(cardType) {
            case 'loyalty':
                this.addFrontSideFixedLogoField(this.getApp().getConfig().getParameter('frontLoyaltyLogoDark'));
            break;
            case 'promo':
                this.addFrontSideFixedLogoField(this.getApp().getConfig().getParameter('frontPromoLogoDark'));
            break;
            case 'gift':
                this.addFrontSideFixedLogoField(this.getApp().getConfig().getParameter('frontGiftLogoDark'));
            break;
        }
    },

    loadBackSideFixedElements: function() {

        var cardType = this.getModule().getCardDesignModel().getCurrentCardDesign().type.toLowerCase();

        this.addBackSideLogo();
        this.addBackSideCardTextLabel();

        switch(cardType) {
            case 'loyalty':
                this.addBackSideDisclaimerText(this.getApp().getConfig().getParameter('cardDisclaimerTextLoyalty'));
            break;
            case 'promo':
                this.addBackSideDisclaimerText(this.getApp().getConfig().getParameter('cardDisclaimerTextPromo'));
            break;
            case 'gift':
                this.addBackSideDisclaimerText(this.getApp().getConfig().getParameter('cardDisclaimerTextGift'));
            break;
        }
    },

    loadFrontSideNonPrintingElements: function() {
        //this.addFrontSideFixedOverlay();
    },

    loadBackSideNonPrintingElements: function() {

        var magStripe = this.addBackSideMagStripe();
        var cardNumber = this.addBackSideCardNumber('123456789101234');

        var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectFixedParams');

        var params = $.extend({}, fabricObjectParams, {
            id: 'back-side-non-printing',
            elementType: 'non-printing-group',
        });

        var backSideGroup = new fabric.Group([magStripe, cardNumber], params);
        this.getModule().getCardDesignModel().getBackCardSideModel().setNonPrintingElementsGroup(backSideGroup);
        this.getModule().getCardBackSideDesignEditor().add(backSideGroup);

        this.addBackSideBoundingBox();

        this.getModule().getCardBackSideDesignEditor().renderAll();
    },

    addTextField: function(text, id, options) {

        var textField;
        var argOptions = options || {};
        var localOptions;
        var params;
        var selectField = this.getModule().getModuleElement().find('.tool-font-size');

        if(this.getModule().getCurrentCardSide() === 'front') {
            argOptions.fontSize = 22;
            textField = this.callSuper('Controllers.CardDesigner', 'addTextField', [text, id, argOptions]);
        }
        else if(this.getModule().getCurrentCardSide() === 'back') {

            if(this.getCurrentCardDesignSideModel().getTextField()) {
                this.getApp().showNotification('information', 'You can only have one text field on back side of the card.');
                return null;
            }
            else {
                localOptions = {
                    left: 17,
                    top: 220,
                    fontSize: 16
                };

                textField = this.callSuper('Controllers.CardDesigner', 'addTextField', [text, id, localOptions]);

                this.getCurrentCardDesignSideModel().setTextField(textField);
            }
        }

        params = $.extend(argOptions, localOptions);
        selectField.val(params.fontSize);
        this.setIsCardDesignChanged(true);

        return textField;
    },

    addBackSideMagStripe: function(nonPrintingGroup) {

        var options = options || {};

        var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectFixedParams');
        var elementId = this.generateElementId('mag-stripe');

        var params = $.extend(options, fabricObjectParams, {
            id: elementId,
            elementType: 'fixed-item',
            fill: 'rgb(0,0,0)',
            top: 17.007874,
            width: 476,
            height: 69.476609541,
        });

        var magStripe = new fabric.Rect(params);

        this.getModule().getCardDesignModel().getBackCardSideModel().addCardDesignElement(magStripe);

        return magStripe;
    },

    addBackSideCardTextLabel: function() {

        var options = options || {};

        var groupId = this.generateElementId('cardnum-group');
        var cardBackNumberLabelId = this.generateElementId('cardnum-label');

        var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectFixedParams');

        var params = $.extend(options, fabricObjectParams, {
            id: groupId,
            elementType: 'fixed-item',
            left: -1,
            top: 86.484475541,
        });

        var cnPlaceholder = new fabric.Rect({
            fill: 'rgb(255,255,255)',
            width: 477,
            height: 28,
            left: 0
        });

        var cardNumberLabel = new fabric.Text('Your card number is:', {
            top: 7,
            left: 20,
            color: 'rgb(0,0,0)',
            fontSize: 13,
            fontFamily: 'Arial',
            id: cardBackNumberLabelId
        });

        var group = new fabric.Group([ cnPlaceholder, cardNumberLabel ], params);

        this.getModule().getCardBackSideDesignEditor().add(group);
        this.getModule().getCardDesignModel().getBackCardSideModel().addCardDesignElement(group);
        this.getModule().getCardDesignModel().getBackCardSideModel().addFixedElement(group);

        this.extendObjectsSerializationFunction(group);

        return group;
    },

    addBackSideCardNumber: function(cardNumberText) {

        var options = options || {};

        var backNumberId = this.generateElementId('card-back-number');
        var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectFixedParams');

        var params = $.extend(options, fabricObjectParams, {
            id: backNumberId,
            elementType: 'fixed-item',
            top: 93.484475541,
            left: 145,
            fill: 'rgb(0,0,0)',
            fontSize: 13,
            fontFamily: 'Arial'
        });

        var cardNumber = new fabric.Text(cardNumberText, params);

        this.getModule().getCardDesignModel().getBackCardSideModel().addCardDesignElement(cardNumber);

        return cardNumber;
    },

    addBackSideLogo: function() {
        var that = this;

        var options = options || {};
        var elementId = this.generateElementId('fixed-logo-back');
        var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectFixedParams');

        var params = $.extend(options, fabricObjectParams, {
            id: elementId,
            elementType: 'fixed-item',
            left: 280,
            top: 200,
            width: 180,
            height: 70
        });

        var logo = new fabric.Image.fromURL('/images/logo-back.png', function(img) {
            img.set(params);

            img.perPixelTargetFind = false;
            img.targetFindTolerance = 8;

            that.getModule().getCardBackSideDesignEditor().add(img);

            that.getModule().getCardDesignModel().getBackCardSideModel().addCardDesignElement(img);
            that.getModule().getCardDesignModel().getBackCardSideModel().addFixedElement(img);
            that.extendObjectsSerializationFunction(img);

            that.getModule().getCardBackSideDesignEditor().setActiveObject(img);
        });
    },

    addBackSideDisclaimerText: function(text) {

        var options = options || {};

        var elementId = this.generateElementId('disclaimer-text');
        var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectFixedParams');

        var params = $.extend(options, fabricObjectParams, {
            id: elementId,
            elementType: 'fixed-item',
            left: 17,
            top: 131.484475541,
            color: 'rgb(0,0,0)',
            width: 442,
            fontSize: 8.9,
            lineHeight: 1,
            textAlign: 'left',
            fontFamily: 'Arial'
        });

        var disclaimerText = new fabric.Textbox(text ,params);

        this.getModule().getCardBackSideDesignEditor().add(disclaimerText);

        this.getModule().getCardDesignModel().getBackCardSideModel().addCardDesignElement(disclaimerText);
        this.getModule().getCardDesignModel().getBackCardSideModel().addFixedElement(disclaimerText);
        this.extendObjectsSerializationFunction(disclaimerText);

        this.getModule().getCardBackSideDesignEditor().setActiveObject(disclaimerText);

        return disclaimerText;
    },

    addBackSideBoundingBox: function() {

        var options = options || {};

        var elementId = this.generateElementId('bounding-box');
        var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectFixedParams');

        var params = $.extend(options, fabricObjectParams, {
            id: elementId,
            elementType: 'fixed-item',
            left: 3,
            top: 173,
            width: 272,
            height: 130,
            fill: 'transparent'
        });

        var boundingBox = new fabric.Rect(params);
        this.getModule().getCardDesignModel().getBackCardSideModel().setElementsBoundingBox(boundingBox);
    },

    setBackgroundImage: function(imageUrl) {

        var that = this;

        if(this.getCurrentCardDesignSideModel().getBackgroundImage()) {
            this.getCurrentCardDesignEditor().remove(this.getCurrentCardDesignSideModel().getBackgroundImage());
        }

        this.addImageObject(imageUrl, 'background-image', {hasControls: false, backgroundActive: true});

        this.getCurrentCardDesignSideModel().setBackgroundColor('');
        this.getCurrentCardDesignEditor().setBackgroundColor('rgb(255, 255, 255)');
    },

    setLogoImage: function(imageUrl) {
        var that = this;
        if(this.getCurrentCardDesignSideModel().getLogoImage()) {
            this.getCurrentCardDesignSideModel().getLogoImage().setSrc(imageUrl, function(){that.getCurrentCardDesignEditor().renderAll();});
            this.getCurrentCardDesignEditor().renderAll();
        }
        else {
            var options = {scaleX: 0.2, scaleY: 0.2};
            this.addImageObject(imageUrl, 'logo-image', options);
        }
    },

    hideFixedElements: function() {

        var fixedElements = this.getModule().getCardDesignModel().getBackCardSideModel().getFixedElements();

        var onChangeHandler = function() {
            that.getModule().getCardBackSideDesignEditor().renderAll();
        };

        var onCompleteHandler = function() {
            console.log('Hide non printing elements animation complete');
        };

        for(var i in fixedElements) {

            fixedElements[i].animate('opacity', 0, {
                duration: 100,
                onChange: onChangeHandler,
                onComplete: onCompleteHandler,
                easing: fabric.util.ease.easeOutSine
            });
        }
    },

    hideNonPrintingElements: function(completeCallback, changeCallback) {

        var nonPrintingElementsGroup = this.getModule().getCardDesignModel().getBackCardSideModel().getNonPrintingElementsGroup();

        var that = this;

        nonPrintingElementsGroup.animate('opacity', 0, {
            duration: 100,
            onChange: changeCallback,
            onComplete: completeCallback,
            easing: fabric.util.ease.easeOutSine
        });
    },

    showNonPrintingElements: function() {

        var nonPrintingElementsGroup = this.getModule().getCardDesignModel().getBackCardSideModel().getNonPrintingElementsGroup();

        var that = this;

        nonPrintingElementsGroup.animate('opacity', 1, {
            duration: 100,
            onChange: function() {
                that.getModule().getCardBackSideDesignEditor().renderAll();
            },
            onComplete: function() {
                that.getModule().getCardBackSideDesignEditor().add(nonPrintingElementsGroup);
            },
            easing: fabric.util.ease.easeOutSine
        });
    },

    fabricImageLoadedCallback: function(params, image) {

        this.callSuper('Controllers.CardDesigner', 'fabricImageLoadedCallback', [params, image]);
        image.originalImageHeight = image.height;
        image.originalImageWidth = image.width;

        if(image.elementType === 'background-image') {
            this.getCurrentCardDesignSideModel().setBackgroundImage(image);
            image.backgroundActive = true;
            image.moveTo(0);
            this.autoFitBackgroundImage(image);
        }
        else if(image.elementType === 'logo-image') {

            this.getCurrentCardDesignSideModel().setLogoImage(image);
            image.moveTo(0);

            if(this.getCurrentCardDesignSideModel().getBackgroundImage()) {
                image.moveTo(1);
            }

            this.getModule().createDesignElementCloseButton(image, this.getModule().getCurrentCardSide());
            this.positionDesignElementCloseButton(image);
        }

        this.elementInputFieldsValuesEventHandler(image);
    },

    autoFitBackgroundImage: function(canvasImageObj) {

        var designEditor = this.getCurrentCardDesignEditor();
        var imageRatio = canvasImageObj.width/canvasImageObj.height;
        var editorRatio = designEditor.getWidth()/designEditor.getHeight();
        var dif, width, height, top, left;

        this.updateBackgroundScaleSlider(1);

        canvasImageObj.setScaleX(1);
        canvasImageObj.setScaleY(1);
        canvasImageObj.setWidth(canvasImageObj.originalImageWidth);
        canvasImageObj.setHeight(canvasImageObj.originalImageHeight);

        if(imageRatio > editorRatio) {

            dif =  designEditor.getHeight()/canvasImageObj.height;

            width = canvasImageObj.width * dif;
            height = designEditor.getHeight();

            left = (designEditor.getWidth() - width)/2.0;
            canvasImageObj.setLeft(left);
        }
        else {

            dif =  designEditor.getWidth()/canvasImageObj.width;

            width = designEditor.getWidth();
            height = canvasImageObj.height * dif;

            top = (designEditor.getHeight() - height)/2.0;
            canvasImageObj.setTop(top);
        }

        canvasImageObj.userBackgroundPosition = false;
        this.updateBackgroundScaleSlider(width / canvasImageObj.width);
    },

    centerBackgroundImage: function(canvasImageObj) {

        var designEditor = this.getCurrentCardDesignEditor();
        var left,top;

        left = -(canvasImageObj.getWidth() - designEditor.getWidth())/2;
        top = -(canvasImageObj.getHeight() - designEditor.getHeight())/2;

        canvasImageObj.setLeft(left);
        canvasImageObj.setTop(top);

        canvasImageObj.setCoords({
            left: left,
            top: top
        });
    }
});
