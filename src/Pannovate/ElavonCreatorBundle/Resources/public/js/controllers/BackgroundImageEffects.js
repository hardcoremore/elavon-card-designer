CLASS.define({

    name: 'Controllers.BackgroundImageEffects',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);

        var that = this;
        var imagePreviewCanvas;
        var selectedFilter;

        this.applyFilter = function(filter) {

            var obj = imagePreviewCanvas.getActiveObject();
            obj.filters = [];
            obj.applyFilters();

            obj.filters[0] = filter;
            obj.applyFilters(imagePreviewCanvas.renderAll.bind(imagePreviewCanvas));
        };

        this.backgrodundImageEffectButtonClickHandler = function(ev) {

            imagePreviewCanvas = new fabric.Canvas("background-image-canvas");
            that.cloneImageObject(imagePreviewCanvas);
            that.getModule().getBackgroundImageEffectsPopup().panpopup('open');
        };

        this.cloneImageObject = function(imagePreviewCanvas) {

            var currentSideModel;

            if(that.getModule().getCurrentCardSide() === 'front') {
                currentSideModel = that.getModule().getCardDesignModel().getFrontCardSideModel();
            }
            else {
                currentSideModel = that.getModule().getCardDesignModel().getBackCardSideModel();
            }

            var newObject = fabric.util.object.clone(currentSideModel.getBackgroundImage());
            imagePreviewCanvas.add(newObject);
            imagePreviewCanvas.setActiveObject(newObject);
        };

        this.effectButtonClickHandler = function(ev) {

            that.cloneImageObject(imagePreviewCanvas);
            var button = $(ev.currentTarget);
            var effectFunction;
            var f = fabric.Image.filters;

            switch (button.data('effect')) {
                case 'original': 
                    
                    selectedFilter = false;
                    break;
                case 'grayscale': 
                    selectedFilter = new f.Grayscale();
                    break;
                case 'sepia':
                    selectedFilter = new f.Sepia();
                    break;
                case 'invert':
                    selectedFilter = new f.Invert();
                    break;
                case 'blur':
                    selectedFilter = new f.Convolute({
                        matrix: [ 
                            1/9, 1/9, 1/9,
                            1/9, 1/9, 1/9,
                            1/9, 1/9, 1/9
                        ]
                    });
                    break;
                case 'darken':
                    selectedFilter = new f.Tint({
                        color: '#000000',
                        opacity: 0.5
                    });
                    break;
                case 'lighten':
                    selectedFilter = new f.Tint({
                        color: '#FFFFFF',
                        opacity: 0.5
                    });
                    break;
                case 'grain':
                    selectedFilter = new f.Noise({
                        noise: parseInt(200, 10)
                    });
                    break;
            }

            if(selectedFilter) {
                that.applyFilter(selectedFilter);
            }
        };

        this.applyEffectsButtonClickHandler = function() {

            if(that.getModule().getCurrentCardSide() === 'front') {
                that.getModule().getCardDesignModel().getFrontCardSideModel().setBackgroundFilter(selectedFilter);
            }
            else {
                that.getModule().getCardDesignModel().getBackCardSideModel().setBackgroundFilter(selectedFilter);
            }

            
            that.getModule().getBackgroundImageEffectsPopup().panpopup('close');
        };

        this.imageEffectsBackButtonClickHandler = function() {

            that.getModule().getApplyEffectButton().off('click', this.applyEffectsButtonClickHandler);
            that.getModule().getImageEffectsBackButton().off('click', this.imageEffectsBackButtonClickHandler);
            that.getModule().getBackgroundImageEffectsPopup().panpopup('close');
        };
    },

    prototypeMethods: {

        init: function() {

        },

        addEvents: function() {

            this.getModule().getEffectsButtonContainer().on('click', '.effect-button', this.effectButtonClickHandler);
            this.getModule().getBackgroundImageEffectsButton().on('click', this.backgrodundImageEffectButtonClickHandler);
            this.getModule().getApplyEffectButton().on('click', this.applyEffectsButtonClickHandler);
            this.getModule().getImageEffectsBackButton().on('click', this.imageEffectsBackButtonClickHandler);
        },

        removeEvents: function() {
            this.getModule().getEffectsButtonContainer().off('click', '.effect-button', this.effectButtonClickHandler);
            this.getModule().getBackgroundImageEffectsButton().off('click', this.backgrodundImageEffectButtonClickHandler);
            this.getModule().getApplyEffectButton().off('click', this.applyEffectsButtonClickHandler);
            this.getModule().getImageEffectsBackButton().off('click', this.imageEffectsBackButtonClickHandler);
        },
    },

    extendPrototypeFrom: 'Base.Controller'
});