CLASS.define({

    name: 'Controllers.CardDesigner',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);
        this.extendFromClass('Controllers.EventHandlers.CardDesigner');

        var that = this;

        var currentCardDesignSideModel;

        var currentCardDesignEditor;

        var cardSaveProgressInterval;
        var cardSaveProgressLoaderId;
        var cardSaveLoaderNotification;

        var isCardDesignChanged = false;

        this.setIsCardDesignChanged = function(isChanged)  {
            isCardDesignChanged = isChanged;
        };

        this.getIsCardDesignChanged = function() {
            return isCardDesignChanged;
        };

        this.setCurrentCardDesignSideModel = function(cardDesignSideModel) {
            currentCardDesignSideModel = cardDesignSideModel;
        };

        this.getCurrentCardDesignSideModel = function() {
            return currentCardDesignSideModel;
        };

        this.setCurrentCardDesignEditor = function(editor) {
            currentCardDesignEditor = editor;
        };

        this.getCurrentCardDesignEditor = function() {
            return currentCardDesignEditor;
        };

        this.setCardSaveProgressInterval = function(interval) {
            cardSaveProgressInterval = interval;
        };

        this.getCardSaveProgressInterval = function() {
            return cardSaveProgressInterval;
        };

        this.setCardSaveProgressLoaderId = function(loaderId) {
            cardSaveProgressLoaderId = loaderId;
        };

        this.getCardSaveProgressLoaderId = function() {
            return cardSaveProgressLoaderId;
        };

        this.setCardSaveLoaderNotification = function(loaderNotification) {
            cardSaveLoaderNotification = loaderNotification;
        };

        this.getCardSaveLoaderNotification = function() {
            return cardSaveLoaderNotification;
        };
    },

    prototypeMethods: {

        init: function() {

            this.setCurrentCardDesignEditor(this.getModule().getCardFrontSideDesignEditor());
            this.getModule().getModuleElement().find('.color-pallete').hide();
        },

        addEvents: function() {

            var moduleElement = this.getModule().getModuleElement();

            moduleElement.find('.tool-button').on('click', this.toolButtonClickHandler);
            moduleElement.find('.control-button').on('click', this.controlButtonClickHandler);
            moduleElement.find('.browse-file-input').on('change', this.browseFileInputChangeEventHandler);
            moduleElement.find('select').on('change', this.selectInputChangeEventHandler);
            moduleElement.find('.text-color-button-tool').on('click', this.colorPalleteToolButtonClickHandler);
            moduleElement.find('.color-pallete').on('click', 'li', this.colorSelectedEventHandler);

            var constants = this.getNamespaceValue('Static.Constants');
            $(document).on(constants.COLOR_PALLETE_COLOR_CHANGED_EVENT, this.colorPalleteColorChangedHandler);
            $(document).on(constants.FABRIC_IMAGE_LOADED_EVENT, this.fabricImageLoadedEventHandler);

            $(document).on(this.getModule().getCardDesignModel().events.READ_LAST_MODIFIED_COMPLETE, this.readLastModifiedDesignCompleteHandler);
            $(document).on(this.getModule().getCardDesignModel().events.READ_LAST_MODIFIED_ERROR, this.readLastModifiedDesignErrorHandler);
            $(document).on(this.getModule().getCardDesignModel().events.SAVE_CARD_DESIGN_COMPLETE, this.saveCardDesignCompleteEventHandler);

            $(document).on(this.getModule().getCardDesignModel().events.CREATE_COMPLETE, this.cardDesignCreateCompleteEventHandler);

            $(document).on(this.getModule().getFacebookModel().events.PHOTO_SELECTED, this.socialMediaPhotoSelectedEventHandler);
            $(document).on(this.getModule().getFlickrModel().events.PHOTO_SELECTED, this.socialMediaPhotoSelectedEventHandler);

            var frontSideModel = this.getModule().getCardDesignModel().getFrontCardSideModel();

            $(document).on(frontSideModel.events.SAVE_CARD_DESIGN_SIDE_COMPLETE, this.saveCardSideDesignCompleteEventHandler);
            $(document).on(frontSideModel.events.READ_FILE_UPLOAD_PROGRESS_COMPLETE, this.readCardDesignImageUploadProgressCompleteHandler);


            this.getModule().getSaveCardDesignButton().on('click', this.saveCardDesignButtonClickHandler);
            this.getModule().getCardFrontSideSwitchButton().on('click', this.cardSideButtonClickHandler);
            this.getModule().getCardBackSideSwitchButton().on('click', this.cardSideButtonClickHandler);

            this.getModule().getCardFrontSideDesignEditor().on('object:scaling', this.cardDesignEditorObjectScalingEventHandler);
            this.getModule().getCardBackSideDesignEditor().on('object:scaling', this.cardDesignEditorObjectScalingEventHandler);
            this.getModule().getCardFrontSideDesignEditor().on('object:rotating', this.cardDesignEditorObjectRotatingEventHandler);
            this.getModule().getCardBackSideDesignEditor().on('object:rotating', this.cardDesignEditorObjectRotatingEventHandler);
            this.getModule().getCardFrontSideDesignEditor().on('object:removed', this.cardFrontSideElementRemovedEvent);
            this.getModule().getCardBackSideDesignEditor().on('object:removed', this.cardBackSideElementRemovedEvent);
            this.getModule().getCardFrontSideDesignEditor().on('object:selected', this.cardFrontSideElementSelectedEvent);
            this.getModule().getCardBackSideDesignEditor().on('object:selected', this.cardBackSideElementSelectedEvent);
            this.getModule().getCardFrontSideDesignEditor().on('selection:cleared', this.cardFrontSideSelectionClearedEventHandler);
            this.getModule().getCardBackSideDesignEditor().on('selection:cleared', this.cardBackSideSelectionClearedEventHandler);

            this.getModule().getCardFrontSideDesignEditor().on('text:changed', this.cardFrontSideTextChangedEventHandler);
            this.getModule().getCardBackSideDesignEditor().on('text:changed', this.cardBackSideTextChangedEventHandler);
        },

        startController: function() {

            this.setCurrentCardDesignSideModel(this.getModule().getCardDesignModel().getFrontCardSideModel());

            $(this.getModule().getCardFrontSideDesignEditor().wrapperEl).hide();
            $(this.getModule().getCardBackSideDesignEditor().wrapperEl).hide();
        },

        removeEvents: function() {

            var moduleElement = this.getModule().getModuleElement();

            moduleElement.find('.tool-button').on('click', this.toolButtonClickHandler);
            moduleElement.find('.control-button').off('click', this.controlButtonClickHandler);
            moduleElement.find('.browse-file-input').off('change', this.browseFileInputChangeEventHandler);
            moduleElement.find('select').off('change', this.selectInputChangeEventHandler);
            moduleElement.find('.text-color-button-tool').on('click', this.colorPalleteToolButtonClickHandler);
            moduleElement.find('.color-pallete').off('click', 'li', this.colorSelectedEventHandler);

            var constants = that.getNamespaceValue('Static.Constants');
            $(document).off(constants.COLOR_PALLETE_COLOR_CHANGED_EVENT, this.colorPalleteColorChangedHandler);
            $(document).off(constants.FABRIC_IMAGE_LOADED_EVENT, this.fabricImageLoadedEventHandler);

            $(document).off(this.getModule().getCardDesignModel().events.READ_LAST_MODIFIED_COMPLETE, this.readLastModifiedDesignCompleteHandler);
            $(document).off(this.getModule().getCardDesignModel().events.READ_LAST_MODIFIED_ERROR, this.readLastModifiedDesignErrorHandler);
            $(document).off(this.getModule().getCardDesignModel().events.CREATE_COMPLETE, this.cardDesignCreateCompleteEventHandler);

            $(document).off(this.getModule().getFacebookModel().events.PHOTO_SELECTED, this.socialMediaPhotoSelectedEventHandler);
            $(document).off(this.getModule().getFlickrModel().events.PHOTO_SELECTED, this.socialMediaPhotoSelectedEventHandler);

            var frontSideModel = this.getModule().getCardDesignModel().getFrontCardSideModel();

            $(document).off(frofftSideModel.events.SAVE_CARD_DESIGN_SIDE_COMPLETE, this.saveCardSideDesignCompleteEventHandler);
            $(document).off(frontSideModel.events.READ_FILE_UPLOAD_PROGRESS_COMPLETE, this.readCardDesignImageUploadProgressCompleteHandler);


            this.getModule().getSaveCardDesignButton().off('click', this.saveCardDesignButtonClickHandler);
            this.getModule().getCardFrontSideSwitchButton().off('click', this.cardSideButtonClickHandler);
            this.getModule().getCardBackSideSwitchButton().off('click', this.cardSideButtonClickHandler);

            this.getModule().getCardFrontSideDesignEditor().off('object:scaling', this.cardDesignEditorObjectScalingEventHandler);
            this.getModule().getCardBackSideDesignEditor().off('object:scaling', this.cardDesignEditorObjectScalingEventHandler);
            this.getModule().getCardFrontSideDesignEditor().off('object:rotating', this.cardDesignEditorObjectRotatingEventHandler);
            this.getModule().getCardBackSideDesignEditor().off('object:rotating', this.cardDesignEditorObjectRotatingEventHandler);
            this.getModule().getCardFrontSideDesignEditor().off('object:removed', this.cardFrontSideElementRemovedEvent);
            this.getModule().getCardBackSideDesignEditor().off('object:removed', this.cardBackSideElementRemovedEvent);
            this.getModule().getCardFrontSideDesignEditor().off('object:selected', this.cardFrontSideElementSelectedEvent);
            this.getModule().getCardBackSideDesignEditor().off('object:selected', this.cardBackSideElementSelectedEvent);
            this.getModule().getCardFrontSideDesignEditor().off('selection:cleared', this.cardFrontSideSelectionClearedEventHandler);
            this.getModule().getCardBackSideDesignEditor().off('selection:cleared', this.cardBackSideSelectionClearedEventHandler);

            this.getModule().getCardFrontSideDesignEditor().off('text:changed', this.cardFrontSideTextChangedEventHandler);
            this.getModule().getCardBackSideDesignEditor().off('text:changed', this.cardBackSideTextChangedEventHandler);
        },

        changeCardSide: function(side) {

            this.getModule().getDesignElementsCloseButtonsContainer().children().hide();

            if(side === 'front') {

                this.setCurrentCardDesignSideModel(this.getModule().getCardDesignModel().getFrontCardSideModel());
                this.setCurrentCardDesignEditor(this.getModule().getCardFrontSideDesignEditor());

                this.getModule().getCardFrontSideSwitchButton().addClass('active');
                this.getModule().getCardBackSideSwitchButton().removeClass('active');
                /*
                $(this.getModule().getCardBackSideDesignEditor().wrapperEl).removeClass('flipInY');
                $(this.getModule().getCardBackSideDesignEditor().wrapperEl).addClass('animated flipOutY').hide();

                $(this.getModule().getCardFrontSideDesignEditor().wrapperEl).show(1).removeClass('flipOutY');
                $(this.getModule().getCardFrontSideDesignEditor().wrapperEl).addClass('animated flipInY');
                */
                $(this.getModule().getCardBackSideDesignEditor().wrapperEl).fadeOut(100);
                
                    $(this.getModule().getCardFrontSideDesignEditor().wrapperEl).fadeIn(100);
                
            }
            else {

                this.setCurrentCardDesignSideModel(this.getModule().getCardDesignModel().getBackCardSideModel());
                this.setCurrentCardDesignEditor(this.getModule().getCardBackSideDesignEditor());

                this.getModule().getCardBackSideSwitchButton().addClass('active');
                this.getModule().getCardFrontSideSwitchButton().removeClass('active');
                /*
                $(this.getModule().getCardFrontSideDesignEditor().wrapperEl).removeClass('flipInY');
                $(this.getModule().getCardFrontSideDesignEditor().wrapperEl).addClass('flipOutY').hide();

                $(this.getModule().getCardBackSideDesignEditor().wrapperEl).show(1).removeClass('flipOutY');
                $(this.getModule().getCardBackSideDesignEditor().wrapperEl).addClass('flipInY');
                */
                $(this.getModule().getCardFrontSideDesignEditor().wrapperEl).fadeOut(100);
                
                    $(this.getModule().getCardBackSideDesignEditor().wrapperEl).fadeIn(100);
                
            }
        },

        addImageObject: function(fileUrl, type, options) {

            var that = this;

            var elementId = this.generateElementId('image');

            var localOptions = {
                id: elementId,
                elementType: type,
                left: 0,
                top: 0,
                angle: 0,
                perPixelTargetFind: false,
                targetFindTolerance: 8,
                hasControls: true,
            };

            var argOptions = options || {};
            var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectDefaultParams');

            var params = $.extend(localOptions, fabricObjectParams, argOptions);

            var func = function(params, image){
                console.log(this);
                console.log(params);
                console.log(image);
            };

            var imageObj = new fabric.Image.fromURL(fileUrl, this.fabricImageLoadedCallback.bind(this, params),
            {
                crossOrigin: 'anonymous'
            });
        },

        addTextField: function(text, id, options) {

            var elementId = this.generateElementId('text');
            var argOptions = options || {};

            var localOptions = {
                id: elementId,
                elementType: 'text-field',
                left: 50,
                top: 50,
                width: 260,
                fill: 'rgb(0,0,0)',
                fontFamily: 'Verdana',
            };

            var fabricObjectParams = this.getApp().getConfig().getParameter('fabricOjbectDefaultParams');
            var params = $.extend(localOptions, fabricObjectParams, argOptions);
            var textField = new fabric.Textbox(text, params);

            this.getCurrentCardDesignEditor().add(textField);

            this.getCurrentCardDesignSideModel().addCardDesignElement(textField, elementId);

            this.initFabricElement(textField);

            this.getModule().createDesignElementCloseButton(textField, this.getModule().getCurrentCardSide());
            this.positionDesignElementCloseButton(textField);

            return textField;
        },

        generateElementId: function(prefix) {
            return prefix + '-' + new Date().getTime();
        },

        getActiveElement: function() {
            return this.getCurrentCardDesignEditor().getActiveObject();
        },

        positionDesignElementCloseButton: function(element) {

            var absCoords = this.getCurrentCardDesignEditor().getAbsoluteCoords(element);
            var button = this.getModule().getDesignElementsCloseButtonsContainer().find('#' + element.id);

            var centerLeftPosition = (absCoords.left - button.width() / 2);
            var centerTopPosition = (absCoords.top - button.height() / 2);

            var left = centerLeftPosition + element.getWidth() - button.width() - 10 + 'px';
            var top = centerTopPosition - button.height() + 30 + 'px';

            button.css('left', left);
            button.css('top', top);
        },

        removeDesignElement: function(elementId) {

            var element = this.getCurrentCardDesignSideModel().getCardDesignElement(elementId);

            this.getCurrentCardDesignEditor().remove(element);
            this.getCurrentCardDesignEditor().renderAll();

            this.getModule().getModuleElement().find('#' + elementId).remove();
        },

        changeTextFieldColor: function(textField, color) {

            if(this.isDesignElementTextField(this.getActiveElement())) {
                textField.setColor(color);
                this.getCurrentCardDesignEditor().renderAll();
            }
            else {
                this.throwException('Invalid text field', 1500, 'controller');
            }
        },

        changeTextFieldFontSize: function(textField, fontSize) {

            if(this.isDesignElementTextField(this.getActiveElement())) {

                if(this.getCurrentCardDesignSideModel().getCardSide() === 'back') {

                    textField.setFontSize(fontSize);

                    this.autoFitBackSideText(textField);
                }
                else {
                    textField.setFontSize(fontSize);
                }

                this.getCurrentCardDesignEditor().renderAll();
            }
            else {
                this.throwException('Invalid text field', 1500, 'controller');
            }
        },

        changeTextFieldFont: function(textField, font) {

            if(this.isDesignElementTextField(this.getActiveElement())) {
                textField.setFontFamily(font);
                this.getCurrentCardDesignEditor().renderAll();
            }
            else {
                this.throwException('Invalid text field', 1500, 'controller');
            }
        },

        isDesignElementTextField: function(element) {

            var designElementType = element.type;

            if(designElementType === 'i-text' || designElementType === 'text' || designElementType === 'textbox' || element.name === 'disclaimer-text') {
                return true;
            }
            else {
                return false;
            }
        },

        saveCardDesign: function(saveAsNew) {

            this.saveCardFrontDesign(saveAsNew);
            this.startSaveCardProgress(saveAsNew);
        },

        saveCardFrontDesign: function(saveAsNew) {

            this.getModule().getCardFrontSideDesignEditor().deactivateAll();

            var fabricExportMultiply = this.getApp().getConfig().getParameter('fabricExportMultiply');
            var imgFrontBase64 = this.getModule().getCardFrontSideDesignEditor().toDataURLWithMultiplier('image/png', fabricExportMultiply, 1);
            var imageFrontBlob = this.getModule().getCardDesignModel().dataURItoBlob(imgFrontBase64);
            var designJsonData = this.getModule().getCardFrontSideDesignEditor().toJSON();

            this.getModule().getCardDesignModel().saveCardFrontDesign(imageFrontBlob, designJsonData, saveAsNew);
        },

        saveCardBackDesign: function(saveAsNew) {

            this.getModule().getCardBackSideDesignEditor().deactivateAll();

            var fabricExportMultiply = this.getApp().getConfig().getParameter('fabricExportMultiply');
            var imgBackBase64 = this.getModule().getCardBackSideDesignEditor().toDataURLWithMultiplier('image/png', fabricExportMultiply, 1);
            var imageBackBlob = this.getModule().getCardDesignModel().dataURItoBlob(imgBackBase64);
            var designJsonData = this.getModule().getCardBackSideDesignEditor().toJSON();

            this.getModule().getCardDesignModel().saveCardBackDesign(imageBackBlob, designJsonData, saveAsNew);
        },

        startSaveCardProgress: function() {

            var intervalTime = this.getApp().getConfig().getParameter('fileUploadProgressInterval');
            this.setCardSaveProgressLoaderId(this.getModule().getCardDesignModel().generateUploadId());

            var notification = this.getApp().showNotification('loader', 'We are saving your design...', this.getCardSaveProgressLoaderId());
            this.setCardSaveLoaderNotification(notification);

            this.setCardSaveProgressInterval(setInterval(
                this.imageUploadProgressIntervalCallback.bind(this),
                intervalTime
            ));
        },

        imageUploadProgressIntervalCallback: function() {

            var frontSideModel = this.getModule().getCardDesignModel().getFrontCardSideModel();
            var backSideModel = this.getModule().getCardDesignModel().getBackCardSideModel();

            var frontSideUploadItems = frontSideModel.getSaveCardDesignUploadIds();
            var backSideUploadItems = backSideModel.getSaveCardDesignUploadIds();

            var uploadItem;
            var timeoutCallback;

            if(frontSideModel.getCardDesignUploadStatus() === frontSideModel.CARD_UPLOAD_STATUS_UPLOADING_DESIGN_ELEMENT ||
               frontSideModel.getCardDesignUploadStatus() === frontSideModel.CARD_UPLOAD_STATUS_UPLOADING_DESIGN_IMAGE ) {

                if(frontSideUploadItems.length > 0) {

                    for(var i in frontSideUploadItems) {

                        uploadItem = frontSideUploadItems[i];
                        timeoutCallback = (function(index) {
                            return function(){
                                frontSideModel.readFileUploadProgress(frontSideUploadItems[index]);
                            };
                        }(i));

                        setTimeout(timeoutCallback, 150);
                    }
                }
            }
            else if (backSideModel.getCardDesignUploadStatus() === backSideModel.CARD_UPLOAD_STATUS_UPLOADING_DESIGN_ELEMENT ||
                     backSideModel.getCardDesignUploadStatus() === backSideModel.CARD_UPLOAD_STATUS_UPLOADING_DESIGN_IMAGE) {
                if(backSideUploadItems.length > 0) {
                    for(var i in backSideUploadItems) {

                        uploadItem = backSideUploadItems[i];
                        timeoutCallback = (function(index) {
                            return function(){
                                backSideModel.readFileUploadProgress(backSideUploadItems[index]);
                            }
                        }(i));

                        setTimeout(timeoutCallback, 150);
                    }
                }
            }
        },

        stopSaveCardProgress: function() {

            clearInterval(this.getCardSaveProgressInterval());

            this.getApp().updateLoaderNotificationProgress(
                this.getCardSaveProgressLoaderId(),
                100
            );

            var that = this;

            setTimeout(function(){
                that.getCardSaveLoaderNotification().close();
            }, 3000);
        },

        extendObjectsSerializationFunction: function(element) {

            var that = this;

            element.toObject = (function(toObject, elementType) {

                return function() {

                    var serializationProperties = that.getApp().getConfig().getParameter('fabricGlobalSerializationPropertyName');

                    if(elementType === 'image') {
                        var imageSerializationProperties = that.getApp().getConfig().getParameter('fabricImageSerializationPropertyName');
                        serializationProperties = serializationProperties.concat(imageSerializationProperties);
                    }

                    var serializationPropertiesObject = {};

                    for(var i in serializationProperties) {
                        serializationPropertiesObject[serializationProperties[i]] = this[serializationProperties[i]];
                    }

                    return fabric.util.object.extend(toObject.call(this), serializationPropertiesObject);
              };
            })(element.toObject, element.type);
        },

        fabricImageLoadedCallback: function(params, image) {

            image.set(params);

            this.getCurrentCardDesignEditor().add(image);
            this.getCurrentCardDesignSideModel().addCardDesignElement(image, params.id);

            this.initFabricElement(image);

            var constants = this.getNamespaceValue('Static.Constants');

            var e = $.Event(constants.FABRIC_IMAGE_LOADED_EVENT, {image:image});
            $(document).trigger(e);
        },

        designEditorLoadFromJsonCallback: function(designEditor, cardSide) {

            designEditor.calcOffset();

            var objects = designEditor.getObjects();
            var that = this;

            var element;

            for(var i = 0, len = objects.length; i < len; i++) {

                element = objects[i];

                this.initFabricElement(element);

                that.getModule().createDesignElementCloseButton(element, cardSide);
                that.positionDesignElementCloseButton(element);
            }

            designEditor.renderAll();
        },

        designElementLoadedCallback: function(cardSide, element, elementInstance) {},

        initFabricElement: function(element) {

            this.extendObjectsSerializationFunction(element);
            element.on('moving', this.designElementMovingEventHandler);
            this.getCurrentCardDesignEditor().setActiveObject(element);
        },

        getCardSideModelFromSide: function(cardSide) {
            if(cardSide === 'front') {
                return this.getModule().getCardDesignModel().getFrontCardSideModel();
            }
            else {
                return this.getModule().getCardDesignModel().getBackCardSideModel();
            }
        },

        getDesignEditorFromSide: function(cardSide) {
            if(cardSide === 'front') {
                return this.getModule().getCardFrontSideDesignEditor();
            }
            else {
                return this.getModule().getCardBackSideDesignEditor();
            }
        }
    },

    extendPrototypeFrom: 'Base.Controller'

});
