var CLASS = (function(){

    var namespaceHolder = {};
    var classDefinitions = {};

    var singletonInstances = {};

    var superMethods = {};
    var classMethods = {};

    var fn = function(){};

    fn.prototype = {

        namespace: '',

        getDefinition: function() {
            return this.getClassDefinition(this.getNamespace());
        },

        setClassDefinition: function(className, def) {
            classDefinitions[className] = def;
        },

        getClassDefinition: function(className) {
            return classDefinitions[className];
        },

        getClassMethods: function(className) {
            return classMethods[className];
        },

        addClassMethod: function(className, methodName, method) {

            if(classMethods[className]) {
                classMethods[className][methodName] = method;
            }
        },

        setNamespace: function(ns) {
            this.namespace = ns;
        },

        getNamespace: function() {
            return this.namespace;
        },

        setClassMethods: function(className, methods) {
            classMethods[className] = methods;
        },

        getSuperMethods:  function() {
            return superMethods;
        },

        createNamespace: function (namespaceName, namespaceValue, namespaceObject) {

            var namespaceParts = namespaceName.split('.');

            var namespace = namespaceObject || namespaceHolder;

            for (var i = 0, len = namespaceParts.length, part; i < len; i += 1) {
                
                part = namespaceParts[i];

                // create a property if it doesn't exist
                if (typeof namespace[part] === "undefined") {

                    if(i === len-1) {
                        namespace[part] = namespaceValue;
                    }
                    else {
                        namespace[part] = {};
                        namespace = namespace[part];
                    }
                }
                else {
                    namespace = namespace[part];
                }
            }
        },

        getNamespaceValue: function(namespaceName, namespaceObject) {

            var namespaceParts = namespaceName.split('.');
            var namespace = namespaceObject || namespaceHolder;

            for (var i = 0, len = namespaceParts.length, part; i < len; i += 1) {

                part = namespaceParts[i];

                if(namespace[part]) {
                    namespace = namespace[part];    
                }
                else {
                    console.log("Namespace part '" + part + "' does not exists for namespace: " + namespaceName)
                    this.throwException("Namespace part '" + part + "' does not exists", 1500, 'kernel');
                }
            }

            return namespace;
        },

        define: function(classData) {
            
            if(this.isString(classData.name) !== true || classData.name.length < 1) {
                this.throwException('Class must have a name.', 1500, 'kernel');
            }
            else if(this.isFunction(classData.definition) !== true) {
                this.throwException('Class must have a constructor definition.', 1500, 'kernel');
            }

            classData.configured = false;

            this.setClassDefinition(classData.name, classData);
        },

        configure: function(className) {

            var classData = this.getClassDefinition(className);

            this.createNamespace(classData.name, classData.definition);

            this.extendPrototype(classData.definition, fn);

            if(this.isObject(classData.prototypeMethods)) {

                var pmKeys = Object.keys(classData.prototypeMethods);
                var methods = this.getClassMethods(classData.name) || {};

                for(var pm in pmKeys) {
                    methods[pmKeys[pm]] = classData.prototypeMethods[pmKeys[pm]];
                }

                this.setClassMethods(classData.name, methods);
            }

            if(classData.extendPrototypeFrom) {

                if(this.isArray(classData.extendPrototypeFrom)) {

                    for(var c in classData.extendPrototypeFrom) {

                        if(this.isString(classData.extendPrototypeFrom[c])) {
                            this.extendPrototypeFromClassName(classData.definition, classData.extendPrototypeFrom[c]);
                        }
                        else {

                            this.extendPrototype(classData.definition, classData.extendPrototypeFrom[c]);
                        }
                    }
                }
                else if(this.isObject(classData.extendPrototypeFrom)) {
                    this.extendPrototype(classData.definition, classData.extendPrototypeFrom);
                }
                else if(this.isString(classData.extendPrototypeFrom)) {
                    this.extendPrototypeFromClassName(classData.definition, classData.extendPrototypeFrom);
                }
            }

            if(this.isObject(classData.prototypeMethods)) {
                this.extendPrototype(classData.definition, classData.prototypeMethods);
            }

            classData.definition.prototype.setNamespace(classData.name);

            classData.configured = true;

            this.setClassDefinition(classData.name, classData);

            return classData;
        },

        get: function(className) {

            try{
                var classData = this.getClassDefinition(className);

                if(classData.configured === false) {                    
                    classData = this.configure(className);
                }

                return this.getNamespaceValue(className);
            }
            catch(error) {

                console.log("Classname requested: " + className);

                if(error.hasOwnProperty('getMessage')) {
                    console.log(error.getMessage());
                }
                else {
                    console.log(error);
                }
            }
        },

        getInstance: function(className, arguments, singleton) {

            var classDefinition = this.get(className);

            if(this.isArray(arguments) && arguments.length > 0) {
                arguments.splice(0, 0, classDefinition);
            }

            // sets singleton default value to true
            singleton = singleton !== false;

            if(singleton === true) {

                if(singletonInstances[className]) {
                    return singletonInstances[className];
                }

                try {
                    singletonInstances[className] = new (Function.prototype.bind.apply(classDefinition, arguments));
                    return singletonInstances[className];
                }
                catch(error) {
                    console.log(error);
                    this.throwException("Instance of class '" + className + "' could not be created.", 1500, 'kernel');
                }
            }
            else {

                try {
                    var instance = new (Function.prototype.bind.apply(classDefinition, arguments));
                    return instance;
                }
                catch(error) {
                    console.log(error);
                    this.throwException("Instance of class '" + className + "' could not be created.", 1500, 'kernel');
                }
            }
        },

        extendFromClass: function(parentClass, arguments) {

            try {

                var classData = this.getClassDefinition(parentClass);

                if(classData.configured === false) {                    
                    classData = this.configure(parentClass);
                }

                var parentClassDef = this.isString(parentClass) ? this.get(parentClass) : parentClass;
                    parentClassDef.apply(this, arguments);
            }
            catch(error) {
                console.log("Tried to extend from parent class: " + parentClass);
                console.log(error);
            }
        },

        callSuper: function(className, methodName, arguments) {
            
            var classMethods = this.getClassMethods(className);

            if(classMethods && classMethods.hasOwnProperty(methodName)) {
                return classMethods[methodName].apply(this, arguments);   
            }
            else {
                this.throwException('Super method: "' + methodName + '" does not exists.', 1500, 'kernel');
            }
        },

        throwException: function(message, code, type) {
            throw this.getInstance('Base.Exception', [message, code]);
        },

        extendPrototypeFromClassName: function(newClass, baseClassName) {

            var prototypeClass = this.get(baseClassName);

            if(this.isFunction(prototypeClass) === true) {
                this.extendPrototype(newClass, prototypeClass);
            }
            else {
                this.throwException('Invalid class name "' + baseClassName + '" of prototype class definition.', 1500, 'kernel');
            }
        },

        extendPrototype: function(newClass, baseClass) {

            if(!newClass || !newClass.prototype) {
                this.throwException('Invalid new class definition at CLASS::extendPrototype', 1500, 'kernel');
            }
            else if(this.isFunction(baseClass) !== true && this.isObject(baseClass) !== true) {
                this.throwException('Invalid base class definition at CLASS::extendPrototype', 1500, 'kernel');
            }


            if(this.isFunction(baseClass)) {                
                newClass.prototype = Object.create(baseClass.prototype);
            }
            else {

                for(var p in baseClass) {
                    newClass.prototype[p] = baseClass[p];
                }
            }
        },

        addToPrototype: function(originalClass, prototypeData) {
            for(var p in prototypeData) {
              originalClass.prototype[p] = prototypeData[p];
            }
        },

        isArray: function(arrayVar) {
            return Object.prototype.toString.call(arrayVar) === '[object Array]';
        },

        isDate: function(date) {
            return Object.prototype.toString.call(date) === '[object Date]';
        },

        isString: function(string) {
            return Object.prototype.toString.call(string) === '[object String]';
        },

        isObject: function(object) {
            return Object.prototype.toString.call(object) === '[object Object]';
        },

        isFunction: function(functionDefintion) {
            return Object.prototype.toString.call(functionDefintion) === '[object Function]';
        }
    };

    return new fn();

}());