CLASS.createNamespace('Events.Model.ShoppingCart', {

    VALIDATION_ERROR: 'Model.ShoppingCart.validationError',

    CREATE_COMPLETE: 'Model.ShoppingCart.createComplete',
    CREATE_ERROR: 'Model.ShoppingCart.createError',

    READ_COMPLETE: 'Model.ShoppingCart.readComplete',
    READ_ERROR: 'Model.ShoppingCart.readError',

    READ_ALL_COMPLETE: 'Model.ShoppingCart.readAllComplete',
    READ_ALL_ERROR: 'Model.ShoppingCart.readAllError',

    FIND_BY_ID_COMPLETE: 'Model.ShoppingCart.findByIdComplete',
    FIND_BY_ID_ERROR: 'Model.ShoppingCart.findByIdError',

    UPDATE_COMPLETE: 'Model.ShoppingCart.updateComplete',
    UPDATE_ERROR: 'Model.ShoppingCart.updateError',

    PATCH_COMPLETE: 'Model.ShoppingCart.patchComplete',
    PATCH_ERROR: 'Model.ShoppingCart.patchError',

    DELETE_COMPLETE: 'Model.ShoppingCart.deleteComplete',
    DELETE_ERROR: 'Model.ShoppingCart.deleteError',
});
