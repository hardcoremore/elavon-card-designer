CLASS.createNamespace('Events.Model.CardDesign', {

    VALIDATION_ERROR: 'Model.CardDesign.validationError',

    CREATE_COMPLETE: 'Model.CardDesign.createComplete',
    CREATE_ERROR: 'Model.CardDesign.createError',

    FIND_BY_ID_COMPLETE: 'Model.CardDesign.findByIdComplete',
    FIND_BY_ID_ERROR: 'Model.CardDesign.findByIdError',

    READ_LAST_MODIFIED_COMPLETE: 'Model.CardDesign.readLastModifiedComplete',
    READ_LAST_MODIFIED_ERROR: 'Model.CardDesign.readLastModifiedError',

    SAVE_CARD_DESIGN_COMPLETE: 'Model.CardDesign.saveCardCardDesignComplete',
    SAVE_CARD_DESIGN_ERROR: 'Model.CardDesign.saveCardCardDesignError',

    READ_COMPLETE: 'Model.CardDesign.readComplete',
    READ_ERROR: 'Model.CardDesign.readError',

    READ_RECENT_DESIGNS_COMPLETE: 'Model.CardDesign.readRecentDesignsComplete',
    READ_RECENT_DESIGNS_ERROR: 'Model.CardDesign.readRecentDesignsError',
    
    READ_ALL_COMPLETE: 'Model.CardDesign.readAllComplete',

    DELETE_COMPLETE: 'Model.CardDesign.deleteComplete',
    DELETE_ERROR: 'Model.CardDesign.deleteError'
});