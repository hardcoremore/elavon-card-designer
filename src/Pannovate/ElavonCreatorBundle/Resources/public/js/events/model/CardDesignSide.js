CLASS.createNamespace('Events.Model.CardDesignSide', {

    VALIDATION_ERROR: 'Model.CardDesignSide.validationError',

    CREATE_COMPLETE: 'Model.CardDesignSide.createComplete',
    CREATE_ERROR: 'Model.CardDesignSide.createError',

    FIND_BY_ID_COMPLETE: 'Model.CardDesignSide.findByIdComplete',
    FIND_BY_ID_ERROR: 'Model.CardDesignSide.findByIdError',

    READ_LAST_MODIFIED_COMPLETE: 'Model.CardDesignSide.readLastModifiedComplete',
    READ_LAST_MODIFIED_ERROR: 'Model.CardDesignSide.readLastModifiedError',

    UPLOAD_FILE_COMPLETE: 'Model.CardDesignSide.uploadFileComplete',
    UPLOAD_FILE_ERROR: 'Model.CardDesignSide.uploadFileError',

    ALL_DESIGN_ELEMENTS_UPLOADED_COMPLETE: 'Model.CardDesignSide.allCardDesignSideUploadElementsUploadComplete',

    SAVE_CARD_DESIGN_SIDE_COMPLETE: 'Model.CardDesignSide.saveCardCardDesignSideComplete',
    SAVE_CARD_DESIGN_SIDE_ERROR: 'Model.CardDesignSide.saveCardCardDesignSideError',

    SAVE_IMAGE_FROM_URL_COMPLETE: 'Model.CardDesignSide.saveImageFromUrlComplete',
    SAVE_IMAGE_FROM_URL_ERROR: 'Model.CardDesignSide.saveImageFromUrlError',

    READ_FILE_UPLOAD_PROGRESS_COMPLETE: 'Model.CardDesignSide.readFileUploadProgressComplete',
    READ_FILE_UPLOAD_PROGRESS_ERROR: 'Model.CardDesignSide.readFileUploadProgressError',

    READ_COMPLETE: 'Model.CardDesignSide.readComplete',
    READ_ERROR: 'Model.CardDesignSide.readError',

    BACKGROUND_IMAGE_FILTER_SELECTED: 'Model.CardDesignSide.backgroundImageFilterSelected'
});