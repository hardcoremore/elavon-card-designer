CLASS.createNamespace('Events.Model.Country', {

    VALIDATION_ERROR: 'Model.Country.validationError',

    CREATE_COMPLETE: 'Model.Country.createComplete',
    CREATE_ERROR: 'Model.Country.createError',

    READ_COMPLETE: 'Model.Country.readComplete',
    READ_ERROR: 'Model.Country.readError',

    UPDATE_COMPLETE: 'Model.Country.updateComplete',
    UPDATE_ERROR: 'Model.Country.updateError',

    PATCH_COMPLETE: 'Model.Country.patchComplete',
    PATCH_ERROR: 'Model.Country.patchError',

    DELETE_COMPLETE: 'Model.Country.deleteComplete',
    DELETE_ERROR: 'Model.Country.deleteError',

    READ_FOR_SELECT_COMPLETE: 'Model.Country.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Model.Country.readForSelectError'
});
