CLASS.createNamespace('Events.Model.BillingAddress', {

    VALIDATION_ERROR: 'Model.BillingAddress.validationError',

    CREATE_COMPLETE: 'Model.BillingAddress.createComplete',
    CREATE_ERROR: 'Model.BillingAddress.createError',

    READ_COMPLETE: 'Model.BillingAddress.readComplete',
    READ_ERROR: 'Model.BillingAddress.readError',

    READ_ALL_COMPLETE: 'Model.BillingAddress.readAllComplete',
    READ_ALL_ERROR: 'Model.BillingAddress.readAllError',

    UPDATE_COMPLETE: 'Model.BillingAddress.updateComplete',
    UPDATE_ERROR: 'Model.BillingAddress.updateError',

    PATCH_COMPLETE: 'Model.BillingAddress.patchComplete',
    PATCH_ERROR: 'Model.BillingAddress.patchError',

    DELETE_COMPLETE: 'Model.BillingAddress.deleteComplete',
    DELETE_ERROR: 'Model.BillingAddress.deleteError',
});
