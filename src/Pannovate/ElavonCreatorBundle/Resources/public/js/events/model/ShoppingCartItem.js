CLASS.createNamespace('Events.Model.ShoppingCartItem', {

    VALIDATION_ERROR: 'Model.ShoppingCartItem.validationError',

    CREATE_COMPLETE: 'Model.ShoppingCartItem.createComplete',
    CREATE_ERROR: 'Model.ShoppingCartItem.createError',

    READ_COMPLETE: 'Model.ShoppingCartItem.readComplete',
    READ_ERROR: 'Model.ShoppingCartItem.readError',

    READ_ALL_COMPLETE: 'Model.ShoppingCartItem.readAllComplete',
    READ_ALL_ERROR: 'Model.ShoppingCartItem.readAllError',

    UPDATE_COMPLETE: 'Model.ShoppingCartItem.updateComplete',
    UPDATE_ERROR: 'Model.ShoppingCartItem.updateError',

    PATCH_COMPLETE: 'Model.ShoppingCartItem.patchComplete',
    PATCH_ERROR: 'Model.ShoppingCartItem.patchError',

    DELETE_COMPLETE: 'Model.ShoppingCartItem.deleteComplete',
    DELETE_ERROR: 'Model.ShoppingCartItem.deleteError',

    READ_ALL_FOR_SHOPPING_CART_COMPLETE: 'Model.ShoppingCartItem.readAllForShoppingCartComplete',
    READ_ALL_FOR_SHOPPING_CART_ERROR: 'Model.ShoppingCartItem.readAllForShoppingCartError',

    ITEM_UPDATED: 'Model.ShoppingCartItem.itemUpdated'
});
