CLASS.createNamespace('Events.Model.CardDesignOrder', {

    VALIDATION_ERROR: 'Model.CardDesignOrder.validationError',

    CREATE_COMPLETE: 'Model.CardDesignOrder.createComplete',
    CREATE_ERROR: 'Model.CardDesignOrder.createError',

    FIND_BY_ID_COMPLETE: 'Model.CardDesignOrder.findByIdComplete',
    FIND_BY_ID_ERROR: 'Model.CardDesignOrder.findByIdError',

    READ_LAST_MODIFIED_COMPLETE: 'Model.CardDesignOrder.readLastModifiedComplete',
    READ_LAST_MODIFIED_ERROR: 'Model.CardDesignOrder.readLastModifiedError',

    READ_COMPLETE: 'Model.CardDesignOrder.readComplete',
    READ_ERROR: 'Model.CardDesignOrder.readError',

    READ_RECENT_CARD_DESIGN_ORDERS_COMPLETE: 'Model.CardDesignOrder.readRecentCardDesignOrdersComplete',
    READ_RECENT_CARD_DESIGN_ORDERS_ERROR: 'Model.CardDesignOrder.readRecentCardDesignOrdersError',
    
    READ_ALL_COMPLETE: 'Model.CardDesignOrder.readAllComplete',

    DELETE_COMPLETE: 'Model.CardDesignOrder.deleteComplete',
    DELETE_ERROR: 'Model.CardDesignOrder.deleteError'
});