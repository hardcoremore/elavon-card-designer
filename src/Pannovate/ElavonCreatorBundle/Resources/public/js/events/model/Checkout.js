CLASS.createNamespace('Events.Model.Checkout', {

    READ_PAYMENT_PARAMS_COMPLETE: 'Model.Checkout.getPaymentParamsComplete',
    READ_PAYMENT_PARAMS_ERROR: 'Model.Checkout.getPaymentParamsError',
    
    PAYMENT_RESPONSE_PARAMS_COMPLETE: 'Model.Checkout.readPaymenResponseParamsComplete',
    PAYMENT_RESPONSE_PARAMS_ERROR: 'Model.Checkout.readPaymentResponseParamsError',

});