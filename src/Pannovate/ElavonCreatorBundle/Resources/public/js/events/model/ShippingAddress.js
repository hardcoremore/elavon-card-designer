CLASS.createNamespace('Events.Model.ShippingAddress', {

    VALIDATION_ERROR: 'Model.ShippingAddress.validationError',

    CREATE_COMPLETE: 'Model.ShippingAddress.createComplete',
    CREATE_ERROR: 'Model.ShippingAddress.createError',

    READ_COMPLETE: 'Model.ShippingAddress.readComplete',
    READ_ERROR: 'Model.ShippingAddress.readError',

    READ_ALL_COMPLETE: 'Model.ShippingAddress.readAllComplete',
    READ_ALL_ERROR: 'Model.ShippingAddress.readAllError',

    UPDATE_COMPLETE: 'Model.ShippingAddress.updateComplete',
    UPDATE_ERROR: 'Model.ShippingAddress.updateError',

    PATCH_COMPLETE: 'Model.ShippingAddress.patchComplete',
    PATCH_ERROR: 'Model.ShippingAddress.patchError',

    DELETE_COMPLETE: 'Model.ShippingAddress.deleteComplete',
    DELETE_ERROR: 'Model.ShippingAddress.deleteError',
});
