CLASS.define({
    
    name: 'App.ElavonClient',

    definition: function(c) {

        this.extendFromClass('App.LoginApplication', [c]);

        var that = this;

        var selectCurrencyList;
        var cartHeaderContainer;

        var shoppingCartModel;
        var shoppingCartItemModel;
        var currencyModel;
        var cardPricingModel;

        this.setShoppingCartModel = function(model) {
            shoppingCartModel = model;
        };

        this.getShoppingCartModel = function() {
            return shoppingCartModel;
        };

        this.setShoppingCartItemModel = function(model) {
            shoppingCartItemModel = model;
        };

        this.getShoppingCartItemModel = function() {
            return shoppingCartItemModel;
        };

        this.setCurrencyModel = function(model) {
            currencyModel = model;
        };

        this.getCurrencyModel = function() {
            return currencyModel;
        };

        this.setCardPricingModel = function(model) {
            cardPricingModel = model;
        };

        this.getCardPricingModel = function() {
            return cardPricingModel;
        };

        this.setSelectCurrencyList = function(list) {
            selectCurrencyList = list;
        };

        this.getSelectCurrencyList = function() {
            return selectCurrencyList;
        };

        this.setCartHeaderContainer = function(cartHeader) {
            cartHeaderContainer = cartHeader;
        };

        this.getCartHeaderContainer = function() {
            return cartHeaderContainer;
        };

        this.currencyReadAllCompleteHandler = function(ev) {
            that.createCurrencyHeaderItems();
            that.getCardPricingModel().readAll({scope:that});
        };

        this.currencyReadAllErrorHandler = function(ev) {};

        this.authenticationCompleteEventHandler = function(ev) {

            if(ev.user.shoppingCart) {
                that.getCurrencyModel().readAll({scope:that}); // previous startApp();
            }
            else {
                that.getShoppingCartModel().create({user:ev.user.id}, {scope:that});
            }
        };

        this.createShoppingCartCompleteEventHandler = function(ev) {
            that.getUserModel().getCurrentUser().shoppingCart = ev.responseData;
            that.getCurrencyModel().readAll({scope:that}); // previous startApp();
        };

        this.createShoppingCartItemCompleteEventHandler = function(ev) {
            that.getModuleController().loadModule('Modules.ShoppingCart', ev.responseData, true);
        };

        this.createShoppingCartItemErrorEventHandler = function(ev) {
            if(ev.code !== 400) {
                that.showNotification('error', 'Adding card design to shoping cart failed :(');
            }
        };

        this.readCardPricingErrorHandler = function() {
            that.showNotification('error', 'Unable to load card prices. Card checkout  will be disabled!');
        };

        this.readCardPricingCompleteHandler = function(ev) {
            that.startApp();
        };

        this.changeCurrencyClickedHandler = function(ev) {

            var clickedCurrency = $(ev.target).parent();
            var currencySymbol = $(ev.target).parent().data('symbol');

            if(!clickedCurrency.hasClass('active-currency')) {
                clickedCurrency.addClass('active-currency');
                clickedCurrency.siblings().removeClass('active-currency');
                that.getCartHeaderContainer().find('.currency-symbol').text(currencySymbol);

                that.getUserModel().getCurrentUser().currency.id = clickedCurrency.data('id');
            }
        };

        this.shoppingCartItemUpdatedEventHandler = function(ev) {};
    },

    prototypeMethods: {

        init: function() {

            this.callSuper('App.LoginApplication', 'init');

            this.setSelectCurrencyList($("#select-currency-list"));
            this.setCartHeaderContainer($("#cart-header-container"));

            this.getUserModel().setFanfareId($('body').first().data('user-id'));

            this.setShoppingCartModel(this.getModel('Model.ShoppingCart'));
            this.setShoppingCartItemModel(this.getModel('Model.ShoppingCartItem'));
            this.setCurrencyModel(this.getModel('Model.Currency'));
            this.setCardPricingModel(this.getModel('Model.CardPricing'));

            this.getShoppingCartItemModel().setCardPricingModel(this.getCardPricingModel());
            this.getShoppingCartItemModel().setUserModel(this.getUserModel());

            this.setAuthenticationController(this.getInstance('Controllers.Authentication', [this]));
            this.getAuthenticationController().init();

            $(document).on(this.getCardPricingModel().events.READ_ALL_COMPLETE, this.readCardPricingCompleteHandler);
            $(document).on(this.getCardPricingModel().events.READ_ALL_ERROR, this.readCardPricingErrorHandler);

            $(document).on(this.getShoppingCartModel().events.CREATE_COMPLETE, this.createShoppingCartCompleteEventHandler);

            $(document).on(this.getShoppingCartItemModel().events.CREATE_COMPLETE, this.createShoppingCartItemCompleteEventHandler);
            $(document).on(this.getShoppingCartItemModel().events.CREATE_ERROR, this.createShoppingCartItemErrorEventHandler);
            $(document).on(this.getShoppingCartItemModel().events.ITEM_UPDATED, this.shoppingCartItemUpdatedEventHandler);

            $(document).on(this.getCurrencyModel().events.READ_ALL_COMPLETE, this.currencyReadAllCompleteHandler);
            $(document).on(this.getCurrencyModel().events.READ_ALL_ERROR, this.currencyReadAllErrorHandler);

            $(document).on(this.getAuthenticationController().AUTHENTICATION_COMPETE_EVENT, this.authenticationCompleteEventHandler);

            this.getSelectCurrencyList().on('click', this.changeCurrencyClickedHandler);
        },

        startApp: function() {

            var currentUser = this.getUserModel().getCurrentUser();
            currentUser.programs = currentUser.programs.split(',');
            this.getUserModel().setCurrentUser(currentUser);

            this.callSuper('App.LoginApplication', 'startApp');
            this.showNotification('information', 'Welcome to your card design studio');

            this.getCartHeaderContainer().find('.cart-price-total').text(this.getUserModel().getCurrentUser().shoppingCart.cartPriceTotal);
            this.getCartHeaderContainer().find('.currency-symbol').text(this.getUserModel().getCurrentUser().currency.symbol);

            this.getContentAllHolder().show();
        },

        createCurrencyHeaderItems: function() {

            this.getSelectCurrencyList().empty();

            var liEl, spanEL;
            var currency;
            var currencies = this.getCurrencyModel().getLoadedData();

            for(var i = 0, len = currencies.length; i < len; i++) {

                currency = currencies[i];

                spanEl = $('<span/>', {
                    text: currency.code
                });

                liEl = $('<li/>', {
                    'data-id': currency.id,
                    'class': 'currency-list-item',
                    'data-symbol': currency.symbol,
                    'data-rate': currency.rate
                });

                liEl.append(spanEl);

                if(currency.id.toString() === this.getUserModel().getCurrentUser().currency.id.toString()) {
                    liEl.addClass('active-currency');
                }

                this.getSelectCurrencyList().append(liEl);
            }
        }
    },

    extendPrototypeFrom: 'App.LoginApplication'
});
