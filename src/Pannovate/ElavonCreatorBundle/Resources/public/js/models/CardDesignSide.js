CLASS.define({

    name: 'Model.CardDesignSide',

    definition: function(app) {

        this.extendFromClass('Base.Model', [app]);

        this.UPLOAD_CARD_DESIGN_IMAGE_INTENTION = 'upload-card-design-image';
        this.UPLOAD_CARD_DESIGN_IMAGE_ELEMENT_INTENTION = 'upload-card-image-element';

        this.CARD_SIDE_FRONT = 'front';
        this.CARD_SIDE_BACK = 'back';

        this.CARD_UPLOAD_STATUS_IDLE = 'CardDesignUpload.Status.idle';
        this.CARD_UPLOAD_STATUS_UPLOADING_DESIGN_ELEMENT = 'CardDesignUpload.Status.uploadingDesignElement';
        this.CARD_UPLOAD_STATUS_UPLOADING_DESIGN_IMAGE = 'CardDesignUpload.Status.uploadingDesignImage';
        this.CARD_UPLOAD_STATUS_COMPLETE = 'CardDesignUpload.Status.complete';

        var that = this;

        var currentCardDesign;
        var cardDesignImage;
        var cardDesignImageData;
        var cardSide;

        var cardDesignUploadImages = [];
        var saveCardDesignUploadIds = [];
        var cardDesignImageElementFiles = {};
        var cardSideDesignElements = {};

        var currentlySelectedElement;

        var cardDesignUploadStatus = this.CARD_UPLOAD_STATUS_IDLE;

        this.setCardDesignUploadStatus = function(status) {
            cardDesignUploadStatus = status;
        };

        this.getCardDesignUploadStatus = function() {
            return cardDesignUploadStatus;
        };

        this.setCardSide = function(side) {
            cardSide = side;
        };

        this.getCardSide = function() {
            return cardSide;
        };

        this.getCurrentCardDesign = function() {
            return currentCardDesign;
        };

        this.setCurrentCardDesign = function(design) {
            currentCardDesign = design;
        };

        this.addSaveCardDesignUploadId = function(uploadId) {
            saveCardDesignUploadIds.push(uploadId);
        };

        this.removeSaveCardDesignUploadId = function(uploadId) {

            var index = saveCardDesignUploadIds.indexOf(uploadId);

            if(index > -1) {
                saveCardDesignUploadIds.splice(index, 1);
            }
        };

        this.getSaveCardDesignUploadIds = function() {
            return saveCardDesignUploadIds;
        };

        this.resetSaveCardDesignUploadIds = function() {
            saveCardDesignUploadIds = [];
        };

        this.addCardDesignUploadImage = function(uploadId) {
            cardDesignUploadImages.push(uploadId);
        };

        this.removeCardDesignUploadImage = function(uploadImage) {

            var index = cardDesignUploadImages.indexOf(uploadImage);

            if(index > -1) {
                cardDesignUploadImages.splice(index, 1);    
            }
        };

        this.getCardDesignUploadImages = function() {
            return cardDesignUploadImages;
        };

        this.resetCardDesignUploadImages = function() {
            cardDesignUploadImages = [];
        };

        this.getCardDesignImageElementFiles = function() {
            return cardDesignImageElementFiles;
        };

        this.addCardDesignImageElementFile = function(blobHashUrl, file) {
            cardDesignImageElementFiles[blobHashUrl] = file;
        };

        this.removeCardDesignImageElementFile = function(blobHashUrl) {
            delete cardDesignImageElementFiles[blobHashUrl];
        };

        this.getCardDesignImageElement = function(blobHashUrl) {
            return cardDesignImageElementFiles[blobHashUrl];
        };

        this.setCardDesignData = function(data) {
            cardDesignData = data;
        };

        this.getCardDesignData = function() {
            return cardDesignData;
        };

        this.setCardDesignImage = function(designImage) {
            cardDesignImage = designImage;
        };

        this.getCardDesignImage = function() {
            return cardDesignImage;
        };

        this.setCardDesignImageData = function(imageData) {
            cardDesignImageData = imageData;
        };

        this.getCardDesignImageData = function() {
            return cardDesignImageData;
        };

        this.addCardDesignElement = function(element, id) {
            cardSideDesignElements[id] = element;
        };

        this.getCardDesignElement = function(id) {

            if(cardSideDesignElements.hasOwnProperty(id)) {
                return cardSideDesignElements[id];
            }
            else {
                return null;
            }
        };

        this.resetCardDesignElements = function() {
            cardSideDesignElements = {};
        };

        this.getCardSideElements = function() {
            return cardSideDesignElements;
        };

        this.setCurrentlySelectedElement = function(element) {
            currentlySelectedElement = element;
        };

        this.getCurrentlySelectedElement = function() {
            return currentlySelectedElement;
        };

        this.uploadFileCompleteEventHandler = function(ev) {

            // check if this model was uploading file
            if(ev.scope.hasOwnProperty('model') && ev.scope.model === that) {

                // check of the intention of file upload
                if(ev.scope.hasOwnProperty('intention')) {
                    
                    var e;

                    if(ev.scope.intention === that.UPLOAD_CARD_DESIGN_IMAGE_ELEMENT_INTENTION) {

                        that.removeCardDesignImageElementFile(ev.scope.blobHashUrl);
                        that.removeSaveCardDesignUploadId(ev.scope.uploadId);

                        that.replaceBlobUrlWithRealUrlInCardDesign(ev.scope.blobHashUrl, ev.responseData.file_url_path);

                        if(that.getSaveCardDesignUploadIds().length === 0) {

                            that.saveCardDesignImage({scope:ev.scope.parentScope});

                            e = $.Event(that.events.ALL_DESIGN_ELEMENTS_UPLOADED_COMPLETE, {scope: that.getCardSide()});
                            $(document).trigger(e);
                        }
                    }
                    else if(ev.scope.intention === that.UPLOAD_CARD_DESIGN_IMAGE_INTENTION) {

                        $(document).off(that.events.UPLOAD_FILE_COMPLETE, that.uploadFileCompleteEventHandler);

                        var cardDesign = that.getCurrentCardDesign();

                        if(that.getCardSide() === 'front') {
                            cardDesign.frontImageUrl = ev.responseData.file_url_path;
                            cardDesign.frontDesignImageData = that.getCardDesignImageData();
                        }
                        else {
                            cardDesign.backImageUrl = ev.responseData.file_url_path;
                            cardDesign.backDesignImageData = that.getCardDesignImageData();
                        }

                        that.resetSaveCardDesignUploadIds();
                        that.resetCardDesignUploadImages();
                        that.setCardDesignUploadStatus(that.CARD_UPLOAD_STATUS_COMPLETE);


                        e = $.Event(that.events.SAVE_CARD_DESIGN_SIDE_COMPLETE, {scope: ev.scope.parentScope});
                        $(document).trigger(e);
                    }
                }
            }
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.ElavonCardDesignSide'),

        /**
         * Upload card design image.
         *
         * @param {Object} imageFile Image to be saved.
         * @param {Object} postData Parameters supplied with the image.
         */
        uploadCardDesignImage: function(imageFile, designId, imageData, parentScope) {

            var postData = {};
                postData.intention = this.UPLOAD_CARD_DESIGN_IMAGE_INTENTION;
                postData.card_side = this.getCardSide();
                postData.image_data = imageData;
                postData.id = designId;

            var url = this.getApp().getConfig().getBaseAPIUrl() + '/upload-file';
            var uploadId = this.generateUploadId();

            var options = {
                uploadId: uploadId,
                showLoader: false,
                scope: {
                    uploadId: uploadId,
                    intention: postData.intention,
                    model: this,
                    parentScope: parentScope 
                }
            };

            this.setCardDesignUploadStatus(this.CARD_UPLOAD_STATUS_UPLOADING_DESIGN_IMAGE);
            this.addSaveCardDesignUploadId(uploadId);
            this.uploadFile(url, imageFile, postData, options);
        },

        /**
         * Upload card design image.
         *
         * @param {Object} imageFile Image to be saved.
         * @param {Object} postData Parameters supplied with the image.
         */
        uploadCardImageElement: function(imageFile, blobHashUrl, postData, parentScope) {

            postData = postData || {};
            postData.intention = this.UPLOAD_CARD_DESIGN_IMAGE_ELEMENT_INTENTION;

            var url = this.getApp().getConfig().getBaseAPIUrl() + '/upload-file';
            var uploadId = this.generateUploadId();

            var options = {
                uploadId: uploadId,
                showLoader: false,
                scope: {
                    intention: postData.intention,
                    uploadId: uploadId,
                    blobHashUrl: blobHashUrl,
                    model: this,
                    parentScope: parentScope
                }
            };

            this.setCardDesignUploadStatus(this.CARD_UPLOAD_STATUS_UPLOADING_DESIGN_ELEMENT);
            this.addSaveCardDesignUploadId(uploadId);
            this.uploadFile(url, imageFile, postData, options);
        },

        readFileUploadProgress: function(uploadId) {

            var url = this.getApp().getConfig().getBaseAPIUrl() + '/read-file-upload-progress';
            this.callSuper('Base.Model', 'readFileUploadProgress', [url, uploadId, {scope:this, showLoader: false}]);
        },

        saveCardDesignImage: function(options) {

            var imageData = JSON.stringify(this.getCardDesignImageData());
            var designId = this.getCurrentCardDesign().id;

            this.uploadCardDesignImage(this.getCardDesignImage(), designId, imageData, options.scope);
        },

        saveCardDesign: function(designData, cardDesignImage, cardDesignImageData, options) {

            this.setCardDesignData(designData);
            this.setCardDesignImage(cardDesignImage);
            this.setCardDesignImageData(cardDesignImageData);

            $(document).on(this.events.UPLOAD_FILE_COMPLETE, this.uploadFileCompleteEventHandler);

            if(Object.keys(this.getCardDesignImageElementFiles()).length === 0) {
                this.saveCardDesignImage(options);
                return;    
            }

            var element;
            var imageElementData;
            var imageElementFile;

            for(var elIndex in cardDesignImageData.objects) {

                element = cardDesignImageData.objects[elIndex];

                if(element.type === 'image' && element.src.indexOf('blob:') !== -1) {

                    imageElementData = this.getImageElementPostData(designData, element);

                    this.addCardDesignUploadImage(imageElementData);

                    imageElementFile = this.getCardDesignImageElement(element.src);

                    if(imageElementFile) {
                        this.uploadCardImageElement(
                            imageElementFile,
                            element.src,
                            imageElementData,
                            options.scope
                        );
                    }
                }
            }
        },

        getImageElementPostData: function(designData, element) {
            return {
                id: designData.id,
                element_type: element.type,
                card_side: this.getCardSide()
            };
        },

        replaceBlobUrlWithRealUrlInCardDesign: function(blobUrl, realUrl) {

            var designImageData = this.getCardDesignImageData();
            var fabricElement;

            for(var i = 0, len = designImageData.objects.length; i < len; i++) {

                fabricElement = designImageData.objects[i];

                if(fabricElement.type === 'image' && fabricElement.src.toString() === blobUrl.toString()) {

                    fabricElement.src = realUrl;
                    designImageData.objects[i] = fabricElement;
                    break;
                }
            }

            this.setCardDesignImageData(designImageData);
        },

        readCardDesignSaveProgress: function() {

        },

        updateImage: function(designId, cropParameters, options) {

            if(cropParameters) {

                $.extend(options, true, {

                    url: this.getApp().getConfig().getBaseAPIUrl() + this.getUrl() + '/' + designId + '/update-image',

                    success: function(responseData, textStatus, jqXHR) {
                        that.dispatchSuccess(that.events.UPDATE_COMPLETE, responseData, options, jqXHR);
                    },

                    error: function(error, jqXHR, textStatus) {
                        that.dispatchError(that.events.UPDATE_ERROR, error, options, jqXHR);
                    }
                });

                options.data = cropParameters;
                this.getApp().getRequest().POST(options);
            }
            else {

                var e = $.Event(
                    that.events.VALIDATION_ERROR,
                    {
                        validationErrors:this.getValidationErrors(),
                        validationErrorMessage: "Crop parameters are invalid",
                        scope: options.scope
                    }
                );

                $(document).trigger(e);
            }
        },

        saveImageFromUrl: function(designId, imageUrl, cropParameters, options) {

            if(cropParameters) {

                $.extend(options, true, {

                    url: this.getApp().getConfig().getBaseAPIUrl() + this.getUrl() + '/' + designId + '/save-image-from-url',

                    success: function(responseData, textStatus, jqXHR) {
                        that.dispatchSuccess(that.events.SAVE_FROM_URL_COMPLETE, responseData, options, jqXHR);
                    },

                    error: function(error, jqXHR, textStatus) {
                        that.dispatchError(that.events.SAVE_FROM_URL_ERROR, error, options, jqXHR);
                    }
                });

                options.data = cropParameters;
                options.data.image_url = imageUrl;

                this.getApp().getRequest().POST(options);
            }
            else {

                var e = $.Event(
                    that.events.VALIDATION_ERROR,
                    {
                        validationErrors:this.getValidationErrors(),
                        validationErrorMessage: "Crop parameters are invalid",
                        scope: options.scope
                    }
                );

                $(document).trigger(e);
            }
        }
    },

    extendPrototypeFrom: 'Base.Model'
});
