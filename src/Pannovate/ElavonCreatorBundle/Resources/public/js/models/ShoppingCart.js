CLASS.define({

    name: 'Model.ShoppingCart',

    definition: function(app, countryModel) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/shopping-cart';
        var isUserLoggedIn = false;
        var currentUser = null;
        var fanfareId = '';

        var fields = [];

        var validationRules = [];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.ShoppingCart'),
    },
    
    extendPrototypeFrom: ['Base.Model']
});
