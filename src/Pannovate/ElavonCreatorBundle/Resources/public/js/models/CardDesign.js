CLASS.define({

    name: 'Model.CardDesign',

    definition: function(app) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/design';

        var recentDesigns;
        var cardDesignData;
        var cardDesignImage;
        var currentCardDesign;

        var frontCardSideModel;
        var backCardSideModel;

        var fields = [
            {
                name: 'id',
                type: 'integer'
            },
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'type',
                type: 'string'
            }
        ];

        var validations = [
            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'type'}
        ];

        this.setFrontCardSideModel = function(model) {
            frontCardSideModel = model;
        };

        this.getFrontCardSideModel = function() {
            return frontCardSideModel;
        };

        this.setBackCardSideModel = function(model) {
            backCardSideModel = model;
        };

        this.getBackCardSideModel = function() {
            return backCardSideModel;
        };

        this.setCurrentCardDesign = function(cardDesign) {
            currentCardDesign = cardDesign;
        };

        this.getCurrentCardDesign = function() {
            return currentCardDesign;
        };

        this.setRecentDesigns = function(designs) {
            recentDesigns = designs;
        };

        this.getRecentDesigns = function() {
            return recentDesigns;
        };

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validations;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.CardDesign'),

        saveCardFrontDesign: function(imageFile, serializedDesignData, saveAsNew) {

            this.getFrontCardSideModel().saveCardDesign(
                this.getCurrentCardDesign(),
                imageFile,
                serializedDesignData,
                {
                    scope: {
                        sideModel: this.getFrontCardSideModel(),
                        saveAsNew: saveAsNew
                    }
                }
            );
        },

        saveCardBackDesign: function(imageFile, serializedDesignData, saveAsNew) {

            this.getBackCardSideModel().saveCardDesign(
                this.getCurrentCardDesign(),
                imageFile,
                serializedDesignData,
                {
                    scope: {
                        sideModel: this.getBackCardSideModel(),
                        saveAsNew: saveAsNew
                    }
                }
            );
        },

        readRecentDesigns: function(options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/get-recent-designs'),
                showLoader: options.showLoader,

                success: function(responseData, textStatus, jqXHR){
                    that.setRecentDesigns(responseData);
                    that.dispatchSuccess(that.events.READ_RECENT_DESIGNS_COMPLETE, responseData, options, jqXHR, jqXHR);
                    that.modelReadComplete(that.events.READ_RECENT_DESIGNS_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_RECENT_DESIGNS_ERROR, error, options, jqXHR, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

        modelDeleteComplete: function(ev, id, options) {

            this.callSuper('Base.Model', 'modelDeleteComplete', [ev, id, options]);

            var recentDesigns = this.getRecentDesigns();
            var item;

            if(this.isArray(recentDesigns) && recentDesigns.length > 0) {

                for(var i = 0, len = recentDesigns.length; i < len; i++) {

                    item = recentDesigns[i];

                    if(item.id.toString() === id.toString()) {
                        recentDesigns.splice(i, 1);
                        break;
                    }
                }

                this.setRecentDesigns(recentDesigns);
            }
        }
    },

    extendPrototypeFrom: 'Base.Model'
});
