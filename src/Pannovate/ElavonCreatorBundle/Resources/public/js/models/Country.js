CLASS.define({

    name: 'Model.Country',

    definition: function(app) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/country';

        var fields = [
            {
                name: 'name',
                type: 'string'
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'name'},
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.Country'),
    },
    
    extendPrototypeFrom: ['Base.Model']
});
