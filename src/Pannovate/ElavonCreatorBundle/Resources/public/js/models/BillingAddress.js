CLASS.define({

    name: 'Model.BillingAddress',

    definition: function(app, countryModel) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/billing-address';

        var fields = [
            {
                name: 'company',
                type: 'string'
            },
            {
                name: 'firstName',
                type: 'string'
            },
            {
                name: 'lastName',
                type: 'string'
            },
            {
                name: 'email',
                type: 'string'
            },
            {
                name: 'address',
                type: 'string',
            },
            {
                name: 'city',
                type: 'string',
            },
            {
                name: 'postcode',
                type: 'string',
            },
            {
                name: 'country',
                type: 'relation',
                relationModel: countryModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'phone',
                type: 'string',
            },
        ];

        var validationRules = [
            {type: 'presence',  field: 'company'},
            {type: 'presence',  field: 'firstName'},
            {
                type: 'length', 
                field: 'firstName',
                min:3,
                max:16,
                message: "First Name must be between 3 and 16 characters."
            },

            {type: 'presence',  field: 'lastName'},
            {
                type: 'length', 
                field: 'lastName',
                min:3,
                max:16,
                message: "Last Name must be between 3 and 16 characters."
            },

            {type: 'presence',  field: 'email'},
            {type: 'format',    field: 'email', pattern: this.EMAIL_VALIDATION_REGEX, message:"Wrong Email Format"},

            {type: 'presence',  field: 'city'},
            {
                type: 'length', 
                field: 'city',
                min:3,
                max:32,
                message: "City must be between 3 and 32 characters."
            },

            {type: 'presence',  field: 'postcode'},
            {
                type: 'length', 
                field: 'postcode',
                min:5,
                message: "Postcode must be minimum 5 characters long."
            },

            {type: 'presence',  field: 'country'},
            {type: 'presence',  field: 'phone'},
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.BillingAddress'),
    },
    
    extendPrototypeFrom: ['Base.Model']
});
