CLASS.define({

    name: 'Model.ElavonCardDesignSide',

    definition: function(app, textFieldsCountLimit, imageObjectsCountLimit) {

        this.extendFromClass('Model.CardDesignSide', [app]);

        var backgroundImage;
        var backgroundColor;
        var fixedElements = [];
        var logoImage;
        var textField;

        var imageFieldsCount = 0;
        var textFieldsCount = 0;

        var nonPrintingElementsGroup;
        var elementsBoundingBox;

        var backgroundImageFilter;

        this.getTextFieldsCountLimit = function() {
            return textFieldsCountLimit;
        };

        this.getImageObjectsCountLimit = function() {
            return imageObjectsCountLimit;
        };

        this.setBackgroundColor = function(color) {
            backgroundColor = color;
        };

        this.getBackgroundColor = function() {
            return backgroundColor;
        };

        this.setBackgroundImage = function(bckImage) {
            backgroundImage = bckImage;
        };

        this.getBackgroundImage = function() {
            return backgroundImage;
        };

        this.setBackgroundColor = function(bckColor) {
            backgroundColor = bckColor;
        };

        this.getBackgroundColor = function() {
            return backgroundColor;
        };

        this.setLogoImage = function(logoImg) {
            logoImage = logoImg;
        };

        this.getLogoImage = function() {
            return logoImage;
        };

        this.setTextField = function(txtField) {
            textField = txtField;
        };

        this.getTextField = function() {
            return textField;
        };

        this.addFixedElement = function(element) {
            fixedElements.push(element);
        };

        this.getFixedElements = function() {
            return fixedElements;
        };

        this.resetFixedElements = function() {
            fixedElements = [];
        };

        this.setNonPrintingElementsGroup = function(group) {
            nonPrintingElementsGroup = group;
        };

        this.getNonPrintingElementsGroup = function() {
            return nonPrintingElementsGroup;
        };

        this.setElementsBoundingBox = function(boundingBox) {
            elementsBoundingBox = boundingBox;
        };

        this.getElementsBoundingBox = function() {
            return elementsBoundingBox;
        };

        this.setBackgroundFilter = function(filter) {
            
            if(backgroundImageFilter !== filter) {

                backgroundImageFilter = filter;

                var e = $.Event(
                    this.events.BACKGROUND_IMAGE_FILTER_SELECTED,
                    {
                        filter: filter,
                        sideModel: this
                    }
                );

                $(document).trigger(e);
            }
        };

        this.getBackgroundFilter = function() {
            return backgroundImageFilter;
        };
    },

    prototypeMethods: {

        getImageElementPostData: function(designData, element) {
            return {
                id: designData.id,
                element_type: element.elementType,
                card_side: this.getCardSide()
            };
        },
    },

    extendPrototypeFrom: 'Model.CardDesignSide',
});