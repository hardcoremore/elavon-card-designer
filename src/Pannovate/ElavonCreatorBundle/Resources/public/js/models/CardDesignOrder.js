CLASS.define({

    name: 'Model.CardDesignOrder',

    definition: function(app) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/card-design-order';

        var recentCardDesignOrders;
        var cardDesignData;
        var cardDesignImage;
        var currentCardDesignOrder;

        var frontCardSideModel;
        var backCardSideModel;

        var fields = [
            {
                name: 'id',
                type: 'integer'
            },
            {
                name: 'shoppingCart',
                type: 'integer',
            },
            {
                name: 'billingAddress',
                type: 'integer',
            },
            {
                name: 'shippingAddress',
                type: 'integer',
            },
            {
                name: 'shippingSameAsBilling',
                type: 'integer',
            }
        ];

        var validations = [
            {type: 'presence',  field: 'shoppingCart'},
            {type: 'presence',  field: 'billingAddress'},
            {
                type: 'presence',
                anyField: ['shippingAddress', 'shippingSameAsBilling'],
                message: 'This field is not valid',
                globalMessage: "You have to define either shipping address or to mark that billing address is same as shipping address.",
                onlyOne: true
            },
        ];

        this.setRecentCardDesignOrders = function(designs) {
            recentCardDesignOrders = designs;
        };

        this.getRecentCardDesignOrders = function() {
            return recentCardDesignOrders;
        };

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validations;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.CardDesignOrder'),

        readRecentCardDesignOrders: function(options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/get-recent-designs'),
                showLoader: options.showLoader,

                success: function(responseData, textStatus, jqXHR){
                    that.setRecentCardDesignOrders(responseData);
                    that.dispatchSuccess(that.events.READ_RECENT_CARD_DESIGN_ORDERS_COMPLETE, responseData, options, jqXHR, jqXHR);
                    that.modelReadComplete(that.events.READ_RECENT_CARD_DESIGN_ORDERS_ERROR, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_RECENT_DESIGNS_ERROR, error, options, jqXHR, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        }
    },

    extendPrototypeFrom: 'Base.Model'
});
