CLASS.define({

    name: 'Model.ShoppingCartItem',

    definition: function(app) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/shopping-cart-item';
        var cardPricingModel;
        var userModel;
        var cartTotalPrice = 0;

        var fields = [
            {
                name: 'cardDesign',
                type: 'integer'
            },
            {
                name: 'quantity',
                type: 'integer'
            },
            {
                name: 'pricePerCard',
                type: 'decimal'
            },
            {
                name: 'cardDenomination',
                type: 'json'  
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'cardDesign'},
            {
                type: 'range', 
                field: 'cardDesign',
                min:1,
                message: "Invalid card design"
            },

            {type: 'presence',  field: 'quantity'},
            {
                type: 'range', 
                field: 'quantity',
                min:1,
                message: "Quantity must be minimum 1"
            },

            {type: 'presence',  field: 'pricePerCard'},
            {
                type: 'range', 
                field: 'pricePerCard',
                min: 0.0001,
                message: "pricePerCard must be minimum 0.0001"
            }
        ];

        this.setItemsTotalPrice = function(totalPrice) {
            cartTotalPrice = totalPrice;
        };

        this.getItemsTotalPrice = function() {
            return cartTotalPrice;
        };

        this.setCardPricingModel = function(model) {
            cardPricingModel = model;
        };

        this.getCardPricingModel = function() {
            return cardPricingModel;
        };

        this.setUserModel = function(model) {
            userModel = model;
        };

        this.getUserModel = function() {
            return userModel;
        };

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.ShoppingCartItem'),

        readAllForCurrentUser: function(options) {
            
            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/list-all-for-current-user'),

                showLoader: options.showLoader,

                success: function(responseData, textStatus, jqXHR) {

                    var loadedItem;
                    var loadedData = that.getLoadedData() || [];
                    var itemsTotalPrice = 0;

                    for(var i = 0, len = responseData.length; i < len; i++) {

                        loadedItem = that.getLoadedDataRowByProperty('id', responseData[i].id);

                        if(loadedItem === null) {
                            loadedData.push(responseData[i]);
                        }

                        itemsTotalPrice += responseData[i].totalPrice;
                    }

                    that.setItemsTotalPrice(itemsTotalPrice);
                    that.setLoadedData(loadedData);
                    that.dispatchSuccess(that.events.READ_ALL_FOR_SHOPPING_CART_COMPLETE, responseData, options, jqXHR, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_ALL_FOR_SHOPPING_CART_ERROR, error, options, jqXHR, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

        updateItemQuantity: function(shoppingCartItemId, quantity) {

            var usersCurrency = this.getUserModel().getCurrentUser().currency;
            var cardPricingData = this.getCardPricingModel().getCardPricingForNumberOfCards(quantity, usersCurrency.id);
            var shoppingCartItem = this.getLoadedDataRowByProperty('id', shoppingCartItemId);
            var designType = shoppingCartItem.cardDesign.type.toString().toLowerCase();

            var calculatedPrice = cardPricingData[designType] * quantity;
            
            shoppingCartItem.pricePerCard = cardPricingData[designType];
            shoppingCartItem.quantity = quantity;
            shoppingCartItem.totalPrice = calculatedPrice;

            var loadedData = this.getApp().getShoppingCartItemModel().getLoadedData();
            var itemsTotalPrice = 0;

            for(var i = 0, len = loadedData.length; i < len; i++) {
                itemsTotalPrice += loadedData[i].totalPrice;
            }

            this.setItemsTotalPrice(itemsTotalPrice);

            var e = $.Event(this.events.ITEM_UPDATED, {shoppingCartItem: shoppingCartItem});
            $(document).trigger(e);

            var patchData = {
                quantity: shoppingCartItem.quantity,
                pricePerCard: shoppingCartItem.pricePerCard,
                totalPrice: shoppingCartItem.totalPrice,
            };

            this.patch(shoppingCartItem.id, patchData, {scope:this, showLoader:false});
        },

        findByDesignId: function(cardDesignId) {

            var loadedData = this.getLoadedData();
            var shoppingCartItem = null;

            if(loadedData) {

                for(var i = 0, len = loadedData.length; i < len; i++) {

                    shoppingCartItem = loadedData[i];

                    if(shoppingCartItem.cardDesign.id.toString() === cardDesignId.toString()) {
                        break;
                    }
                    else {
                        shoppingCartItem = null;                        
                    }
                }
            }

            return shoppingCartItem;
        }
    },
    
    extendPrototypeFrom: ['Base.Model']
});
