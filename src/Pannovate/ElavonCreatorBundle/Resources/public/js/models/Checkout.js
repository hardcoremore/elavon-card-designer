CLASS.define({

    name: 'Model.Checkout',

    definition: function(app) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/payment';

        this.getUrlPart = function() {
            return urlPart;
        }
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.Checkout'),

        readPaymentParams: function (orderId, options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/get-checkout-params/' + orderId),

                success: function (responseData, textStatus, jqXHR) {
                    that.dispatchSuccess(that.events.READ_PAYMENT_PARAMS_COMPLETE, responseData, options, jqXHR);
                },

                error: function (error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_PAYMENT_PARAMS_ERROR, error, options, jqXHR);
                }

            });

            this.getApp().getRequest().GET(options);
        },

        readPaymentResponseParams: function (options) {

            var that = this;

            $.extend(options, true, {
                url: this.getUrl('/payment-response-params'),
                success: function (responseData, textStatus, jqXHR) {
                    that.dispatchSuccess(that.events.PAYMENT_RESPONSE_PARAMS_COMPLETE, responseData, options, jqXHR);
                },
                error: function (error, jqXHR, textStatus) {
                    that.dispatchError(that.events.PAYMENT_RESPONSE_PARAMS_ERROR, error, options, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

        getCheckoutForm: function (options) {

            var that = this;

            $.extend(options, true, {
                url: this.getUrl('/checkout-form'),
                success: function (responseData, textStatus, jqXHR) {
                    that.dispatchSuccess(that.events.READ_CHECKOUT_FORM_COMPLETE, responseData, options, jqXHR);
                },
                error: function (error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_CHECKOUT_FORM_ERROR, error, options, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },
    },

    extendPrototypeFrom: 'Base.Model'
});
