<?php

namespace Pannovate\ElavonCreatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ModuleViewsController extends Controller
{
    public function homeViewAction()
    {
        return $this->render('ElavonCreatorBundle:ModuleViews:home.html.twig');
    }

    public function elavonCardDesignerViewAction()
    {
        return $this->render('ElavonCreatorBundle:ModuleViews:elavonCardDesigner.html.twig');
    }

    public function shoppingCartViewAction()
    {
        return $this->render('ElavonCreatorBundle:ModuleViews:shoppingCart.html.twig');
    }

    public function myDesignStudioViewAction()
    {
        return $this->render('ElavonCreatorBundle:ModuleViews:myDesignStudio.html.twig');
    }

    public function checkoutViewAction()
    {
        return $this->render('ElavonCreatorBundle:ModuleViews:checkout.html.twig');
    }

    public function thankYouViewAction()
    {
        return $this->render('ElavonCreatorBundle:ModuleViews:thankYou.html.twig');
    }
}
