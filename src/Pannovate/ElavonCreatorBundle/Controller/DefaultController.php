<?php

namespace Pannovate\ElavonCreatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render(
            'ElavonCreatorBundle:Default:index.html.twig',
             array('fanfareId' => $this->get('session')->get('fanfareId'))
        );
    }

    public function notFoundAction()
    {
        return $this->redirectToRoute('elavon_creator_login_check');
    }

    public function privacyPolicyAction()
    {
        $response = new Response();

        $response->setContent('We are open source. Everything is allowed :)');
        
        return $response;
    }
}
