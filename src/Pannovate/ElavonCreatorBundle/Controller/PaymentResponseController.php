<?php

namespace Pannovate\ElavonCreatorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PaymentResponseController extends Controller
{
    public function getPaymentResponseViewAction(Request $request)
    {   
        $responseCode = $request->query->get('RESPONSECODE');
        $responseText = '';

        if($responseCode === 'A') 
        {
            $responseText = 'Thank you for your purchase. Your order has been placed successfully.';
        }
        elseif($responseCode === 'D')
        {
            $responseText = 'Your order has been declined. Please contact support for more information.';
        }
        elseif($responseCode === 'R')
        {
            $responseText = 'Your order has been referred.';
        }
        else
        {
            $responseText = 'There was some problem with your order.';
        }

        $data = [
            "message" => $responseText,
            "orderId" => $request->query->get('ORDERID')
        ];

        return $this->render(
            'ElavonCreatorBundle:Payment:paymentThankYouPage.html.twig',
            $data
        );
    }

}