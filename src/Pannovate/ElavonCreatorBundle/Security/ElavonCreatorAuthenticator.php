<?php

// src/AppBundle/Security/ApiKeyAuthenticator.php
namespace Pannovate\ElavonCreatorBundle\Security;

use Symfony\Component\Security\Http\Authentication\SimpleFormAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ElavonCreatorAuthenticator implements SimpleFormAuthenticatorInterface, AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface
{
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function createToken(Request $request, $username, $password, $providerKey)
    {
        if(strlen($username) < 1 || strlen($password) < 1)
        {
            throw new BadCredentialsException('Invalid credentials, access is denied.');
        }

        $this->container->get('session')->set('fanfareId', $request->get('fanfareId'));

        return new UsernamePasswordToken($username, md5($password), $providerKey);
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $apiKey = $token->getCredentials();
        $validKey = md5('fanfare');

        $user = $userProvider->loadUserByUsername($token->getUsername());

        if(!$user) {
            throw new AuthenticationException(
                sprintf('Fanfare User: "%s" not found.', $token->getUsername())
            );
        }

        if($validKey !== $apiKey) {
            throw new AuthenticationException(
                sprintf('API Key "%s" does not exist.', $apiKey)
            );
        }

        return new UsernamePasswordToken(
            $user,
            $apiKey,
            $providerKey,
            $user->getRoles()
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof UsernamePasswordToken && $token->getProviderKey() === $providerKey;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $url = $this->container->get('router')->generate('elavon_creator_homepage',array(), true);
        return new RedirectResponse($url, 301);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        die("Fanfare authentication failed: " . $exception->getMessage());
    }
}