<?php

namespace Pannovate\BaseAPIBundle\ValueObject;

class BadInputError
{
    public $message;
    public $code;
}