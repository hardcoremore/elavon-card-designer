<?php

namespace Pannovate\BaseAPIBundle\ValueObject;

class ResourceResponseData
{
    public $rowsCount;
    public $totalRowsCount;
    public $pageNumber;
    public $totalPageCount;
    public $listData;
}