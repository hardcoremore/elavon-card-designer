<?php

namespace Pannovate\BaseAPIBundle\Services;

use Pannovate\BaseAPIBundle\ValueObject\ResourceResponseData;
use Pannovate\BaseAPIBundle\Strategy\Search\SearchStrategy;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RequestStack;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

use Pannovate\BaseAPIBundle\Error\ErrorCodes;

class ResourceResponseCreator
{
    protected $container;
    protected $requestStack;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->requestStack = $requestStack;
    }

    public function createPageResponse(EntityRepository $repository, SearchStrategy $searchStrategy = NULL)
    {   
        $entitiesConfig = $this->container->getParameter('entity.config');
        $entityConfig = array();
        $entityConfigFields = array();

        if(array_key_exists($repository->getClassName(), $entitiesConfig))
        {
            $entityConfig = $entitiesConfig[$repository->getClassName()];
            $entityConfigFields = $entityConfig['fields'];
        }

        $queryParameters = $this->requestStack->getCurrentRequest()->query;

        $pageNumber = $queryParameters->get('pageNumber');
        $rowsPerPage = $queryParameters->get('rowsPerPage');
        $sortColumnName = $queryParameters->get('sortColumnName');
        $sortColumnOrder = $queryParameters->get('sortColumnOrder');
        
        if($rowsPerPage < 1)
        {
            $rowsPerPage = 10;
        }

        $queryBuilder = $repository->createQueryBuilder('pr')
                                   ->select('count(pr)');

        if($searchStrategy)
        {
            $searchStrategy->bindParameters($queryBuilder);
        }

        $offset = 0;
        $totaRowsCount = $queryBuilder->getQuery()->getSingleScalarResult();

        if((int)$pageNumber === 1)
        {
            $offset = 0;
        }
        else if($pageNumber > 1)
        {
            $offset = ($pageNumber - 1) * $rowsPerPage;
        }

        if(strlen($sortColumnName) > 0 AND strlen($sortColumnOrder) > 0)
        {

            $queryBuilder = $repository->createQueryBuilder('pr')
                                       
                                       ->setFirstResult($offset)
                                       ->setMaxResults($rowsPerPage)
                                       ->orderBy('t.name', $sortColumnOrder);

            if(isset($entityConfigFields[$sortColumnName]) && isset($entityConfigFields[$sortColumnName]['orderBy']))
            {
                $orderByFieldName = $entityConfigFields[$sortColumnName]['orderBy'];

                $queryBuilder->leftJoin('pr.' . $sortColumnName, 'rel')
                             ->orderBy('rel.' . $orderByFieldName, $sortColumnOrder);
            }
            else
            {
                $queryBuilder->orderBy('pr.' . $sortColumnName, $sortColumnOrder);
            }
        }
        else
        {
            $queryBuilder = $repository->createQueryBuilder('pr')
                                       ->setFirstResult($offset)
                                       ->setMaxResults($rowsPerPage);
        }


        if($searchStrategy)
        {
            $searchStrategy->bindParameters($queryBuilder);
        }
            
        $query = $queryBuilder->getQuery();


        $data = new ResourceResponseData();

        $data->listData = $query->getResult();
        $data->rowsCount = count($data->listData);
        $data->totalRowsCount = (int)$totaRowsCount;
        $data->pageNumber = (int)$pageNumber;
        $data->totalPageCount = ceil($totaRowsCount / $rowsPerPage);

        return $data;
    }

    public function createSuccessfullyUpdateResponse($resource, $resourceRouteName)
    {
        $response = new Response($content = '', $status = 200);
        
        $response->headers->set('Location',
        $this->container->get('router')->generate(
            $resourceRouteName, array('id' => $resource->getId()),
            true // absolute
        ));

        return $response;
    }

    public function createConstraintValidationError($errorMessage, $entityName)
    {
        $data = new BadInputError();
        $data->message = $entityName . " integrity validation failed.";
        $data->code = ErrorCodes::INTEGRITY_CONSTRAINT;

        return $data;
    }
}
