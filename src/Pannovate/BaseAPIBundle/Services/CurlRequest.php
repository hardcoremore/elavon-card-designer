<?php

namespace Pannovate\BaseAPIBundle\Services;

class CurlRequest
{
    const GET_METHOD = 'POST';
    const POST_METHOD = 'POST';
    const PUT_METHOD = 'PUT';
    const DELETE_METHOD = 'DELETE';



    private $options;

    public function __construct($options)
    {
        $this->options = $options;
    }

    public function post($url, $params, $headers = null)
    {
        return $this->request(self::POST_METHOD, $url, $params, $headers);
    }

    public function get($url, $headers = null)
    {
        return $this->request(self::GET_METHOD, $url, $params, $headers);
    }

    public function put($url, $params, $headers = null)
    {
        return $this->request(self::PUT_METHOD, $url, $params, $headers);
    }

    public function delete($url, $headers = null)
    {
        return $this->request(self::DELETE_METHOD, $url, $params, $headers);
    }

    protected function request($method, $url, $params, $headers)
    {
        $ch = null;

        if(is_string($url) && strlen($url) > 0)
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
        }
        else
        {
            throw new \Exception("Url is invalid.");
        }


        if(isset($this->options['httpauth_username']) && isset($this->options['httpauth_password']))
        {
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $this->options['httpauth_username'] . ':' . $this->options['httpauth_password']);
        }

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");

        if(isset($this->options['follow_location']))
        {
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        }

        if(isset($this->options['max_redirections']) && is_numeric($this->options['max_redirections']))
        {
            curl_setopt($ch, CURLOPT_MAXREDIRS, $this->options['max_redirections']);
        }
        else
        {
            curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
        }

        if(is_array($headers) && count($headers) > 0)
        {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        if(isset($this->options['user_agent']))
        {
            curl_setopt($ch, CURLOPT_USERAGENT, $this->options['user_agent']);
        }

        switch($method)
        {
            case self::POST_METHOD:
                curl_setopt($ch, CURLOPT_POST, true);
            break;

            case self::PUT_METHOD:
                curl_setopt($ch, CURLOPT_PUT, true);
            break;

            case self::DELETE_METHOD:
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
            break;
        }

        if($params && is_array($params) && count($params) > 0)
        {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        }
        else if($params && is_string($params))
        {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }

        $responseString = curl_exec($ch);

        $headerLength = curl_getinfo($ch, CURLINFO_HEADER_SIZE);

        $headerContent = substr($responseString, 0, $headerLength);
        $bodyContent = substr($responseString, $headerLength);

        curl_close($ch);

        $response = [];
        $response["header"] = $headerContent;
        $response["body"] = $bodyContent;
        $response["headerItems"] = $this->parseHeaders($headerContent);

        return $response;
    }

    protected function parseHeaders($headersString)
    {
        $headers = [];

        $headerItem;

        foreach (explode("\r\n", $headersString) AS $i => $line)
        {
            if ($i === 0)
            {
                $headers['http_code'] = $line;
            }
            else
            {
                $headerItem = explode(': ', $line);

                if(count($headerItem) >= 2)
                {
                    $headers[$headerItem[0]] = $headerItem[1];
                }
            }
        }

        return $headers;
    }
}
