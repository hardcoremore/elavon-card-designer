<?php

namespace Pannovate\BaseAPIBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

use Symfony\Component\DependencyInjection\ContainerInterface;

use FOS\RestBundle\View\View;

use JMS\Serializer\SerializationContext;

class APIHelper
{
    private $container;
    private $requestStack;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->requestStack = $requestStack;
    }

    public function getQuickSearchView($repositoryName, $searchStrategyName, $serializationContextGroups)
    {   
        $request = $this->requestStack->getCurrentRequest();

        $em = $this->container->get('doctrine');
        $repository = $em->getRepository($repositoryName);

        $responseCreator = $this->container->get("pannovate.basemodelbundle.resource_response_creator");

        //$propertyName = $request->query->get('property');
      
        $searchStrategy = $this->container->get($searchStrategyName);
        $pageResponse = $responseCreator->createPageResponse($repository, $searchStrategy);
        
        $view = View::create($pageResponse, 200);

        if(is_array($serializationContextGroups) && count($serializationContextGroups) > 0)
        {
            $serializationContext = SerializationContext::create();
            $serializationContext->setSerializeNull(true);
            $serializationContext->setGroups($serializationContextGroups);

            $view->setSerializationContext($serializationContext);
        }

        return $view;
    }
}
