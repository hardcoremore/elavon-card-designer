<?php

namespace Pannovate\BaseAPIBundle\Controller;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseModelBundle\Base\Model;
use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class UserController extends FOSRestController
{
    protected $container;
    protected $userModel;

    public function __construct(ContainerInterface $container, Model $userModel)
    {
        $this->container = $container;
        $this->userModel = $userModel;
    }

    public function getUserModel()
    {
        return $this->userModel;
    }

    public function createAction(Request $request)
    {
        try
        {
            $user = $this->userModel->create($request->request->all());
            
            $url = $this->container->get('router')->generate(
                'pannovate_baseapibundle_user_get',
                array(
                    'id' => $user->getId()
                ),
                true // absolute
            );

            return $this->handleView(
                $this->view(
                    $user,
                    201,
                    array(
                        "Location" => $url
                    )
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function getUserAction($id)
    {
        $this->userModel = $this->container->get($this->userModelServiceName);
        $user = $this->userModel->get($id);

        if($user)
        {
            
            $view = $this->view($user, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }   
    }

    public function getCurrentUserAction()
    {
        $token = $this->container->get('security.token_storage')->getToken();

        $user = $this->userModel->get($token->getUser()->getId());

        $view = $this->view($user, 200)
                     ->setSerializationContext($this->createSerializationContext());

        return $this->handleView($view);
    }

    public function listAllAction(Request $request)
    {
        $users = $this->userModel->readAll();

        if($users)
        {
            $view = $this->view($users, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }
    }

    public function logoutSuccessAction()
    {
        return $this->handleView(
            $this->view("You have logged out successfully.", 200)
        );
    }

    /**
     * This method is called when user does not have permissions to access some area of the API.
     */

    public function loginRequiredAction(Request $request)
    {
        $authenticationError = $request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);

        if($authenticationError)
        {
            $request->getSession()->set(SecurityContext::AUTHENTICATION_ERROR, NULL);
            return $this->handleView($this->view($authenticationError->getMessage(), 400));
        }
        else
        {
            $data = new BadInputError();
            $data->message = "You have to log in to access the Elavon Creator API.";
            $data->code = ErrorCodes::LOGIN_REQUIRED;

            return $this->handleView($this->view($data, 403));
        }
    }

    public function patchAction(Request $request, $id)
    {
        try
        {
            $userModel = $this->userModel;
            $user = $userModel->patch($id, $request->request->all());

            return $this->handleView($this->view(null,204));
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups($this->getSerializationGroups());

        return $context;
    }

    protected function getSerializationGroups()
    {
        return array('user');
    }
}
