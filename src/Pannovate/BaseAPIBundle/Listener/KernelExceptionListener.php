<?php

namespace Pannovate\BaseAPIBundle\Listener;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;
use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\Exception\Security\FailedLoginsException;

class KernelExceptionListener
{

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Handles security related exceptions.
     *
     * @param GetResponseForExceptionEvent $event An GetResponseForExceptionEvent instance
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if($exception instanceof AccessDeniedException)
        {
            $response = new Response(json_encode("You have to log in to access the API"), 403);
            $response->headers->set('Content-Type', 'text/plain');

            $event->setResponse($response);
        }
        else if($exception instanceof FailedLoginsException)
        {
            $view = View::create(array('message' => $exception->getMessage(), 'code' => 1403), 403);

            $response = $this->container->get('fos_rest.view_handler')->handle($view);
            $event->setResponse($response);
        }
        else if($exception instanceof InvalidFormDataException) {

            $formErrors = [];
            $form = $exception->getErrorMessages();
            $formFields = $exception->getErrorMessages()->all();

            if($form->getErrors() && $form->getErrors()->current()) {
                $formErrors['message'] = $form->getErrors()->current()->getMessage();
            }
            else {
                $formErrors['message'] = "Form is invalid";    
            }
            
            $formErrors['errors'] = [];

            foreach ($formFields as $key => $value)
            {
                if($form[$key]->getErrors()->current())
                {
                    $formErrors['errors'][$key] = [$form[$key]->getErrors()->current()->getMessage()];
                }
            }

            $view = View::create($formErrors, 400);

            $response = $this->container->get('fos_rest.view_handler')->handle($view);
            $event->setResponse($response);
        }
    }
}