<?php

namespace Pannovate\ElavonAdminPanelAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

use Pannovate\ElavonModelBundle\Entity\ElavonUser;
use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class CustomerDetailsController extends FOSRestController
{
    public function getUserDesignsAction($id)
    {
        $cardDesignModel = $this->get('pannovate.ecmodelbundle.model.card_design');
        $elavonUserModel = $this->get('pannovate.ecmodelbundle.model.elavon_user');

        $user = $elavonUserModel->get($id);

        $recentDesigns = $cardDesignModel->getRecentDesigns($user);

        if($recentDesigns)
        {
            $view = $this->view($recentDesigns, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        //$context->setGroups(array('image_gallery_category'));

        return $context;
    }
}
