<?php

namespace Pannovate\ElavonAdminPanelAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class CardPricingController extends FOSRestController
{
	public function createAction(Request $request)
    {
        try
        {
            $cardPricingModel = $this->get('pannovate.ecmodelbundle.model.card_pricing');
            $cardPricing = $cardPricingModel->create($request->request->all());
               
            $url = $this->container->get('router')->generate(
                'pannovate_elavon_admin_panel_api_card_pricing_get',
                array(
                    'id' => $cardPricing->getId()
                ),
                true // absolute
            );

            return $this->handleView(
                $this->view(
                    $cardPricing,
                    201,
                    array(
                        "Location" => $url
                    )
                )
            );
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function getCardPricingAction($id)
    {
        $cardPricingModel = $this->get('pannovate.ecmodelbundle.model.card_pricing');
        $cardPricing = $cardPricingModel->get($id);

        if($cardPricing)
        {
            $view = $this->view($cardPricing, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }   
    }

    public function listAllAction(Request $request)
    {
        $cardPricingModel = $this->get('pannovate.ecmodelbundle.model.card_pricing');
        $cardPricings = $cardPricingModel->readAll();

        if($cardPricings)
        {
            $view = $this->view($cardPricings, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view); 
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function listForCurrencyAction(Request $request, $id)
    {
        $cardPricing = $this->get('pannovate.ecmodelbundle.model.card_pricing');
        $cardPricingForCurrency = $cardPricing->listForCurrency($id);

        if($cardPricingForCurrency)
        {
            $view = $this->view($cardPricingForCurrency, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view); 
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function updateAction(Request $request, $id)
    {
        try
        {
            $cardPricingModel = $this->get('pannovate.ecmodelbundle.model.card_pricing');
            $cardPricing = $cardPricingModel->update($id, $request->request->all());

            return $this->handleView(
                $this->view(
                    $cardPricing,
                    204
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function patchAction(Request $request, $id)
    {
        try
        {
            $cardPricingModel = $this->get('pannovate.ecmodelbundle.model.card_pricing');
            $cardPricing = $cardPricingModel->patch($id, $request->request->all());

            return $this->handleView(
                $this->view(
                    $cardPricing,
                    204
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(array('card_pricing'));

        return $context;
    }
}