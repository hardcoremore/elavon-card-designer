<?php

namespace Pannovate\ElavonAdminPanelAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\SecurityContextInterface;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

class FileUploadController extends FOSRestController
{
    public function uploadFileAction(Request $request)
    {
        $allPostData = $request->request->all();
        $imageGalleryItemModel = $this->get('pannovate.ecmodelbundle.model.image_gallery_item');

        $newImageGalleryItem = $imageGalleryItemModel->saveGalleryImageItem($allPostData);

        $responseData = [
            'category_id' => $request->get('category'),
            'file_url_path' =>  $newImageGalleryItem->getFileUrl(),
            'id' => $newImageGalleryItem->getId(),
            'name' => $newImageGalleryItem->getName()
        ];

        return $this->handleView($this->view($responseData), 200);
    }
}
