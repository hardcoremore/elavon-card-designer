<?php

namespace Pannovate\ElavonAdminPanelAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class ShippingChargeController extends FOSRestController
{
	public function createAction(Request $request)
    {
        try
        {
            $shippingChargeModel = $this->get('pannovate.ecmodelbundle.model.shipping_charge');
            $shippingCharge = $shippingChargeModel->create($request->request->all());
            
            $url = $this->container->get('router')->generate(
                'pannovate_elavon_admin_panel_api_shipping_charge_get',
                array(
                    'id' => $shippingCharge->getId()
                ),
                true // absolute
            );

            return $this->handleView(
                $this->view(
                    $shippingCharge,
                    201,
                    array(
                        "Location" => $url
                    )
                )
            );
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function getShippingChargeAction($id)
    {
        $shippingChargeModel = $this->get('pannovate.ecmodelbundle.model.shipping_charge');
        $shippingCharge = $shippingChargeModel->get($id);

        if($shippingCharge)
        {
            $view = $this->view($shippingCharge, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }   
    }

    public function listAllAction(Request $request)
    {
        $shippingChargeModel = $this->get('pannovate.ecmodelbundle.model.shipping_charge');
        $shippingCharges = $shippingChargeModel->readAll();

        if($shippingCharges)
        {
            $view = $this->view($shippingCharges, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view); 
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function updateAction(Request $request, $id)
    {
        try
        {
            $shippingChargeModel = $this->get('pannovate.ecmodelbundle.model.shipping_charge');
            $shippingCharge = $shippingChargeModel->update($id, $request->request->all());

            return $this->handleView(
                $this->view(
                    $shippingCharge,
                    204
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function patchAction(Request $request, $id)
    {
        try
        {
            $shippingChargeModel = $this->get('pannovate.ecmodelbundle.model.shipping_charge');
            $shippingCharge = $shippingChargeModel->patch($id, $request->request->all());

            return $this->handleView(
                $this->view(
                    $shippingCharge,
                    204
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(array('shipping_charge', 'country'));

        return $context;
    }
}