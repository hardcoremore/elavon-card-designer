<?php

namespace Pannovate\ElavonAdminPanelAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class CurrencyController extends FOSRestController
{
	public function createAction(Request $request)
    {
        try
        {
            $currencyModel = $this->get('pannovate.ecmodelbundle.model.currency');
            $currency = $currencyModel->create($request->request->all());
            
            $url = $this->container->get('router')->generate(
                'pannovate_elavon_admin_panel_api_currency_get',
                array(
                    'id' => $currency->getId()
                ),
                true // absolute
            );

            return $this->handleView(
                $this->view(
                    $currency,
                    201,
                    array(
                        "Location" => $url
                    )
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function getCurrencyAction($id)
    {
        $currencyModel = $this->get('pannovate.ecmodelbundle.model.currency');
        $currency = $currencyModel->get($id);

        if($currency)
        {
            $view = $this->view($currency, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }   
    }

    public function getBaseCurrencyAction()
    {
        $currencyModel = $this->get('pannovate.ecmodelbundle.model.currency');
        $currentBaseCurrency = $currencyModel->getBaseCurrency();

        if($currentBaseCurrency)
        {
            $view = $this->view($currentBaseCurrency, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }
    }

    public function listAllAction(Request $request)
    {
        $currencyModel = $this->get('pannovate.ecmodelbundle.model.currency');
        $currencies = $currencyModel->readAll();

        if($currencies)
        {
            $view = $this->view($currencies, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view); 
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function listCurrenciesSelectAction()
    {
        $currencyModel = $this->get('pannovate.ecmodelbundle.model.currency');
        $currencies = $currencyModel->readForSelect();

        if($currencies)
        {
            $view = $this->view($currencies, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view);
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function updateAction(Request $request, $id)
    {
        try
        {
            $currencyModel = $this->get('pannovate.ecmodelbundle.model.currency');
            $currency = $currencyModel->update($id, $request->request->all());

            return $this->handleView(
                $this->view(
                    $currency,
                    204
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function patchAction(Request $request, $id)
    {
        try
        {
            $currencyModel = $this->get('pannovate.ecmodelbundle.model.currency');
            $currency = $currencyModel->patch($id, $request->request->all());

            return $this->handleView(
                $this->view(
                    $currency,
                    204
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(array('currency'));

        return $context;
    }
}