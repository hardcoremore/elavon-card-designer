<?php

namespace Pannovate\ElavonAdminPanelAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class ImageGalleryController extends FOSRestController
{
	public function createCategoryAction(Request $request)
    {
        try
        {
            $imageGalleryCategoryModel = $this->get('pannovate.ecmodelbundle.model.image_gallery_category');
            $imageGslleryCategory = $imageGalleryCategoryModel->create($request->request->all());

            $url = $this->container->get('router')->generate(
                'pannovate_elavon_admin_panel_api_image_category_get',
                array(
                    'id' => $imageGslleryCategory->getId()
                ),
                true // absolute
            );

            return $this->handleView(
                $this->view(
                    $imageGslleryCategory,
                    201,
                    array(
                        "Location" => $url
                    )
                )
            );
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

	public function getImageCategoryAction($id)
    {
        $imageGalleryCategoryModel = $this->get('pannovate.ecmodelbundle.model.image_gallery_category');
        $imageCategory = $imageGalleryCategoryModel->get($id);

        if($imageCategory)
        {
            $view = $this->view($imageCategory, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

	public function getAllCategoriesAction(Request $request)
    {
        $imageGalleryCategoryModel = $this->get('pannovate.ecmodelbundle.model.image_gallery_category');
        $imageCategories = $imageGalleryCategoryModel->readAll();

        if($imageCategories)
        {
            $view = $this->view($imageCategories, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view);
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

	public function getAllCategoryImagesAction(Request $request)
    {   
        $imageGalleryItemModel = $this->get('pannovate.ecmodelbundle.model.image_gallery_item');
        $albumCategorieItems = $imageGalleryItemModel->findByCategoryId($request->get('id'));

        if($albumCategorieItems)
        {
            $view = $this->view($albumCategorieItems, 200)
                         ->setSerializationContext($this->createSerializationContex2());

            return $this->handleview($view);
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

	protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(array('image_gallery_category'));

        return $context;
    }

	protected function createSerializationContex2()
    {
        $serializationContext = $this->get('pannovate.baseapibundle.serialization_context');
        $serializationContext->setSerializeNull(true);
        $serializationContext->setGroups(array('image_gallery_item'));

        return $serializationContext;
    }
}
