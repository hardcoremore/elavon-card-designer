<?php

namespace Pannovate\ElavonAdminPanelAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class OrderDetailsController extends FOSRestController
{
    public function updateAction(Request $request, $id)
    {
        try
        {
            $cardDesignOrderModel = $this->get('pannovate.ecmodelbundle.model.card_design_order');
            $order = $cardDesignOrderModel->patch($id, $request->request->all());

            return $this->handleView(
                $this->view(
                    $order,
                    204
                )
            );
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }
}
