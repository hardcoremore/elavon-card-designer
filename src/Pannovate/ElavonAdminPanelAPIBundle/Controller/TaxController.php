<?php

namespace Pannovate\ElavonAdminPanelAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class TaxController extends FOSRestController
{
	public function createAction(Request $request)
    {
        try
        {
            $taxModel = $this->get('pannovate.ecmodelbundle.model.tax');
            $tax = $taxModel->create($request->request->all());

            $url = $this->container->get('router')->generate(
                'pannovate_elavon_admin_panel_api_tax_get',
                array(
                    'id' => $tax->getId()
                ),
                true // absolute
            );

            return $this->handleView(
                $this->view(
                    $tax,
                    201,
                    array(
                        "Location" => $url
                    )
                )
            );
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function getTaxAction($id)
    {
        $taxModel = $this->get('pannovate.ecmodelbundle.model.tax');
        $tax = $taxModel->get($id);

        if($tax)
        {
            $view = $this->view($tax, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function listAllAction(Request $request)
    {
        $taxModel = $this->get('pannovate.ecmodelbundle.model.tax');
        $currencies = $taxModel->readAll();

        if($currencies)
        {
            $view = $this->view($currencies, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view);
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function updateAction(Request $request, $id)
    {
        try
        {
            $taxModel = $this->get('pannovate.ecmodelbundle.model.tax');
            $tax = $taxModel->update($id, $request->request->all());

            return $this->handleView(
                $this->view(
                    $tax,
                    204
                )
            );
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function patchAction(Request $request, $id)
    {
        try
        {
            $taxModel = $this->get('pannovate.ecmodelbundle.model.tax');
            $tax = $taxModel->patch($id, $request->request->all());

            return $this->handleView(
                $this->view(
                    $tax,
                    204
                )
            );
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(array('tax', 'country'));

        return $context;
    }
}
