<?php

namespace Pannovate\BaseModelBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader;

use Pannovate\BaseModelBundle\Exception\Form\FormFieldNotUpdatableException;

class ConfigFormType extends AbstractType
{
    protected $container;
    protected $requestStack;

    protected $formConfig;

    protected $formFields;
    protected $formConfigName;
    protected $formServiceNamespace;

    public function __construct(ContainerInterface $container, RequestStack $requestStack, $formConfigName)
    {
        $this->container = $container;
        $this->requestStack = $requestStack;
        $this->formConfigName = $formConfigName;
    }

    public function setFormServiceNamespace($nameSpace)
    {
        $this->formServiceNamespace = $nameSpace; 
    }

    public function getFormServiceNamespace()
    {
        return $this->formServiceNamespace;
    }

    public function getFormConfiguration()
    {
        return $this->formConfig;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $requestMethod = $this->requestStack->getCurrentRequest()->getMethod();

        $postData = $this->requestStack->getCurrentRequest()->request->all();

        if(isset($this->formConfig['extendFieldsFrom']))
        {
            $extendFieldsFormConfig = $this->getFormConfigFromParameterName($this->formConfig['extendFieldsFrom']);

            if(is_array($extendFieldsFormConfig['fields']) && count($extendFieldsFormConfig['fields']) > 0)
            {
                $formMetadata = $this->getFormMetadata($extendFieldsFormConfig['default_options']["data_class"]);
                $this->processFields($builder, $extendFieldsFormConfig["fields"], $postData, $formMetadata, $options);
            }
        }

        if(isset($this->formConfig['fields']))
        {
            $this->formFields = $this->formConfig['fields'];

            $formMetadata = $this->getFormMetadata($this->formConfig['default_options']['data_class']);
            $this->processFields($builder, $this->formFields, $postData, $formMetadata, $options);
        }
        
    }

    public function getFormMetadata($formClassName)
    {
        return $this->container->get('validator')
                               ->getMetadataFactory()
                               ->getMetadataFor($formClassName);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults($this->formConfig["default_options"]);
    }

    public function getName() 
    {
        return $this->formConfig["name"];
    }
    
    protected function processFields(FormBuilderInterface $builder, array $formFields, array $postData, $formMetadata, array $options)
    {
        $isPropertyUpdate = (isset($options["intention"]) AND $options["intention"] === "patch");
        $isResourceUpdate = (isset($options["intention"]) AND $options["intention"] === "update");
        $isUpdate = $isPropertyUpdate || $isResourceUpdate;

        if($isPropertyUpdate)
        {
            foreach($postData as $key => $val)
            {
                if(array_key_exists($key, $formFields))
                {
                    $this->addField($builder, $key, $formFields[$key], $isUpdate);
                }
            }
        }
        else
        {
            $isInPostData = FALSE;
            $fieldIsRequired = FALSE;
            
            // create form dinamically from config
            foreach($formFields as $key => $val)
            {
                if($isResourceUpdate)
                {
                    if(isset($val['postNameAlias']))
                    {
                        $isInPostData = array_key_exists($val['postNameAlias'], $postData);
                        
                        //if alias is not found try with the post key
                        if($isInPostData === false)
                        {
                            $isInPostData = array_key_exists($key, $postData);
                        }
                    }
                    else
                    {
                        $isInPostData = array_key_exists($key, $postData);
                    }

                    $fieldIsRequired = $this->checkIfFieldIsRequired($key, $formMetadata);

                    if($fieldIsRequired OR $isInPostData)
                    {
                        $this->addField($builder, $key, $val, $isUpdate);
                    }    
                }
                // all form fields for the new record are created
                else
                {
                    $this->addField($builder, $key, $val, $isUpdate);
                }
            }
        }
    }

    protected function addField(FormBuilderInterface $builder, $fieldName, $fieldOptions, $isUpdate)
    {
        // check if field is only present for certain operation
        if(isset($fieldOptions['addForOperation']))
        {
            if($isUpdate && in_array('update', $fieldOptions['addForOperation']) === FALSE)
            {
                return;
            }
            else if($isUpdate === FALSE && in_array('create', $fieldOptions['addForOperation']) === FALSE)
            {
                return;
            }
        }

        $denyUpdate = $isUpdate && 
                      isset($fieldOptions["allow_update"]) && 
                      $fieldOptions["allow_update"] === "false";
        
        if($denyUpdate)
        {
            throw new FormFieldNotUpdatableException("Form field $key is not updatable.", 400);
        }
        
        if(isset($fieldOptions["options"]))
        {
            if($fieldOptions['type'] === 'collection' && isset($fieldOptions['options']['type']))
            {
                $fieldOptions['options']['type'] = $this->container->get($fieldOptions['options']['type']);
            }

            $builder->add($fieldName, $fieldOptions["type"], $fieldOptions["options"]);
        }
        else
        {
            $builder->add($fieldName, $fieldOptions["type"]);
        }
    }

    protected function checkIfFieldIsRequired($key, $formMetadata) 
    {
        $fieldMetadata = NULL;

        if($formMetadata->hasMemberMetadatas($key))
        {  
            $fieldMetadata = $formMetadata->getMemberMetadatas($key);
 
            foreach($fieldMetadata as $key => $val)
            {
                if(is_a($val, "Symfony\Component\Validator\Constraints\NotBlank"))
                {
                    return TRUE;
                }
            }

            return FALSE;
        }
        
        return FALSE;
    }

    public function loadFormConfig()
    {
        $formConfigParameterName = $this->getFormServiceNamespace() . '.' . $this->formConfigName;
        $this->formConfig = $this->getFormConfigFromParameterName($formConfigParameterName);
    }

    public function getFormConfigFromParameterName($formConfigParameterName)
    {
        return $this->container->getParameter($formConfigParameterName);
    }    
}