<?php

namespace Pannovate\BaseModelBundle\Base;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;
use Pannovate\BaseModelBundle\Entity\User;
use Pannovate\BaseModelBundle\Services\FileUploadService;

abstract class Model
{
    protected $container;
    protected $entityManager;
    protected $entityName;
    protected $formType;

    protected $saveEntityArguments;

    public function __construct(ContainerInterface $container, EntityManager $entityManager)
    {
        $this->container = $container;
        $this->entityManager = $entityManager;
    }

    public function setEntityName($entityName)
    {
        $this->entityName = $entityName;
    }

    public function getEntityName()
    {
        return $this->entityName;
    }

    public function setFormType(AbstractType $formType)
    {
        $this->formType = $formType;
    }

    protected function updateEntityBeforeCreate($entity){}
    protected function updateEntityBeforeUpdate($entity){}

    public function create($data, ...$arguments)
    {
        $this->saveEntityArguments = $arguments;

        $form = $this->container->get('form.factory')->create($this->formType);

        // fill the form with data
        $form->submit($data);
        $entity = $form->getData();

        if($form->isValid())
        {
            $this->updateEntityBeforeCreate($entity);

            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return $entity;
        }
        else
        {
            $exception = new InvalidFormDataException("Form data is invalid", 400);
            $exception->setErrorMessages($form);

            throw $exception;
        }
    }

    public function get($id)
    {
        return $this->entityManager->find($this->entityName, $id);
    }
    
    public function update($id, $data, ...$arguments)
    {
        $this->saveEntityArguments = $arguments;
        $this->save($id, $data, 'update');
    }

    public function patch($id, $data, ...$arguments)
    {
        $this->saveEntityArguments = $arguments;
        $this->save($id, $data, 'patch');
    }

    protected function save($id, $data, $intention)
    {
        $entity = $this->get($id);

        if($entity)
        {
            $form = $this->container->get('form.factory')->create($this->formType, $entity, ["intention" => $intention]);

            // fill the form from the request data
            $form->submit($data);

            if($form->isValid())
            {
                $this->updateEntityBeforeUpdate($entity);
                $this->entityManager->flush();

                return $entity;
            }
            else
            {   
                $exception = new InvalidFormDataException("Form data is invalid", 400);
                $exception->setErrorMessages($form);

                throw $exception;
            }
        }
    }

    public function readForSelect()
    {
        $data = $this->entityManager
                       ->getRepository($this->entityName)->readForSelect();

        return $data;
    }

    public function readAll()
    {
        $data = $this->entityManager
                    ->getRepository($this->entityName)
                    ->findAll();

        return $data;
    }

    public function delete($id)
    {
        $entity = $this->entityManager
                        ->getRepository($this->entityName)
                        ->find($id);
        

        $this->entityManager->remove($entity);
        $this->entityManager->flush();
        
        return true;
    }
}
