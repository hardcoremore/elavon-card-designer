<?php

namespace Pannovate\BaseModelBundle\Model;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Pannovate\BaseModelBundle\Base\Model;
use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class UserModel extends Model
{
    protected function updateEntityBeforeCreate($entity)
    {
        $factory = $this->container->get('security.encoder_factory');
        $encoder = $factory->getEncoder($entity);

        // encode password
        $password = $encoder->encodePassword($entity->getPassword(), $entity->getSalt());
        
        // set encoded password
        $entity->setPassword($password);
    }

    public function find($email)
    {
        return $this->entityManager->getRepository($this->entityName)->findOneByEmail($email);
    }
}