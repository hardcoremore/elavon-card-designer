<?php

namespace Pannovate\BaseModelBundle\Exception\Form;

class InvalidFormDataException extends \Exception
{
    protected $errorMessages;

    public function setErrorMessages($errorMessages)
    {
        $this->errorMessages = $errorMessages;
    }

    public function getErrorMessages()
    {
        return $this->errorMessages;
    }
}