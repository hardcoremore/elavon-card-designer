<?php

namespace Pannovate\BaseModelBundle\Exception\Form;

class FormFieldNotUpdatableException extends \Exception
{
}