CLASS.createNamespace('Events.Model.CardDesignOrder', {

    VALIDATION_ERROR: 'Model.CardDesignOrder.validationError',

    CREATE_COMPLETE: 'Model.CardDesignOrder.createComplete',
    CREATE_ERROR: 'Model.CardDesignOrder.createError',

    READ_COMPLETE: 'Model.CardDesignOrder.readComplete',
    READ_ERROR: 'Model.CardDesignOrder.readError',

    READ_ALL_COMPLETE: 'Model.CardDesignOrder.readAllComplete',
    READ_ALL_ERROR: 'Model.CardDesignOrder.readAllError',

    UPDATE_COMPLETE: 'Model.CardDesignOrder.updateComplete',
    UPDATE_ERROR: 'Model.CardDesignOrder.updateError',

    PATCH_COMPLETE: 'Model.CardDesignOrder.patchComplete',
    PATCH_ERROR: 'Model.CardDesignOrder.patchError',

    DELETE_COMPLETE: 'Model.CardDesignOrder.deleteComplete',
    DELETE_ERROR: 'Model.CardDesignOrder.deleteError',
});
