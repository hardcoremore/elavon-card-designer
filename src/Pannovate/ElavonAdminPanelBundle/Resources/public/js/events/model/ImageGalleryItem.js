CLASS.createNamespace('Events.Model.ImageGalleryItem', {

    VALIDATION_ERROR: 'Model.ImageGalleryItem.validationError',

    CREATE_COMPLETE: 'Model.ImageGalleryItem.createComplete',
    CREATE_ERROR: 'Model.ImageGalleryItem.createError',

    READ_COMPLETE: 'Model.ImageGalleryItem.readComplete',
    READ_ERROR: 'Model.ImageGalleryItem.readError',

    UPDATE_COMPLETE: 'Model.ImageGalleryItem.updateComplete',
    UPDATE_ERROR: 'Model.ImageGalleryItem.updateError',

    PATCH_COMPLETE: 'Model.ImageGalleryItem.patchComplete',
    PATCH_ERROR: 'Model.ImageGalleryItem.patchError',

    DELETE_COMPLETE: 'Model.ImageGalleryItem.deleteComplete',
    DELETE_ERROR: 'Model.ImageGalleryItem.deleteError',

    READ_FOR_SELECT_COMPLETE: 'Model.ImageGalleryItem.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Model.ImageGalleryItem.readForSelectError',

    UPLOAD_FILE_COMPLETE: 'Model.ImageGalleryItem.uploadFileComplete',
    UPLOAD_FILE_ERROR: 'Model.ImageGalleryItem.uploadFileError',

    READ_FILE_UPLOAD_PROGRESS_COMPLETE: 'Model.ImageGalleryItem.readFileUploadProgressComplete',
    READ_FILE_UPLOAD_PROGRESS_ERROR: 'Model.ImageGalleryItem.readFileUploadProgressError',

    PHOTO_SELECTED: 'Model.ImageGalleryItem.PHOTO_SELECTED'

});
