CLASS.createNamespace('Events.Model.ShippingCharge', {

    VALIDATION_ERROR: 'Model.ShippingCharge.validationError',

    CREATE_COMPLETE: 'Model.ShippingCharge.createComplete',
    CREATE_ERROR: 'Model.ShippingCharge.createError',

    READ_COMPLETE: 'Model.ShippingCharge.readComplete',
    READ_ERROR: 'Model.ShippingCharge.readError',

    UPDATE_COMPLETE: 'Model.ShippingCharge.updateComplete',
    UPDATE_ERROR: 'Model.ShippingCharge.updateError',

    PATCH_COMPLETE: 'Model.ShippingCharge.patchComplete',
    PATCH_ERROR: 'Model.ShippingCharge.patchError',

    DELETE_COMPLETE: 'Model.ShippingCharge.deleteComplete',
    DELETE_ERROR: 'Model.ShippingCharge.deleteError',

    READ_FOR_SELECT_COMPLETE: 'Model.ShippingCharge.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Model.ShippingCharge.readForSelectError'
});
