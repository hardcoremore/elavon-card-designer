CLASS.createNamespace('Events.Model.OrderItemDetails', {

    UPDATE_COMPLETE: 'Model.OrderItemDetails.updateComplete',
    UPDATE_ERROR: 'Model.OrderItemDetails.updateError',
});
