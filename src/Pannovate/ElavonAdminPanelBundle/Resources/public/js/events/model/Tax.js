CLASS.createNamespace('Events.Model.Tax', {

    VALIDATION_ERROR: 'Model.Tax.validationError',

    CREATE_COMPLETE: 'Model.Tax.createComplete',
    CREATE_ERROR: 'Model.Tax.createError',

    READ_COMPLETE: 'Model.Tax.readComplete',
    READ_ERROR: 'Model.Tax.readError',

    UPDATE_COMPLETE: 'Model.Tax.updateComplete',
    UPDATE_ERROR: 'Model.Tax.updateError',

    PATCH_COMPLETE: 'Model.Tax.patchComplete',
    PATCH_ERROR: 'Model.Tax.patchError',

    DELETE_COMPLETE: 'Model.Tax.deleteComplete',
    DELETE_ERROR: 'Model.Tax.deleteError',

    READ_FOR_SELECT_COMPLETE: 'Model.Tax.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Model.Tax.readForSelectError'
});
