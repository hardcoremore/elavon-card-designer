CLASS.createNamespace('Events.Model.ImageGalleryCategory', {

    VALIDATION_ERROR: 'Model.ImageGalleryCategory.validationError',

    CREATE_COMPLETE: 'Model.ImageGalleryCategory.createComplete',
    CREATE_ERROR: 'Model.ImageGalleryCategory.createError',

    READ_COMPLETE: 'Model.ImageGalleryCategory.readComplete',
    READ_ERROR: 'Model.ImageGalleryCategory.readError',

    UPDATE_COMPLETE: 'Model.ImageGalleryCategory.updateComplete',
    UPDATE_ERROR: 'Model.ImageGalleryCategory.updateError',

    PATCH_COMPLETE: 'Model.ImageGalleryCategory.patchComplete',
    PATCH_ERROR: 'Model.ImageGalleryCategory.patchError',

    DELETE_COMPLETE: 'Model.ImageGalleryCategory.deleteComplete',
    DELETE_ERROR: 'Model.ImageGalleryCategory.deleteError',

    READ_FOR_SELECT_COMPLETE: 'Model.ImageGalleryCategory.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Model.ImageGalleryCategory.readForSelectError',

    READ_ALL_COMPLETE: 'Model.ImageGalleryCategory.readAllComplete'
});
