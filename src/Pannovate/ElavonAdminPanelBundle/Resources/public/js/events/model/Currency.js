CLASS.createNamespace('Events.Model.Currency', {

    VALIDATION_ERROR: 'Model.Currency.validationError',

    CREATE_COMPLETE: 'Model.Currency.createComplete',
    CREATE_ERROR: 'Model.Currency.createError',

    READ_COMPLETE: 'Model.Currency.readComplete',
    READ_ERROR: 'Model.Currency.readError',

    READ_ALL_COMPLETE: 'Model.Currency.readAllComplete',
    READ_ALL_ERROR: 'Model.Currency.readAllError',

    UPDATE_COMPLETE: 'Model.Currency.updateComplete',
    UPDATE_ERROR: 'Model.Currency.updateError',

    PATCH_COMPLETE: 'Model.Currency.patchComplete',
    PATCH_ERROR: 'Model.Currency.patchError',

    DELETE_COMPLETE: 'Model.Currency.deleteComplete',
    DELETE_ERROR: 'Model.Currency.deleteError',

    READ_FOR_SELECT_COMPLETE: 'Model.Currency.readForSelectComplete',
    READ_FOR_SELECT_ERROR: 'Model.Currency.readForSelectError',

    READ_BASE_CURRENCY_COMPLETE: 'Model.Currency.readBaseCurrencyComplete',
    READ_BASE_CURRENCY_ERROR: 'Model.Currency.readBaseCurrencyError' 
});
