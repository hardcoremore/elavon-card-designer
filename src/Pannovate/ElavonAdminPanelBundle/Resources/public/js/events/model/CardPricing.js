CLASS.createNamespace('Events.Model.CardPricing', {

    VALIDATION_ERROR: 'Model.CardPricing.validationError',

    CREATE_COMPLETE: 'Model.CardPricing.createComplete',
    CREATE_ERROR: 'Model.CardPricing.createError',

    READ_COMPLETE: 'Model.CardPricing.readComplete',
    READ_ERROR: 'Model.CardPricing.readError',

    READ_ALL_COMPLETE: 'Model.CardPricing.readAllComplete',
    READ_ALL_ERROR: 'Model.CardPricing.readAllError',

    UPDATE_COMPLETE: 'Model.CardPricing.updateComplete',
    UPDATE_ERROR: 'Model.CardPricing.updateError',

    PATCH_COMPLETE: 'Model.CardPricing.patchComplete',
    PATCH_ERROR: 'Model.CardPricing.patchError',

    DELETE_COMPLETE: 'Model.CardPricing.deleteComplete',
    DELETE_ERROR: 'Model.CardPricing.deleteError',
});
