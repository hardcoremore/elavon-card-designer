CLASS.define({

    name: 'Modules.SystemActions',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        this.getModuleName = function() {
            return 'SystemActions';
        };
    },
    prototypeMethods: {

        setControls: function() {

            var customersGrid = $("#system-actions-table").grid({
                multiselect: false,
                autoLoad: false,
                localSort: true,
                cellEdit: true,
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true
                    },
                    {
                        header: 'Role',
                        id: 'role',
                        dataMap: 'role'
                    },
                    {
                        header: 'Email',
                        id: 'email',
                        dataMap: 'email'
                    },
                    {
                        header: 'IP',
                        id: 'ip',
                        dataMap: 'ip'
                    },
                    {
                        header: 'Time',
                        id: 'time',
                        dataMap: 'time'
                    },
                    {
                        header: 'Action',
                        id: 'action',
                        dataMap: 'action'
                    }
                ],
                data: [
                    {id: '123456', name: 'Zlaja', role: 'Administrator', email: 'zlatko@paneleven.com', ip: '178.148.23.2', time: '12/04/2015 @ 04:20:56', action: 'Changed order item no. #12 status to "Approved"'},
                    {id: '123456', name: 'Zlaja', role: 'Administrator', email: 'zlatko@paneleven.com', ip: '178.148.23.2', time: '12/04/2015 @ 04:20:56', action: 'Changed order item no. #12 status to "Approved"'},
                    {id: '123456', name: 'Zlaja', role: 'Administrator', email: 'zlatko@paneleven.com', ip: '178.148.23.2', time: '12/04/2015 @ 04:20:56', action: 'Changed order item no. #12 status to "Approved"'},
                    {id: '123456', name: 'Zlaja', role: 'Administrator', email: 'zlatko@paneleven.com', ip: '178.148.23.2', time: '12/04/2015 @ 04:20:56', action: 'Changed order item no. #12 status to "Approved"'},
                    {id: '123456', name: 'Zlaja', role: 'Administrator', email: 'zlatko@paneleven.com', ip: '178.148.23.2', time: '12/04/2015 @ 04:20:56', action: 'Changed order item no. #12 status to "Approved"'},
                    {id: '123456', name: 'Zlaja', role: 'Administrator', email: 'zlatko@paneleven.com', ip: '178.148.23.2', time: '12/04/2015 @ 04:20:56', action: 'Changed order item no. #12 status to "Approved"'},
                    {id: '123456', name: 'Zlaja', role: 'Administrator', email: 'zlatko@paneleven.com', ip: '178.148.23.2', time: '12/04/2015 @ 04:20:56', action: 'Changed order item no. #12 status to "Approved"'},
                    {id: '123456', name: 'Zlaja', role: 'Administrator', email: 'zlatko@paneleven.com', ip: '178.148.23.2', time: '12/04/2015 @ 04:20:56', action: 'Changed order item no. #12 status to "Approved"'},
                    {id: '123456', name: 'Zlaja', role: 'Administrator', email: 'zlatko@paneleven.com', ip: '178.148.23.2', time: '12/04/2015 @ 04:20:56', action: 'Changed order item no. #12 status to "Approved"'},
                ],

                onCellSelected: function(ev, ui) {
                    console.log("Cell selected: ", ui);
                },

                onSortColumnChange: function(ev, ui) {
                    console.log("Sort column change: ", ui);
                },

                onRowSelected: function(ev, ui) {
                    console.log("Row selected: ", ui);
                },

                onRowDoubleClick: function(ev, ui) {
                    console.log("Double click: ", ui);
                },

                onCellEdited: function(ev, ui) {
                    console.log('On cell edited: ');
                    console.log(ui);
                }
            });
        }
    },

    extendPrototypeFrom: 'Base.Module'
});