CLASS.define({

    name: 'Modules.CardPricing',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        this.FORM_SUBMITTED_EVENT = 'Events.Controller.CardPricing.formSubmitted';

        var cardPricingDataGrid;
        var createCardPricingButton;
        var createCardPricingForm;
        
        var cardPricingController;
        var cardPricingFormController;

        var cardPricingModel;
        var currencyModel;
        var currenciesList;

        var currencySelect;

        this.setCardPricingDataGrid = function(dataGrid) {
            cardPricingDataGrid = dataGrid;
        };

        this.getCardPricingDataGrid = function() {
            return cardPricingDataGrid;
        };

        this.setCreateCardPricingButton = function(button) {
            createCardPricingButton = button;
        };

        this.getCreateCardPricingButton = function() {
            return createCardPricingButton;
        };

        this.setCreateCardPricingForm = function(form) {
            createCardPricingForm = form;
        };

        this.getCreateCardPricingForm = function() {
            return createCardPricingForm;
        };

        this.setCurrenciesList = function(list) {
            currenciesList = list;
        };

        this.getCurrenciesList = function() {
            return currenciesList;
        };

        this.setCurrencySelect = function(select) {
            currencySelect = select;
        };

        this.getCurrencySelect = function() {
            return currencySelect;
        };

        /*-------------------------------------------
         *           MODELS get/set
         *------------------------------------------*/

        this.setCardPricingModel = function(model) {
            cardPricingModel = model;
        };

        this.getCardPricingModel = function() {
            return cardPricingModel;
        };

        this.setCurrencyModel = function(model) {
            currencyModel = model;
        };

        this.getCurrencyModel = function() {
            return currencyModel;
        };


        this.getModuleName = function() {
            return 'CardPricing';
        };


        /*-------------------------------------------
         *           CONTROLLERS get/set
         *------------------------------------------*/

        this.setCardPricingController = function(controller) {
            cardPricingController = controller;
        };

        this.getCardPricingController = function() {
            return cardPricingController;
        };

        this.setCardPricingFormController = function(formController) {
            cardPricingFormController = formController;
        };

        this.getCardPricingFormController = function() {
            return cardPricingFormController;
        };

    },
        
    prototypeMethods: {

        initModule: function() {
            
            /*------- DEFINE MODELS --------*/
            this.setCurrencyModel(this.getApp().getModel('Model.Currency'));
            this.setCardPricingModel(this.getApp().getModel('Model.CardPricing', [this.getCurrencyModel()]));

            /*------- INITIALIZE CONTROLLERS --------*/
            this.setCardPricingController(this.getApp().getController('Controllers.CardPricing', [this]));
            this.setCardPricingFormController(this.getApp().getController('Controllers.Form', [this, this.getCardPricingModel()]));

            this.getCardPricingFormController().setFormEvents({
                FORM_SUBMITTED_EVENT: this.FORM_SUBMITTED_EVENT
            });
        },

        startModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getCardPricingFormController());
            this.getApp().startController(this.getCardPricingController());
        },
        setControls: function() {
            
            var that = this;

            var cardPricingGrid = $("#card-pricing-table").grid({

                cellEditAjaxOptions: {
                    getUrlCallback: function(cellName, cellEditedValue, rowData) {
                        return that.getCardPricingModel().getUrl('/' + rowData.id);
                    },
                },

                multiselect: false,
                autoLoad: false,
                localSort: true,
                cellEdit: true,
                cellEditType: 'remote',
                model: [
                    {
                        header: 'Minimum Cards',
                        id: 'minimumCards',
                        dataMap: 'minimum_cards',
                        editable: true,
                    },
                    {
                        header: 'Maximum Cards',
                        id: 'maximumCards',
                        dataMap: 'maximum_cards',
                        editable: true,
                    },
                    {
                        header: 'Loyalty',
                        id: 'loyalty',
                        dataMap: 'loyalty',
                        editable: true
                    },
                    {
                        header: 'Gift',
                        id: 'gift',
                        dataMap: 'gift',
                        editable: true,
                    },
                    {
                        header: 'Promo',
                        id: 'promo',
                        dataMap: 'promo',
                        editable: true
                    }
                ],

                onCellSelected: function(ev, ui) {
                    console.log("Cell selected: ", ui);
                },

                onSortColumnChange: function(ev, ui) {
                    console.log("Sort column change: ", ui);
                },

                onRowSelected: function(ev, ui) {
                    console.log("Row selected: ", ui);
                },

                onRowDoubleClick: function(ev, ui) {
                    console.log("Double click: ", ui);
                },

                onCellEdited: function(ev, ui) {
                    console.log('On cell edited: ');
                    console.log(ui);
                }
            });
            
            this.setCreateCardPricingForm(this.getModuleElement().find('#create-card-pricing-form'));
            this.getCardPricingFormController().setFormContainer(this.getCreateCardPricingForm());

            this.setCardPricingDataGrid(cardPricingGrid);
            this.setCreateCardPricingButton(this.getModuleElement().find('#create-card-pricing-button'));

            this.setCurrenciesList(this.getModuleElement().find('#card-pricing-currencies-list'));

            var currencySelectElement = this.getCreateCardPricingForm().find('select[name="currency"]');
            var currencySelectBoxIt = currencySelectElement.selectBoxIt({
                // Uses the jQuery 'fadeIn' effect when opening the drop down
                showEffect: "fadeIn",

                // Sets the jQuery 'fadeIn' effect speed to 400 milleseconds
                showEffectSpeed: 400,

                // Uses the jQuery 'fadeOut' effect when closing the drop down
                hideEffect: "fadeOut",

                // Sets the jQuery 'fadeOut' effect speed to 400 milleseconds
                hideEffectSpeed: 400
            });

            this.setCurrencySelect(currencySelectBoxIt);
        }
    },

    extendPrototypeFrom: 'Base.Module'
});