CLASS.define({

    name: 'Modules.RecentOrders',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        var orderDetailsPanel;
        var orderListGrid;
        var orderDetailsListGrid;


        var orderDetailsData;

        var cardDesignOrderModel;

        var cardDesignOrderController;

        var orderTotalsDetailsContainer;
        var closeOrderDetailsPanelButton;
        
        this.setOrderDetailsPanel = function(panel) {
            orderDetailsPanel = panel;
        };

        this.getOrderDetailsPanel = function() {
            return orderDetailsPanel;
        };

        this.setCloseOrderDetailsPanelButton = function(button) {
            closeOrderDetailsPanelButton = button;
        };

        this.getCloseOrderDetailsPanelButton = function() {
            return closeOrderDetailsPanelButton;
        };

        this.setOrderDetailsData = function(data) {
            orderDetailsData = data;
        };

        this.getOrderDetailsData = function() {
            return orderDetailsData;
        };
        
        this.setOrderTotalsDetailsContainer = function(container) {
            orderTotalsDetailsContainer = container;
        };

        this.getOrderTotalsDetailsContainer = function() {
            return orderTotalsDetailsContainer;
        };

        this.setOrderListGrid = function(grid) {
            orderListGrid = grid;
        };

        this.getOrderListGrid = function() {
            return orderListGrid;
        };

        this.setOrderDetailsListGrid = function(grid) {
            orderDetailsListGrid = grid;
        };

        this.getOrderDetailsListGrid = function() {
            return orderDetailsListGrid;
        };

        this.setCardDesignOrderModel = function(model) {
            cardDesignOrderModel = model;
        };

        this.getCardDesignOrderModel = function() {
            return cardDesignOrderModel;
        };

        this.setCardDesignOrderController = function(controller) {
            cardDesignOrderController = controller;
        };

        this.getCardDesignOrderController = function() {
            return cardDesignOrderController;
        };

        this.getModuleName = function() {
            return 'RecentOrders';
        };
    },

    prototypeMethods: {

        initModule: function() {
            
            /*------- DEFINE MODELS --------*/
            this.setCardDesignOrderModel(this.getApp().getModel('Model.CardDesignOrder', [this]));
            
            /*------- INITIALIZE CONTROLLERS --------*/
             this.setCardDesignOrderController(this.getApp().getController('Controllers.CardDesignOrder', [this]));
        },

        startModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getCardDesignOrderController());

            this.getOrderDetailsPanel().hide();
        },

        setControls: function() {
            
            var that = this;            

            this.setOrderDetailsPanel(this.getModuleElement().find('.order-details-pannel'));
            this.setCloseOrderDetailsPanelButton(this.getModuleElement().find('#close-order-details-panel'));
            this.setOrderTotalsDetailsContainer(this.getModuleElement().find('.order-total-table'));

            var orderListGrid = $("#order-table").grid({
                url: this.getCardDesignOrderModel().getUrl('/list-all'),
                multiselect: false,
                autoLoad: true,
                localSort: true,
                cellEdit: true,
                model: [
                    {
                        header: 'Order No.',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'First Name',
                        id: 'firstName',
                        dataMap: 'user',
                        formatter: function(cellValue) {
                            if(cellValue.firstName) {
                                return cellValue.firstName;
                            }
                            else {
                                return 'n/a';
                            }
                        }
                    },
                    {
                        header: 'Last Name',
                        id: 'lastName',
                        dataMap: 'user',
                        formatter: function(cellValue) {
                            if(cellValue.lastName) {
                                return cellValue.lastName;
                            }
                            else {
                                return 'n/a';
                            }
                        }
                    },
                    {
                        header: 'Date',
                        id: 'date',
                        dataMap: 'createdDatetime'
                    },
                    {
                        header: 'Quantity',
                        id: 'quantity',
                        dataMap: 'cardDesignOrderItems',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                var quantity = 0;
                                for(var i = 0, len = cellValue.length; i < len; i++)
                                    
                                    quantity += cellValue[i].quantity;
                                    return quantity;
                            }
                            else {
                                return 0;
                            }
                        }
                    },
                    {
                        header: 'Amount',
                        id: 'amount',
                        dataMap: 'orderPriceTotal'
                    },
                    {
                        header: 'Order Status',
                        id: 'status',
                        dataMap: 'cardDesignOrderItems',
                        formatter: function(cellValue) {
                            if(cellValue[0]) {
                                return cellValue[0].status;
                            }
                            else {
                                return 'n/a';
                            }
                        }
                    },
                    {
                        header: 'Country',
                        id: 'country',
                        dataMap: 'user',
                        formatter: function(cellValue) {
                            if(cellValue.city) {
                                return cellValue.city;
                            }
                            else {
                                return 'n/a';
                            }
                        }
                    },
                    {
                        header: 'Order Action(s)',
                        id: 'action',
                        dataMap: 'action'
                    }
                ],

                onCellSelected: function(ev, ui) {
                    //console.log("Cell selected: ", ui);
                },

                onSortColumnChange: function(ev, ui) {
                    //console.log("Sort column change: ", ui);
                },

                onRowSelected: function(ev, ui) {
                    
                    that.getOrderDetailsListGrid().grid('removeAllRows', true);
                    that.setOrderDetailsData(ui.rowData);

                    //console.log(ui.rowData);

                    if(ui.rowData && ui.rowData.cardDesignOrderItems) {
                        for(var i = 0, len = ui.rowData.cardDesignOrderItems.length; i < len; i++) {
                            that.getOrderDetailsListGrid().grid('addRow', ui.rowData.cardDesignOrderItems[i]);
                        }
                        
                        that.addOrderTotals(ui.rowData);
                    }

                    that.getOrderDetailsPanel().show();

                },

                onRowDoubleClick: function(ev, ui) {
                    //console.log("Double click: ", ui);
                },

                onCellEdited: function(ev, ui) {
                    //console.log('On cell edited: ');
                    //console.log(ui);
                },
                
            });
            
            this.setOrderListGrid(orderListGrid);


            var orderItemsGrid = $("#order-items-table").grid({
                multiselect: false,
                autoLoad: false,
                localSort: true,
                cellEdit: true,
                model: [
                    {
                        header: 'Item Number.',
                        id: 'id',
                        dataMap: 'id',
                    },
                    {
                        header: 'Product Type',
                        id: 'type',
                        dataMap: 'type'
                    },
                    {
                        header: 'Quantity',
                        id: 'quantity',
                        dataMap: 'quantity'
                    },
                    {
                        header: 'Amount',
                        id: 'orderPriceTotal',
                        dataMap: 'orderPriceTotal',
                    },
                    {
                        header: 'Order Status',
                        id: 'status',
                        dataMap: 'status'
                    },
                    {
                        header: 'Item Total',
                        id: 'orderItemsCount',
                        dataMap: 'orderItemsCount',
                    },
                    {
                        header: 'Order Action(s)',
                        id: 'action',
                        dataMap: 'action'
                    }
                ],

                onCellSelected: function(ev, ui) {
                    //console.log("Cell selected: ", ui);
                },

                onSortColumnChange: function(ev, ui) {
                    //onsole.log("Sort column change: ", ui);
                },

                onRowSelected: function(ev, ui) {

                    var data = that.getOrderDetailsData();

                    var orderData = {
                        order: data,
                        orderItem: ui.rowData
                    };

                    that.getApp().getModuleController().loadModule('Modules.OrderItemDetails', orderData, true);
                },

                onRowDoubleClick: function(ev, ui) {
                    //console.log("Double click: ", ui);
                },

                onCellEdited: function(ev, ui) {
                    //console.log('On cell edited: ');
                    //console.log(ui);
                },
            });

            this.setOrderDetailsListGrid(orderItemsGrid);
        },

        addOrderTotals: function(data) {

            var container = this.getOrderTotalsDetailsContainer();
            var shippingChargeField = container.find('#shipping-charge');
            var totalTaxField = container.find('#total-tax');
            var totalOrderField = container.find('#total-order');

            shippingChargeField.text(data.shippingChargePriceTotal);
            totalTaxField.text(data.taxChargePriceTotal);
            totalOrderField.text(data.orderPriceTotal);
        }
    },

    extendPrototypeFrom: 'Base.Module'
});
