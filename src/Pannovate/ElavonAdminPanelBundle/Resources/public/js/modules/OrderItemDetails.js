CLASS.define({

    name: 'Modules.OrderItemDetails',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        var orderItemDetailsModel;
        var orderItemDetailsController;
        var addressInfoDiv;
        var billingInfoDiv;
        var shippingInfoDiv;
        var cardSideItemDiv;
        var approveButton;
        var rejectButton;

        this.setApproveButton = function(button) {
            approveButton = button;
        };

        this.getApproveButton = function() {
            return approveButton;
        };

        this.setRejectButton = function(button) {
            rejectButton = button;
        };

        this.getRejectButton = function() {
            return rejectButton;
        };

        this.getModuleName = function() {
            return 'OrderItemDetails';
        };

        this.setAddressInfoDiv = function(div) {
            addressInfoDiv = div;
        };

        this.getAddressInfoDiv = function() {
            return addressInfoDiv;
        };

        this.setBillingInfoDiv = function(div) {
            billingInfoDiv = div;
        };

        this.getBillingInfoDiv = function() {
            return billingInfoDiv;
        };

        this.setShippingInfoDiv = function(div) {
            shippingInfoDiv = div;
        };

        this.getShippingInfoDiv = function() {
            return shippingInfoDiv;
        };

        this.setCardSideItemDiv = function(div) {
            cardSideItemDiv = div;
        };

        this.getCardSideItemDiv = function() {
            return cardSideItemDiv;
        };

        /*-------------------------------------------
         *           MODELS get/set
         *------------------------------------------*/

        this.setOrderItemDetailsModel = function(model) {
            orderItemDetailsModel = model;
        };

        this.getOrderItemDetailsModel = function() {
            return orderItemDetailsModel;
        };

        /*-------------------------------------------
        *           CONTROLLERS get/set
        *------------------------------------------*/

        this.setOrderItemDetailsController = function(controller) {
            orderItemDetailsController = controller;
        };

        this.getOrderItemDetailsController = function() {
            return orderItemDetailsController;
        };
    },

    prototypeMethods: {

        initModule: function() {
            
            /*------- DEFINE MODELS --------*/
            this.setOrderItemDetailsModel(this.getApp().getModel('Model.OrderItemDetails'));

            /*------- INITIALIZE CONTROLLERS --------*/
            this.setOrderItemDetailsController(this.getApp().getController('Controllers.OrderItemDetails', [this]));
        },

        startModule: function() {
            
            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getOrderItemDetailsController());

            if(this.isObject(this.getModuleData())) {
                this.addOrderItemDetails(this.getModuleData());
            }

            this.setModuleData(null);
        },

        reinitializeModule: function(data, permissions) {

            this.callSuper('Base.Module', 'reinitializeModule', [data, permissions]);
            this.addOrderItemDetails(data);
        },

        setControls: function() {

            this.setAddressInfoDiv(this.getModuleElement().find('#address-info-div'));
            this.setBillingInfoDiv(this.getModuleElement().find('#billing-info-div'));
            this.setShippingInfoDiv(this.getModuleElement().find('#shipping-info-div'));
            this.setCardSideItemDiv(this.getModuleElement().find('#card-side-info-div'));
            this.setApproveButton(this.getModuleElement().find('#approve-button'));
            this.setRejectButton(this.getModuleElement().find('#reject-button'));
        },

        addOrderItemDetails: function(itemData) {

            var addressInfoTemplate = 'address-info-item-template';
            var billingInfoTemplate = 'billing-info-item-template';
            var shippingInfoTemplate = 'shipping-info-item-template';
            var cardSideItemTemplate = 'card-site-item-template';

            var addressInfoCompiledItem = this.getApp().compileTemplate(addressInfoTemplate, itemData);
            var billingInfoCompiledItem = this.getApp().compileTemplate(billingInfoTemplate, itemData);
            var shippingInfoCompiledItem = this.getApp().compileTemplate(shippingInfoTemplate, itemData);
            var cardSideInfoCompiledItem = this.getApp().compileTemplate(cardSideItemTemplate, itemData);

            this.getAddressInfoDiv().empty().append(addressInfoCompiledItem);
            this.getBillingInfoDiv().empty().append(billingInfoCompiledItem);
            this.getShippingInfoDiv().empty().append(shippingInfoCompiledItem);
            this.getCardSideItemDiv().empty().append(cardSideInfoCompiledItem);

            $("#order-item-header").text('Order no.' + itemData.order.id +', Item no. ' +itemData.orderItem.id);
        }
    },

    extendPrototypeFrom: 'Base.Module'
});