CLASS.define({

    name: 'Modules.TaxSettings',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        this.FORM_SUBMITTED_EVENT = 'Events.Controller.TaxSettings.formSubmitted';

        var taxSettingsDataGrid;
        var createTaxButton;
        var createTaxForm;

        var taxModel;
        var countryModel;

        this.setTaxDataGrid = function(dataGrid) {
            taxSettingsDataGrid = dataGrid;
        };

        this.getTaxDataGrid = function() {
            return taxSettingsDataGrid;
        };

        this.setCreateTaxButton = function(button) {
            createTaxButton = button;
        };

        this.getCreateTaxButton = function() {
            return createTaxButton;
        };

        this.setCreateTaxForm = function(form) {
            createTaxForm = form;
        };

        this.getCreateTaxForm = function() {
            return createTaxForm;
        };

        /*-------------------------------------------
         *           MODELS get/set
         *------------------------------------------*/

        this.setTaxModel = function(model) {
            taxModel = model;
        };

        this.getTaxModel = function() {
            return taxModel;
        };

        this.setCountryModel = function(model) {
            countryModel = model;
        };

        this.getCountryModel = function() {
            return countryModel;
        };


        this.getModuleName = function() {
            return 'TaxSettings';
        };


        /*-------------------------------------------
         *           CONTROLLERS get/set
         *------------------------------------------*/

        this.setTaxController = function(controller) {
            taxController = controller;
        };

        this.getTaxController = function() {
            return taxController;
        };

        this.setTaxFormController = function(formController) {
            taxFormController = formController;
        };

        this.getTaxFormController = function() {
            return taxFormController;
        };

    },

    prototypeMethods: {

         initModule: function() {
            
            /*------- DEFINE MODELS --------*/
            this.setCountryModel(this.getApp().getModel('Model.Country'));
            this.setTaxModel(this.getApp().getModel('Model.Tax', [this.getCountryModel()]));
            

            /*------- INITIALIZE CONTROLLERS --------*/
            this.setTaxController(this.getApp().getController('Controllers.Tax', [this]));
            this.setTaxFormController(this.getApp().getController('Controllers.Form', [this, this.getTaxModel()]));

            this.getTaxFormController().setFormEvents({
                FORM_SUBMITTED_EVENT: this.FORM_SUBMITTED_EVENT
            });
        },

        startModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getTaxFormController());
            this.getApp().startController(this.getTaxController());
        },

        setControls: function() {

            var that = this;

            var taxGrid = $("#tax-data-table").grid({
                url: this.getTaxModel().getUrl('/list-all'),

                cellEditAjaxOptions: {
                    getUrlCallback: function(cellName, cellEditedValue, rowData) {
                        return that.getTaxModel().getUrl('/' + rowData.id);
                    },
                },

                autoLoad: true,
                cellEdit: true,
                cellEditType: 'remote',
                model: [
                    {
                        header: 'Tax Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true
                    },
                    {
                        header: 'Tax Amount',
                        id: 'amount',
                        dataMap: 'amount',
                        editable: true,
                        formatter: function(cellValue, rowData, rowIndex) {
                            return cellValue + "%";
                        },
                    },
                    {
                        header: 'Country',
                        id: 'country',
                        dataMap: 'country',
                        editable: true,
                        formatter: function(cellValue, rowData, rowIndex) {
                            return cellValue.name;
                        },

                        editOptions: {
                            type: 'select', // e.g. 'text', textarea, select, 'checkbox'
                            availableOptions: [],
                            valueProperty: 'id',
                            labelProperty: 'name',
                            initializeInputCallback: function(input) {

                                //input.parent().addClass('activeTdClass');

                                return input.selectBoxIt({
                                    // Uses the jQuery 'fadeIn' effect when opening the drop down
                                    showEffect: "fadeIn",

                                    // Sets the jQuery 'fadeIn' effect speed to 400 milleseconds
                                    showEffectSpeed: 400,

                                    // Uses the jQuery 'fadeOut' effect when closing the drop down
                                    hideEffect: "fadeOut",

                                    // Sets the jQuery 'fadeOut' effect speed to 400 milleseconds
                                    hideEffectSpeed: 400
                                });
                            }
                        }
                    },
                ],

                onCellSelected: function(ev, ui) {
                    console.log("Cell selected: ", ui);
                },

                onSortColumnChange: function(ev, ui) {
                    console.log("Sort column change: ", ui);
                },

                onRowSelected: function(ev, ui) {
                    console.log("Row selected: ", ui);
                },

                onRowDoubleClick: function(ev, ui) {
                    console.log("Double click: ", ui);
                },

                onCellEdited: function(ev, ui) {
                    console.log('On cell edited: ');
                    console.log(ui);
                }
            });
    
            this.setCreateTaxForm(this.getModuleElement().find('#create-tax-form'));
            this.getTaxFormController().setFormContainer(this.getCreateTaxForm());

            this.setTaxDataGrid(taxGrid);
            this.setCreateTaxButton(this.getModuleElement().find('#create-tax-button'));

            var countrySelectElement = this.getTaxFormController().getFormElementByName('country');
            var countrySelectBoxIt = countrySelectElement.selectBoxIt({
                // Uses the jQuery 'fadeIn' effect when opening the drop down
                showEffect: "fadeIn",

                // Sets the jQuery 'fadeIn' effect speed to 400 milleseconds
                showEffectSpeed: 400,

                // Uses the jQuery 'fadeOut' effect when closing the drop down
                hideEffect: "fadeOut",

                // Sets the jQuery 'fadeOut' effect speed to 400 milleseconds
                hideEffectSpeed: 400
            });
        }
    },

    extendPrototypeFrom: 'Base.Module'
});