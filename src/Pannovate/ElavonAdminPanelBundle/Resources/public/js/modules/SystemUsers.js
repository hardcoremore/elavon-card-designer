CLASS.define({

    name: 'Modules.SystemUsers',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        this.getModuleName = function() {
            return 'SystemUsers';
        };
    },
    prototypeMethods: {

        setControls: function() {

            var customersGrid = $("#system-users-table").grid({
                multiselect: false,
                autoLoad: false,
                localSort: true,
                cellEdit: true,
                model: [
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name'
                    },
                    {
                        header: 'Email',
                        id: 'email',
                        dataMap: 'email',
                        editable: true
                    },
                    {
                        header: 'City',
                        id: 'city',
                        dataMap: 'city'
                    },
                    {
                        header: 'Country',
                        id: 'country',
                        dataMap: 'country'
                    },
                    {
                        header: 'Last Active Date',
                        id: 'last_date',
                        dataMap: 'last_date'
                    },
                    {
                        header: 'Role',
                        id: 'role',
                        dataMap: 'role'
                    }
                ],

                onCellSelected: function(ev, ui) {
                    console.log("Cell selected: ", ui);
                },

                onSortColumnChange: function(ev, ui) {
                    console.log("Sort column change: ", ui);
                },

                onRowSelected: function(ev, ui) {
                    console.log("Row selected: ", ui);
                },

                onRowDoubleClick: function(ev, ui) {
                    console.log("Double click: ", ui);
                },

                onCellEdited: function(ev, ui) {
                    console.log('On cell edited: ');
                    console.log(ui);
                }
            });
        }
    },

    extendPrototypeFrom: 'Base.Module'
});