CLASS.define({

    name: 'Modules.ShippingCharge',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        this.FORM_SUBMITTED_EVENT = 'Events.Controller.ShippingCharge.formSubmitted';

        var ShippingChargeDataGrid;
        var createShippingChargeButton;
        var createShippingChargeForm;

        var shippingChargeModel;
        var countryModel;

        var countrySelect;

        this.setShippingChargeDataGrid = function(dataGrid) {
            ShippingChargeDataGrid = dataGrid;
        };

        this.getShippingChargeDataGrid = function() {
            return ShippingChargeDataGrid;
        };

        this.setCreateShippingChargeButton = function(button) {
            createShippingChargeButton = button;
        };

        this.getCreateShippingChargeButton = function() {
            return createShippingChargeButton;
        };

        this.setCreateShippingChargeForm = function(form) {
            createShippingChargeForm = form;
        };

        this.getCreateShippingChargeForm = function() {
            return createShippingChargeForm;
        };

        this.setCountrySelect = function(select) {
            countrySelect = select;
        };

        this.getCountrySelect = function() {
            return countrySelect;
        };

        /*-------------------------------------------
         *           MODELS get/set
         *------------------------------------------*/

        this.setShippingChargeModel = function(model) {
            shippingChargeModel = model;
        };

        this.getShippingChargeModel = function() {
            return shippingChargeModel;
        };

        this.setCountryModel = function(model) {
            countryModel = model;
        };

        this.getCountryModel = function() {
            return countryModel;
        };


        this.getModuleName = function() {
            return 'ShippingCharge';
        };


        /*-------------------------------------------
         *           CONTROLLERS get/set
         *------------------------------------------*/

        this.setShippingChargeController = function(controller) {
            shippingChargeController = controller;
        };

        this.getShippingChargeController = function() {
            return shippingChargeController;
        };

        this.setShippingChargeFormController = function(formController) {
            shippingChargeFormController = formController;
        };

        this.getShippingChargeFormController = function() {
            return shippingChargeFormController;
        };

    },

    prototypeMethods: {

         initModule: function() {
            
            /*------- DEFINE MODELS --------*/
            this.setCountryModel(this.getApp().getModel('Model.Country'));
            this.setShippingChargeModel(this.getApp().getModel('Model.ShippingCharge', [this.getCountryModel()]));
            

            /*------- INITIALIZE CONTROLLERS --------*/
            this.setShippingChargeController(this.getApp().getController('Controllers.ShippingCharge', [this]));
            this.setShippingChargeFormController(this.getApp().getController('Controllers.Form', [this, this.getShippingChargeModel()]));

            this.getShippingChargeFormController().setFormEvents({
                FORM_SUBMITTED_EVENT: this.FORM_SUBMITTED_EVENT
            });
        },

        startModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getShippingChargeFormController());
            this.getApp().startController(this.getShippingChargeController());
        },

        setControls: function() {

            var that = this;

            var shippingChargeGrid = $("#shipping-charge-table").grid({
                url: this.getShippingChargeModel().getUrl('/list-all'),

                cellEditAjaxOptions: {
                    getUrlCallback: function(cellName, cellEditedValue, rowData) {
                        return that.getShippingChargeModel().getUrl('/' + rowData.id);
                    },
                },

                autoLoad: true,
                cellEdit: true,
                cellEditType: 'remote',
                model: [
                    {
                        header: 'Country',
                        id: 'country',
                        dataMap: 'country',
                        editable: true,
                        formatter: function(cellValue, rowData, rowIndex) {
                            return cellValue.name;
                        },

                        editOptions: {
                            type: 'select',
                            availableOptions: [],
                            valueProperty: 'id',
                            labelProperty: 'name',
                            initializeInputCallback: function(input) {
                                return input.selectBoxIt({
                                    // Uses the jQuery 'fadeIn' effect when opening the drop down
                                    showEffect: "fadeIn",

                                    // Sets the jQuery 'fadeIn' effect speed to 400 milleseconds
                                    showEffectSpeed: 400,

                                    // Uses the jQuery 'fadeOut' effect when closing the drop down
                                    hideEffect: "fadeOut",

                                    // Sets the jQuery 'fadeOut' effect speed to 400 milleseconds
                                    hideEffectSpeed: 400
                                });
                            }
                        }
                    },
                    {
                        header: 'Amount',
                        id: 'amount',
                        dataMap: 'amount',
                        editable: true
                    },
                   
                ],

                onCellSelected: function(ev, ui) {
                    console.log("Cell selected: ", ui)
                },

                onSortColumnChange: function(ev, ui) {
                    console.log("Sort column change: ", ui)
                },

                onRowSelected: function(ev, ui) {
                    console.log("Row selected: ", ui)
                },

                onRowDoubleClick: function(ev, ui) {
                    console.log("Double click: ", ui);
                },

                onCellEdited: function(ev, ui) {
                    console.log('On cell edited: ');
                    console.log(ui);
                }
            });
    
            this.setCreateShippingChargeForm(this.getModuleElement().find('#create-shipping-charge-form'));
            this.getShippingChargeFormController().setFormContainer(this.getCreateShippingChargeForm());

            this.setShippingChargeDataGrid(shippingChargeGrid);
            this.setCreateShippingChargeButton(this.getModuleElement().find('#create-shipping-charge-button'));

            var countrySelectElement = this.getCreateShippingChargeForm().find('select[name="country"]');
            var countrySelectBoxIt = countrySelectElement.selectBoxIt({
                // Uses the jQuery 'fadeIn' effect when opening the drop down
                showEffect: "fadeIn",

                // Sets the jQuery 'fadeIn' effect speed to 400 milleseconds
                showEffectSpeed: 400,

                // Uses the jQuery 'fadeOut' effect when closing the drop down
                hideEffect: "fadeOut",

                // Sets the jQuery 'fadeOut' effect speed to 400 milleseconds
                hideEffectSpeed: 400
            });

            this.setCountrySelect(countrySelectBoxIt);
        }
    },

    extendPrototypeFrom: 'Base.Module'
});