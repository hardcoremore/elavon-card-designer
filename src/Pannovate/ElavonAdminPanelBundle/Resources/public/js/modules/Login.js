CLASS.define({

    name: 'Modules.Login',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        var loginForm;
        var loginButton;
        var loginFormErrorMessageContainer;

        var authenticationController;
        var userModel;

        this.setLoginForm = function(form){
            loginForm = form;
        };

        this.getLoginForm = function(){
            return loginForm;
        };

        this.setLoginButton = function(button){
            loginButton = button;
        };

        this.getLoginButton = function(){
            return loginButton;
        };

        this.setLoginFormErrorMessageContainer = function(errorMessageCnt){
            loginFormErrorMessageContainer = errorMessageCnt;
        };

        this.getLoginFormErrorMessageContainer = function(){
            return loginFormErrorMessageContainer;
        };

        /*----------------  END OF MODELS get/set  ------------------- */


        /*-------------------------------------------
         *           CONTROLLERS get/set
         *------------------------------------------*/
        this.setAuthenticationController = function(authController) {
            authenticationController = authController;
        };

        this.getAuthenticationController = function() {
            return authenticationController;
        };
        

        this.getModuleName = function() {
            return 'Login';
        };
    },

    prototypeMethods: {

        initModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'initModule');

            /*------- INITIALIZE CONTROLLERS --------*/
            this.setAuthenticationController(this.getApp().getController('Controllers.Authentication', [this]));
        },

        startModule: function() {
            this.callSuper('Base.Module', 'startModule');
            this.getApp().startController(this.getAuthenticationController());
        },

        setControls: function() {
            this.callSuper('Base.Module', 'setControls');
            this.setLoginForm(this.getModuleElement().find('#login-form'));
            this.setLoginButton(this.getModuleElement().find('#login-button'));
            this.setLoginFormErrorMessageContainer(this.getModuleElement().find('#login-form-error-message-container'));            
        }
    },

    extendPrototypeFrom: 'Base.Module'
});
