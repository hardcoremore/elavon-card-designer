CLASS.define({

    name: 'Modules.Customer',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        var customerDetailsController;

        var customerDetailsModel;
        var cardDesignOrderModel;

        var customerDetailsDiv;
        var recentDesignsDiv;
        var customerNameElement;

        this.getModuleName = function() {
            return 'Customer';
        };

        this.setCustomerDetailsDiv = function(div) {
            customerDetailsDiv = div;
        };

        this.getCustomerDetailsDiv = function() {
            return customerDetailsDiv;
        };

        this.setRecentDesignsDiv = function(div) {
            recentDesignsDiv = div;
        };

        this.getRecentDesignsDiv = function() {
            return recentDesignsDiv;
        };

        /*-------------------------------------------
         *           MODELS get/set
         *------------------------------------------*/

        this.setCustomerDetailsModel = function(model) {
            customerDetailsModel = model;
        };

        this.getCustomerDetailsModel = function() {
            return customerDetailsModel;
        };

        this.setCardDesignOrderModel = function(model) {
            cardDesignOrderModel = model;
        };

        this.getCardDesignOrderModel = function() {
            return cardDesignOrderModel;
        };

        /*-------------------------------------------
        *           CONTROLLERS get/set
        *------------------------------------------*/

        this.setCustomerDetailsController = function(controller) {
            customerDetailsController = controller;
        };

        this.getCustomerDetailsController = function() {
            return customerDetailsController;
        };
    },
    prototypeMethods: {

        initModule: function() {

            /*------- DEFINE MODELS --------*/
            this.setCustomerDetailsModel(this.getApp().getModel('Model.CustomerDetails', [this]));
            this.setCardDesignOrderModel(this.getApp().getModel('Model.CardDesignOrder', [this]));

            /*------- INITIALIZE CONTROLLERS --------*/
            this.setCustomerDetailsController(this.getApp().getController('Controllers.CustomerDetails', [this]));
        },

        startModule: function() {
            
            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getCustomerDetailsController());

            if(this.isObject(this.getModuleData())) {
                this.addCustomerDetails(this.getModuleData());
            }
            
            this.setModuleData(null);
        },

        reinitializeModule: function(data, permissions) {

            this.callSuper('Base.Module', 'reinitializeModule', [data, permissions]);
            this.addCustomerDetails(data);
        },

        setControls: function() {
            
            var that = this;

            var customersGrid = $("#test-table-customer").grid({
                url: that.getCardDesignOrderModel().getUrl('/list-all'),
                multiselect: false,
                autoLoad: true,
                localSort: true,
                cellEdit: true,
                model: [
                    {
                        header: 'Order no.',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Date',
                        id: 'date',
                        dataMap: 'user',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.createdDatetime;
                            }
                            else {
                                return 'n/a';
                            }
                        }
                    },
                    {
                        header: 'Order Items',
                        id: 'order_items',
                        dataMap: 'cardDesignOrderItems',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                var itemType = '';
                                for(var i = 0, len = cellValue.length; i < len; i++) {
                                    itemType += cellValue[i].cardDesign.type + ' ,';
                                }
                                var comma = itemType.lastIndexOf(' ,');
                                return itemType.substring(0, comma);
                            }
                            else {
                                return 'n/a';
                            }
                        }
                    },
                    {
                        header: 'Notes',
                        id: 'notes',
                        dataMap: 'notes',
                        editable: true,
                    },
                    {
                        header: 'Status',
                        id: 'status',
                        dataMap: 'cardDesignOrderItems',
                        formatter: function(cellValue) {
                            var itemStatus;
                            for(var i = 0, len = cellValue.length; i < len; i++) {
                                if(cellValue[i].status === 'pending_approval') {
                                    itemStatus = 'pending_approval';
                                }
                            }
                            return itemStatus;
                        }
                    },
                    {
                        header: 'Total',
                        id: 'total',
                        dataMap: 'orderPriceTotal'
                    }
                ],

                onCellSelected: function(ev, ui) {
                    console.log("Cell selected: ", ui);
                },

                onSortColumnChange: function(ev, ui) {
                    console.log("Sort column change: ", ui);
                },

                onRowSelected: function(ev, ui) {
                    console.log("Row selected: ", ui);
                },

                onRowDoubleClick: function(ev, ui) {
                    console.log("Double click: ", ui);
                },

                onCellEdited: function(ev, ui) {
                    console.log('On cell edited: ');
                    console.log(ui);
                }
            });

            that.setCustomerDetailsDiv(that.getModuleElement().find('#customer-details'));
            that.setRecentDesignsDiv(that.getModuleElement().find('#recent-designs'));
        },

        addCustomerDetails: function(itemData) {

            var templateId = 'customer-details-item-template';
            var compiledItem = this.getApp().compileTemplate(templateId, itemData);

            this.getCustomerDetailsDiv().append(compiledItem);

            $("#customer-name").text(itemData.firstName + ' ' + itemData.lastName);
        }
    },

    extendPrototypeFrom: 'Base.Module'
});