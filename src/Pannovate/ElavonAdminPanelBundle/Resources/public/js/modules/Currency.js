CLASS.define({

    name: 'Modules.Currency',

    definition: function(app, permissions) {

        this.extendFromClass('Base.Module', [app, permissions]);

        this.FORM_SUBMITTED_EVENT = 'Events.Controller.Currency.formSubmitted';

        var currenciesDataGrid;
        var createCurrencyButton;
        var createCurrencyForm;

        var setCurrencyAsBaseButton;
        var changeCurrencyConfirmPopup;

        var currencyModel;
        var currencyFormController;

        var countrySelect;

        this.setCurrenciesDataGrid = function(dataGrid) {
            currenciesDataGrid = dataGrid;
        };

        this.getCurrenciesDataGrid = function() {
            return currenciesDataGrid;
        };

        this.setCreateCurrencyButton = function(button) {
            createCurrencyButton = button;
        };

        this.getCreateCurrencyButton = function() {
            return createCurrencyButton;
        };

        this.setSetCurrencyAsBaseButton = function(button) {
            setCurrencyAsBaseButton = button;
        };

        this.getSetCurrencyAsBaseButton = function() {
            return setCurrencyAsBaseButton;
        };

        this.setCreateCurrencyForm = function(currencyForm) {
            createCurrencyForm = currencyForm;
        };

        this.getCreateCurrencyForm = function() {
            return createCurrencyForm;
        };

        this.setChangeCurrencyConfirmPopup = function(popup) {
            changeCurrencyConfirmPopup = popup;
        };

        this.getChangeCurrencyConfirmPopup = function() {
            return changeCurrencyConfirmPopup;
        };

        this.setCountrySelect = function(select) {
            countrySelect = select;
        };

        this.getCountrySelect = function() {
            return countrySelect;
        };

        /*-------------------------------------------
         *           MODELS get/set
         *------------------------------------------*/

        this.setCurrencyModel = function(model) {
            currencyModel = model;
        };

        this.getCurrencyModel = function() {
            return currencyModel;
        };

        this.setCountryModel = function(model) {
            countryModel = model;
        };

        this.getCountryModel = function() {
            return countryModel;
        };

        /*-------------------------------------------
         *           CONTROLLERS get/set
         *------------------------------------------*/

        this.setCurrencyController = function(controller) {
            currencyController = controller;
        };

        this.getCurrencyController = function() {
            return currencyController;
        };

        this.setCurrencyFormController = function(formController) {
            currencyFormController = formController;
        };

        this.getCurrencyFormController = function() {
            return currencyFormController;
        };

        this.getModuleName = function() {
            return 'Currency';
        };
    },

    prototypeMethods: {

        initModule: function() {

            /*------- DEFINE MODELS --------*/
            this.setCountryModel(this.getApp().getModel('Model.Country'));
            this.setCurrencyModel(this.getApp().getModel('Model.Currency', [this.getCountryModel()]));


            /*------- INITIALIZE CONTROLLERS --------*/
            this.setCurrencyController(this.getApp().getController('Controllers.Currency', [this]));
            this.setCurrencyFormController(this.getApp().getController('Controllers.Form', [this, this.getCurrencyModel()]));


            this.getCurrencyFormController().setFormEvents({
                FORM_SUBMITTED_EVENT: this.FORM_SUBMITTED_EVENT
            });
        },

        startModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getCurrencyFormController());
            this.getApp().startController(this.getCurrencyController());
        },

        setControls: function() {

            var that = this;

            var currencyGrid = $("#currencies-data-grid").grid({
                url: this.getCurrencyModel().getUrl('/list-all'),

                cellEditAjaxOptions: {
                    getUrlCallback: function(cellName, cellEditedValue, rowData) {
                        return that.getCurrencyModel().getUrl('/' + rowData.id);
                    },
                },

                autoLoad: true,
                cellEdit: true,
                cellEditType: 'remote',
                model: [
                    {
                        header: 'ID',
                        id: 'id',
                        dataMap: 'id'
                    },
                    {
                        header: 'Name',
                        id: 'name',
                        dataMap: 'name',
                        editable: true,
                    },
                    {
                        header: 'Code',
                        id: 'code',
                        dataMap: 'code',
                        editable: true,
                    },
                    {
                        header: 'Symbol',
                        id: 'symbol',
                        dataMap: 'symbol',
                        editable: true
                    },
                    {
                        header: 'Rate',
                        id: 'rate',
                        dataMap: 'rate',
                        editable: true
                    }
                ],

                onCellSelected: function(ev, ui) {
                    console.log("Cell selected: ", ui);
                },

                onSortColumnChange: function(ev, ui) {
                    console.log("Sort column change: ", ui);
                },

                onRowSelected: function(ev, ui) {
                    console.log("Row selected: ", ui);
                },

                onRowDoubleClick: function(ev, ui) {
                    console.log("Double click: ", ui);
                },

                onCellEdited: function(ev, ui) {
                    console.log('On cell edited: ');
                    console.log(ui);
                }
            });

            this.setCurrenciesDataGrid(currencyGrid);

            this.setCreateCurrencyForm(this.getModuleElement().find('#create-currency-form'));
            this.getCurrencyFormController().setFormContainer(this.getCreateCurrencyForm());

            this.setCreateCurrencyButton(this.getModuleElement().find('#create-currency-button'));
            this.setSetCurrencyAsBaseButton(this.getModuleElement().find('#set-currency-as-base-button'));

            var changeBaseCurrencyPopupOptions = {
                compileTemplateFunction: this.getApp().compileTemplate,
                compileTemplateData: {
                    title: 'Do you really want to change base currency?',
                    okLabel: 'Yes',
                    cancelLabel: 'No'
                },

                onFormSubmitted: this.getCurrencyController().changeBaseCurrencyPopupSubmittedHandler
            };

            this.setChangeCurrencyConfirmPopup(this.getModuleElement().find("#change-base-currency-popup").panpopup(changeBaseCurrencyPopupOptions));

            var countryCelectElement = this.getCreateCurrencyForm().find('select[name="country"]');
            var countrySelectBoxIt = countryCelectElement.selectBoxIt({
                // Uses the jQuery 'fadeIn' effect when opening the drop down
                showEffect: "fadeIn",

                // Sets the jQuery 'fadeIn' effect speed to 400 milleseconds
                showEffectSpeed: 400,

                // Uses the jQuery 'fadeOut' effect when closing the drop down
                hideEffect: "fadeOut",

                // Sets the jQuery 'fadeOut' effect speed to 400 milleseconds
                hideEffectSpeed: 400
            });

            this.setCountrySelect(countrySelectBoxIt);
        },

        changeBaseCurrency: function(newBaseCurrency) {
            this.getCurrencyModel().patch(newBaseCurrency.id, {isBase: true}, {scope:this});
        }
    },

    extendPrototypeFrom: 'Base.Module'
});
