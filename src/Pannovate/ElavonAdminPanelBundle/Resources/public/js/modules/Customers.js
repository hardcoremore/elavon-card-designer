CLASS.define({

    name: 'Modules.Customers',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        var that = this;
        
        var customersController;
        var customersModel;

        /*-------------------------------------------
         *           MODELS get/set
         *------------------------------------------*/

        this.setCustomersModel = function(model) {
            customersModel = model;
        };

        this.getCustomersModel = function() {
            return customersModel;
        };

        this.getModuleName = function() {
            return 'Customers';
        };

        /*-------------------------------------------
         *           CONTROLLERS get/set
         *------------------------------------------*/

        this.setCustomersController = function(controller) {
            customersController = controller;
        };

        this.getCustomersController = function() {
            return customersController;
        };

    },

    prototypeMethods: {

        initModule: function() {
            
            /*------- DEFINE MODELS --------*/
            this.setCustomersModel(this.getApp().getModel('Model.Customers'));

            /*------- INITIALIZE CONTROLLERS --------*/
            this.setCustomersController(this.getApp().getController('Controllers.Customers', [this]));
        },

        startModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getCustomersController());
        },

        setControls: function() {

            var that = this;
            var customersGrid = $("#test-table-customers").grid({
                url: this.getApp().getUserModel().getUrl('/list-all'),
                multiselect: false,
                autoLoad: true,
                localSort: true,
                cellEdit: true,
                model: [
                    {
                        header: 'FFID no.',
                        id: 'id',
                        dataMap: 'username'
                    },
                    {
                        header: 'First Name',
                        id: 'firstName',
                        dataMap: 'firstName'
                    },
                    {
                        header: 'Last Name',
                        id: 'lastName',
                        dataMap: 'lastName'
                    },
                    {
                        header: 'E-mail',
                        id: 'email',
                        dataMap: 'email',
                        editable: true
                    },
                    {
                        header: 'City',
                        id: 'city',
                        dataMap: 'city'
                    },
                    {
                        header: 'Country',
                        id: 'country',
                        dataMap: 'country',
                        formatter: function(cellValue) {
                            if(cellValue) {
                                return cellValue.name;
                            }
                            else {
                                return 'n/a';
                            }
                        }
                    },
                    {
                        header: 'Last Activity Date',
                        id: 'date',
                        dataMap: 'date'
                    },
                    {
                        header: 'Role',
                        id: 'role',
                        dataMap: 'role'
                    }
                ],

                onCellSelected: function(ev, ui) {
                    //console.log("Cell selected: ", ui);
                },

                onSortColumnChange: function(ev, ui) {
                    console.log("Sort column change: ", ui);
                },

                onRowSelected: function(ev, ui) {
                    that.getApp().getModuleController().loadModule('Modules.Customer', ui.rowData, true);
                },

                onRowDoubleClick: function(ev, ui) {
                    console.log("Double click: ", ui);
                },

                onCellEdited: function(ev, ui) {
                    console.log('On cell edited: ');
                    console.log(ui);
                }
            });
        }
    },

    extendPrototypeFrom: 'Base.Module'
});
