CLASS.define({

    name: 'Modules.ImageGallery',

    definition: function(app, permissions) {

        this.extendFromClass('Base.Module', [app, permissions]);

        this.FORM_SUBMITTED_EVENT = 'Events.Controller.ImageGallery.formSubmitted';

        var createCategoryButton;
        var createCategoryForm;
        var imageCategyItemsUl;

        var imageGalleryCategoryModel;
        var imageGalleyItemModel;

        var imageGalleryController;
        var imageGalleryFormController;

        var currentImageGalleryCategoryId;

        this.setCreateCategoryButton = function(button) {
			createCategoryButton = button;
        };

        this.getCreateCategoryButton = function() {
			return createCategoryButton;
        };

        this.setCreateCategoryForm = function(form) {
			createCategoryForm = form;
        };

        this.getCreateCategoryForm = function() {
			return createCategoryForm;
        };

        this.setImageCategyItemsUl = function(ul) {
            imageCategyItemsUl = ul;
        };

        this.getImageCategyItemsUl = function() {
            return imageCategyItemsUl;
        };

        this.setCurrentImageGalleryCategoryId = function(id) {
            currentImageGalleryCategoryId = id;
        };

        this.getCurrentImageGalleryCategoryId = function() {
            return currentImageGalleryCategoryId;
        };

        /*-------------------------------------------
         *           MODELS get/set
         *------------------------------------------*/

        this.setImageGalleryCategoryModel = function(model) {
        	imageGalleryCategoryModel = model;
        };

        this.getImageGalleryCategoryModel = function() {
        	return imageGalleryCategoryModel;
        };

        this.setImageGalleyItemModel = function(model) {
        	imageGalleyItemModel = model;
        };

        this.getImageGalleyItemModel = function() {
        	return imageGalleyItemModel;
        };

        this.getModuleName = function() {
            return 'ImageGallery';
        };

        /*-------------------------------------------
         *           CONTROLLERS get/set
         *------------------------------------------*/

	    this.setImageGalleryController = function(controller) {
	    	imageGalleryController = controller;
	    };

	    this.getImageGalleryController = function() {
	    	return imageGalleryController;
	    };

	    this.setImageGalleryFormController = function(controller) {
	    	imageGalleryFormController = controller;
	    };

	    this.getImageGalleryFormController = function() {
	    	return imageGalleryFormController;
	    };
    },

    prototypeMethods: {

        initModule: function() {
            /*------- DEFINE MODELS --------*/
            this.setImageGalleryCategoryModel(this.getApp().getModel('Model.ImageGalleryCategory'));
            this.setImageGalleyItemModel(this.getApp().getModel('Model.ImageGalleryItem'));


            /*------- INITIALIZE CONTROLLERS --------*/
            this.setImageGalleryController(this.getApp().getController('Controllers.ImageGallery', [this]));
            this.setImageGalleryFormController(this.getApp().getController('Controllers.Form', [this, this.getImageGalleryCategoryModel()]));

            this.getImageGalleryFormController().setFormEvents({
                FORM_SUBMITTED_EVENT: this.FORM_SUBMITTED_EVENT
            });
        },

        startModule: function() {

            /*------- CALL PARENT METHOD --------*/
            this.callSuper('Base.Module', 'startModule');

            this.getApp().startController(this.getImageGalleryController());
            this.getApp().startController(this.getImageGalleryFormController());
        },

        setControls: function() {

            this.setCreateCategoryForm(this.getModuleElement().find('#create-image-category-form'));
            this.getImageGalleryFormController().setFormContainer(this.getCreateCategoryForm());

            this.setCreateCategoryButton(this.getModuleElement().find('#create-image-category-button'));
            this.setImageCategyItemsUl(this.getModuleElement().find('#image-category-items'));
        }
    },

    extendPrototypeFrom: 'Base.Module'
});
