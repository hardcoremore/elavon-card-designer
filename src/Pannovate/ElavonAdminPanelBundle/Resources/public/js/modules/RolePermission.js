CLASS.define({

    name: 'Modules.RolePermission',

    definition: function(app, permissions) {
        
        this.extendFromClass('Base.Module', [app, permissions]);

        this.getModuleName = function() {
            return 'RolePermission';
        };
    },
    prototypeMethods: {

        setControls: function() {

            var customersGrid = $("#role-table").grid({
                multiselect: false,
                autoLoad: false,
                localSort: true,
                cellEdit: true,
                model: [
                    {
                        header: 'Role',
                        id: 'role',
                        dataMap: 'role'
                    },
                    {
                        header: 'ManageProducts',
                        id: 'manage_products',
                        dataMap: 'manage_products'
                    },
                    {
                        header: 'Manage Orders',
                        id: 'manage_orders',
                        dataMap: 'manage_orders'
                    },
                    {
                        header: 'Manage Users',
                        id: 'manage_users',
                        dataMap: 'manage_users'
                    },
                    {
                        header: 'Manage Galleries',
                        id: 'manage_gal',
                        dataMap: 'manage_gal'
                    },
                    {
                        header: 'Manage Settings',
                        id: 'manage_set',
                        dataMap: 'manage_set'
                    },
                    {
                        header: 'Manage Accounts',
                        id: 'manage_acc',
                        dataMap: 'manage_acc'
                    },
                    {
                        header: 'Manage Price List',
                        id: 'manage_price',
                        dataMap: 'manage_price'
                    }
                ],
                data: [
                    {
                    	role: 'Administrator', 
                    	manage_orders: 'Yes',
                    	manage_products: 'Yes',
                    	manage_users: 'Yes',
                    	manage_gal: 'Yes',
                    	manage_set: 'Yes',
                    	manage_acc: 'Yes',
                    	manage_price: 'Yes'
                    },
                    {
                    	role: 'Marketing', 
                    	manage_orders: 'Yes',
                    	manage_products: 'No',
                    	manage_users: 'No',
                    	manage_gal: 'Yes',
                    	manage_set: 'No',
                    	manage_acc: 'No',
                    	manage_price: 'No'
                    },
                ],

                onCellSelected: function(ev, ui) {
                    console.log("Cell selected: ", ui);
                },

                onSortColumnChange: function(ev, ui) {
                    console.log("Sort column change: ", ui);
                },

                onRowSelected: function(ev, ui) {
                    console.log("Row selected: ", ui);
                },

                onRowDoubleClick: function(ev, ui) {
                    console.log("Double click: ", ui);
                },

                onCellEdited: function(ev, ui) {
                    console.log('On cell edited: ');
                    console.log(ui);
                }
            });
        }
    },

    extendPrototypeFrom: 'Base.Module'
});