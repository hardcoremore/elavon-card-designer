$.extend(CLASS.getNamespaceValue('Globals.Parameters'), true, {

    env: 'dev',

    baseUrl: "http://ukpr.creator.local/app_dev.php",
    apiBaseUrl: '/api/v1',
    moduleViewBaseUrl: '/client/module',
    defaultModule: 'RecentOrders',

    facebookAppId: '846749345441175',
    facebookAPIVersion: 'v2.4'

});