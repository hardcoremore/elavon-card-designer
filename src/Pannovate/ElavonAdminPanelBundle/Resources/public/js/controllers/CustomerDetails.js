CLASS.define({

    name: 'Controllers.CustomerDetails',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);

        var that = this;

        this.recentDesignsCompleteHandler = function(ev) {

            var templateId = 'recent-design-item-template';
            for(i = 0; i < Math.min(ev.responseData.length, 3); i++) {
                var compiledItem = that.getApp().compileTemplate(templateId, ev.responseData[i]);
                that.getModule().getRecentDesignsDiv().append(compiledItem);
            }
            $("#cutomer-name").text(ev.responseData.firstName);
        };
    },

    prototypeMethods: {

        init: function() {
            this.getModule().getCustomerDetailsModel().getCustomerRecentDesigns(this.getModule().getModuleData().id, {scope:this});
        },

        addEvents: function() {
            
            var moduleElement = this.getModule().getModuleElement();
            $(document).on(this.getModule().getCustomerDetailsModel().events.CUSTOMER_RECENT_DESIGNS_COMPLETE, this.recentDesignsCompleteHandler);
        },

        removeEvents: function() {
            
            var moduleElement = this.getModule().getModuleElement();
            $(document).off(this.getModule().getCustomerDetailsModel().events.CUSTOMER_RECENT_DESIGNS_COMPLETE, this.recentDesignsCompleteHandler);
        },

        startController: function() {

        }
    },

    extendPrototypeFrom: 'Base.Controller'
});