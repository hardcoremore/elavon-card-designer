CLASS.define({

    name: 'Controllers.Customers',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);
    },

    prototypeMethods: {

        addEvents: function() {
            
            var moduleElement = this.getModule().getModuleElement();
        },

        removeEvents: function() {
            
            var moduleElement = this.getModule().getModuleElement();
        },

        startController: function() {

        }
    },

    extendPrototypeFrom: 'Base.Controller'
});