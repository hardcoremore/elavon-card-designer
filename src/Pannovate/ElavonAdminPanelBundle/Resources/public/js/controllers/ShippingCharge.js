CLASS.define({

    name: 'Controllers.ShippingCharge',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);

        var that = this;

        this.readCountriesForSelectCompleteHandler = function(ev) {

            that.getModule()
                .getShippingChargeFormController()
                .populateRelationField('country', ev.responseData);

            that.getModule().getCountrySelect().selectBoxIt('refresh');
            that.getModule().getShippingChargeDataGrid().grid('setModelEditOptions', 'country', {availableOptions:ev.responseData});
        };

        this.readCountriesForSelectErrorHandler = function(ev) {
            that.getApp().showNotification('error', 'Countries could not be loaded from server.');
        };

        this.taxFormSubmittedEventHandler = function(ev) {

            if(ev.scope === 'create') {
                that.getModule().getShippingChargeModel().create(ev.formData, {scope:this});
            }
        };

        this.taxValidationErrorHandle = function(ev) {
            that.getModule().getShippingChargeFormController().displayFormErrors(ev.validationErrors);
        };

        this.taxCreateCompleteHandle = function(ev) {

            that.getModule().getShippingChargeDataGrid().grid('addRow', ev.responseData);
            that.getModule().getShippingChargeFormController().resetForm();

            that.getApp().showNotification('success', 'You have successfully created tax.');

        };

        this.taxCreateErrorHandle = function(ev) {
            that.getModule().getShippingChargeFormController().displayFormErrors(ev.error);
        };

        this.taxPatchCompleteHandler = function(ev) {
            that.getModule().getChangeShippingChargeConfirmPopup().panpopup('close');
            that.getModule().getShippingChargeDataGrid().grid('reloadGrid');
            that.getApp().showNotification('success', 'You have succesfully changed base tax');
        };

        this.taxPatchErrorHandle = function(ev) {

        };
    },

    prototypeMethods: {

        addEvents: function() {

            $(document).on(this.getModule().FORM_SUBMITTED_EVENT, this.taxFormSubmittedEventHandler);

            $(document).on(this.getModule().getShippingChargeModel().events.CREATE_COMPLETE, this.taxCreateCompleteHandle);
            $(document).on(this.getModule().getShippingChargeModel().events.CREATE_ERROR, this.taxCreateErrorHandle);

            $(document).on(this.getModule().getShippingChargeModel().events.PATCH_COMPLETE, this.taxPatchCompleteHandler);
            $(document).on(this.getModule().getShippingChargeModel().events.PATCH_ERROR, this.taxPatchErrorHandler);

            $(document).on(this.getModule().getShippingChargeModel().events.VALIDATION_ERROR, this.taxValidationErrorHandle);

            $(document).on(this.getModule().getCountryModel().events.READ_FOR_SELECT_COMPLETE, this.readCountriesForSelectCompleteHandler);
            $(document).on(this.getModule().getCountryModel().events.READ_FOR_SELECT_ERROR, this.readCountriesForSelectErroreHandler);
        },

        removeEvents: function() {

            $(document).off(this.getModule().FORM_SUBMITTED_EVENT, this.taxFormSubmittedEventHandler);

            $(document).off(this.getModule().getShippingChargeModel().events.CREATE_COMPLETE, this.taxCreateCompleteHandle);
            $(document).off(this.getModule().getShippingChargeModel().events.CREATE_ERROR, this.taxCreateErrorHandle);

            $(document).off(this.getModule().getShippingChargeModel().events.PATCH_COMPLETE, this.taxPatchCompleteHandler);
            $(document).off(this.getModule().getShippingChargeModel().events.PATCH_ERROR, this.taxPatchErrorHandler);

            $(document).off(this.getModule().getShippingChargeModel().events.VALIDATION_ERROR, this.taxValidationErrorHandle);

            $(document).off(this.getModule().getCountryModel().events.READ_FOR_SELECT_COMPLETE, this.readCountriesForSelectCompleteHandler);
            $(document).off(this.getModule().getCountryModel().events.READ_FOR_SELECT_ERROR, this.readCountriesForSelectErroreHandler);
        },

        startController: function() {
                
            var countrySelectData = this.getModule().getCountryModel().getSelectData();

            if(!countrySelectData) {
                this.getModule().getCountryModel().readForSelect({scope:this});
            }
            else {
                this.getModule().getShippingChargeFormController().populateRelationField('country', countrySelectData);
                this.getModule().getShippingChargeDataGrid().grid('setModelEditOptions', 'country', {availableOptions:countrySelectData});
            }
        }
    },

    extendPrototypeFrom: 'Base.Controller'
});