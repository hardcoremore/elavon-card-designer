CLASS.define({

    name: 'Controllers.CardDesignOrder',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);

        var that = this;

        this.cardDesignOrderReadAllCompleteHandler = function(ev) {
        };

        this.closeOrderDetailsPanelButtonClickHandler = function(ev) {
            that.getModule().getOrderDetailsPanel().hide();
        };
    },

    prototypeMethods: {

        addEvents: function() {
            
            var moduleElement = this.getModule().getModuleElement();

            moduleElement.find('#card-pricing-currencies-list').on('click', this.chooseCurrencyClickHandler);

            $(document).on(this.getModule().getCardDesignOrderModel().events.CREATE_COMPLETE, this.cardDesignOrderCreateCompleteHandle);
            $(document).on(this.getModule().getCardDesignOrderModel().events.CREATE_ERROR, this.cardDesignOrderCreateErrorHandle);
            $(document).on(this.getModule().getCardDesignOrderModel().events.READ_ALL_COMPLETE, this.cardDesignOrderReadAllCompleteHandler);
            $(document).on(this.getModule().getCardDesignOrderModel().events.READ_ALL_ERROR, this.cardDesignOrderReadAllErrorHandler);
            $(document).on(this.getModule().getCardDesignOrderModel().events.PATCH_COMPLETE, this.cardDesignOrderPatchCompleteHandler);
            $(document).on(this.getModule().getCardDesignOrderModel().events.PATCH_ERROR, this.cardDesignOrderPatchErrorHandler);
            $(document).on(this.getModule().getCardDesignOrderModel().events.VALIDATION_ERROR, this.cardDesignOrderValidationErrorHandle);

            this.getModule().getCloseOrderDetailsPanelButton().on('click', this.closeOrderDetailsPanelButtonClickHandler);
        },

        removeEvents: function() {
            
            var moduleElement = this.getModule().getModuleElement();

            moduleElement.find('#card-pricing-currencies-list').off('click', this.chooseCurrencyClickHandler);

            $(document).off(this.getModule().getCardDesignOrderModel().events.CREATE_COMPLETE, this.cardDesignOrderCreateCompleteHandle);
            $(document).off(this.getModule().getCardDesignOrderModel().events.CREATE_ERROR, this.cardDesignOrderCreateErrorHandle);
            $(document).off(this.getModule().getCardDesignOrderModel().events.PATCH_COMPLETE, this.cardDesignOrderPatchCompleteHandler);
            $(document).off(this.getModule().getCardDesignOrderModel().events.PATCH_ERROR, this.cardDesignOrderPatchErrorHandler);
            $(document).off(this.getModule().getCardDesignOrderModel().events.VALIDATION_ERROR, this.cardDesignOrderValidationErrorHandle);
        },

        startController: function() {
            this.getModule().getCardDesignOrderModel().readAll({scope:this});
        }
    },

    extendPrototypeFrom: 'Base.Controller'
});