CLASS.define({

    name: 'Controllers.ImageGallery',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);

        var that = this;

        this.imageGalleryCategoryFormSubmittedEventHandler = function(ev) {

            if(ev.scope === 'create') {
                that.getModule().getImageGalleryCategoryModel().create(ev.formData, {scope:this});
            }
        };

        this.imageGalleryCategoryValidationErrorHandle = function(ev) {
            that.getModule().getImageGalleryFormController().displayFormErrors(ev.validationErrors);
        };

        this.imageGalleryCategoryCreateCompleteHandle = function(ev) {

            that.getModule().getImageCategyItemsUl().append('<li id="'+ev.responseData.id+'">'+ev.responseData.name+'</li>');

            that.getApp().showNotification('success', 'You have successfully added new album category.');
        };

        this.imageGalleryReadAllCategoriesCompleteHandle = function(ev) {

            if(ev.responseData.length){
                for (i = 0; i < ev.responseData.length; i++){
                    that.getModule().getImageCategyItemsUl().append('<li id="'+ev.responseData[i].id+'">'+ev.responseData[i].name+'</li>');
                }

                that.getModule().getImageGalleryCategoryModel().getImagesByCategory(ev.responseData[0].id, {scope:this});
                $("#image-category-items").find("#"+ev.responseData[0].id).addClass('active');
                that.getModule().setCurrentImageGalleryCategoryId(ev.responseData[0].id);
            }
            
        };

        this.browseFileInputChangeEventHandler = function(ev) {
            
            var file = ev.target.files[0];
            
            var fileUrl = URL.createObjectURL(file);
            var categoryId = that.getModule().getCurrentImageGalleryCategoryId();
            that.getModule().getImageGalleyItemModel().uploadGalleryImage(file, {category: categoryId, name: 'My Image'}, fileUrl);
        };

        this.imageUploadCompleteEventHandler = function(ev) {
            console.log("Image upload complete");
            that.addImageItemToBankvaultGallery(ev.responseData.name, ev.responseData.id, ev.responseData.file_url_path);
        };

        this.imageUploadErrorEventHandler = function(ev) {
            console.log("Image upload error");
            console.log(ev.responseData);
        };

        this.imageCategoryItemUlClickHandler = function(ev) {

            that.getModule().getImageGalleryCategoryModel().getImagesByCategory(ev.target.id, {scope:this});
            that.getModule().setCurrentImageGalleryCategoryId(ev.target.id);

            $("#image-category-items").find("li").removeClass('active');
            $("#image-category-items").find("#"+ev.target.id).addClass('active');
        };

        this.addImageItemToBankvaultGallery = function(name, id, fileUrl) {
            
            html = that.getApp().compileTemplate(
                "album-photo-item-template",
                {
                    name: name,
                    photoId: id,
                    originalImage: fileUrl
                }
            );
            var item = $(html);
            $("#bankvault-category-images").append(item);
        }

        this.imagesReadCompleteEventHandler = function(ev) {
           
            $("#bankvault-category-images").empty();

            $.each(ev.responseData, function(index){
                that.addImageItemToBankvaultGallery(this.name, this.id, this.fileUrl);
            });
        }
    },

    prototypeMethods: {

        addEvents: function() {

            var moduleElement = this.getModule().getModuleElement();

            $(document).on(this.getModule().FORM_SUBMITTED_EVENT, this.imageGalleryCategoryFormSubmittedEventHandler);

            $(document).on(this.getModule().getImageGalleryCategoryModel().events.CREATE_COMPLETE, this.imageGalleryCategoryCreateCompleteHandle);
            $(document).on(this.getModule().getImageGalleryCategoryModel().events.CREATE_ERROR, this.imageGalleryCategoryCreateErrorHandle);
            $(document).on(this.getModule().getImageGalleryCategoryModel().events.PATCH_COMPLETE, this.imageGalleryCategoryPatchCompleteHandler);
            $(document).on(this.getModule().getImageGalleryCategoryModel().events.PATCH_ERROR, this.imageGalleryCategoryPatchErrorHandler);
            $(document).on(this.getModule().getImageGalleryCategoryModel().events.VALIDATION_ERROR, this.imageGalleryCategoryValidationErrorHandle);
            $(document).on(this.getModule().getImageGalleryCategoryModel().events.READ_ALL_COMPLETE, this.imageGalleryReadAllCategoriesCompleteHandle);
            $(document).on(this.getModule().getImageGalleryCategoryModel().events.READ_COMPLETE, this.imagesReadCompleteEventHandler);

            $(document).on(this.getModule().getImageGalleyItemModel().events.UPLOAD_FILE_COMPLETE, this.imageUploadCompleteEventHandler);
            $(document).on(this.getModule().getImageGalleyItemModel().events.UPLOAD_FILE_ERROR, this.imageUploadErrorEventHandler);
            

            this.getModule().getImageCategyItemsUl().on('click', this.imageCategoryItemUlClickHandler);

            moduleElement.find('.browse-file-input').on('change', this.browseFileInputChangeEventHandler);
        },

        removeEvents: function() {

            var moduleElement = this.getModule().getModuleElement();

            $(document).off(this.getModule().FORM_SUBMITTED_EVENT, this.imageGalleryCategoryFormSubmittedEventHandler);

            $(document).off(this.getModule().getImageGalleryCategoryModel().events.CREATE_COMPLETE, this.imageGalleryCategoryCreateCompleteHandle);
            $(document).off(this.getModule().getImageGalleryCategoryModel().events.CREATE_ERROR, this.imageGalleryCategoryCreateErrorHandle);
            $(document).off(this.getModule().getImageGalleryCategoryModel().events.PATCH_COMPLETE, this.imageGalleryCategoryPatchCompleteHandler);
            $(document).off(this.getModule().getImageGalleryCategoryModel().events.PATCH_ERROR, this.imageGalleryCategoryPatchErrorHandler);
            $(document).off(this.getModule().getImageGalleryCategoryModel().events.VALIDATION_ERROR, this.imageGalleryCategoryValidationErrorHandle);
            $(document).off(this.getModule().getImageGalleryCategoryModel().events.READ_ALL_COMPLETE, this.imageGalleryReadAllCategoriesCompleteHandle);
            $(document).off(this.getModule().getImageGalleryCategoryModel().events.READ_COMPLETE, this.imagesReadCompleteEventHandler);

            $(document).off(this.getModule().getImageGalleyItemModel().events.UPLOAD_FILE_COMPLETE, this.imageUploadCompleteEventHandler);
            $(document).off(this.getModule().getImageGalleyItemModel().events.UPLOAD_FILE_ERROR, this.imageUploadErrorEventHandler);
            

            this.getModule().getImageCategyItemsUl().off('click', this.imageCategoryItemUlClickHandler);

            moduleElement.find('.browse-file-input').off('change', this.browseFileInputChangeEventHandler);
        },

        startController: function() {

            this.getModule().getImageGalleryCategoryModel().readAll({scope: this});
        }
    },

    extendPrototypeFrom: 'Base.Controller'
});
