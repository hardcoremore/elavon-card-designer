CLASS.define({

    name: 'Controllers.CardPricing',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);

        var that = this;


        this.cardPricingFormSubmittedEventHandler = function(ev) {

            if(ev.scope === 'create') {
                that.getModule().getCardPricingModel().create(ev.formData, {scope:this});
            }
        };

        this.cardPricingValidationErrorHandle = function(ev) {
            console.log('Validation error');
            that.getModule().getCardPricingFormController().displayFormErrors(ev.validationErrors);
        };

        this.cardPricingCreateCompleteHandle = function(ev) {
            that.getModule().getCardPricingDataGrid().grid('addRow', ev.responseData);
            that.getModule().getCardPricingFormController().resetForm();

            that.getApp().showNotification('success', 'You have successfully created card pricing.');

        };

        this.cardPricingCreateErrorHandle = function(ev) {
            that.getModule().getCardPricingFormController().displayFormErrors(ev.error);
        };

        this.cardPricingPatchCompleteHandler = function(ev) {
            that.getModule().getChangeCardPricingConfirmPopup().panpopup('close');
            that.getModule().getCardPricingDataGrid().grid('reloadGrid');
            that.getApp().showNotification('success', 'You have succesfully changed base card pricing');
        };

        this.cardPricingPatchErrorHandle = function(ev) {

        };

        this.readBaseCurrencyCompleteHandler = function(ev) {
            that.getModule().getCurrencyModel().readForSelect({scope:this});

            that.getModule().getCardPricingDataGrid().grid(
                'option',
                'url',
                that.getModule().getCardPricingModel().getUrl('/list-for-currency/'+ ev.responseData.id)
            );

            that.getModule().getCardPricingDataGrid().grid('reloadGrid');
        };

        this.readCurrencyForSelectCompleteHandler = function(ev) {

            that.getModule()
                .getCardPricingFormController()
                .populateRelationField('currency', ev.responseData, that.getModule().getCurrencyModel().getBaseCurrency().id);

            that.getModule().getCurrencySelect().selectBoxIt('refresh');

            $.each(ev.responseData, function(index) {

                var liEl;

                if(this.isBase === true) {
                    liEl = $('<li/>', {
                        text: this.name + '(Base)',
                        'class': 'active',
                        'data-id': this.id
                    });
                }
                else {
                    liEl = $('<li/>', {
                        text: this.name,
                        'data-id': this.id
                    });
                }

                that.getModule().getCurrenciesList().append(liEl);
            });
        };

        this.chooseCurrencyClickHandler = function(ev) {

            var currencyId = $(ev.target).data('id');
            var currentCurrency = $(ev.target);
            var currencies = $(ev.currentTarget);
            
            currencies.children().each(function(){
                var element = $(this);

                if(element.hasClass('active')) {
                    element.removeClass('active');
                }
            });

            currentCurrency.addClass('active');
            
            that.getModule().getCardPricingDataGrid().grid(
                'option',
                'url',
                that.getModule().getCardPricingModel().getUrl('/list-for-currency/'+ currencyId)
            );

            that.getModule().getCardPricingDataGrid().grid('reloadGrid');
        };
    },

    prototypeMethods: {

        addEvents: function() {
            
            var moduleElement = this.getModule().getModuleElement();

            moduleElement.find('#card-pricing-currencies-list').on('click', this.chooseCurrencyClickHandler);

            $(document).on(this.getModule().FORM_SUBMITTED_EVENT, this.cardPricingFormSubmittedEventHandler);

            $(document).on(this.getModule().getCardPricingModel().events.CREATE_COMPLETE, this.cardPricingCreateCompleteHandle);
            $(document).on(this.getModule().getCardPricingModel().events.CREATE_ERROR, this.cardPricingCreateErrorHandle);

            $(document).on(this.getModule().getCurrencyModel().events.READ_BASE_CURRENCY_COMPLETE, this.readBaseCurrencyCompleteHandler);
            $(document).on(this.getModule().getCurrencyModel().events.READ_FOR_SELECT_COMPLETE, this.readCurrencyForSelectCompleteHandler);

            $(document).on(this.getModule().getCardPricingModel().events.PATCH_COMPLETE, this.cardPricingPatchCompleteHandler);
            $(document).on(this.getModule().getCardPricingModel().events.PATCH_ERROR, this.cardPricingPatchErrorHandler);

            $(document).on(this.getModule().getCardPricingModel().events.VALIDATION_ERROR, this.cardPricingValidationErrorHandle);
        },

        removeEvents: function() {
            
            var moduleElement = this.getModule().getModuleElement();

            moduleElement.find('#card-pricing-currencies-list').off('click', this.chooseCurrencyClickHandler);
            $(document).off(this.getModule().FORM_SUBMITTED_EVENT, this.cardPricingFormSubmittedEventHandler);

            $(document).off(this.getModule().getCardPricingModel().events.CREATE_COMPLETE, this.cardPricingCreateCompleteHandle);
            $(document).off(this.getModule().getCardPricingModel().events.CREATE_ERROR, this.cardPricingCreateErrorHandle);

            $(document).off(this.getModule().getCurrencyModel().events.READ_BASE_CURRENCY_COMPLETE, this.readBaseCurrencyCompleteHandler);
            $(document).off(this.getModule().getCurrencyModel().events.READ_FOR_SELECT_COMPLETE, this.readCurrencyForSelectCompleteHandler);

            $(document).off(this.getModule().getCardPricingModel().events.PATCH_COMPLETE, this.cardPricingPatchCompleteHandler);
            $(document).off(this.getModule().getCardPricingModel().events.PATCH_ERROR, this.cardPricingPatchErrorHandler);

            $(document).off(this.getModule().getCardPricingModel().events.VALIDATION_ERROR, this.cardPricingValidationErrorHandle);
        },

        startController: function() {
            this.getModule().getCurrencyModel().readBaseCurrency({scope:this});
        }
    },

    extendPrototypeFrom: 'Base.Controller'
});