CLASS.define({
    
    name: 'Controllers.Authentication',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);

        var that = this;

        this.loginCompleteEventHandler = function(ev) {
            that.startApplication();
        };

        this.loginErrorEventHandler = function(ev) {

            if(ev.error.code === 400) {
                that.getModule().getLoginFormErrorMessageContainer().text('Invalid username or password').show();
            }
            else{
                that.getApp().throwException("Error ocurred while trying to log in user.", 1500, 'kernel');
            }
        };

        this.logoutErrorEventHandler = function(ev) {
            console.log("LOGOUT ERROR HANDLER");
        };

        this.loginButtonClickHandler = function(ev) {

            that.getModule().getLoginFormErrorMessageContainer().hide();
            that.loginUser();
        };

        this.loginFormKeyDownHandler = function(ev) {
            
            // if enter is not pressed do not try to log in user
            if(ev && ev.type === 'keydown') {
                if(ev.keyCode !== $.ui.keyCode.ENTER) {
                    return;
                }
            }

            that.loginUser();
        }
    },

    prototypeMethods: {

        init: function() {

            var userModel = this.getApp().getUserModel();

            $(document).on(userModel.events.LOAD_CURRENT_USER_COMPLETE, this.loadCurrentUserCompleteHandler);
            $(document).on(userModel.events.LOAD_CURRENT_USER_ERROR, this.loadCurrentUserErrorHandler);

            $(document).on(userModel.events.LOGIN_COMPLETE, this.loginCompleteEventHandler);
            $(document).on(userModel.events.LOGIN_ERROR, this.loginErrorEventHandler);

            $(document).on(userModel.events.CREATE_COMPLETE, this.userCreateCompleteEventHandler);
            $(document).on(userModel.events.CREATE_ERROR, this.userCreateErrorEventHandler);

            $(document).on(userModel.events.LOGOUT_COMPLETE, this.logoutCompleteEventHandler);
            $(document).on(userModel.events.LOGOUT_ERROR, this.logoutErrorEventHandler);
        },

        addEvents: function() {
            this.getModule().getLoginButton().on('click', this.loginButtonClickHandler);
            this.getModule().getLoginButton().on('keydown', this.loginFormKeyDownHandler);
            this.getModule().getLoginForm().find('input').on('keydown', this.loginFormKeyDownHandler);
        },

        removeEvents: function() {
            this.getModule().getLoginButton().off('click', this.loginButtonClickHandler);
            this.getModule().getLoginButton().off('keydown', this.loginFormKeyDownHandler);
            this.getModule().getLoginForm().find('input').off('keydown', this.loginFormKeyDownHandler);
        },

        startApplication: function() {
            this.getModule().destroyModule();
            this.getApp().startApp();
        },

        loginUser: function() {

            var username = this.getModule().getLoginForm().find('input[name="username"]').blur().val();
            var password = this.getModule().getLoginForm().find('input[name="password"]').blur().val();

            this.getApp().getUserModel().login(
                username,
                password,
                {scope:this}
            );
        }
    },

    extendPrototypeFrom: 'Base.Controller'
});