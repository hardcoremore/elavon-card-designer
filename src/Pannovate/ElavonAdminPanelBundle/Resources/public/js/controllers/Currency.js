CLASS.define({

    name: 'Controllers.Currency',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);

        var that = this;

        this.readCountriesForSelectCompleteHandler = function(ev) {
            that.getModule()
                .getCurrencyFormController()
                .populateRelationField('country', ev.responseData);

            that.getModule().getCountrySelect().selectBoxIt('refresh');
        };

        this.readCountriesForSelectErrorHandler = function(ev) {
            that.getApp().showNotification('error', 'Countries could not be loaded from server.');
        };

        this.currencyFormSubmittedEventHandler = function(ev) {

            if(ev.scope === 'create') {
                that.getModule().getCurrencyModel().create(ev.formData, {scope:this});
            }
        };

        this.currencyValidationErrorHandle = function(ev) {
            that.getModule().getCurrencyFormController().displayFormErrors(ev.validationErrors);
        };

        this.currencyCreateCompleteHandle = function(ev) {

            that.getModule().getCurrenciesDataGrid().grid('addRow', ev.responseData);
            that.getModule().getCurrencyFormController().resetForm();

            that.getApp().showNotification('success', 'You have successfully created currency.');

        };

        this.currencyCreateErrorHandle = function(ev) {
            that.getModule().getCurrencyFormController().displayFormErrors(ev.error);
        };

        this.setCurrencyAsBaseButtonClickedHandler = function(ev) {

            var rowData = that.getModule().getCurrenciesDataGrid().grid('getSelectedRowData');

            if(rowData && that.isObject(rowData)) {

                if(rowData.isBase === true) {
                    that.getApp().showNotification('information', 'Currency '+rowData.name+' is already set as base currency.');    
                }
                else {
                    that.getModule().getChangeCurrencyConfirmPopup().panpopup('open');    
                }                
            }
            else {
                that.getApp().showNotification('error', 'You have to select currency.');
            }
        };

        this.changeBaseCurrencyPopupSubmittedHandler = function(ev) {

           var rowData = that.getModule().getCurrenciesDataGrid().grid('getSelectedRowData');

           that.getModule().changeBaseCurrency(rowData);
        };

        this.currencyPatchCompleteHandler = function(ev) {
            that.getModule().getChangeCurrencyConfirmPopup().panpopup('close');
            that.getModule().getCurrenciesDataGrid().grid('reloadGrid');
            that.getApp().showNotification('success', 'You have succesfully changed base currency');
        };

        this.currencyPatchErrorHandle = function(ev) {

        };
    },

    prototypeMethods: {

        addEvents: function() {

            $(document).on(this.getModule().FORM_SUBMITTED_EVENT, this.currencyFormSubmittedEventHandler);

            $(document).on(this.getModule().getCurrencyModel().events.CREATE_COMPLETE, this.currencyCreateCompleteHandle);
            $(document).on(this.getModule().getCurrencyModel().events.CREATE_ERROR, this.currencyCreateErrorHandle);

            $(document).on(this.getModule().getCurrencyModel().events.PATCH_COMPLETE, this.currencyPatchCompleteHandler);
            $(document).on(this.getModule().getCurrencyModel().events.PATCH_ERROR, this.currencyPatchErrorHandler);

            $(document).on(this.getModule().getCurrencyModel().events.VALIDATION_ERROR, this.currencyValidationErrorHandle);

            $(document).on(this.getModule().getCountryModel().events.READ_FOR_SELECT_COMPLETE, this.readCountriesForSelectCompleteHandler);
            $(document).on(this.getModule().getCountryModel().events.READ_FOR_SELECT_ERROR, this.readCountriesForSelectErroreHandler);

            this.getModule().getSetCurrencyAsBaseButton().on('click', this.setCurrencyAsBaseButtonClickedHandler);
        },

        removeEvents: function() {

            $(document).off(this.getModule().FORM_SUBMITTED_EVENT, this.currencyFormSubmittedEventHandler);

            $(document).off(this.getModule().getCurrencyModel().events.CREATE_COMPLETE, this.currencyCreateCompleteHandle);
            $(document).off(this.getModule().getCurrencyModel().events.CREATE_ERROR, this.currencyCreateErrorHandle);

            $(document).off(this.getModule().getCurrencyModel().events.PATCH_COMPLETE, this.currencyPatchCompleteHandler);
            $(document).off(this.getModule().getCurrencyModel().events.PATCH_ERROR, this.currencyPatchErrorHandler);

            $(document).off(this.getModule().getCurrencyModel().events.VALIDATION_ERROR, this.currencyValidationErrorHandle);

            $(document).off(this.getModule().getCountryModel().events.READ_FOR_SELECT_COMPLETE, this.readCountriesForSelectCompleteHandler);
            $(document).off(this.getModule().getCountryModel().events.READ_FOR_SELECT_ERROR, this.readCountriesForSelectErroreHandler);

            this.getModule().getSetCurrencyAsBaseButton().off('click', this.setCurrencyAsBaseButtonClickedHandler);
        },

        startController: function() {
            if(!this.getModule().getCountryModel().getSelectData()) {
                this.getModule().getCountryModel().readForSelect({scope:this});
            }
            else {
                this.getModule().getCurrencyFormController().populateRelationField('country', this.getModule().getCountryModel().getSelectData());
            }
        }
    },

    extendPrototypeFrom: 'Base.Controller'
});