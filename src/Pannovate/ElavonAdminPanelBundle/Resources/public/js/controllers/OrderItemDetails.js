CLASS.define({

    name: 'Controllers.OrderItemDetails',

    definition: function(app, module) {

        this.extendFromClass('Base.Controller', [app, module]);

        var that = this;

        var orderId = this.getModule().getModuleData().order.id;

        this.approveButtonClickHandler = function(ev) {
            that.getModule().getOrderItemDetailsModel().changeOrderStatus(orderId, {status: 'approved'}, {scope:that});
        };

        this.rejectButtonClickHandler = function(ev) {
            that.getModule().getOrderItemDetailsModel().changeOrderStatus(orderId, {status: 'rejected'}, {scope:that});
        };
    },

    prototypeMethods: {

        init: function() {
        },

        addEvents: function() {

            this.getModule().getApproveButton().on('click', this.approveButtonClickHandler);
            this.getModule().getRejectButton().on('click', this.rejectButtonClickHandler);
        },

        removeEvents: function() {

            this.getModule().getApproveButton().off('click', this.approveButtonClickHandler);
            this.getModule().getRejectButton().off('click', this.rejectButtonClickHandler);
        },
    },

    extendPrototypeFrom: 'Base.Controller'
});