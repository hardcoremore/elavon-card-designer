CLASS.define({

    name: 'Model.CardPricing',

    definition: function(app, currencyModel) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/card-pricing';

        var listForCurrency;
        
        var fields = [
            {
                name: 'minimumCards',
                type: 'integer'
            },
            {
                name: 'maximumCards',
                type: 'integer'
            },
            {
                name: 'loyalty',
                type: 'integer'
            },
            {
                name: 'gift',
                type: 'integer'
            },
            {
                name: 'promo',
                type: 'integer'
            },
            {
                name: 'currency',
                type: 'relation',
                relationModel: currencyModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id',
                formatter: function(value) {
                    return value.name + '('+value.symbol+')';
                }
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'minimumCards'},
            {
                type: 'range', 
                field: 'minimumCards',
                min: 1,
                message: "Minimum Cards value must be minimum 1."
            },
            {type: 'presence',  field: 'maximumCards'},
            {type: 'presence',  field: 'loyalty'},
            {type: 'presence',  field: 'gift'},
            {type: 'presence',  field: 'promo'},
            {type: 'presence',  field: 'currency'}
        ];


        this.setListForCurrency = function(currency) {
            listForCurrency = currency;
        };

        this.getListForCurrency = function() {
            return listForCurrency;
        };

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.CardPricing'),

        listForCurrency: function(options) {
            
            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/list-for-currency'),
                showLoader: options.showLoader,

                success: function(responseData, textStatus, jqXHR){
                    that.setListForCurrency(responseData);
                    that.dispatchSuccess(that.events.READ_BASE_CURRENCY_COMPLETE, responseData, options, jqXHR, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_BASE_CURRENCY_ERROR, error, options, jqXHR, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

        getCardPricesForCurrency: function(currencyId) {

            var allCardPricingData = this.getLoadedData();
            var cardPricingData = [];

            if(allCardPricingData) {

                for(var i = 0, len = allCardPricingData.length; i < len; i++) {

                    if(allCardPricingData[i].currency.id.toString() === currencyId.toString()) {
                        cardPricingData.push(allCardPricingData[i]);
                    }
                }
            }
            

            return cardPricingData;
        },

        getCardPricingForNumberOfCards: function(numberOfCards, currencyId) {

            var cardPricingData = this.getCardPricesForCurrency(currencyId);
            var cardPrice = null;
            
            for(var i = 0, len = cardPricingData.length; i < len; i++) {

                if(numberOfCards >= cardPricingData[i].minimum_cards && numberOfCards <= cardPricingData[i].maximum_cards) {

                    if(currencyId) {
                        cardPrice = cardPricingData[i];
                        break;
                    }
                }
            }

            if(!cardPrice){

                this.sortByObjectPropertyDESC(cardPricingData, 'maximum_cards');
                cardPrice = cardPricingData[0];
            }

            return cardPrice;
        }
    },
    
    extendPrototypeFrom: ['Base.Model']
});
