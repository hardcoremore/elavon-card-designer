CLASS.define({

    name: 'Model.ImageGalleryCategory',

    definition: function(app) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/image-gallery-category';

        var fields = [
            {
                name: 'name',
                type: 'string'
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'name'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    prototypeMethods: {

        getImagesByCategory: function(categoryId, options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/album-images/' + categoryId),

                success: function(responseData, textStatus, jqXHR){
                    
                    that.dispatchSuccess(that.events.READ_COMPLETE, responseData, options, jqXHR, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_ERROR, error, options, jqXHR, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);

        },

        events: CLASS.getNamespaceValue('Events.Model.ImageGalleryCategory'),
    },

    extendPrototypeFrom: ['Base.Model']
});
