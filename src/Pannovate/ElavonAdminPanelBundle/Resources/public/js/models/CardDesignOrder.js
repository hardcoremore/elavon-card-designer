CLASS.define({

    name: 'Model.CardDesignOrder',

    definition: function(app, currencyModel) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/card-design-order';


        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.CardDesignOrder'),

        
    },
    
    extendPrototypeFrom: ['Base.Model']
});
