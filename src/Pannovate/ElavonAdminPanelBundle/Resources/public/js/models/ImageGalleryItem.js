CLASS.define({

    name: 'Model.ImageGalleryItem',

    definition: function(app) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/image-gallery';
        var currentImageUploadId;
        var selectedPhoto;

        var fields = [
            {
                name: 'category',
                type: 'integer'
            },
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'fileName',
                type: 'string'
            },
            {
                name: 'filePath',
                type: 'string'
            },
            {
                name: 'fileUrl',
                type: 'string'
            },
            {
                name: 'active',
                type: 'integer'
            }
        ];

        var validationRules = [

        ];

        this.setCurrentUploadId = function(id) {
            currentImageUploadId = id;
        };

        this.getCurrentUploadId = function() {
            return currentImageUploadId;
        };

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };

        this.setSelectedPhoto = function(photo) {

            selectedPhoto = photo;

            var e = $.Event(
                this.events.PHOTO_SELECTED,
                {
                    photo: selectedPhoto
                }
            );

            $(document).trigger(e);
        };

        this.getSelectedPhoto = function() {
            return selectedPhoto;
        }
    },

    prototypeMethods: {

        uploadGalleryImage: function(imageFile, postData, blobHashUrl) {
            
            postData.intention = 'gallery-image-item';

            var url = this.getApp().getConfig().getBaseAPIUrl() + '/upload-file';
            var uploadId = this.generateUploadId();

            var options = {
                uploadId: uploadId,
                scope: {
                    intention: postData.intention,
                    uploadId: uploadId,
                    blobHashUrl: blobHashUrl,
                    model: this
                }
            };

            this.setCurrentUploadId(uploadId);
            this.uploadFile(url, imageFile, postData, options);
        },

        events: CLASS.getNamespaceValue('Events.Model.ImageGalleryItem'),
    },



    extendPrototypeFrom: ['Base.Model']
});
