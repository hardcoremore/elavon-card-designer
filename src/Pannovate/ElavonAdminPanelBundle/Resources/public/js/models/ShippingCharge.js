CLASS.define({

    name: 'Model.ShippingCharge',

    definition: function(app, countryModel) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/shipping-charge';

        var fields = [
            {
                name: 'country',
                type: 'relation',
                relationModel: countryModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            },
            {
                name: 'amount',
                type: 'integer'
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'country'},
            {type: 'presence',  field: 'amount'},
            {
                type: 'range', 
                field: 'amount',
                min: 1,
                message: "ShippingCharge amount must be minimum 1."
            }
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.ShippingCharge'),
    },
    
    extendPrototypeFrom: ['Base.Model']
});
