CLASS.define({

    name: 'Model.ElavonAdminPanelUser',

    definition: function(app) {

        this.extendFromClass('Model.User', [app]);

        this.USERNAME_LOGIN_POST_PROPERTY_NAME = '__elavon_admin_api_login_username__';
        this.PASSWORD_LOGIN_POST_PROPERTY_NAME = '__elavon_admin_api_login_password__';
    },

    extendPrototypeFrom: ['Model.User']

});