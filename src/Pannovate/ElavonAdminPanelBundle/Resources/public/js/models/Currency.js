CLASS.define({

    name: 'Model.Currency',

    definition: function(app, countryModel) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/currency';

        var baseCurrency;
        
        var fields = [
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'code',
                type: 'string'
            },
            {
                name: 'symbol',
                type: 'string'
            },
            {
                name: 'rate',
                type: 'string',
            },
            {
                name: 'country',
                type: 'relation',
                relationModel: countryModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'code'},
            {type: 'presence',  field: 'symbol'},
            {
                type: 'length', 
                field: 'symbol',
                min:1,
                max:1,
                message: "Symbol must be one character only."
            },
            {type: 'presence',  field: 'rate'},
            {type: 'presence',  field: 'country'}
        ];

        this.setBaseCurrency = function(currency) {
            baseCurrency = currency;
        };

        this.getBaseCurrency = function() {
            return baseCurrency;
        };

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };

    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.Currency'),

        readBaseCurrency: function(options) {
            
            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/base'),
                showLoader: options.showLoader,

                success: function(responseData, textStatus, jqXHR){
                    that.setBaseCurrency(responseData);
                    that.dispatchSuccess(that.events.READ_BASE_CURRENCY_COMPLETE, responseData, options, jqXHR, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_BASE_CURRENCY_ERROR, error, options, jqXHR, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

    },
    
    extendPrototypeFrom: ['Base.Model']
});
