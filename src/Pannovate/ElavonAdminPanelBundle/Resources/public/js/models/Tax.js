CLASS.define({

    name: 'Model.Tax',

    definition: function(app, countryModel) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/tax';

        var fields = [
            {
                name: 'name',
                type: 'string'
            },
            {
                name: 'amount',
                type: 'integer'
            },
            {
                name: 'country',
                type: 'relation',
                relationModel: countryModel,
                labelPropertyName: 'name',
                valuePropertyName: 'id'
            }
        ];

        var validationRules = [

            {type: 'presence',  field: 'name'},
            {type: 'presence',  field: 'amount'},
            {
                type: 'range', 
                field: 'amount',
                min: 1,
                max: 100,
                message: "Tax amount must be between 1 and 100."
            },
            {type: 'presence',  field: 'country'}
        ];

        this.getUrlPart = function() {
            return urlPart;
        };

        this.getFields = function() {
            return fields;
        };

        this.getValidationRules = function() {
            return validationRules;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.Tax'),
    },
    
    extendPrototypeFrom: ['Base.Model']
});
