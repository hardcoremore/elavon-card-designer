CLASS.define({

    name: 'Model.OrderItemDetails',

    definition: function(app) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/order-item-details';

        this.getUrlPart = function() {
            return urlPart;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.OrderItemDetails'),

        changeOrderStatus: function(orderId, status, options) {
            
            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/' + orderId),

                success: function(responseData, textStatus, jqXHR){
                    that.dispatchSuccess(that.events.UPDATE_COMPLETE, responseData, options, jqXHR, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.UPDATE_COMPLETE, error, options, jqXHR, jqXHR);
                }
            });

            this.getApp().getRequest().PUT(options);
        },
    },
    
    extendPrototypeFrom: ['Base.Model']
});
