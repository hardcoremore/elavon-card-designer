CLASS.define({

    name: 'Model.CustomerDetails',

    definition: function(app) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;
        var urlPart = '/customer-details';

        this.getUrlPart = function() {
            return urlPart;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.CustomerDetails'),

        getCustomerRecentDesigns: function(id, options) {
            
            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/get-recent-designs/' + id),

                success: function(responseData, textStatus, jqXHR){
                    that.dispatchSuccess(that.events.CUSTOMER_RECENT_DESIGNS_COMPLETE, responseData, options, jqXHR, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.CUSTOMER_RECENT_DESIGNS_ERROR, error, options, jqXHR, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },
    },
    
    extendPrototypeFrom: ['Base.Model']
});
