CLASS.define({
    
    name: 'App.ElavonAdminPanelClient',

    definition: function(c) {

        this.extendFromClass('App.LoginApplication', [c]);
        
        var that = this;

        this.loadCurrentUserCompleteHandler = function(ev) {
            
            $(document).off(that.getUserModel().events.LOAD_CURRENT_USER_COMPLETE, that.loadCurrentUserCompleteHandler);
            $(document).off(that.getUserModel().events.LOAD_CURRENT_USER_ERROR, that.loadCurrentUserErrorHandler);

            that.startApp();
        };

        this.loadCurrentUserErrorHandler = function(ev) {

            if(ev.error.code === 403) {
                that.getModuleController().loadModule('Modules.Login');
            }
            else {
                console.log('Failed to load user from session. Code: ' + ev.error.code);
            }
        };
    },

    prototypeMethods: {

        init: function() {

            this.callSuper('App.LoginApplication', 'init');

            this.setUserModel(this.getModel('Model.ElavonAdminPanelUser', null, true));

            $(document).on(this.getUserModel().events.LOAD_CURRENT_USER_COMPLETE, this.loadCurrentUserCompleteHandler);
            $(document).on(this.getUserModel().events.LOAD_CURRENT_USER_ERROR, this.loadCurrentUserErrorHandler);

            this.getUserModel().loadCurrentUser({scope:this});
        },

        startApp: function() {
            this.callSuper('App.LoginApplication', 'startApp');
            $('#login-module-holder').hide();
            this.getContentAllHolder().show();
        },

        startEntryModule: function() {

            var entryUrlData = this.getUrlData();
            var currentUrlData = this.getHistoryController().parseUrl(window.location.href);

            // skip loading login module again after app starts
            if(currentUrlData.moduleName === 'Login') {

                if(entryUrlData.moduleName && entryUrlData.moduleName !== 'Login') {
                    this.loadModuleFromUrl(entryUrlData);
                }
                else {
                    this.loadDefaultModule();
                }
            }
            else if(entryUrlData && entryUrlData.moduleName && entryUrlData.moduleName.length > 0) {
                this.loadModuleFromUrl(entryUrlData);
            }
            else {
                this.loadDefaultModule();
            }
        }
    },

    extendPrototypeFrom: ['App.LoginApplication']
});
