<?php

namespace Pannovate\ElavonAdminPanelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ModuleViewsController extends Controller
{
    public function homeViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:home.html.twig');
    }

    public function loginViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:login.html.twig');
    }

    public function recentOrdersViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:recentOrders.html.twig');
    }

    public function customersViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:customers.html.twig');
    }

    public function customerViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:customer.html.twig');
    }

    public function orderItemDetailsViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:orderItemDetails.html.twig');
    }

    public function imageGalleryViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:imageGallery.html.twig');
    }

    public function currencyViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:currency.html.twig');
    }

    public function cardPricingViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:cardPricing.html.twig');
    }

    public function taxSettingsViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:taxSettings.html.twig');
    }

    public function shippingChargeViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:shippingCharge.html.twig');
    }

    public function rolePermissionViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:rolePermission.html.twig');
    }

    public function systemUsersViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:systemUsers.html.twig');
    }

    public function systemActionsViewAction()
    {
        return $this->render('ElavonAdminPanelBundle:ModuleViews:systemActions.html.twig');
    }

}
