CLASS.define({

    name: 'App.LoginApplication',

    definition: function(c) {

        this.extendFromClass('Base.Application', [c]);

        var that = this;
        var contentAllHolder = null;

        var userModel = null;
        var authenticationController;
        var loaderNotifications = {};

        this.setContentAllHolder = function(cel) {
            contentAllHolder = cel;
        };

        this.getContentAllHolder = function() {
            return contentAllHolder;
        };

        this.addLoaderNotification = function(loaderId, notificationObject) {
            loaderNotifications[loaderId] = notificationObject;
        };

        this.removeLoaderNotification = function(loaderId) {
            delete loaderNotifications[loaderId];
        };

        this.setAuthenticationController = function(lc) {
            authenticationController = lc;
        };

        this.getAuthenticationController = function() {
            return authenticationController;
        };

        this.setUserModel = function(um) {
            userModel = um;
        };

        this.getUserModel = function() {
            return userModel;
        };
    },

    prototypeMethods: {

        /**
        *
        * Initialize the application
        *
        */
        init: function() {

            this.callSuper('Base.Application', 'init');

            this.setContentAllHolder($("#app-content-all-holder"));

            this.setLoaderElement($("#main-loader"));
            
            this.setUserModel(this.getModel('Model.User', null, true));

            this.setModuleController(this.getInstance('Controllers.Module', [this]));
            this.startController(this.getModuleController());

            this.getContentAllHolder().hide();
        },

        startApp: function() {

            var that = this;

            var defaultModuleNamespace = this.getConfig().getParameter('defaultModuleNamespace');

            //Start Module By Name on Click on Menu Item
            $("body").on('click', '[data-module]', function(ev){

                var module = $(this).data('module');
                var namespace = $(this).data('module-namespace') || defaultModuleNamespace;
                var data = $(this).data('module-data') || null;
                var reinitialize = $(this).data('module-reinitialize') || false;
                var moduleClassName = namespace + '.' + module;

                that.getModuleController().loadModule(moduleClassName, data, reinitialize);

            });

            this.startEntryModule();
        },

        startEntryModule: function() {

            var urlData = this.getUrlData();

            if(urlData && urlData.moduleName && urlData.moduleName.length > 0) {
                this.loadModuleFromUrl(urlData);
            }
            else {
                this.loadDefaultModule();    
            }
        },

        loadDefaultModule: function() {

            var defaultModuleNamespace = this.getConfig().getParameter('defaultModuleNamespace');
            this.getModuleController().loadModule(defaultModuleNamespace + '.' + this.getConfig().getParameter('defaultModule'));
        },

        loadModuleFromUrl: function(urlData) {

            var defaultModuleNamespace = this.getConfig().getParameter('defaultModuleNamespace');
            this.getModuleController().loadModule(defaultModuleNamespace + '.' + urlData.moduleName);
        },

        /**
         *
         * Get module permissions
         *
         * @param {String} moduleClassName
         *
         * @return {Array} Returns array with actions that module is allowed to
         *
         */
        getModulePermissions: function(moduleClassName) {

            if(!this.getUserModel().getCurrentUser()) {
                return [];
            }

            var permissions = this.getUserModel().getCurrentUser().permissions;

            if(permissions && permissions.hasOwnProperty(moduleClassName)) {
                return permissions[moduleClassName];
            }

            return [];
        },

        getNotificationHtml: function(type, notificationData) {
            return  this.compileTemplate("noty-" + type + "-notification", notificationData);
        },

        showNotification: function(type, message, loaderId) {

            var htmlTemplate = this.getNotificationHtml(
                type,
                {
                    loaderId: loaderId,
                    message: message
                }
            );

            var closeWidth = null;
            var speed = 100;

            if(!loaderId) {
                closeWidth = ['click'];
                speed = 500;
            }

            var notification = noty({
                text: htmlTemplate,
                type: type,
                dismissQueue: true,
                timeout: this.getConfig().getParameter(type + 'NotificationDuration'),
                layout: 'topRight',
                closeWith: closeWidth,
                theme: 'fanfarecation',
                maxVisible: 10,
                animation: {
                    open: 'animated bounceInRight',
                    close: 'animated bounceOutRight',
                    easing: 'swing',
                    speed: speed
                }
            });

            if(loaderId) {
                this.addLoaderNotification(loaderId, notification);
            }

            return notification;
        },

        updateLoaderNotificationProgress: function(loaderId, progressAmmount) {
            $("#loader-" + loaderId).find('.progress_bar').css('width', progressAmmount + "%");
        },

        showLoader: function() {
            this.getLoaderElement().show();
        },

        hideLoader: function() {

            var allRequests = this.getRequest().getAllRequestsOptions();
            var keys = Object.keys(allRequests);
            var doesRequestWithLoaderExists = false;

            if(keys && keys.length > 0) {
                for(var i = 0, len = keys.length; i < len; i++) {
                    if(allRequests[keys[i]].showLoader !== false) {
                        doesRequestWithLoaderExists = true;
                        break;                        
                    }
                }    
            }

            if(doesRequestWithLoaderExists === false) {
                this.getLoaderElement().hide();
            }
        },

        blockElement: function(element, message) {
            EC.helpers.ui.blockElement(element, message);
        },

        unblockElement: function(element) {
            EC.helpers.ui.unblockElement(element);
        }
    },

    extendPrototypeFrom: 'Base.Application'
});