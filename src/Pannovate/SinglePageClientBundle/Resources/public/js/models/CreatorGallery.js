CLASS.define({

    name: 'Model.GreatorGallery',

    definition: function(app) {

        /*this.extendFromClass('Base.Model', [app]);

        var that = this;

        var photosTotal;

        var selectedPhoto;

        this.getTotalPhotos = function() {
            return photosTotal;
        };

        this.getSelectedPhoto = function() {
            return selectedPhoto;
        };

        this.setSelectedPhoto = function(photo) {

            selectedPhoto = photo;

            var e = $.Event(
                this.events.PHOTO_SELECTED,
                {
                    photo: selectedPhoto
                }
            );

            $(document).trigger(e);
        };

        this.resetSelectedPhoto = function() {
            selectedPhoto = null;
        };

        this.getUrl = function() {
            return this.getApp().getConfig().getParameter('flickrApiUrl');
        };

        this.setFlickrApiKey = function(apiKey) {
            flickrApiKey = apiKey;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.Flickr'),

        getAlbumImages: function (filters, options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl() + '?method=creator.gallery.alnum.getPhotos',

                parameters: filters,

                success: function (responseData, textStatus, jqXHR) {
                    photosPaging      = responseData.photos.pages;
                    photosCurrentPage = responseData.photos.page;
                    photosTotal       = responseData.photos.total;
                    that.dispatchSuccess(that.events.READ_USER_PHOTOS_COMPLETE, responseData, options, jqXHR);
                },

                error: function (error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_USER_PHOTOS_ERROR, error, options, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },*/
    },

    extendPrototypeFrom: ['Base.Model'],
});
