CLASS.define({

    name: 'Model.Flickr',

    definition: function(app) {

        this.extendFromClass('Base.Model', [app]);

        var that = this;

        var photosPaging;
        var photosCurrentPage;
        var photosTotal;

        var flickrApiKey;
        var currentFlickrUser;
        var selectedPhoto;

        this.getTotalPhotos = function() {
            return photosTotal;
        };

        this.getSelectedPhoto = function() {
            return selectedPhoto;
        };

        this.setSelectedPhoto = function(photo) {

            selectedPhoto = photo;

            var e = $.Event(
                this.events.PHOTO_SELECTED,
                {
                    photo: selectedPhoto
                }
            );

            $(document).trigger(e);
        };

        this.resetSelectedPhoto = function() {
            selectedPhoto = null;
        };

        this.getUrl = function() {
            return this.getApp().getConfig().getParameter('flickrApiUrl');
        };

        this.getPhotosPaging = function () {
            return photosPaging;
        };

        this.getPhotosCurrentPage = function () {
            return photosCurrentPage;
        };

        this.setFlickrApiKey = function(apiKey) {
            flickrApiKey = apiKey;
        };

        this.getFlickrApiKey = function() {
            return flickrApiKey;
        };

        this.setCurrentFlickrUser = function(flickrUser) {
            currentFlickrUser = flickrUser;
        };

        this.getCurrentFlickrUser = function() {
            return currentFlickrUser;
        };
    },

    prototypeMethods: {

        events: CLASS.getNamespaceValue('Events.Model.Flickr'),

        readUserId: function (username, options) {

            var that = this;

            if(this.isString(username) && username.length > 0) {

                var data = {
                    api_key: this.getFlickrApiKey(),
                    username: username,
                    format: 'json',
                    nojsoncallback: 1
                };

                $.extend(options, true, {

                    url: this.getUrl() + '?method=flickr.people.findByUsername',

                    parameters: data,

                    success: function (responseData, textStatus, jqXHR) {
                        that.setCurrentFlickrUser(responseData.user);
                        that.dispatchSuccess(that.events.READ_USER_ID_COMPLETE, responseData, options, jqXHR);
                    },

                    error: function (error, jqXHR, textStatus) {
                        that.dispatchError(that.events.READ_USER_ID_ERROR, error, options, jqXHR);
                    }
                });

                this.getApp().getRequest().GET(options);
            }
            else {

                var e = $.Event(
                    that.events.VALIDATION_ERROR,
                    {
                        validationErrors: {'username': 'Flickr username is invalid'},
                        scope: options.scope
                    }
                );

                $(document).trigger(e);
            }
        },

        getUserImages: function (filters, options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl() + '?method=flickr.people.getPhotos',

                parameters: filters,

                success: function (responseData, textStatus, jqXHR) {
                    photosPaging      = responseData.photos.pages;
                    photosCurrentPage = responseData.photos.page;
                    photosTotal       = responseData.photos.total;
                    that.dispatchSuccess(that.events.READ_USER_PHOTOS_COMPLETE, responseData, options, jqXHR);
                },

                error: function (error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_USER_PHOTOS_ERROR, error, options, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },
    },

    extendPrototypeFrom: 'Base.Model'
});
