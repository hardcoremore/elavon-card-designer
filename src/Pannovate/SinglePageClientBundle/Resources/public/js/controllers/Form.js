CLASS.define({

    name: 'Controllers.Form',

    definition: function(app, module, model) {

		this.extendFromClass('Base.Controller', [app, module]);

        var that = this;
        var isFormChanged = false;
        var formContainer;
        var submitButton;
        var resetButton;
        var entityModel = model;
        var formEvents;

        this.setFormEvents = function(events) {
            formEvents = events;
        };

        this.getFormEvents = function() {
            return formEvents;
        };

        this.setIsFormChanged = function(changed) {
        	isFormChanged = changed;
    	};

    	this.getIsFormChanged = function() {
        	return isFormChanged;
    	};

        this.setFormContainer = function(form) {
            formContainer = form;
        };

    	this.getFormContainer = function() {
    		return formContainer;
    	};

        this.setEntityModel = function(model) {
            entityModel = model;
        };

        this.getEntityModel = function() {
            return entityModel;
        };

    	this.setSubmitButton = function(button) {
    		submitButton = button;
    	};

    	this.getSubmitButton = function() {
    		return submitButton;
    	};

        this.setResetButton = function(button) {
            resetButton = button;
        };

        this.getResetButton = function() {
            return resetButton;
        };

    	this.formChangedHandler = function(ev) {
    		if(ev.notifyFormForChange !== false) {
                that.setIsFormChanged(true);
            }
    	};

    	this.formSubmitButtonHandler = function(ev) {

    		var formData = that.getFormData(that.getFormContainer());

    		var e = $.Event(that.getFormEvents().FORM_SUBMITTED_EVENT, {
                formData: formData,
                scope: $(ev.currentTarget).data('role'),
                module: that.getModule()
            });

            $(document).trigger(e);
    	};

        this.formResetButtonClickHandler = function(ev) {

            that.resetForm();

            var e = $.Event(that.getFormEvents().FORM_CANCELED_EVENT);
            $(document).trigger(e);  
        };
    },

    prototypeMethods: {
    
        init: function() {

            if(!this.getSubmitButton()) {
                this.setSubmitButton(this.getFormContainer().find('.submit-button'));    
            }

            if(!this.getResetButton()) {
                this.setResetButton(this.getFormContainer().find('.reset-button'));
            }
        },

        addEvents: function() {

        	this.getSubmitButton().on('click', this.formSubmitButtonHandler);
            this.getResetButton().on('click', this.formResetButtonClickHandler);

        	// listen when form inputs change
            this.getFormContainer().find('input, select, textarea').on('change', this.formChangedHandler);
        },

        removeEvents: function() {

        	this.getSubmitButton().off('click', this.formSubmitButtonHandler);
            this.getResetButton().off('click', this.formResetButtonClickHandler);

        	this.getFormContainer().find('input, select, textarea').on('change', this.formChangedHandler);
        },

        /**
         *
         * Display form errors for every form field.
         *
         * @param {Object} errors Object containing all field errors. 
            Every key of errors object is form field name in model field definition.
            Every value of errors object is array containing error messages for that field.
         *
         */
        displayFormErrors: function(errors) {

            this.hideAllFormErrors();

            if(errors.message && errors.message.length > 0) {
                this.showFormError(errors.message);
            }

            var errorIndex = '';
            var that = this;

            var errorMessages = errors;

            // case for response from server
            if(errors.errors && errors.errors.children) {
                errorMessages = errors.errors.children;
            }

            $.each(errorMessages, function(index){

                if(this.hasOwnProperty('errors')) {
                    errorMessages = this.errors;
                }
                else if(this.hasOwnProperty('children')) {
                    errorMessages = this.children;
                }
                else {
                    errorMessages = this;
                }

                for(errorIndex in errorMessages) {
                    that.showFormFieldError(index, errorMessages[errorIndex]);
                }
            });
        },

        /**
         *
         * Show error message when form is submitted.
         * @param {String} message Message to show for the form
         *
         */
        showFormError: function(message) {
            this.getFormContainer().find('.form-status-msg').text(message).removeClass('hidden');
        },

        /**
         *
         * Display error for single form field element.
         *
         * @param {String} fieldName Field name defined in model
         * @param {String} message Error message to show
         *
         */
        showFormFieldError: function(fieldName, message) {

            var formFieldErrorElement = this.getFormContainer().find('.form-field-error-' + fieldName).first();
            var errorsListElement = formFieldErrorElement.find('ul').first();

            // if form field set is inactive do not display errors for that field
            if(formFieldErrorElement.closest('.form-field-set').hasClass('form-field-set-inactive')) {
                return;
            }

            // create form field errors ul if does not exist
            if(errorsListElement.length === 0) {
                errorsListElement = $('<ul/>');
                formFieldErrorElement.append(errorsListElement);
            }

            errorsListElement.append($('<li/>', {
                text: message
            }));

            formFieldErrorElement.show();
        },

        /**
         *
         * Populate form with data.
         *
         * @param {Object} data Data to populate form with
         *
         */
        populateForm: function(data) {

            this.resetForm();

        	var that = this;

            $.each(data, function(index) {
                that.populateFormField(index, this);
            });
        },

        populateFormField: function(fieldName, data, formatter) {

            var fieldModelData = this.getEntityModel().getFieldByName(fieldName);

            if(fieldModelData && fieldModelData.type === 'relation') {
                this.setRelationFieldSelectedValue(fieldModelData, data);
            }
            else {
                var fieldElement = this.getFormElementByName(fieldName);
                fieldElement.val(data.toString());
            }
        },

        setRelationFieldSelectedValue: function(fieldModelData, value) {
            var fieldElement = this.getFormElementByName(fieldModelData.name);
                fieldElement.val(this.getRelationFieldValue(fieldModelData, value));
        },

        populateRelationField: function(fieldName, data, selectedValue) {

        	var that = this;
            var relationField = this.getFormElementByName(fieldName);
            var fieldModelData = this.getEntityModel().getFieldByName(fieldName);

            this.resetRelationField(relationField);

            if(this.isArray(data) === false) {
                data = [data];
            }

            $.each(data, function(index) {

            	var relationFieldValue = that.getRelationFieldValue(fieldModelData, this);

            	var option = $('<option/>', {
                    text: that.formatRelationFieldLabel(fieldModelData, this, fieldModelData.formatter),
                    value: relationFieldValue
                });

            	if(typeof selectedValue  !== 'undefined' && relationFieldValue === selectedValue) {
            		option.attr('selected', true);
            	}

            	relationField.append(option);
            });
        },

        formatRelationFieldLabel: function(fieldModelData, value, formatter) {
        	if(this.isFunction(formatter)) {
        		return formatter(value);
        	}
        	else {
        		return value[fieldModelData.labelPropertyName];
        	}
        },

        getRelationFieldValue: function(fieldModelData, data) {

        	if(this.isObject(data) && data.hasOwnProperty(fieldModelData.valuePropertyName)) {
        		return data[fieldModelData.valuePropertyName];
        	}
        	else {
        		return data;
        	}
        },

        resetRelationField: function(relationField) {

            if(relationField.is("select")) {
                relationField.find('option:not(:first)').remove();
            }
            else if(relationField.is('input')) {
                relationField.val('');
            }
        },

        getFormData: function() {

        	var formData = {};

        	//get all data from the HTML form inputs
            this.getFormContainer().find("input[name], textarea[name], select[name]").each(function(index){

                input = $(this);
                fieldName = input.attr('name');

                formData[fieldName] = input.val();
            });

            return formData;
        },

        /**
         *
         * Reset all fields in the form.
         *
         *
         */
        resetForm: function() {

            // reset form fields
    		this.getFormContainer()
    		    .find("input[name], textarea[name], select[name]")
    		    .each(function(index){

    		        var fieldElement = $(this);

    		        if(fieldElement.prop('disabled') === false) {
    		            if(fieldElement.is(':checkbox')) {
    		                fieldElement.prop('checked', false);
    		            }
    		            else {
    		                fieldElement.val('');
    		            }
    		        }
    		        else {
    		            fieldElement.val('');
    		        }
    		});

			this.hideAllFormErrors();
		},

		hideAllFormErrors: function() {

	        var formFieldsErrors = this.getFormContainer().find('.form-field-error');
	        var that = this;

	        this.hideFormError();

	        formFieldsErrors.each(function(index){
	        	that.hideFormFieldError($(this));
	        });
    	},

	    hideFormError: function() {
	        this.getFormContainer().find('.form-status-msg').empty().addClass('hidden');
	    },

	    hideFormFieldError: function(formFieldErrorEl) {
	        formFieldErrorEl.empty().addClass('hidden');
	    },

	    getFormElementByName: function(elementName) {

            var formElement = this.getFormContainer().find("[name='"+elementName+"']");

            if(formElement.length > 0) {
                return formElement.first();
            }

        	return formElement;
    	}
    },

    extendPrototypeFrom: 'Base.Controller'
});