CLASS.define({
    
    name: 'Controllers.Module',

    definition: function(app) {

        this.extendFromClass('Base.Controller', [app]);

        var that = this;

        var moduleContainer = null;
        var modules = {};
        var activeModule = null;
        var moduleViews = {};

        var previousModule = null;

        this.setModuleView = function(moduleClassName, view) {
            moduleViews[moduleClassName] = view;
        };

        this.getModuleView = function(moduleClassName) {
            return moduleViews[moduleClassName];
        };


        this.moduleViewLoadCompleteEventHandler = function(ev) {

            that.setModuleView(ev.moduleClassName, ev.moduleView);
            that.initializeModuleView(ev.module, ev.moduleView);

            var currentModuleView = that.getModuleContainer().find('[data-module-namespace="'+ev.module.namespace+'"]');

            // remove the old view
            if(currentModuleView) {
                currentModuleView.remove();
            }

            var moduleHolderId = ev.module.getModuleElement().data('module-holder');

            if(moduleHolderId && moduleHolderId.length > 1) {

                var moduleHolderEl = $(moduleHolderId);

                if(moduleHolderEl.length === 1) {

                    currentModuleView = moduleHolderEl.find('[data-module-namespace="'+ev.module.namespace+'"]').length === 1;

                    if(currentModuleView) {
                        currentModuleView.remove();
                    }

                    moduleHolderEl.append(ev.module.getModuleElement());  
                }
                else {
                    that.throwException('Invalid module holder id: ' + moduleHolderId, 1500, 'kernel');
                }
            }
            else {
                that.getModuleContainer().append(ev.module.getModuleElement());
            }

            that.getApp().hideLoader();
            that.startModule(ev.module);
        };

        this.moduleReinitializedEventHandler = function(ev) {
            that.activateModule(ev.module);
        };

        //Hide loader on Module loading Error
        this.moduleLoadErrorEventHandler = function(ev) {
            that.getApp().hideLoader();
            that.getApp().resolveError({
                code: 1500,
                message: "Module could not be loaded. Please try again."
            });
        };

        this.moduleReadyEventHandler = function(ev) {
           that.activateModule(ev.module);
        };

        this.moduleDestroyedEventHandler = function(ev) {

            console.log("Module destroyed: " + ev.module.getModuleName());

            if(that.getActiveModule() === ev.module) {
                that.setActiveModule(null);
            }

            delete modules[ev.module.namespace];
        };

        this.resizeWindowHandler = function(ev) {
            if(that.getActiveModule()) {
                that.getActiveModule().updateModuleDimensions();
            }
        };

        this.getModuleContainer = function() {
            return moduleContainer;
        };

        this.setModuleContainer = function(mc) {
            moduleContainer = mc;
        };

        this.getModules = function() {
            return modules;
        };

        this.getActiveModule = function() {
            return activeModule;
        };

        this.setActiveModule = function(m) {

            if(activeModule) {
                previousModule = activeModule;
            }

            activeModule = m;
        };

        this.getPreviousModule = function() {
            return previousModule;
        };

        this.registerModule = function(m) {

            if(typeof modules[m.getDefinition().name] === 'undefined') {
                modules[m.getDefinition().name] = m;
            }
        };

        this.isValidModuleName = function(moduleName) {
            var pattern = new RegExp('^[a-zA-Z0-9]+$');
            return pattern.test(moduleName);
        };
    },

    prototypeMethods: {

        init: function() {
            this.setModuleContainer($("#main-module-container"));
        },


        addEvents: function() {

            var constants = this.getNamespaceValue('Static.Constants');

            $(document).on(constants.MODULE_VIEW_LOAD_COMPLETE_EVENT, this.moduleViewLoadCompleteEventHandler);
            $(document).on(constants.MODULE_VIEW_LOAD_ERROR_EVENT, this.moduleViewLoadErrorEventHandler);
            
            $(document).on(constants.MODULE_LOAD_COMPLETE_EVENT, this.moduleLoadCompleteEventHandler);
            $(document).on(constants.MODULE_LOAD_ERROR_EVENT, this.moduleLoadErrorEventHandler);

            $(document).on(constants.MODULE_REINITIALIZED_EVENT, this.moduleReinitializedEventHandler);
            
            $(document).on(constants.MODULE_READY_EVENT, this.moduleReadyEventHandler);
            $(document).on(constants.MODULE_DESTROYED_EVENT, this.moduleDestroyedEventHandler);

            $(window).on('resize orientationchange', this.resizeWindowHandler);
        },

        removeEvents: function() {

            var constants = this.getNamespaceValue('Static.Constants');

            $(document).off(constants.MODULE_VIEW_LOAD_COMPLETE_EVENT, this.moduleViewLoadCompleteEventHandler);
            $(document).off(constants.MODULE_VIEW_LOAD_ERROR_EVENT, this.moduleViewLoadErrorEventHandler);
            
            $(document).off(constants.MODULE_LOAD_COMPLETE_EVENT, this.moduleLoadCompleteEventHandler);
            $(document).off(constants.MODULE_LOAD_ERROR_EVENT, this.moduleLoadErrorEventHandler);

            $(document).off(constants.MODULE_REINITIALIZED_EVENT, this.moduleReinitializedEventHandler);
            
            $(document).off(constants.MODULE_READY_EVENT, this.moduleReadyEventHandler);
            $(document).off(constants.MODULE_DESTROYED_EVENT, this.moduleDestroyedEventHandler);

            $(window).off('resize orientationchange', this.resizeWindowHandler);
        },

        startDefaultModule: function() {
            var defaultModuleName = this.getApp().getConfig().getParameter('defaultModule');
            this.loadModule(defaultModuleName);
        },

        loadModule: function(moduleClassName, moduleData, reinitialize, updateUrl) {

            if(moduleClassName === 'Logout') {
                this.getApp().getUserModel().logout();
                return;
            }

            var loadedModules = this.getModules();

            try {

                var module = loadedModules[moduleClassName];

                //if module is loaded already
                if(module) {

                    if(reinitialize === true) {
                        module.reinitializeModule(moduleData);
                    }
                    else {

                        if(this.getActiveModule() && this.getActiveModule().getDefinition().name !== moduleClassName) {
                            this.getActiveModule().hideModule();
                        }

                        this.setActiveModule(module);
                        module.showModule();    
                    }

                    if(updateUrl !== false) {
                        this.getApp().getHistoryController().goToHashUrl(this.getActiveModule().getModuleName());
                    }

                    return module;
                }
                else {

                    module = this.initializeModule(moduleClassName, moduleData);                    

                    return module;
                }
            }
            catch(error) {                

                if(error.hasOwnProperty('getMessage')) {
                    console.log(error.getMessage());
                }
                else {
                    console.log(error);
                }
            }
        },

        loadModuleView: function(module, options) {

            var that = this;
            var constants = this.getNamespaceValue('Static.Constants');

            var requestId = this.getApp().getRequest().generateRequestId();
            options = options || {};
            options.showLoader = true;

            this.getApp().getRequest().setRequestsOptions(requestId, options);
            that.getApp().showLoader();

            $.ajax({

                url: this.getApp().getConfig().getModuleViewBaseUrl() + '/' + module.getModuleName() + '/view',
                type: 'GET',
                dataType: 'html',

                error: function(jqXHR, textStatus, errorThrown) {

                    that.getApp().getRequest().removeRequestOptions(requestId);

                    var ev = $.Event(constants.MODULE_VIEW_LOAD_ERROR_EVENT, {module:module, moduleClassName: module.namespace, error:errorThrown});
                    $(document).trigger(ev);
                },

                success: function(data, textStatus, jqXHR) {

                    that.getApp().getRequest().removeRequestOptions(requestId);

                    var ev = $.Event(constants.MODULE_VIEW_LOAD_COMPLETE_EVENT, {module:module, moduleView: data, moduleClassName: module.namespace, jqXHR:jqXHR});
                    $(document).trigger(ev);
                }
            });
        },

        initializeModuleView: function(module, moduleView) {

            var moduleElement = $(moduleView).first();
                moduleElement.attr('data-module-namespace', module.namespace);

            module.setModuleElement(moduleElement);
            module.setModuleViewLoaded(true);
        },

        initializeModule: function(moduleClassName, moduleData) {

            var permissions = this.getApp().getModulePermissions(moduleClassName);

            var module = this.getInstance(moduleClassName, [this.getApp(), permissions]);
                module.setModuleData(moduleData);

            var moduleView = this.getModuleView(moduleClassName);

            if(moduleView) {
                this.initializeModuleView(module, moduleView);
                this.startModule(module);
            }
            else {
                this.loadModuleView(module);
                this.getApp().showLoader();
            }
        },

        startModule: function(module) {

            if(module.getInitialized() === false) {
                module.initModule();
            }

            module.startModule();
        },

        /*
         * hide currently active module, register new module, set new module as active,
         * and show module
         *  we can have only one module active at the time
         */
        activateModule: function(module) {

            if(this.getActiveModule()) {
                this.getActiveModule().hideModule();
            }

            this.registerModule(module);
            this.setActiveModule(module);

            module.showModule();

            this.getApp().getHistoryController().goToHashUrl(module.getModuleName());

            var urlData = this.getApp().getHistoryController().parseUrl(window.location.href);
            
            module.updateModuleStateHistory(urlData);
        },

        destroyModule: function(module) {
            module.destroyModule();
        }
    },

    extendPrototypeFrom: 'Base.Controller'
});