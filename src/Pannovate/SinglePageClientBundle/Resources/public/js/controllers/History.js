CLASS.define({

    name: 'Controllers.History',

    definition: function(app) {

        this.extendFromClass('Base.Controller', [app]);

        var that = this;

        this.historyChangedEventHandler = function(ev) {
            that.changeUrl(ev.target.location.href);            
        };
    },

    prototypeMethods: {

        init: function() {

            $(window).on('popstate', this.historyChangedEventHandler);
        },

        changeUrl: function(url) {

            var urlData = this.parseUrl(url);

            var moduleController = this.getApp().getModuleController();

            if(moduleController.getActiveModule() && urlData.moduleName === moduleController.getActiveModule().getModuleName()) {

                currentModuleName = moduleController.getActiveModule().getModuleName();

                if(moduleController.getActiveModule().hasHistory()) {
                    moduleController.getActiveModule().resolveHistoryChange(urlData);
                    return;
                }
                else {
                    moduleController.getActiveModule().updateModuleStateHistory(urlData);
                }
            }
            else {

                // @to-do implement loading same module from another namespace programatically
                var defaultModuleNamespace = this.getApp().getConfig().getParameter('defaultModuleNamespace');

                moduleController.loadModule(defaultModuleNamespace + '.' + urlData.moduleName, null, false, false);
            }
        },

        goToHashUrl: function(hashName) {

            var urlData = this.parseUrl(window.location.href);

            if(hashName !== urlData.hashValue) {

                var url = urlData.baseUrl + "#" + hashName;

                var appName = this.getApp().getConfig().getParameter('appName');

                history.pushState(urlData.moduleName, appName + " - " + hashName, url);
            }
        },

        parseUrl: function(url) {

            var hashIndex = url.indexOf("#");
            var moduleName = '';
            var baseUrl = '';

            if(hashIndex !== -1) {

                baseUrl = url.substring(0, hashIndex);

                var hashValue = url.substring(hashIndex + 1, url.length);

                var moduleNameLengthIndex = hashValue.indexOf('/');

                if( moduleNameLengthIndex === -1) {
                    moduleNameLengthIndex = url.length;
                }

                moduleName = hashValue.substring(0, moduleNameLengthIndex);
            }

            return {
                baseUrl: baseUrl,
                moduleName: moduleName,
                hashValue: hashValue
            }
        },
    }
});