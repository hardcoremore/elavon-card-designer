CLASS.define({

    name: 'Base.Controller',

    definition: function(app, module) {

        var mainApp = app;
        var controlledModule = module; 

        this.getApp = function() {
            return mainApp;
        };

        this.setModule = function(module) {
            controlledModule = module;
        };

        this.getModule = function() {
            return controlledModule;
        };
    },

    prototypeMethods: {

        onlyNumbersKeyDownHandler: function(ev) {

            // left and right arrow
            if(ev.keyCode === 37 || ev.keyCode === 39){
                return true;
            }

            var inputField = $(ev.currentTarget);
            var inputFieldValue = $(ev.currentTarget).val();

            var isNumber = !isNaN(parseInt(ev.key));
            var isBackSpace = ev.keyCode === 8;

            if(!isNumber && !isBackSpace) {
                return false;
            }
            else if(isBackSpace && inputFieldValue.length === 1) {
                return false;
            }
        },

        init: function() {},

        addEvents: function() {},

        removeEvents: function() {},

        startController: function() {},

        stopController: function() {}
    }
});