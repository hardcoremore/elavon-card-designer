CLASS.define({

    name: 'Base.RESTRequest',

    definition: function(app) {

        var mainApp = app;
        var requestsOptions = {};

        this.getApp = function() {
            return mainApp;
        };

        this.setRequestsOptions = function(id, options) {
            requestsOptions[id] = options;
        };

        this.getRequestsOptions = function(id) {
            return requestsOptions[id];
        };

        this.removeRequestOptions = function(id) {
            delete requestsOptions[id];
        };

        this.getAllRequestsOptions = function() {
            return requestsOptions;
        }
    },

    prototypeMethods: {

        generateRequestId: function(requestType) {

            var time = new Date().getTime();
            var requestId = requestType + "-" + (Math.random() * 999999999) + "-" + time;

            return requestId;
        },

        GET: function(options) {

            var that = this;

            var requestId = this.generateRequestId('GET');
                options.requestId = requestId;

            this.setRequestsOptions(requestId, options);

            if(options.showLoader !== false) {
                this.getApp().showLoader();
            }

            $.ajax({
                url: options.url,
                dataType: "json",
                type: "GET",
                data: options.parameters,
                success: function(data, textStatus, jqXHR) {
                    that.requestSuccess(options, data, textStatus, jqXHR);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    that.requestError(options, jqXHR, textStatus, errorThrown);
                },
                complete: function(jqXHR, textStatus) {
                    that.requestComplete(jqXHR, textStatus);
                }
            });
        },

        POST: function(options) {

            var that = this;

            var requestId = this.generateRequestId('POST');
                options.requestId = requestId;

            this.setRequestsOptions(requestId, options);

            if(options.showLoader !== false) {
                this.getApp().showLoader();
            }

            $.ajax({

                url: options.url,
                dataType: options.dataType || "json",
                type: "POST",
                data: options.data,
                processData: options.processData,
                contentType: options.contentType,

                success: function(data, textStatus, jqXHR) {
                    that.requestSuccess(options, data, textStatus, jqXHR);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    that.requestError(options, jqXHR, textStatus, errorThrown);
                },
                complete: function(jqXHR, textStatus) {
                    that.requestComplete(jqXHR, textStatus);
                }
            });
        },

        PUT: function(options) {

            var that = this;

            var requestId = this.generateRequestId('PUT');
                options.requestId = requestId;

            this.setRequestsOptions(requestId, options);

            if(options.showLoader !== false) {
                this.getApp().showLoader();
            }

            $.ajax({
                url: options.url,
                dataType: "text",
                type: "PUT",
                data: options.data,
                success: function(data, textStatus, jqXHR) {
                    that.requestSuccess(options, data, textStatus, jqXHR);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    that.requestError(options, jqXHR, textStatus, errorThrown);
                },
                complete: function(jqXHR, textStatus) {
                    that.requestComplete(jqXHR, textStatus);
                }
            });
        },

        PATCH: function(options) {

            var that = this;

            var requestId = this.generateRequestId('PATCH');
                options.requestId = requestId;

            this.setRequestsOptions(requestId, options);

            if(options.showLoader !== false) {
                this.getApp().showLoader();
            }

            $.ajax({
                url: options.url,
                dataType: options.dataType || "text",
                type: "PATCH",
                data: options.data,
                success: function(data, textStatus, jqXHR) {
                    that.requestSuccess(options, data, textStatus, jqXHR);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    that.requestError(options, jqXHR, textStatus, errorThrown);
                },
                complete: function(jqXHR, textStatus) {
                    that.requestComplete(jqXHR, textStatus);
                }
            });
        },

        DELETE: function(options) {
            
            var that = this;

            var requestId = this.generateRequestId('DELETE');
                options.requestId = requestId;

            this.setRequestsOptions(requestId, options);

            if(options.showLoader !== false) {
                this.getApp().showLoader();
            }

            $.ajax({
                url: options.url,
                type: "DELETE",
                success: function(data, textStatus, jqXHR) {
                    that.requestSuccess(options, data, textStatus, jqXHR);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    that.requestError(options, jqXHR, textStatus, errorThrown);
                },
                complete: function(jqXHR, textStatus) {
                    that.requestComplete(jqXHR, textStatus);
                }
            });
        },


        requestSuccess: function(options, data, textStatus, jqXHR) {
            
            this.removeRequestOptions(options.requestId);

            if(this.isFunction(options.success)) {
                options.success.call(this, data, textStatus, jqXHR);
            }
        },

        requestError: function(options, jqXHR, textStatus, errorThrown) {

            this.removeRequestOptions(options.requestId);

            var error = jqXHR.responseJSON;

            try {

                error = jqXHR.responseJSON;

                if(!error) {
                    error = JSON.parse(jqXHR.responseText);
                }

                if(!error) {
                    error = errorThrown;
                }
            }
            catch(err) {
                error = err;
            }

            if(this.isString(error)) {

                error = {
                    message: error,
                    code: jqXHR.status
                }
            }
            else if(this.isObject(error) && error.hasOwnProperty('code') === false) {
                error.code = jqXHR.status;
            }

            if(this.isFunction(options.error)) {
                options.error.call(this, error, jqXHR, textStatus);
            }
            else {
                this.throwException('Request error', 1501, 'http');
            }
        },

        requestComplete: function(jqXHR, textStatus) {
            this.getApp().hideLoader();
        }
    }
});