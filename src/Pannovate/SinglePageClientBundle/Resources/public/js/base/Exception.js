CLASS.define({

    name: 'Base.Exception',

    definition: function(errorMessage, code) {

        this.getMessage = function() {
            return errorMessage
        };

        this.getCode = function() {
            return code;
        }
    }
});