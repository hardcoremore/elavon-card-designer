CLASS.define({

    name: 'Base.Application',

    definition: function(c) {

        var that = this;

        var config = c;

        var request = null;
        var localStorage;
        var urlData;

        var historyController;
        var moduleController;

        var loaderElement = null;

        this.getConfig = function() {
            return config;
        };

        this.getRequest = function() {
            return request;
        };

        this.setRequest = function(r) {
            request = r;
        };

        this.setLocalStorage = function(ls) {
            localStorage = ls;
        };

        this.getLocalStorage = function() {
            return localStorage;
        };

        this.setUrlData = function(data) {
            urlData = data;
        };

        this.getUrlData = function() {
            return urlData;
        };

        this.setModuleController = function(mc) {
            moduleController = mc;
        };

        this.getModuleController = function() {
            return moduleController;
        };

        this.setHistoryController = function(hc) {
            historyController = hc;
        };

        this.getHistoryController = function() {
            return historyController;
        };



        this.setLoaderElement = function(el) {
            loaderElement = el;
        };

        this.getLoaderElement = function() {
            return loaderElement;
        };

        this.globalErrorEventHandler = function(errorMsg, scriptName, lineNumber, colNumber, error) {

            var message = error.hasOwnProperty('getMessage') ? error.getMessage() : error;

            if(console) {
                console.log('Uncaught error handler.\nMessage: "' + message + '".\nAt script: ' + scriptName + '.\nOn line: ' + lineNumber + ", at col: " + colNumber);
            }

            if(error) {

                var code = error.hasOwnProperty('getCode') ? error.getCode() : 1500;

                that.resolveError({
                    code: code,
                    message: 'Message: ' + message
                });
            }

            return true;
        };
    },

    prototypeMethods: {

        init: function() {

            window.onerror = this.globalErrorEventHandler;

            // initialize request implementation
            this.setRequest(this.getInstance('Base.RESTRequest', [this]));

            // initialize app controllers
            this.setHistoryController(this.getInstance('Controllers.History', [this]));
            this.getHistoryController().init();

            var urlData = this.getHistoryController().parseUrl(window.location.href);
            this.setUrlData(urlData);
        },

        startApp: function() {},

        /*** THIS METHOD SHOULD BE OVERWRITTEN IN CONCRETE IMPLEMENTATION ***/
        showNotification: function(type, message, id) {
            alert(message);
        },

        compileTemplate: function(templateId, templateData, partials, callBack) {

            var source = $("#" + templateId).html();
            var template = Handlebars.compile(source);

            if(partials && partials.length > 0) {

                var part;

                for(var i = 0; i < partials.length; i++) {
                    part = partials[i];
                    Handlebars.registerPartial(part.dataName, $("#" + part.templateId).html());
                }
            }

            var html = template(templateData);

            if(callBack && this.isFunction(callBack)) {
                callBack.call(this, html);
            }

            return html;
        },

         /**
         *
         * Returns the model instance
         *
         * @param {String} modelClassName Class name of the model
         * @param {Boolean} singleton If singleton is false then new instance of model will be returned. Default value is true. 
         *
         * @return {Object} Returns model instance
         *
         */
        getModel: function(modelClassName, paramsArgs, singleton) {

            try {

                var args = [this];

                if(this.isArray(paramsArgs)) {
                    args = args.concat(paramsArgs);
                }

                return this.getInstance(modelClassName, args, singleton);
            }
            catch (error) {
                console.log(error);
                this.resolveError(error);
            }
        },

        getController: function(controllerClassName, arguments) {

            try {

                var args = [this];

                if(this.isArray(arguments)) {
                    args = args.concat(arguments);
                }

                return this.getInstance(controllerClassName, args, false);
            }
            catch (error) {
                console.log(error);
                this.resolveError(error);
            }
        },

        startController: function(controller) {
            controller.init();
            controller.addEvents();
            controller.startController();
        },

        stopController: function(controller) {
            controller.removeEvents();
            controller.stopController();
        },

        resolveError: function(errorThrown, jqXHR, textStatus) {

            if(this.isObject(errorThrown)) {

                var code = 0;
                var message = 'Error ocurred';

                if(errorThrown.hasOwnProperty('getCode')) {
                    code = errorThrown.getCode();
                }
                else {
                    code = errorThrown.code;
                }

                if(errorThrown.hasOwnProperty('getMessage')) {
                    message = errorThrown.getMessage();
                }
                else {
                    message = errorThrown.message;
                }

                switch(code) {

                    case 1403:

                    break;

                    case 403:
                    case 500:
                        this.showNotification('error', message + " Code: " + code);
                    break;

                    case 1500:
                        this.showNotification('error', "Application error occurred. Error message: '" + message + ". Error Code: " + code + "'");
                    break;
                }
            }
        }
    }
});