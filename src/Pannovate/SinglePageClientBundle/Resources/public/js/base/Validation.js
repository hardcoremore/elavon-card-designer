CLASS.define({

    name: 'Base.Validation',

    definition: function() {

        var validationErrors = {};
        var validationErrorMessage = '';
        
        this.setValidationErrorMessage = function(m) {
            validationErrorMessage = m;
        };

        this.getValidationErrorMessage = function() {
            return validationErrorMessage;
        };

        this.getValidationErrors = function() {
            return validationErrors;
        };
        
        this.setValidationErrors = function(errors) {
            validationErrors = errors;
        };

        this.resetValidationErrors = function() {
            validationErrors = {};
            validationErrorMessage = '';
        };
    },

    prototypeMethods: {

        hasValidationErrors: function() {
            return Object.keys(this.getValidationErrors()).length > 0 || this.getValidationErrorMessage().length > 0;
        },

        /**
         *
         * Validate the supplied data based on the defined validation rules in child model.
         *
         * @param {Object} data Data to be validated against validation rules
         * @param {String} operationType Operation type for validating data
            Some validation rules in the model can be defined to be effective 
            only for certain operation like update or create.
         * 
         * @param {Boolean} Returns true if data is valid, false otherwise.
         *
         */
        validate: function(data, operationType) {

            this.resetValidationErrors();

            var isDataValid = true;
            var validationRules = this.getValidationRules();

            var rule = null;
            var isFieldValueValid = true;

            for(var r in validationRules) {

                rule = validationRules[r];

                if(this.isArray(rule.anyField)) {

                   isFieldValueValid = this.validateAnyFieldRule(rule, data, operationType);
                }
                else {
                    isFieldValueValid = this.validateFieldValue(rule, rule.field, data[rule.field], operationType);
                }
                

                if(isFieldValueValid === false) {
                    isDataValid = false;
                }
            }

            return isDataValid;
        },

        /**
         *
         * Validate the data field against any field validation rule.
           Some validation rules can be defined to be valid if any of defined fields is valid.
           For example you can define validation rule to check if either firstName or lastName field is valid in the data.
         *
         * @param {Object} data Data to be validated against validation rules
         * @param {String} operationType Operation type for validating data
            Some validation rules in the model can be defined to be effective 
            only for certain operation like update or create.
         * 
         * @param {Boolean} Returns true if data is valid, false otherwise.
         *
         */
        validateAnyFieldRule: function(rule, data, operationType) {

            // check if validation is required for specific operation only
            if(rule.forOperation && rule.forOperation.indexOf(operationType) === -1) {
                return true;
            }

            var isValid = true;

            var modelFieldIndex = null;
            var fieldName = null;

            var invalidFieldsNumber = 0;

            var isFieldValueValid = true;
            var invalidFields = [];

            for(modelFieldIndex in rule.anyField) {

                fieldName = rule.anyField[modelFieldIndex];

                isFieldValueValid = this.validateFieldValue(rule, fieldName, data[fieldName], operationType);

                if(isFieldValueValid === false) {
                    invalidFieldsNumber++;
                    invalidFields.push(fieldName);
                }
            }

            var errorMessage = rule.globalMessage;
            var allFieldsAreInvalid = invalidFieldsNumber === rule.anyField.length;
            var oneFieldIsValid = (rule.anyField.length - invalidFieldsNumber) === (rule.anyField.length - 1);

            if(rule.onlyOne === true && (allFieldsAreInvalid || !oneFieldIsValid) ) {
                
                isValid = false;

                if(typeof errorMessage === 'undefined') {
                    errorMessage = "You have to fill only one field from ";
                    errorMessage += rule.anyField.toString() + " fields";
                }

                this.setValidationErrorMessage(errorMessage);
            }
            else if(allFieldsAreInvalid) {

                isValid = false;

                if(typeof errorMessage === 'undefined') {
                    errorMessage = "You have to fill either of the fields ";
                    errorMessage += rule.anyField.toString();
                }

                this.setValidationErrorMessage(errorMessage);
            }


            if(isValid) {

                var fieldErrors = this.getValidationErrors();

                for(var i = 0; i < invalidFields.length; i++) {
                    delete fieldErrors[invalidFields[i]];
                }
            }

            return isValid;
        },

        /**
         *
         * Validate the single field
         *
         * @param {string} fieldName Validator will take all validation rules that are bound to supplied fieldName and validate fieldData against those rules
         * @param {String|Number|Boolean|Object} fieldData Field data to be validated
            Some validation rules in the model can be defined to be effective
            only for certain operation like update or create.
         * 
         * @param {Boolean} Returns true if field data is valid, false otherwise.
         *
         */
        validateField: function(fieldName, fieldData, operationType) {

            var valid = true;
            var validationRules = this.getValidationRules();

            var rule = null;

            for(var r in validationRules) {

                rule = validationRules[r];

                if(rule.field !== fieldName) {
                    continue;
                }

                valid = this.validateFieldValue(validationRules[r], fieldName, fieldData, operationType);
            }

            return valid;
        },

         /**
         *
         * Validate the single cell from jq grid. This is adapter function
         *
         * @param {String|Number|Boolean|Object} newCellValue New value from cell to be validated
         * @param {string} columnName Validator will take all validation rules that are bound to supplied fieldName and validate fieldData against those rules
         * @param {Object} rowData Data from entire row in the jq grid
         *
         * @param {Boolean} Returns true if field data is valid, false otherwise.
         *
         */
        validateCellFromGrid: function(newCellValue, columnName, rowData) {

            return this.validateFieldValue(columnName, newCellValue, 'patch');
        },

        /**
         *
         * Validate the field value against validation rule.
         *
         * @param {object} rule Rule that will be used for validating field value
         * @param {String} fieldName Name of the field for which validation errors will be bound
           @param {String|Number|Boolean|Object} fieldData Field data to be validated
            Some validation rules in the model can be defined to be effective
            only for certain operation like update or create.
         * 
         * @return {Boolean} Returns true if field data is valid, false otherwise.
         *
         */
        validateFieldValue: function(rule, fieldName, fieldData, operationType) {

            var value = fieldData;
            var valid = true;
            
            var errorMessage = "";
            var errorMessages = this.getApp().getConfig().getParameter('errorMessages');

            // check if validation is required for specific operation only
            if(rule.forOperation && rule.forOperation.indexOf(operationType) === -1) {
                return true;
            }


            if(value && !Array.isArray(value)) {
                value = value.toString();
            }

            switch(rule.type) {

                case 'presence':

                    if(!value || value.length < 1) {
                        valid = false;
                        errorMessage = rule.message || errorMessages.fieldIsRequired;
                        this.addValidationError(fieldName, errorMessage);
                    }

                break;

                case 'format':

                        var pattern = new RegExp(rule.pattern);
                        var result = pattern.test(value);

                        if(result !== true) {
                            valid = false;
                            errorMessage = rule.message || errorMessages.fieldInvalidFormat;
                            this.addValidationError(fieldName, errorMessage);
                        }

                break;

                case 'length':

                    if(!value || value.length < rule.min || value.length > rule.max) {
                        valid = false;
                        errorMessage = rule.message || errorMessages.fieldLengthError;
                        this.addValidationError(fieldName, errorMessage);
                    }

                break;

                case 'range':

                    var valueInteger = parseFloat(value);

                    if(valueInteger < rule.min || valueInteger > rule.max) {
                        valid = false;
                        errorMessage = rule.message || errorMessages.fieldRangeError;
                        this.addValidationError(fieldName, errorMessage);
                    }

                break;
            }

            return valid;
        },

        /**
         *
         * Adds validation error for field
         *
         * @param {String} fieldName Name of the field
         * @param {String} errorMessage Error message
         *
         */
        addValidationError: function(fieldName, errorMessage) {
            
            var validationErrors = this.getValidationErrors();
            var fieldErrors = validationErrors[fieldName];

            if(!fieldErrors) {
                fieldErrors = [];
            }

            fieldErrors.push(errorMessage);
            validationErrors[fieldName] = fieldErrors;

            this.setValidationErrors(validationErrors);
        }
    }
});