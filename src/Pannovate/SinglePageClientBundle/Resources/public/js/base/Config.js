CLASS.define({

    name: 'Base.Config',

    definition: function(params) {

        this.getEnv = function() {
            return this.getAllParameters().env;
        },

        this.getBaseUrl = function() {
            return this.getAllParameters().baseUrl;
        },

        this.getBaseAPIUrl = function () {
            return this.getAllParameters().baseUrl + this.getAllParameters().apiBaseUrl;
        },

        this.getModuleViewBaseUrl = function() {
            return this.getAllParameters().baseUrl + this.getAllParameters().moduleViewBaseUrl;
        },

        this.getAllParameters = function() {
            return params;
        },

        this.getParameter = function(name) {

            if(params.hasOwnProperty(name)) {
                return params[name];
            }
            else {
                return null;
            }
        }
    }
});