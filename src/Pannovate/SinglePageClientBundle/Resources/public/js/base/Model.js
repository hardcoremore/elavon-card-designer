CLASS.define({

    name: 'Base.Model',

    definition: function(app) {

        this.extendFromClass('Base.Validation');

        this.EMAIL_VALIDATION_REGEX = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;

        var mainApp = app;

        var selectData = null;
        var loadedData = null;

        this.getApp = function() {
            return mainApp;
        };

        this.getConfigParameter = function(name) {
            return this.getApp().getConfig().getParameter(name);
        };

        this.getLoadedData = function() {
            return loadedData;
        };

        this.setLoadedData = function(data) {
            loadedData = data;
        };

        this.getSelectData = function() {
            return selectData;
        };

        this.setSelectData = function(sd) {
            selectData = sd;
        };

        this.getLoadedDataRowByProperty = function(propertyName, value) {

            var row = null;
            var allLoadedData = this.getLoadedData();
            value = value.toString();

            if(this.isArray(allLoadedData)) {

                for(var i = 0; i < allLoadedData.length; i++) {

                    row = allLoadedData[i];

                    if(row.hasOwnProperty(propertyName) && row[propertyName].toString() === value) {
                        return row;
                    }
                }
            }
            else {
                return null;
            }
        };
    },

    prototypeMethods: {

        getUrl: function(part) {
            return this.getApp().getConfig().getBaseAPIUrl() + this.getUrlPart() + (part || '');
        },

        checkIfCharCodeIsAlhpaNumberic: function(c) {
            return (c > 47 && c < 58) || (c > 64 && c < 90) || (c > 96 && c < 123);
        },

        generateUploadId: function() {

            var r = '';
            var c = 0;

            for(var i = 0; i < 32; i++) {

                // numbers and letters only
                while(this.checkIfCharCodeIsAlhpaNumberic(c) === false) {
                    c = Math.round(Math.random() * 123);
                }

                r += String.fromCharCode(c);

                c = 0;
            }

            return r;
        },

        dataURItoBlob: function(dataURI) {

            var byteString = atob(dataURI.split(',')[1]);

            var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

            var ab = new ArrayBuffer(byteString.length);
            var ia = new Uint8Array(ab);

            for (var i = 0; i < byteString.length; i++) {
                ia[i] = byteString.charCodeAt(i);
            }

            return new Blob([ab], { "type": mimeString });
        },

         /**
         *
         * Returns the record in select data by value.
         *
         * @param {Object} value
         *
         * @return {Object} Returns selected record from select data
         *
         */
        getSelectedRecordByValue: function(value) {

            if(!value) {
                return null;
            }

            var selectData = this.getSelectData();
            value = value.toString();

            for(var record in selectData) {

                if(selectData[record]) {

                    if (selectData[record].toString() === value) {
                        return selectData[record];
                    }
                    else if(selectData[record].id && selectData[record].id.toString() === value) {
                        return selectData[record];
                    }
                }
            }
        },

        uploadFile: function(url, file, postData, options) {

            var uploadId = options.uploadId || this.generateUploadId();
            var that = this;

            $.extend(options, true, {

                url: url + '?X-Progress-ID=' + uploadId,

                success: function(responseData, textStatus, jqXHR) {
                    that.dispatchSuccess(that.events.UPLOAD_FILE_COMPLETE, responseData, options, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.UPLOAD_FILE_ERROR, error, options, jqXHR);
                }
            });

            var data = new FormData();
                data.append('file', file);

            if(postData && this.isObject(postData)) {

                var postKeys = Object.keys(postData);

                // add post parameters to FormData
                for(var i in postKeys) {
                    data.append(postKeys[i], postData[postKeys[i]]);
                }
            }

            options.processData = false;
            options.contentType = false;

            options.data = data;

            this.getApp().getRequest().POST(options);

            return uploadId;
        },

        readFileUploadProgress: function(url, uploadId, options) {

            var that = this;
            options.uploadId = uploadId;
            
            $.extend(options, true, {

                url: url + '?X-Progress-ID=' + uploadId,

                success: function(responseData, textStatus, jqXHR) {
                    that.dispatchSuccess(that.events.READ_FILE_UPLOAD_PROGRESS_COMPLETE, responseData, options, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_FILE_UPLOAD_PROGRESS_ERROR, error, options, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

         /**
         *
         * Create new record in the database.
         *
         * @param {Object} data Data of the record to be saved
         * @param {Object} options Options for creating new record
            {
                data: @param {Object} Data for the record to be created
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
            }
         *
         */
        create: function(data, options) {

            var that = this;

            var constants = this.getNamespaceValue('Static.Constants');

            if(this.validate(data, constants.CREATE_OPERATION)) {

                $.extend(options, true, {

                    url: this.getUrl(),

                    success: function(responseData, textStatus, jqXHR){
                        that.modelCreateComplete(that.events.CREATE_COMPLETE, responseData, options);
                        that.dispatchSuccess(that.events.CREATE_COMPLETE, responseData, options, jqXHR);
                    },

                    error: function(error, jqXHR, textStatus) {
                        that.dispatchError(that.events.CREATE_ERROR, error, options, jqXHR);
                    }
                });

                options.data = data;
                this.getApp().getRequest().POST(options);
            }
            else {

                var e = $.Event(
                    that.events.VALIDATION_ERROR,
                    {
                        validationErrors:this.getValidationErrors(),
                        validationErrorMessage: this.getValidationErrorMessage(),
                        scope: options.scope
                    }
                );

                $(document).trigger(e);
            }
        },

         /**
         *
         * Reads the records. This function is supposed to be called with paging parameters.
         *
         * @param {Object} options Options for creating new record.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
                parameters: @param {Object} What parameters to add on the url.
            }
         *
         */
        read: function(options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/list'),

                success: function(responseData, textStatus, jqXHR) {

                    if(that.checkResponseFormat(responseData) === true) {

                        if(responseData.listData) {
                            loadedData = responseData.listData;
                        }
                        else {
                            loadedData = responseData;
                        }

                        that.dispatchSuccess(that.events.READ_COMPLETE, responseData, options, jqXHR);
                        that.modelReadComplete(that.events.READ_COMPLETE, responseData, options);
                    }
                    else {

                        var error = {
                            code: that.getConfigParameter('errorCodes').invalidResponseFormat,
                            message: that.getConfigParameter('errorCodes').invalidResponseFormatMsg
                        };

                        that.dispatchError(that.events.READ_ERROR, error, options, jqXHR);
                    }
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_ERROR, error, options, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

         /**
         *
         * Reads all the records. This function should not be called with paging parameters.
         *
         * @param {Object} options Options for reading through record.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
                parameters: @param {Object} What parameters to add on the url.
            }
         *
         *
         */
        readAll: function(options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/list-all'),

                success: function(responseData, textStatus, jqXHR) {
                    that.setLoadedData(responseData);
                    that.dispatchSuccess(that.events.READ_ALL_COMPLETE, responseData, options, jqXHR);
                    that.modelReadComplete(that.events.READ_ALL_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_ERROR, error, options, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

         /**
         *
         * Search trough all the records. This function can be called with paging parameters.
         *
         * @param {Object} options Options for searching through records.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
                parameters: @param {Object} What parameters to add on the url.
            }
         *
         *
         */
        quickSearch: function(options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/quick-search'),

                success: function(responseData, textStatus, jqXHR) {

                    if(that.checkResponseFormat(responseData) === true) {
                        loadedData = responseData.listData;
                        that.dispatchSuccess(that.events.READ_COMPLETE, responseData, options, jqXHR);
                        that.modelReadComplete(that.events.READ_COMPLETE, responseData, options);
                    }
                    else {

                        var error = {
                            code: that.getConfigParameter('errorCodes').invalidResponseFormat,
                            message: that.getConfigParameter('errorCodes').invalidResponseFormatMsg
                        };

                        that.dispatchError(that.events.READ_ERROR, error, options, jqXHR);
                    }
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_ERROR, error, options, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

        /**
         *
         * Read all records for select data, i.e. for select combo box.
         *
         * @param {Object} options Options for reading all records for select data.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
                parameters: @param {Object} What parameters to add on the url.
            }
         *
         *
         */
        readForSelect: function(options) {

            var that = this;
            $.extend(options, true, {


                url: this.getUrl('/list-select'),

                success: function(responseData, textStatus, jqXHR){
                    that.setSelectData(responseData);
                    that.dispatchSuccess(that.events.READ_FOR_SELECT_COMPLETE, responseData, options, jqXHR);
                    that.modelReadComplete(that.events.READ_FOR_SELECT_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_FOR_SELECT_ERROR, error, options, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

         /**
         *
         * Search all records for supplied query string for autocomplete.
         *
         * @param {Object} options Options for searching all records for autocomplete.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
                parameters: @param {Object} What parameters to add on the url.
            }
         *
         *
         */
        readForAutoComplete: function(options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/list-auto-complete'),

                success: function(responseData, textStatus, jqXHR){
                    that.dispatchSuccess(that.events.READ_FOR_AUTOCOMPLETE_COMPLETE, responseData, options, jqXHR);
                    that.modelReadComplete(that.events.READ_FOR_AUTOCOMPLETE_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_FOR_AUTOCOMPLETE_ERROR, error, options, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

        /**
         *
         * Search all records for supplied date range if record has start and end properties.
         *
         * @param {Object} options Options for searching all records for supplied date range.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
                parameters: @param {Object} What parameters to add on the url.
            }
         * @param {String} start Start date in format yyyy-mm-dd, i.e. 1987-01-30
         * @param {String} end End date in format yyyy-mm-dd, i.e. 2115-01-30
         *
         */
        readForDateRange: function(options, start, end) {

            var that = this;

            $.extend(options, true, {

                url: this.getApp().getConfig().getBaseAPIUrl() + this.getUrl() + '/listForDateRange/start/' + start + '/end/' + end,

                success: function(responseData, textStatus, jqXHR){
                    that.dispatchSuccess(that.events.READ_FOR_DATE_RANGE_COMPLETE, responseData, options, jqXHR);
                    that.modelReadComplete(that.events.READ_FOR_DATE_RANGE_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_FOR_DATE_RANGE_ERROR, error, options, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

        /**
         *
         * Get record by id.
         *
         * @param {Object} options Options for getting record by id.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
            }
         * @param {Number} id Id of the record
         *
         */
        findById: function(id, options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/' + id),
                showLoader: options.showLoader,

                success: function(responseData, textStatus, jqXHR){
                    that.dispatchSuccess(that.events.FIND_BY_ID_COMPLETE, responseData, options, jqXHR, jqXHR);
                    that.modelReadComplete(that.events.FIND_BY_ID_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.FIND_BY_ID_ERROR, error, options, jqXHR, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

        readLastModified: function(options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/get-last-modified'),
                showLoader: options.showLoader,

                success: function(responseData, textStatus, jqXHR){
                    that.dispatchSuccess(that.events.READ_LAST_MODIFIED_COMPLETE, responseData, options, jqXHR, jqXHR);
                    that.modelReadComplete(that.events.READ_LAST_MODIFIED_COMPLETE, responseData, options);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.READ_LAST_MODIFIED_ERROR, error, options, jqXHR, jqXHR);
                }
            });

            this.getApp().getRequest().GET(options);
        },

        /**
         *
         * Update the record with new data.
         *
         * @param {Number} id Id of the record to update
         * @param {Object} data New data of the record to be updated with
         * @param {Object} options Options for updating record.
            {
                scope: @param {Object} In what scope to invoke success or error callback function
                showLoader: @param {Boolean} If true the global loader will be display while loading
            }
         *
         */
        update: function(id, data, options) {

            var that = this;

            var constants = this.getNamespaceValue('Static.Constants');

            if(this.validate(data, constants.UPDATE_OPERATION)) {

                $.extend(options, true, {
                    url: this.getUrl('/' + id),

                    success: function(responseData, textStatus, jqXHR){
                        selectData = null;
                        that.modelUpdateComplete(that.events.UPDATE_COMPLETE, id, data, options);
                        that.dispatchSuccess(that.events.UPDATE_COMPLETE, responseData, options, jqXHR, jqXHR);
                    },

                    error: function(error, jqXHR, textStatus) {
                        that.dispatchError(that.events.UPDATE_ERROR, error, options, jqXHR, jqXHR);
                    }
                });

                options.data = data;

                this.getApp().getRequest().PUT(options);
            }
            else {

                var e = $.Event(
                    that.events.VALIDATION_ERROR,
                    {
                        validationErrors: this.getValidationErrors(),
                        validationErrorMessage: this.getValidationErrorMessage(),
                        scope: options.scope
                    }
                );

                $(document).trigger(e);
            }
        },

        patch: function(id, data, options) {

            var that = this;

            var constants = this.getNamespaceValue('Static.Constants');

            var isDataValid = true;
            var dataKeys = Object.keys(data);

            for(var i = 0, len = dataKeys.length; i < len; i++) {
                isDataValid = this.validateField(dataKeys[i], data[dataKeys[i]], constants.PATCH_OPERATION);
            }

            if(isDataValid) {

                $.extend(options, true, {

                    url: this.getUrl('/' + id),

                    success: function(responseData, textStatus, jqXHR){
                        that.dispatchSuccess(that.events.PATCH_COMPLETE, responseData, options, jqXHR, jqXHR);
                        that.modelPatchComplete(that.events.PATCH_COMPLETE, responseData, options);
                    },

                    error: function(error, jqXHR, textStatus) {
                        that.dispatchError(that.events.PATCH_ERROR, error, options, jqXHR, jqXHR);
                    }
                });

                options.data = data;

                this.getApp().getRequest().PATCH(options);
            }
            else {

                var e = $.Event(
                    that.events.VALIDATION_ERROR,
                    {
                        validationErrors: this.getValidationErrors(),
                        validationErrorMessage: this.getValidationErrorMessage(),
                        scope: options.scope
                    }
                );

                $(document).trigger(e);
            }
        },

        delete: function(id, options) {

            var that = this;

            $.extend(options, true, {

                url: this.getUrl('/' + id),
                showLoader: options.showLoader,

                success: function(responseData, textStatus, jqXHR) {

                    that.modelDeleteComplete(that.events.DELETE_COMPLETE, id, options);
                    that.dispatchSuccess(that.events.DELETE_COMPLETE, id, options, jqXHR);
                },

                error: function(error, jqXHR, textStatus) {
                    that.dispatchError(that.events.DELETE_ERROR, error, options, jqXHR);
                }
            });

            this.getApp().getRequest().DELETE(options);
        },

        addValidationError: function(fieldName, errorMessage) {

            var validationErrors = this.getValidationErrors();
            var fieldErrors = validationErrors[fieldName];

            if(!fieldErrors) {
                fieldErrors = [];
            }

            fieldErrors.push(errorMessage);
            validationErrors[fieldName] = fieldErrors;

            this.setValidationErrors(validationErrors);
        },

        getFieldByName: function(fieldName) {

            var fields = this.getFields();

            var name = fieldName.toString();

            for(var f in fields) {
                if(fields[f].name === name) {
                    return fields[f];
                }
            }
        },


        dispatchSuccess: function(eventName, data, options, jqXHR) {

            if(this.isString(eventName) && eventName.length > 0) {

                var eventOptionsData = {
                    responseData:data,
                    model: this,
                    jqXHR:jqXHR
                };

                if(options) {
                    $.extend(eventOptionsData, options);
                }

                var e = $.Event(eventName, eventOptionsData);
                $(document).trigger(e);
            }
            else {
                this.throwException("Invalid event name supplied.", 1500, 'kernel');
            }
        },

        dispatchError: function(eventName, error, options, jqXHR) {

            var e = $.Event(eventName, {error:error, scope:options.scope, model:this, jqXHR:jqXHR});
            $(document).trigger(e);
        },

        modelCreateComplete: function(event, data, options) {

            if(this.getSelectData() && this.isArray(this.getSelectData())) {

                var selectData = this.getSelectData();
                    selectData.push(data);

                this.setSelectData(selectData);
            }

            if(this.isArray(this.getLoadedData()) === false) {
                this.setLoadedData([data]);
            }
            else {
                var loadedData = this.getLoadedData();
                    loadedData.push(data);
                this.setLoadedData(loadedData);
            }
        },

        modelReadComplete: function(event, data, options){},

        modelUpdateComplete: function(event, id, data, options) {

            var loadedData = this.getLoadedData();
            var item, fieldModelData, dataKeys, relationSelectData;

            if(this.isArray(loadedData) && loadedData.length > 0) {

                for(var i = 0, len = loadedData.length; i < len; i++) {

                    item = loadedData[i];

                    if(item.id.toString() === id.toString()) {

                        dataKeys = Object.keys(data);

                        for(var k = 0, len = dataKeys.length; k < len; k++) {

                            fieldModelData = this.getFieldByName(dataKeys[k]);

                            if(fieldModelData.type === 'relation') {
                                relationSelectData = fieldModelData.relationModel.getSelectedRecordByValue(data[dataKeys[k]]);
                                item[dataKeys[k]] = relationSelectData;
                            }
                            else {
                                item[dataKeys[k]] = data[dataKeys[k]];
                            }
                        }
                        
                        loadedData[i] = item;
                        break;
                    }
                }

                this.setLoadedData(loadedData);
            }
        },

        modelPatchComplete: function(event, data, options){},

        modelDeleteComplete: function(event, id, options) {

            var loadedData = this.getLoadedData();
            var item;

            if(this.isArray(loadedData) && loadedData.length > 0) {

                for(var i = 0, len = loadedData.length; i < len; i++) {

                    item = loadedData[i];

                    if(item.id.toString() === id.toString()) {
                        loadedData.splice(i, 1);
                        break;
                    }
                }

                this.setLoadedData(loadedData);
            }
        }
    },

    extendPrototypeFrom: 'Base.Validation'

});
