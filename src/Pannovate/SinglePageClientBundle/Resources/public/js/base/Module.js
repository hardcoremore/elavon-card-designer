CLASS.define({

    name: 'Base.Module',

    definition: function(app, permissions) {

        this.constants = this.getNamespaceValue('Static.Constants');

        var mainApp = app;
        var modulePermissions = permissions;
        var moduleElement;

        var moduleViewLoaded = false;
        var moduleLoaded = false;
        var operationType = null;

        var moduleData = null;

        var initialized = false;

        this.setApp = function(app) {
            mainApp = app;
        };

        this.getApp = function() {
            return mainApp;
        };

        this.getPermissions = function() {
            return modulePermissions;
        };

        this.updatePermissions = function(permissions) {
            modulePermissions = permissions;
        };

        this.setModuleElement = function(mel) {
            moduleElement = mel;
        };

        this.getModuleElement = function() {
            return moduleElement;
        };

        this.getOperationType = function() {
            return operationType;
        };

        this.setOperationType = function(opt) {
            operationType = opt;
        };

        this.setModuleLoaded = function(loaded) {
            moduleLoaded = loaded;
        };

        this.getModuleLoaded = function() {
            return moduleLoaded;
        };

        this.setModuleViewLoaded = function(loaded) {
            moduleViewLoaded = loaded;
        };

        this.getModuleViewLoaded = function() {
            return moduleViewLoaded;
        };

        this.setModuleData = function(data) {
            moduleData = data;
        };

        this.getModuleData = function() {
            return moduleData;
        };

        this.setInitialized = function(inited) {
            initialized = inited;
        };

        this.getInitialized = function() {
            return initialized;
        };
    },

    prototypeMethods: {

        constants: null,

        initModule: function() {
            this.resolveModulePermissions();
            this.setInitialized(true);
        },

        startModule: function() {

            this.setControls();
            this.addEvents();

            var ev = $.Event(this.constants.MODULE_READY_EVENT, {module:this});
            $(document).trigger(ev);
        },

        stopModule: function() {

            this.removeEvents();

            var ev = $.Event(this.constants.MODULE_STOPPED_EVENT, {module:this});
            $(document).trigger(ev);
        },

        destroyModule: function(){

            this.hideModule();
            this.stopModule();
            this.removeControls();

            this.getModuleElement().remove();
            this.setModuleElement(null);

            this.setApp(null);

            this.setModuleData(null);

            var ev = $.Event(this.constants.MODULE_DESTROYED_EVENT, {module:this});
            $(document).trigger(ev);
        },

        setControls: function(){
            this.getModuleElement().find('select').selectBoxIt({
                // Uses the jQuery 'fadeIn' effect when opening the drop down
                showEffect: "fadeIn",

                // Sets the jQuery 'fadeIn' effect speed to 400 milleseconds
                showEffectSpeed: 400,

                // Uses the jQuery 'fadeOut' effect when closing the drop down
                hideEffect: "fadeOut",

                // Sets the jQuery 'fadeOut' effect speed to 400 milleseconds
                hideEffectSpeed: 400
            });
        },

        removeControls: function(){},

        addEvents: function(){},
        removeEvents: function(){},

        resolveModulePermissions: function() {},

        reinitializeModule: function(data, permissions) {

            this.setModuleData(data);

            if(permissions) {
                this.updatePermissions(permissions);
            }

            var ev = $.Event(this.constants.MODULE_REINITIALIZED_EVENT, {module:this});
            $(document).trigger(ev);
        },

        showModule: function() {

            this.getModuleElement().show();

            var ev = $.Event(this.constants.MODULE_SHOWN_EVENT, {module:this});
            $(document).trigger(ev);
        },

        hideModule: function() {

            this.getModuleElement().hide();

            var ev = $.Event(this.constants.MODULE_HIDDEN_EVENT, {module:this});
            $(document).trigger(ev);
        },

        /*
         * This method should be implemented in child class
         */
        hasHistory: function() {
            return false;
        },

        /*
         * This method should be implemented in child class
         */
        historyBack: function() {},

        /*
         * This method should be implemented in child class
         */
        updateModuleStateHistory: function(urlData) {},

        /*
         * This method should be implemented in child class
         */
        resetModule: function() {},

        /*
         * This method is fired on active module when window is resized
         */
        updateModuleDimensions: function() {},

        /*
         * This method must be implemented in child class
         */
        getModuleName: function() {
            return 'Module';
        }
    }
});
