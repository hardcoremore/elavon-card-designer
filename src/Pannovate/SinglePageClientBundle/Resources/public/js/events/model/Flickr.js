CLASS.createNamespace('Events.Model.Flickr', {

	VALIDATION_ERROR: 'Model.Flickr.validationError',

    READ_USER_ID_COMPLETE: 'Model.Flickr.readUserIdComplete',
    READ_USER_PHOTOS_COMPLETE: 'Model.Flickr.readUserPhotosOver',
    PHOTO_SELECTED: 'Model.Flickr.PHOTO_SELECTED'
});