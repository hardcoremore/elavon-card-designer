CLASS.createNamespace('Events.Model.User', {

    VALIDATION_ERROR: 'Model.User.validationError',

    CREATE_COMPLETE: 'Model.User.createComplete',
    CREATE_ERROR: 'Model.User.createError',

    READ_COMPLETE: 'Model.User.readComplete',
    READ_ERROR: 'Model.User.readError',

    READ_ALL_COMPLETE: 'Model.User.readAllComplete',
    READ_ALL_ERROR: 'Model.User.readAllError',

    UPDATE_COMPLETE: 'Model.User.updateComplete',
    UPDATE_ERROR: 'Model.User.updateError',

    PATCH_COMPLETE: 'Model.User.patchComplete',
    PATCH_ERROR: 'Model.User.patchError',

    DELETE_COMPLETE: 'Model.User.deleteComplete',
    DELETE_ERROR: 'Model.User.deleteError',

    LOGIN_COMPLETE: 'Model.User.loginComplete',
    LOGIN_ERROR: 'Model.User.loginError',

    LOGOUT_COMPLETE: 'Model.User.logoutComplete',
    LOGOUT_ERROR: 'Model.User.logoutError',

    LOAD_CURRENT_USER_COMPLETE: 'Model.User.loadCurrentUserComplete',
    LOAD_CURRENT_USER_ERROR: 'Model.User.loadCurrentUserError'
});
