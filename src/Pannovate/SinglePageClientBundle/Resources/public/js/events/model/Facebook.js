CLASS.createNamespace('Events.Model.Facebook', {

    LOGIN_COMPLETE: 'Model.Facebook.loginComplete',
    ALBUMS_LOAD_COMPLETE: 'Model.Facebook.albumsLoadComplete',
    ALBUM_COVER_PHOTO_LOAD_COMPLETE: 'Model.Facebook.albumCoverPhotoLoadComplete',
    ALBUM_PHOTOS_LOAD_COMPLETE: 'Model.Facebook.albumPhotosLoadComplete',
    PHOTO_LOAD_COMPLETE: 'Model.Facebook.photoLoadComplete',
    PHOTO_SELECTED: 'Model.Facebook.PHOTO_SELECTED'

});