Number.prototype = {

    formatNumberTaousands: function(tausandSeparator) {
        return this.toString().replace(/\B(?=(\d{3})+(?!\d))/g, tausandSeparator || ",");
    },

    addLeadingZero: function() {

        var numberString = this.toString();

        if(numberString.length === 1) {
            return "0" + numberString;
        }
        else {
            return numberString;
        }
    }
};