fabric.Canvas.prototype.getAbsoluteCoords = function(object) {
    return {
        left: object.left + this._offset.left,
        top: object.top + this._offset.top
    };
};

fabric.Object.prototype.hide = function() {
    this.set({
        opacity: 0,
        selectable: false
    });
};

fabric.Object.prototype.show = function() {
    this.set({
        opacity: 1,
        selectable: true
    });
};