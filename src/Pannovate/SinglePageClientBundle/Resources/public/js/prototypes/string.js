String.prototype = {
    
    firstCharacterToUpperCase: function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    },
  
    lastCharacterToUpperCase: function() {
        return this.charAt(this.length - 1).toUpperCase() + this.slice(0, this.length - 1);
    }
};