/*
 * jQuery Collapse
 * Collapse elements
 *
 * @version v1.0 (20 Oct 2015)
 *
 * Homepage:
 * http://www.pannovate.com
 *
 * Authors:
 * Zlatko Vujicic
 *
 * Dependencies:
 *   jQuery v1.6+
 *    jQuery UI v1.8+
 */
;(function($) {
    $.widget( "ui.collapse", {

        //default options
        options: {
            collapsInClass: 'collapse-in',
            iconClass: 'icon-32'
        },

        // the constructor
        _create: function() {

            this._on( this.element, {
                "click .collapse-item-header": this.headerClickEventHandler
            });
        },

        headerClickEventHandler: function(ev) {
            $(ev.currentTarget).parent().find('.collapse-item-body').toggleClass(this.options.collapsInClass);
            $(ev.currentTarget).find('.icon-indicator').toggleClass(this.options.iconClass);
        }
    });
})(jQuery);