/*
 * jQuery Tab
 * Tab content
 *
 * @version v1.0 (28 Nov 2015)
 *
 * Homepage:
 * http://www.pannovate.com
 *
 * Authors:
 * Caslav Sabani
 *
 * Dependencies:
 *   jQuery v1.6+
 *    jQuery UI v1.8+
 */
;(function($) {
    $.widget( "ui.tab", {

        //default options
        options: {
            contentHolder: '',
            defaultTab: ''
        },

        _tabsListEl: null,
        _contentHolderEl: null,

        // the constructor
        _create: function() {

            this._tabsListEl = $(this.element);
            this._contentHolderEl = $(this.options.contentHolder);

            this._on(this._tabsListEl.find('[data-role="tab-button"]'), {
                click: this._tabsListElementClickHandler
            });

            this.selectTab(this.options.defaultTab, false);

            if(this._contentHolderEl.find('.active-tab').length === 0) {
                this._contentHolderEl.children().first().addClass('active-tab').show();
            }
        },

        selectTab: function(tab, triggerEvent) {

            this._contentHolderEl.children().removeClass('active-tab').hide();

            var tabIndex = 0;
            var tabContent;

            if(this._isNumeric(tab)) {

                if(this._contentHolderEl.children().length > tab) {

                    var tabEl = this._contentHolderEl.children().get(tab);

                    tabContent = $(tabEl);
                    tabContent.addClass('active-tab').show();

                    tabIndex = tab;
                }
            }
            else if(typeof tab === 'string' && tab.length > 1) {
                tabContent = $(tab);
                tabContent.addClass('active-tab').show();
                tabIndex = tabContent.index();
            }

            this._tabsListEl.children().removeClass('active');

            var tabButtonEl = this._tabsListEl.children().get(tabIndex);
            $(tabButtonEl).addClass('active');

            if(triggerEvent) {
                this._trigger('onTabSelected', null, {
                    selectedTab: tab,
                    tabIndex: tabIndex,
                    tabContent: tabContent
                });
            }
        },

        _tabsListElementClickHandler: function(ev) {            
            this.selectTab($(ev.currentTarget).data('tab-content'), true);
        },

        _isNumeric: function(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }
    });

})(jQuery);