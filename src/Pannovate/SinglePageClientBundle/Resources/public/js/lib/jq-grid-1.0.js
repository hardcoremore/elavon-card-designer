/*
* jQuery Grid
*
* @version v1.0 (3 July 2015)
*
* Copyright 2015, Pannovate.
*
*
* Homepage:
* http://www.pannovate.com
*
*
* Authors:
*   Caslav Sabani
*
*
* Dependencies:
*   jQuery v1.6+
*    jQuery UI core v1.8+
*/
;(function($) {

    $.widget('ui.grid', {
        options: {
            autoLoad: true,
            multiselect: false,
            localSort: false,
            cellEdit: false,
            cellEditType: 'local', // how to save cell data. can be local, remote or callback
            selectedRows: [],
            model: [],

            defaultCellEditErrorMessage: 'This value is invalid.',
            lockGridTemplate: null, // function returns the template for content of lock grid div

            /***
            model:[
                {
                    header: 'Monday',
                    id: 'id', // this is requiered field
                    dataMap: 'id',
                    formatter: function(cellValue, rowData, rowIndex){},
                    hidden: true,
                    sortable: true,
                    sortName: 'id',
                    editable: false,
                    editOptions: {
                        type: 'text', // e.g. 'text', textarea, select, 'checkbox'
                        availableOptions: [{value:1, label:'Options 1'}] // data to select from,
                        valueProperty: 'id',
                        labelProperty: 'name',
                        validateCallback: function(editedValue, columnName, rowData) { return true} // function to valdiate value
                        validateRegex: '' // regular expression to validate value. If validateCallback is defined it takes presedence over validateRegex
                        validationErrorMessage: 'This value is not valid' // message to be displayed if value is not valid
                        initializeInputCallback: function(inputElement) { return initializedInput} // function to create widget or other components for cell editing
                        getInputValueCallback: function(initializedInput) {} Get value from cell edit input 
                        setInputValueCallback: function(initializedInput or regular input, cellValue, rowData) {} Set value to cell edit input 
                    }
                }
            ],
            ***/

            url: '',

            ajaxOptions: {
                methodType: 'GET',
                parameters: null,
                getUrlCallback: null,
                dataListPropertyName: 'data'
            },

            cellEditAjaxOptions: {

                url: '',
                method: 'PATCH',
                dataType: 'text',

                // function (cellName, cellEditedValue, rowData)
                // if url is not the same for all cell updates
                // return url in callback
                getUrlCallback: null,

                // function (response, cellName, cellEditedValue) {} Get error message from response when error occurs after cell edit
                parseErrorMessageCallback: null,

                // function (cellName, cellEditedValue, rowData). format data sent to server when saving cell data
                formatDataCallback: null,
            },

            cellEditCallbackOptions: {

                callback: null, // function, callback to call to save cell data when cell is edited

                // callbackArgs array. Arguments to pass to callback function if arguments are static
                // note that cellName, cellEditedValue and rowData will always be first three arguments and
                // arguments defined in callbackArgs will be appended at the end.
                callbackArgs: null,

                // getCallbackArgsCallback(cellName, cellEditedValue, rowData)
                // get arguments for callback if arguments are not static.
                // if getCallbackArgsCallback is defined it has precendence over callbackArgs 
                // and it must return array of arguments
                getCallbackArgsCallback: null,

                callbackScope: null, // scope in which to invoke callback
            },


            sortColumnName: 'id',
            sortColumnOrder: 'desc',

            reader: {
                repeatItems: false,
                dataProperty: null,
                primaryKey: null
            },

            hiddenColumns: [],
            data: [],
            selectedRowIndex: null,
            selectedRowData: null,

            selectedRowEl: null,
            enableShortcuts: true,
            cellEdit: false,
        },

        _tableState: 'normal', // can be 'normal', 'loading', 'locked', 'editing'
        _isTableLocked: false,

        _tHeadEl: null,
        _tBodyEl: null,
        _wrapperOut: null,
        _lockGridContainer: null,

        _currentCellEditRowIndex: -1,
        _currentCellEditColumnName: '',
        _currentCellTdEl: null,
        _currentCellEditInputEl: null,
        _currentCellEditValidationErrorEl: null,
        _currentCellEditInitalizedInputEl: null,

        _currentSortColumnName: '',
        _currentSortColumnOrder: '',
        _maxTableHeight: null,
        _noRecordsFoundTREl: null,
        _scrollbarWidth: null,
        _resizeTimeout: 0,

        getSelectedRowsIndexes: function() {
            return this.options.selectedRows;
        },

        getSelectedRowsData: function() {

            var selectedRowsData = [];

            var selectedRowsIndexes = this.getSelectedRowsIndexes();
            var selectedRowsLen = selectedRowsIndexes.length;

            for(var i = 0; i < selectedRowsLen; i++) {
                selectedRowsData.push($.extend({}, this.options.data[selectedRowsIndexes[i]]));
            }

            return selectedRowsData;
        },

        getCurrentSortColumnName: function() {
            return this._currentSortColumnName;
        },

        getCurrentSortColumnOrder: function() {
            return this._currentSortColumnOrder;
        },

        _create: function() {

            if(this.options.data.length === 0) {
                this.options.data = [];
            }

            this.options.hiddenColumns = [];

            if(this.element.is('table') === false) {
                throw new Error(
                    'Grid can only be instantiated on table tag',
                    1700
                );
            }

            if(this.options.sortColumnName)
            {
                this._currentSortColumnName = this.options.sortColumnName;
            }

            if(this.options.sortColumnOrder)
            {
                this._currentSortColumnOrder = this.options.sortColumnOrder;
            }

            //wrap the table element
            this._wrapperOut =  $('<div/>', {
               class: 'table-wrapper-out'
            });

            var tableWrapper = $('<div/>', {
                class: 'table-wrapper'
            });

            this.element.after(this._wrapperOut);
            this._wrapperOut.append(tableWrapper);

            this._lockGridContainer = $('<div/>', {
                'class': 'jq-grid-lock-container'
            });

            if(this.options.lockGridTemplate && typeof this.options.lockGridTemplate === 'function') {
                this._lockGridContainer.html(this.options.lockGridTemplate());
            }
            else {
                this._lockGridContainer.append($('<div/>', {
                    'class': 'lock-grid-message',
                    text: 'Loading...'
                }));
            }

            this._wrapperOut.append(this._lockGridContainer);

            tableWrapper.append(this.element);

            this.element.addClass('jq-grid');

            this._tHeadEl = $('<thead/>');
            this._tBodyEl = $('<tbody/>');


            var tr = $('<tr/>');

            this._tHeadEl.append(tr);

            var th = null;
            var tha = null;

            if(this.options.multiselect === true) {
                
                th = $('<th/>', {
                    class: 'jq-grid-header jq-grid-header-multiselect'
                });

                var multiSelectCheckBox = this._createCheckBoxForMultiSelect(
                    'jq-grid-multiselect-' + (window.performance.now() * 100000000000),
                    'jq-grid-select-all',
                    true
                );

                th.append(multiSelectCheckBox);
                tr.append(th);
            }

            var that = this;
            $.each(this.options.model, function(index) {

                // skip if column does not have id
                if(this.id && this.id.toString().length > 0) {

                    th = $('<th/>', {
                        class: 'jq-grid-header jq-grid-header-' + this.id,
                        'data-id': this.id
                    });

                    tha = $('<a/>', {
                        text: this.header,
                        role: 'button',
                        'data-id': this.id
                    });

                    if(that.options.sortColumnName === this.id) {
                        tha.addClass('jq-grid-column-sort jq-grid-column-sort-' + that.options.sortColumnOrder);
                    }

                    th.append(tha);

                    if(this.sortable !== false) {
                        that._on(tha, {
                            click: that._headerColumnClickHandler
                        });
                    }

                    tr.append(th);

                    if(this.hidden === true) {
                        that.options.hiddenColumns.push(this.id);
                        th.addClass('jq-grid-column-hidden');
                        th.hide();
                    }

                    tr.append(th);
                }
            });

            this.element.append(this._tHeadEl);
            this.element.append(this._tBodyEl);

            if(this.options.data.length > 1) {
                for(var i = 0, len = this.options.data.length; i < len; i++) {
                    this._renderRow(this.options.data[i], i);
                }
            }

            this._checkRowNumbers();

            if(this.options.autoLoad === true) {
                this._loadData();
            }

            this.setTableScrollbarClass();
            this._setTableWrapperFadeRightPosition();

            this._on( window, {
                resize: this.setTableScrollbarClass,
                orientationchange: this.setTableScrollbarClass
            });

        },

        _createCheckBoxForMultiSelect: function(checkBoxId, checkBoxClass, isSelectAll, rowIndex) {

            var span = $('<span/>', {
                class: 'custom-checkbox-container centered-custom-checkbox'
            });

            var checkbox = $('<input/>',{
                type: 'checkbox',
                id: checkBoxId,
                class: checkBoxClass,
                "data-index": rowIndex
            });

            var label = $('<label/>', {
                for: checkBoxId,
                html: '&nbsp;'
            });

            if(isSelectAll) {
                this._on(checkbox, {
                    click: this._selectAllClickHandler
                });
            }
            else {
                this._on(checkbox, {
                    click: this._selectRowClickHandler
                });
            }

            span.append(checkbox);
            span.append(label);

            return span;
        },

        _selectAllClickHandler: function(event) {

            var checkBox = $(event.currentTarget);
            var isChecked = checkBox.prop('checked') === true;

            if(isChecked) {
                this.selectAll();
            }
            else {
                this.deselectAll();   
            }
        },

        _selectRowClickHandler: function(event) {

            var checkBox = $(event.currentTarget);
            var isChecked = checkBox.prop('checked') === true;

            var rowIndex = checkBox.data('index');
            var selectedRowArrayIndex = this.options.selectedRows.indexOf(rowIndex);

            if(selectedRowArrayIndex !== -1 && isChecked === false) {
                this.options.selectedRows.splice(selectedRowArrayIndex, 1);
            }
            else {
                this.options.selectedRows.push(rowIndex);
            }
        },

        lockGrid: function() {
            this._wrapperOut.addClass('table-blocked');
            this._lockGridContainer.show();

            if(this._currentCellEditInputEl) {
                this._currentCellEditInputEl.blur();
            }

            this._isTableLocked = true;
        },

        unlockGrid: function() {

            this._lockGridContainer.hide();
            this._wrapperOut.removeClass('table-blocked');

            if(this._currentCellEditInputEl) {
                this._currentCellEditInputEl.focus();
            }

            this._isTableLocked = false;


        },

        selectAll: function() {

            this._tBodyEl.find('.jq-grid-select-row').each(function(){
                $(this).prop('checked', true);
            });

            this.options.selectedRows = [];
            var dataLength = this.options.data.length;

            for(var i = 0; i < dataLength; i++) {
                this.options.selectedRows.push(i);
            }
        },

        deselectAll: function() {

            this._tBodyEl.find('.jq-grid-select-row').each(function(){
                $(this).prop('checked', false);
            });

            this.options.selectedRows = [];
        },

        _enableKeyboardShortcuts: function () {
            $(document).off('keydown', $.proxy( this._keydownEventHandler, this ));
            $(document).on('keydown', $.proxy( this._keydownEventHandler, this ));
        },

        _disableKeyboardShortcuts: function () {
            $(document).off('keydown', $.proxy( this._keydownEventHandler, this ));
        },

        _keydownEventHandler: function (event) {

            //check if target is input field or textarea
            var targetsToSkip = ['INPUT', 'TEXTAREA', 'SELECT'];
            var targetType = event.target.tagName.toUpperCase();
            if (targetsToSkip.indexOf(targetType) > -1) { return; }

            //check if table is visible
            if ( !(this._wrapperOut.is(':visible')) ) { return; }

            var currentSelectedRowIndex = this.getSelectedRowIndex();
            var nextRowIndex = 0;
            var keycode = event.keyCode;

            switch (keycode) {
                case 37: /* arrow left */
                    event.preventDefault();
                    event.stopPropagation();
                    this._scrollTableLeft();
                    break;
                case 38: /* arrow up */
                    event.preventDefault();
                    event.stopPropagation();
                    nextRowIndex = currentSelectedRowIndex - 1;
                    this._focusOnRowElementIndex(currentSelectedRowIndex, nextRowIndex);
                    break;
                case 39: /* arrow right */
                    event.preventDefault();
                    event.stopPropagation();
                    this._scrollTableRight();
                    break;
                case 40: /* arrow down */
                    event.preventDefault();
                    event.stopPropagation();
                    nextRowIndex = currentSelectedRowIndex + 1;
                    this._focusOnRowElementIndex(currentSelectedRowIndex, nextRowIndex);
                    break;
                case 13: /* enter */
                    event.preventDefault();
                    event.stopPropagation();
                    this._simulateDoubleClickOnFocusedRow();
                    break;
                default: break;
            }
        },

        _scrollStepHorizontal: 80,

        _scrollTableRight: function () {
            if(!(this._elementHasScrollbar(this.element))) { return; }

            var that = this;
            var tableWrap = this._wrapperOut.find('.table-wrapper')[0];
            var maxScroll = tableWrap.scrollLeftMax;
            if (tableWrap.scrollLeft === maxScroll) { return; }

            $(tableWrap)
                .stop(true)
                .animate({
                    scrollLeft: '+=' + that._scrollStepHorizontal
                }, 100)
        },

        _scrollTableLeft: function () {
            if(!(this._elementHasScrollbar(this.element))) { return; }

            var that = this;
            var tableWrap = this._wrapperOut.find('.table-wrapper')[0];
            if (tableWrap.scrollLeft === 0) { return; }

            $(tableWrap)
                .stop(true)
                .animate({
                    scrollLeft: '-=' + that._scrollStepHorizontal
                }, 100);
        },

        _simulateDoubleClickOnFocusedRow: function () {
            this._trigger('onRowDoubleClick', null, {
                rowData: this.options.selectedRowData,
                rowIndex: this.options.selectedRowIndex
            });
        },

        _focusOnRowElementIndex: function (currentSelectedRowIndex, nextRowIndex) {
            //click on next element if exists
            var currentSelectedRowElement = this._getRowElementByIndex(currentSelectedRowIndex);
            var allRows = currentSelectedRowElement.parent().find('tr');

            if (allRows.length === 0) { return; }

            if(typeof allRows[nextRowIndex] !== 'undefined') {

                $(allRows[nextRowIndex]).trigger('click');

                var elementToCheck = currentSelectedRowElement.closest('.table-wrapper');
                var elementInViewport = this._isElementVisibleIn(allRows[nextRowIndex][0], elementToCheck[0]);

                if(elementInViewport.result === false) {
                    this._scrollTableWrapperToElement(elementInViewport.delta, elementToCheck);
                }
            }
        },

        setModelEditOptions: function(columnName, options) {

            var columnModelData = this.getColumnModelData(columnName);

            if(columnModelData.editOptions) {
                $.extend(columnModelData.editOptions, options);
            }
            else {
                columnModelData.editOptions = options;
            }
        },

        reloadGrid: function() {

            this.removeAllRows(true);
            this._loadData();
        },

        addRow: function(data, rowIndex) {

            if(typeof rowIndex === 'undefined' || rowIndex < 0) {
                rowIndex = this.options.data.length;
            }

            this.options.data.push(data);
            this._renderRow(data, rowIndex);

            if(rowIndex !== this.options.data.length) {
                this._refreshRowsIndexes();
            }

            //check row numbers and show message if 0
            this._checkRowNumbers();
        },

        _checkRowNumbers: function() {

            if(this.options.data.length === 0) {
                this._showEmptyRowsMessage();
            }
            else if(this._noRecordsFoundTREl) {
                this._noRecordsFoundTREl.remove();
            }
        },

        _showEmptyRowsMessage: function() {

            var thLength = this.options.model.length;
            var tbodyElement = this._tBodyEl;

            this._noRecordsFoundTREl = $('<tr/>', {
                class: 'jq-grid-row'
            });

            var td = $('<td/>', {
                class: 'jq-grid-cell jq-grid-cell-spanned-empty',
                colspan: thLength
            });

            var textEl = $('<h2/>', {
                class: 'jq-grid-empty-title',
                text: 'No records found'
            });

            td.append(textEl);
            this._noRecordsFoundTREl.append(td);

            tbodyElement.empty().append(this._noRecordsFoundTREl);
        },

        updateRow: function(rowIndex, rowData) {

            if(this.options.data[rowIndex]) {

                var columnModelData = null;
                var tr = this._getRowElementByIndex(rowIndex);

                this.options.data[rowIndex] = rowData;

                var columnIndex = null;

                for(columnIndex in rowData) {

                    columnModelData = this.getColumnModelData(columnIndex);

                    if(!columnModelData) {
                        continue;
                    }

                    this._updateCellData(rowData, columnModelData, tr);
                }
            }
        },

        removeRow: function(index) {

            if(this.options.data[index]) {

                var tr = this._getRowElementByIndex(index);
                var elIndex = null;

                if(tr.length === 1) {

                    elIndex = tr.data('index');

                    this._off(tr, 'click');
                    this._off(tr, 'dblclick');

                    if(this.options.multiselect) {
                        this._off(tr.find('.jq-grid-select-row'), 'click');
                    }

                    tr.remove();
                    this.options.data.splice(elIndex, 1);

                    this._refreshRowsIndexes();
                }
            }

            //check row numbers and show message if 0
            this._checkRowNumbers();
        },

        removeAllRows: function(removeData) {

            if(true === removeData) {
                this.options.data = [];
            }

            this.options.selectedRowIndex = null;
            this.options.selectedRowData = null;

            this._off(this._tBodyEl.find('jq-grid-row'), 'click');
            this._off(this._tBodyEl.find('jq-grid-row'), 'dblclick');

            if(this.options.multiselect) {
                this._off(this._tBodyEl.find('.jq-grid-select-row'), 'click');
            }

            this._tBodyEl.empty();

            //check row numbers and show message if 0
            this._checkRowNumbers();
        },

        getSelectedRowData: function() {
            return this.options.selectedRowData;
        },

        getSelectedRowIndex: function() {
            return this.options.selectedRowIndex;
        },

        getAllData: function() {
            return this.options.data;
        },

        showColumn: function(columnId) {
            this._tHeadEl.find('.jq-grid-header-' + columnId).show();
            this._tBodyEl.find('.jq-grid-cell-' + columnId).show();
        },

        hideColumn: function(columnId) {
            this._tHeadEl.find('.jq-grid-header-' + columnId).hide();
            this._tBodyEl.find('.jq-grid-cell-' + columnId).hide();
        },

        _rowDoubleClickEventHandler: function () {
            this._trigger('onRowDoubleClick', null, {
                rowData: this.options.selectedRowData,
                rowIndex: this.options.selectedRowIndex
            });
        },

        _getScrollbarWidth: function () {
            if (this._scrollbarWidth === null) {
                this._scrollbarWidth = this._calculateScrollbarWidth();
            }
            return this._scrollbarWidth;
        },

        _setTableWrapperFadeRightPosition: function () {
            this._wrapperOut.find('.table-wrapper-fade').css({
                right: this._getScrollbarWidth(),
                bottom: this._getScrollbarWidth()
            });
            this._wrapperOut.find('.table-wrapper-fade-left').css({
                bottom: this._getScrollbarWidth()
            });
        },

        setTableScrollbarClass: function () {

            if(this._resizeTimeout) {
                clearTimeout(this._resizeTimeout);
            }

            var that = this;
            this._resizeTimeout = setTimeout(function() {

                that._resizeTimeout = null;
                var tableWrap = that._wrapperOut.find('.table-wrapper');

                if (that._elementHasVerticalScrollbar(that.element)) {
                    that._wrapperOut.addClass('has_vertical_scrollbar');
                } else {
                    that._wrapperOut.removeClass('has_vertical_scrollbar');
                }

                if(that._elementHasScrollbar(that.element)) {

                    //set initially fade right and scroll table to the max left
                    tableWrap.scrollLeft(0);
                    that._wrapperOut.addClass('has_scrollbar show_right_fade');

                    //set scroll event listener
                    that._on(
                        tableWrap, {
                            scroll: that._tableScrollEventHandler
                        }
                    );
                } else {
                    that._wrapperOut.removeClass('has_scrollbar show_right_fade show_left_fade');
                    that._off( tableWrap, 'scroll' );
                }
            }, 200);
        },

        _setTableMaxHeight: function () {
            if (this._maxTableHeight === null) {
                this._maxTableHeight = this._wrapperOut.find( '.table-wrapper' ).height();
            }
            return this._maxTableHeight;
        },

        _tableScrollEventHandler: function(event) {
            this._setTableFadeElementsAccordingToScrollPosition();
        },

        _setTableFadeElementsAccordingToScrollPosition: function () {
            //show-hide table-fade element when table is scrolled to the max
            var tableWrap = this._wrapperOut.find('.table-wrapper');
            var that = this;

            var maxScrollLeft;
            if (typeof tableWrap[0].scrollLeftMax !== 'undefined') {
                //property supported only in Firefox
                maxScrollLeft = tableWrap[0].scrollLeftMax;
            } else {
                maxScrollLeft = tableWrap[0].scrollWidth - tableWrap[0].clientWidth;
            }

            if(maxScrollLeft === tableWrap[0].scrollLeft) {

                /* scrolled to the max right */
                if( !this._wrapperOut.hasClass('show_right_fade') ) { return; }

                this._wrapperOut.find('.table-wrapper-fade' )
                    .stop(true)
                    .fadeTo(200, 0, function () {
                        that._wrapperOut.removeClass('show_right_fade');
                    });

                this._wrapperOut.find('.table-wrapper-fade-left' )
                    .stop(true)
                    .fadeTo(200, 1, function () {
                        that._wrapperOut.addClass('show_left_fade');
                    });

            }
            else if(tableWrap[0].scrollLeft === 0) {

                /* scrolled to max left */
                if( !this._wrapperOut.hasClass('show_left_fade') ) { return; }

                this._wrapperOut.find('.table-wrapper-fade' )
                    .stop(true)
                    .fadeTo(200, 1, function () {
                        that._wrapperOut.addClass('show_right_fade');
                    });

                this._wrapperOut.find('.table-wrapper-fade-left' )
                    .stop(true)
                    .fadeTo(200, 0, function () {
                        that._wrapperOut.removeClass('show_left_fade');
                    });

            }
            else {

                /* scrolled somewhere between max left and max right */
                if(this._wrapperOut.hasClass('show_right_fade show_left_fade') ) { return; }

                this._wrapperOut.find('.table-wrapper-fade-left' )
                    .stop(true)
                    .fadeTo(200, 1, function () {
                        that._wrapperOut.addClass('show_left_fade');
                    });

                this._wrapperOut.find('.table-wrapper-fade' )
                    .stop(true)
                    .fadeTo(200, 1, function () {
                        that._wrapperOut.addClass('show_right_fade');
                    });
            }
        },

        _elementHasScrollbar: function (element) {
            return element.width() > element.parent().width();
        },

        _elementHasVerticalScrollbar: function (element) {
            return element.height() > element.parent().height();
        },

        _getRowElementByIndex: function(index) {
            return this._tBodyEl.find("tr:nth-child(" + (index + 1) + ")");
        },

        _refreshRowsIndexes: function() {

            var that = this;
            var rowEl = null;

            this._tBodyEl.find('.jq-grid-row').each(function(index){

                rowEl = $(this);
                rowEl.data('index', index);

                if(rowEl.hasClass('jq-grid-selected-row')) {
                    that.options.selectedRowIndex = index;
                }
            });
        },

        _loadData: function() {

            var that = this;

            var url = this.options.url;

            if(typeof this.options.ajaxOptions.getUrlCallback === 'function') {
                url = this.options.ajaxOptions.getUrlCallback();
            }

            this.lockGrid();

            $.ajax({

                url: this.options.url,
                dataType: "json",
                type: this.options.ajaxOptions.methodType,
                data: this.options.ajaxOptions.parameters,

                success: function(data, textStatus, jqXHR) {

                    var i;

                    if(data instanceof Array) {
                        for(i = 0, len = data.length; i < len; i++) {
                            that.addRow(data[i], i);
                        }
                    }
                    else if(typeof data === 'object') {

                        var propertyName = this.options.ajaxOptions.dataListPropertyName;

                        if(data.hasOwnProperty(propertyName) && data[propertyName] instanceof Array) {

                            var dataList = data[propertyName];

                            for(i = 0, len = dataList.length; i < len; i++) {
                                that.addRow(dataList[i], i);
                            }
                        }
                    }
                    else {
                        console.log("Unable to parse read data");
                    }
                },

                error: function(jqXHR, textStatus, errorThrown) {

                },

                complete: function(jqXHR, textStatus) {
                    that.unlockGrid();
                }
            });
        },

        _renderRow: function(rowData, rowIndex) {

            var tr = $("<tr/>", {
                class:'jq-grid-row',
                'data-index': rowIndex
            });

            if(rowIndex === 0) {
                this._tBodyEl.prepend(tr);
            }
            else {
                this._tBodyEl.find("tr:nth-child(" + rowIndex + ")").after(tr);
            }

            this._on(tr, {
                click: this._rowClickEventHandler,
                dblclick: this._rowDoubleClickEventHandler
            });

            var td;
            var columnModelData;

            if(this.options.multiselect === true) {

                var multiSelectCheckBox = this._createCheckBoxForMultiSelect(
                    'jq-grid-multiselect-' + (window.performance.now() * 100000000000),
                    'jq-grid-select-row',
                    false,
                    rowIndex
                );

                td = $('<td/>', {
                    class:  'jq-grid-cell',
                    'data-index': rowIndex,
                });

                td.append(multiSelectCheckBox);
                tr.append(td);
            }

            for(var columnIndex in this.options.model) {

                columnModelData = this.options.model[columnIndex];

                if(!columnModelData) { continue; }

                var cssDisplayString = '';

                if(columnModelData.hidden === true) {
                    cssDisplayString = 'display: none';
                }

                td = $('<td/>', {
                    class:  'jq-grid-cell jq-grid-cell-' + columnModelData.id,
                    'data-id': columnModelData.id,
                    'data-index': rowIndex,
                    'style': cssDisplayString
                });

                tr.append(td);

                this._updateCellData(rowData, columnModelData, tr);
            }

            this.setTableScrollbarClass();
        },

        _updateCellData: function(rowData, columnModelData, rowElement) {

            if(!rowData || !columnModelData || rowElement < 0) {
                return;
            }

            var td = rowElement.find('.jq-grid-cell-' + columnModelData.id);

            var cellData = '';
            var rowIndex = rowElement.data('index');

            if(typeof columnModelData.formatter === "function") {
                cellData = columnModelData.formatter(rowData[columnModelData.dataMap], rowData, rowIndex) || '';
            }
            else{
                cellData = rowData[columnModelData.dataMap] || '';
            }

            td.empty();

            if(typeof cellData === 'object') {
                td.append(cellData);
            }
            else {
                td.append($.parseHTML(cellData.toString()));
            }

            return cellData;
        },

        getAllColumnData: function() {
            return this.options.model;
        },

        getColumnModelData: function(columnId) {

            for(var index in this.options.model) {
                if(this.options.model[index].id === columnId) {
                    return this.options.model[index];
                }
            }
        },

        _cellEditInputClickHandler: function(ev) {
            //console.log(ev)
        },

        _cellEditInputKeydownHandler: function(ev) {

            // check if table is locked
            if(this._isTableLocked) {
                return;
            }

            //console.log(ev)
            if(ev.keyCode === 13) {
                this.finishCellEdit();
            }
            else  if(ev.keyCode === 27) {
                this.cancelCellEdit();
            }
        },

        _cellEditInputKeyupHandler: function(ev) {
            //console.log(ev)
        },

        _editCellSelectInputChangeHandler: function(ev) {
            this.finishCellEdit();
        },

        _createCellEditInput: function(cellTdEl, columnModelData, rowIndex, rowData) {
            
            this._currentCellTdEl = cellTdEl;
            this._currentCellEditColumnName = columnModelData.id;
            this._currentCellEditRowIndex = rowIndex;

            var inputType = 'text';
            var editOptions = columnModelData.editOptions;

            if(editOptions && editOptions.type) {
                inputType = editOptions.type;
            }

            if(inputType === 'textarea') {
                this._currentCellEditInputEl = $('<textarea/>', {
                    'data-column-id': columnModelData.id,
                    'data-row-index': rowIndex,
                    'class': 'cell-edit-input'
                });
            }
            else if(inputType === 'select') {
                this._currentCellEditInputEl = $('<select/>', {
                    'data-column-id': columnModelData.id,
                    'data-row-index': rowIndex,
                    'class': 'cell-edit-input'
                });

            }
            else {
                this._currentCellEditInputEl = $('<input/>', {
                    type: inputType,
                    'data-column-id': columnModelData.id,
                    'data-row-index': rowIndex,
                    'class': 'cell-edit-input'
                });
            }


            var optionRowData;
            var optionEl;

            if(inputType === 'select' && editOptions && editOptions.availableOptions && editOptions.availableOptions instanceof Array) {

                var value;
                var label;
                var rowDataValue;

                var avialableOptions = columnModelData.editOptions.availableOptions;

                for(var i = 0, len = avialableOptions.length; i < len; i++) {

                    optionRowData = avialableOptions[i];

                    optionEl = $('<option/>');

                    value = this._extractCellEditValue(optionRowData, columnModelData);
                    optionEl.attr('value', value);

                    label = this._extractCellEditLabel(optionRowData, columnModelData);
                    optionEl.text(label);

                    rowDataValue = this._extractCellEditValue(rowData[columnModelData.id], columnModelData);

                    if(rowDataValue.toString() ===  value.toString()) {
                        optionEl.attr('selected', true);
                    }

                    this._currentCellEditInputEl.append(optionEl);
                }

                this._on(this._currentCellEditInputEl, {
                    change: this._editCellSelectInputChangeHandler
                });
            }
            else if(inputType === 'text') {
                this._currentCellEditInputEl.val(rowData[columnModelData.dataMap]);
            }

            cellTdEl.addClass('cell-edit');
            cellTdEl.empty();
            cellTdEl.append(this._currentCellEditInputEl);

            this._currentCellEditInputEl.focus();

            if(editOptions && editOptions.initializeInputCallback && typeof editOptions.initializeInputCallback === 'function') {
                this._currentCellEditInitalizedInputEl = editOptions.initializeInputCallback(this._currentCellEditInputEl);
            };

            if(editOptions && editOptions.setInputValueCallback) {
                var cellEditInput = this._currentCellEditInitalizedInputEl || this._currentCellEditInputEl;
                editOptions.setInputValueCallback(cellEditInput, rowData[columnModelData.id], rowData);
            };

            
            this._on(this._currentCellEditInputEl, {
                click: this._cellEditInputClickHandler,
                keydown: this._cellEditInputKeydownHandler,
                keyup: this._cellEditInputKeyupHandler,
            });
        },


        _restorCellFromEdit: function(value, columnModelData, rowIndex, rowData) {

            this._currentCellTdEl.removeClass('cell-edit');

            var editOptions = columnModelData.editOptions;

            if(columnModelData.formatter && typeof columnModelData.formatter === 'function') {

                var optionData = value;

                if(editOptions && editOptions.availableOptions) {

                    var availableOptions = editOptions.availableOptions;

                    if(availableOptions && availableOptions.length > 0) {

                        var optionValue;

                        for(var i = 0, len = availableOptions.length; i < len; i++) {

                            optionData = availableOptions[i];
                            optionValue = this._extractCellEditValue(optionData, columnModelData);

                            if(optionValue.toString() === value.toString()) {
                                break;
                            }
                        }
                    }
                }

                this._currentCellTdEl.empty().text(columnModelData.formatter(optionData, rowData, rowIndex));
            }
            else {
                this._currentCellTdEl.empty().text(this._extractCellEditLabel(value, columnModelData));
            }

            this._off(this._currentCellEditInputEl, 'change');    
            this._off(this._currentCellEditInputEl, 'click');
            this._off(this._currentCellEditInputEl, 'keydown');
            this._off(this._currentCellEditInputEl, 'keyup');

            this._currentCellTdEl = null;
            this._currentCellEditColumnName = '';
            this._currentCellEditRowIndex = -1;
            this._currentCellEditInitalizedInputEl = null;
        },

        _extractCellEditValue: function(cellData, columnModelData) {

            if(typeof columnModelData.editOptions === 'undefined' || columnModelData.editOptions === null) {
                return cellData;
            }

            var value = cellData;
            var valueProperty = columnModelData.editOptions.valueProperty;

            if(valueProperty && typeof cellData === 'object' && cellData.hasOwnProperty(valueProperty)) {
                value = cellData[valueProperty];
            }
            else if(typeof cellData === 'object' && cellData.hasOwnProperty('id')) {
                value = cellData.id;
            }
            else if(typeof cellData === 'object' && cellData.hasOwnProperty('value')) {
                value = cellData.value;
            }

            return value;
        },

        _extractCellEditLabel: function(cellData, columnModelData) {

            if(typeof columnModelData.editOptions === 'undefined' || columnModelData.editOptions === null) {
                return cellData;
            }

            var label = '';
            var labelProperty = columnModelData.editOptions.labelProperty;

            if(labelProperty && typeof cellData === 'object' && cellData.hasOwnProperty(labelProperty)) {
                label = cellData[labelProperty];
            }
            else if(typeof cellData === 'object' && cellData.hasOwnProperty('name')) {
                label = cellData.name;
            }
            else if(typeof cellData === 'object' && cellData.hasOwnProperty('label')) {
                label = cellData.label;
            }

            return label;
        },

        finishCellEdit: function() {

            if(this._currentCellEditRowIndex === -1) {
                return;
            }

            var columnModelData = this.getColumnModelData(this._currentCellEditColumnName);
            var rowData = this.options.data[this._currentCellEditRowIndex];
            var oldValue = rowData[this._currentCellEditColumnName] || '';

            if(typeof oldValue === 'object') {
                oldValue = this._extractCellEditValue(oldValue, columnModelData);
            }                

            var editedValue = this._currentCellEditInputEl.val();

            if(columnModelData.editOptions && columnModelData.editOptions.getInputValueCallback &&  typeof columnModelData.editOptions.getInputValueCallback === 'function') {
                editedValue = columnModelData.editOptions.getInputValueCallback(this._currentCellEditInitalizedInputEl);
            }

            var isDataValid = true;

            // data did not change
            if(oldValue.toString() === editedValue.toString()) {
                this._restorCellFromEdit(oldValue, columnModelData, this._currentCellEditRowIndex, rowData);
                return true;
            }
            else if(columnModelData.editOptions) {

                var editOptions = columnModelData.editOptions;

                if(editOptions.validateCallback && typeof editOptions.validateCallback === 'function') {
                    isDataValid = editOptions.validateCallback(editedValue, columnModelData.id, rowData);
                }
                else if(editOptions.validateRegex) {
                    var regexPattern = new RegExp(editOptions.validateRegex);
                    isDataValid = regexPattern.test(editedValue);
                }
            }

            if(true === isDataValid) {

                if(this.options.cellEditType === 'local') {

                    this.options.data[this._currentCellEditRowIndex][columnModelData.id] = editedValue;
                    this._restorCellFromEdit(editedValue, columnModelData, this._currentCellEditRowIndex, rowData);

                    this._trigger('onCellEdited', null, {
                        newValue: editedValue,
                        oldValue: oldValue,
                        cellName: this._currentCellEditColumnName,
                        rowIndex: this._currentCellEditRowIndex
                    });

                    return true;
                }
                else if(this.options.cellEditType === 'remote') {

                    var ajaxData = null;
                    var formatDataCallback = this.options.cellEditAjaxOptions.formatDataCallback;

                    if(formatDataCallback && typeof formatDataCallback === 'function') {
                        ajaxData = formatDataCallback(columnModelData.id, editedValue, rowData);
                    }
                    else {
                        ajaxData = {};
                        ajaxData[columnModelData.id] = editedValue;
                    }

                    var that = this;
                    var ajaxParams = $.extend({}, this.options.cellEditAjaxOptions, {

                        success: function(data, textStatus, jqXHR) {

                            that.options.data[that._currentCellEditRowIndex][columnModelData.id] = editedValue;

                            that._trigger('onCellEdited', null, {
                                newValue: editedValue,
                                oldValue: oldValue,
                                cellName: that._currentCellEditColumnName,
                                rowIndex: that._currentCellEditRowIndex,
                                serverResponse: data
                            });

                            that._restorCellFromEdit(
                                editedValue,
                                columnModelData,
                                that._currentCellEditRowIndex,
                                rowData
                            );
                        },

                        error: function(jqXHR, textStatus, errorThrown) {

                            var parseErrorCallback = that.options.cellEditAjaxOptions.parseErrorMessageCallback;

                            if(typeof parseErrorCallback === 'function') {

                                var cellErrorMessage = parseErrorCallback(
                                    jqXHR.responseText,
                                    that._currentCellEditColumnName,
                                    cellEditedValue
                                );

                                that.showCellError(
                                    that._currentCellEditColumnName,
                                    that._currentCellEditRowIndex,
                                    cellErrorMessage || that.options.defaultCellEditErrorMessage,
                                    that._currentCellTdEl
                                );
                            }
                            else {

                                // try to digg the error message from response

                                try {

                                    var errorMessage;

                                    var error = jqXHR.responseJSON;

                                    if(!error) {
                                        error = JSON.parse(jqXHR.responseText);
                                    }

                                    if(!error) {
                                        error = errorThrown;
                                    }

                                    if(typeof error === 'object') {

                                        if(error.hasOwnProperty('message')) {
                                            errorMessage = error.message;
                                        }
                                        else if(error.hasOwnProperty('error')) {
                                            errorMessage = error.error;
                                        }
                                        else if(error.hasOwnProperty('form')) {
                                            errorMessage = error.form;
                                        }
                                        else if(error.hasOwnProperty('errorMessage')) {
                                            errorMessage = error.errorMessage;
                                        }
                                        else if(error.hasOwnProperty('text')) {
                                            errorMessage = error.text;
                                        }

                                        if(typeof errorMessage === 'object' && errorMessage.hasOwnProperty('children')) {
                                            errorMessage = errorMessage.children[that._currentCellEditColumnName].errors;
                                        }
                                        else {
                                            errorMessage = error.toString();   
                                        }

                                    }
                                    else if(typeof error === 'string' && error.length > 0) {
                                        errorMessage = error;
                                    }
                                    else {
                                        errorMessage = that.options.defaultCellEditErrorMessage;
                                    }


                                    if(typeof errorMessage === 'object' && errorMessage.hasO) {

                                    }

                                    that.showCellError(
                                        that._currentCellEditColumnName,
                                        that._currentCellEditRowIndex,
                                        errorMessage,
                                        that._currentCellTdEl
                                    );
                                }
                                catch(err) {
                                    
                                    // whatever is the error use default cell error message

                                    that.showCellError(
                                        that._currentCellEditColumnName,
                                        that._currentCellEditRowIndex,
                                        cellErrorMessage || that.options.defaultCellEditErrorMessage,
                                        that._currentCellTdEl
                                    );
                                }
                            }
                        },

                        complete: function() {
                            that.unlockGrid();
                        }
                    });
                        
                    var getUrlCallback = this.options.cellEditAjaxOptions.getUrlCallback;

                    if(getUrlCallback && typeof getUrlCallback === 'function') {
                        ajaxParams.url = getUrlCallback(columnModelData.id, editedValue, rowData);
                    }

                    ajaxParams.data = ajaxData;
                    ajaxParams.type = ajaxParams.method;

                    delete ajaxParams.method;

                    if(typeof ajaxParams.url === 'string' && ajaxParams.url.length > 0) {
                        this.removeCellError();
                        this.lockGrid();
                        $.ajax(ajaxParams);    
                    }
                    else {
                        this.showCellError(
                            this._currentCellEditColumnName,
                            this._currentCellEditRowIndex,
                            'Invalid cell ajax edit url defined',
                            this._currentCellTdEl
                        );
                    }
                }
                else if(this.options.cellEditType === 'callback') {
                    
                    var cellEditCallbackOptions = this.options.cellEditCallbackOptions;

                    if(typeof cellEditCallbackOptions.callback === 'function') {

                        var calbackArgs = [columnModelData.id, cellEditedValue, rowData];

                        if(typeof cellEditCallbackOptions.getCallbackArgsCallback === 'function') {
                            calbackArgs = cellEditCallbackOptions.getCallbackArgsCallback(columnModelData.id, cellEditedValue, rowData);
                        }
                        else if(cellEditCallbackOptions.callbackArgs instanceof Array) {
                            calbackArgs = calbackArgs.concat(cellEditCallbackOptions.callbackArgs);
                        }

                        if(cellEditCallbackOptions.callbackScope) {
                            cellEditCallbackOptions.callback.apply(
                                cellEditCallbackOptions.callbackScope,
                                callbackArgs
                            );
                        }
                        else {
                            cellEditCallbackOptions.callback.apply(this, callbackArgs);
                        }
                    }
                    else {
                        this.showCellError(
                            this._currentCellEditColumnName,
                            this._currentCellEditRowIndex,
                            'Invalid cell edit callback defined',
                            this._currentCellTdEl
                        );
                    }
                }
                else {

                    this.showCellError(
                        this._currentCellEditColumnName,
                        this._currentCellEditRowIndex,
                        'Invalid cell edit type option defined',
                        this._currentCellTdEl
                    );
                }

                return false;
            }
            else {
               
                this.showCellError(
                    this._currentCellEditColumnName,
                    this._currentCellEditRowIndex,
                    columnModelData.editOptions.validationErrorMessage || this.options.defaultCellEditErrorMessage,
                    this._currentCellTdEl
                );

                return false;
            }
        },

        removeCellError: function() {
            if(this._currentCellEditValidationErrorEl) {
                this._currentCellEditValidationErrorEl.remove();
            }
        },

        showCellError: function(cellName, rowIndex, errorMessage, tdElement) {

            this._currentCellEditValidationErrorEl = $('<div/>', {
                'class': 'ceve-message-container'
            });

            var errorMessageContent = $('<div/>', {
                'class': 'ceve-message-content'
            });

            var errorMessageEl = $('<span/>');    

            if(errorMessage instanceof Array && errorMessage.length > 1) {

                if(errorMessage.length > 1) {
                    var errorsList = $('<ul />');
                    var errorItem;

                    for(var i = 0, len = errorMessage.length; i < len; i++) {
                        errorItem = $('<li />', {
                            text: errorMessage[i]
                        });

                        errorMessageEl.append(errorItem);
                    }
                }
                else {
                    errorMessageEl.text(errorMessage[0]);
                }
            }
            else {
                errorMessageEl.text(errorMessage);
            }
            

            errorMessageContent.append(errorMessageEl);

            this._currentCellEditValidationErrorEl.append(errorMessageContent);

            var cellTdEl;

            if(tdElement) {
                cellTdEl = tdElement;
            }
            else {

                var rowEl = this._tBodyEl.find('tr:nth-child('+(rowIndex + 1) +')');

                cellTdEl = rowEl.find('td[data-id="'+cellName+'"]');
            }

            cellTdEl.prepend(this._currentCellEditValidationErrorEl);
        },

        cancelCellEdit: function() {

            var rowData = this.options.data[this._currentCellEditRowIndex];
            var cellValue = this.options.data[this._currentCellEditRowIndex][this._currentCellEditColumnName];

            this._restorCellFromEdit(
                cellValue,
                this.getColumnModelData(this._currentCellEditColumnName),
                this._currentCellEditRowIndex,
                rowData
            );
        },

        _rowClickEventHandler: function(ev) {

            if(this.selectedRowEl) {
                this.selectedRowEl.removeClass('jq-grid-selected-row');
            }

            this.selectedRowEl = $(ev.currentTarget);
            this.selectedRowEl.addClass('jq-grid-selected-row');

            var tdElement = $(ev.target);

            if(tdElement.is('td') === false) {
                tdElement = tdElement.closest('.jq-grid-cell');
            }

            var columnId = tdElement.data('id');
            var rowIndex = this.selectedRowEl.data('index');
            var rowData = this.options.data[rowIndex];

            this.options.selectedRowData = rowData;
            this.options.selectedRowIndex = rowIndex;

            this._trigger('onCellSelected', null, {
                rowData: rowData,
                rowIndex: rowIndex,
                cellName: $(ev.target).data('id')
            });

            this._trigger('onRowSelected', null, {
                rowData: rowData,
                rowIndex: rowIndex
            });

            if(this.options.cellEdit) {

                var columnModelData = this.getColumnModelData(columnId);
                var isCurrentCellAlreadyBeingEdited = this._currentCellEditColumnName === columnId && this._currentCellEditRowIndex === rowIndex; 

                if(columnModelData.editable) {

                    if(isCurrentCellAlreadyBeingEdited === false) {
                        
                        if(this._currentCellEditRowIndex > -1) {

                            // finish current cell editing if it exists
                            var cellFinishedEditing = this.finishCellEdit();

                            if(cellFinishedEditing) {
                                this._createCellEditInput(tdElement, columnModelData, rowIndex, rowData);
                            }
                        }
                        else {
                            this._createCellEditInput(tdElement, columnModelData, rowIndex, rowData);
                        }
                    }
                }
                else if(this._currentCellEditRowIndex > -1) {
                    this.finishCellEdit();
                }
            }
        },

        _headerColumnClickHandler: function(ev) {

            if(this._currentCellEditRowIndex >= 0) {
                this.cancelCellEdit();
            }

            var el = $(ev.target);

            var currentSortCol = this._tHeadEl.find('.jq-grid-column-sort');

            var columnModelData = this.getColumnModelData(el.data('id'));
            var sortColumnName = columnModelData.sortName || el.data('id');


            if(currentSortCol.data('id') === el.data('id')) {
                el.toggleClass('jq-grid-column-sort-asc');
                el.toggleClass('jq-grid-column-sort-desc');
            }
            else {

                currentSortCol.removeClass('jq-grid-column-sort');
                currentSortCol.removeClass('jq-grid-column-sort-asc');
                currentSortCol.removeClass('jq-grid-column-sort-desc');

                el.addClass('jq-grid-column-sort');
                el.addClass('jq-grid-column-sort-asc');
            }


            var sortColumnOrder = el.hasClass('jq-grid-column-sort-asc') ? 'asc' : 'desc';

            this._currentSortColumnName = sortColumnName;
            this._currentSortColumnOrder = sortColumnOrder;

            var compareFunctionASC = function compare(a,b) {
                if (a[sortColumnName] < b[sortColumnName]) {
                    return -1;
                }
                
                if (a[sortColumnName] > b[sortColumnName])
                {
                    return 1;
                }
                    
                return 0;
            };

            var compareFunctionDESC = function compare(a,b) {
                if (a[sortColumnName] > b[sortColumnName]) {
                    return -1;
                }
                
                if (a[sortColumnName] < b[sortColumnName])
                {
                    return 1;
                }
                    
                return 0;
            };

            if(sortColumnOrder === 'asc') {
                this.options.data.sort(compareFunctionASC);
            }
            else {
                this.options.data.sort(compareFunctionDESC);
            }

            this.removeAllRows();

            for(var i = 0, len = this.options.data.length; i < len; i++) {
                this._renderRow(this.options.data[i], i);
            }

            this._trigger('onSortColumnChange', null, {
                sortColumnName: sortColumnName,
                sortColumnOrder: sortColumnOrder
            });
        },

        _setOption: function(key, value, triggerEvent) {
            this._super(key, value);

            if (key === 'enableShortcuts') {
                if (value) {
                    this._enableKeyboardShortcuts();
                }
                else {
                    this._disableKeyboardShortcuts();
                }
            }
        },

        _isElementVisibleIn: function(element, parentEl) {

            var result = true;
            var delta = 0;

            var elementOffsetTop = element.offsetTop;
            var elementHeight = element.offsetHeight;
            var parentScrollTop = parentEl.scrollTop;
            var parentHeight = parentEl.offsetHeight;

            if(elementOffsetTop + elementHeight > parentScrollTop + parentHeight) {
                result = false;
                delta = (elementOffsetTop + elementHeight) - (parentScrollTop + parentHeight);
            }
            else if(elementOffsetTop < parentScrollTop) {
                result = false;
                delta = elementOffsetTop - parentScrollTop;
            }

            return {
                result: result,
                delta: delta
            };
        },

        _scrollTableWrapperToElement: function (delta, elementToScroll) {

            //add 10px for breathing
            var scrollBreathe = 20;
            var scrollHeightValue = delta;
            var scrollHeightString;

            if (delta > 0) {

                //element is below viewport
                scrollHeightValue += scrollBreathe;
                scrollHeightString = '+=' + scrollHeightValue;
            }
            else {

                //element is above viewport
                scrollHeightValue -= scrollBreathe;
                scrollHeightValue = Math.abs(scrollHeightValue);
                scrollHeightString = '-=' + scrollHeightValue;
            }

            //animate scroll
            elementToScroll.stop(true).animate({
                scrollTop: scrollHeightString
            }, 200);
        },

        _calculateScrollbarWidth: function() {

            var outer = document.createElement("div");
            outer.style.visibility = "hidden";
            outer.style.width = "100px";
            outer.style.msOverflowStyle = "scrollbar";

            document.body.appendChild(outer);

            var widthNoScroll = outer.offsetWidth;
            // force scrollbars
            outer.style.overflow = "scroll";

            // add innerdiv
            var inner = document.createElement("div");
            inner.style.width = "100%";
            outer.appendChild(inner);

            var widthWithScroll = inner.offsetWidth;

            // remove divs
            outer.parentNode.removeChild(outer);

            return widthNoScroll - widthWithScroll;
        }
    });

})(jQuery);