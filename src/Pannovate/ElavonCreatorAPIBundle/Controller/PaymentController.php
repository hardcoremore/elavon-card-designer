<?php

namespace Pannovate\ElavonCreatorAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

class PaymentController extends FOSRestController
{
    public function getCheckoutPaymentParamsAction(Request $request, $orderId)
    {   

        $cardDesignOrderModel = $this->get('pannovate.ecmodelbundle.model.card_design_order');
        $cardDesignOrder = $cardDesignOrderModel->get($orderId);

        $configParams = $this->container->getParameter('checkout_payment_params');
        
        $configParams = $this->container->getParameter('checkout_payment_params');
        $curlRequest = $this->get('pannovate.ecmodelbundle.worldnet_payment_request');
        $currentDate = date('d-m-Y:H:i:s', strtotime("now")) . ':000';

        $orderId = $cardDesignOrder->getId();
        $amount = $cardDesignOrder->getOrderPriceTotal();
        $ffId = $cardDesignOrder->getUser()->getUsername();

        if($cardDesignOrder->getCurrency()->getCode() === 'GBP')
        {
            $terminal = $configParams['terminal_gbp'];
            $currency = 'GBP';
            $secret = $configParams['secret_gbp'];
        }
        else
        {
            $terminal = $configParams['terminal_eur'];
            $currency = 'EUR';
            $secret = $configParams['secret_eur'];
        }

        $worldNetUrl = $configParams['worldnet_url'];
        
        $receiptPageUrl = $configParams['receipt_page_url'];
        
        $hashInputString = $terminal . $orderId . $amount . $currentDate . $receiptPageUrl . $secret;   
        $hashString = md5($hashInputString);
        $requestData = [
            'TERMINALID' => $terminal,
            'ORDERID' => $orderId,
            'CURRENCY' => $currency,
            'AMOUNT' => $amount, 
            'DATETIME' => $currentDate,
            'HASH' => $hashString,
            'RECEIPTPAGEURL' => $receiptPageUrl,
            'WORLDNETURL' => $worldNetUrl,
            'FFID' => $ffId
        ];
        
        return $this->handleView($this->view($requestData, 200));
    }
}
