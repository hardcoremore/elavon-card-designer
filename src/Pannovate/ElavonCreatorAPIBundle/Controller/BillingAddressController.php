<?php

namespace Pannovate\ElavonCreatorAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class BillingAddressController extends FOSRestController
{
	public function createAction(Request $request)
    {
        try
        {
            $billingAddressModel = $this->get('pannovate.ecmodelbundle.model.billing_address');
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $billingAddress = $billingAddressModel->create($request->request->all(), $user);
            
            $url = $this->container->get('router')->generate(
                'pannovate_ecapi_billing_address_get',
                array(
                    'id' => $billingAddress->getId()
                ),
                true // absolute
            );

            return $this->handleView(
                $this->view(
                    $billingAddress,
                    201,
                    array(
                        "Location" => $url
                    )
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function getBillingAddressAction($id)
    {
        $billingAddressModel = $this->get('pannovate.ecmodelbundle.model.billing_address');
        $billingAddress = $billingAddressModel->get($id);

        if($billingAddress)
        {
            $view = $this->view($billingAddress, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }   
    }

    public function listAllAction(Request $request)
    {
        $billingAddressModel = $this->get('pannovate.ecmodelbundle.model.billing_address');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $billingAddresses = $billingAddressModel->readForUser($user);

        if($billingAddresses)
        {
            $view = $this->view($billingAddresses, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view); 
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function updateAction(Request $request, $id)
    {
        try
        {
            $billingAddressModel = $this->get('pannovate.ecmodelbundle.model.billing_address');
            $billingAddress = $billingAddressModel->update($id, $request->request->all());

            return $this->handleView(
                $this->view(
                    $billingAddress,
                    204
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(array('billing_address'));

        return $context;
    }
}