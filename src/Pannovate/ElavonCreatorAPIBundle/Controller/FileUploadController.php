<?php

namespace Pannovate\ElavonCreatorAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\SecurityContextInterface;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

class FileUploadController extends FOSRestController
{
    public function uploadFileAction(Request $request)
    {
        $allPostData = $request->request->all();

        $cardDesignModel = $this->get('pannovate.ecmodelbundle.model.card_design');
        $cardDesignImageElementModel = $this->get('pannovate.ecmodelbundle.model.card_design_image_element');

        $uploadIntention = $request->get('intention');

        if('upload-card-design-image' === $uploadIntention)
        {
            $fileUrlPath = $cardDesignModel->saveCardDesign($allPostData);
        }
        else if('upload-card-image-element' === $uploadIntention)
        {
            $fileUrlPath = $cardDesignImageElementModel->saveCardDesignImageElement($allPostData);
        }

        $responseData = [
            'design_id' => $request->get('design_id'),
            'intention' => $request->get('intention'),
            'file_url_path' =>  $fileUrlPath
        ];

        return $this->handleView($this->view($responseData), 200);
    }

    public function getUploadType(Request $request)
    {
        switch($request->get('intention'))
        {
            case 'upload-card-design-image':
                return 'design';
            break;

            case 'upload-card-image-element':
                return 'design_element';
            break;

            default:
                throw new \Exception("Unknown upload intention.");
            break;
        }
    }
}
