<?php

namespace Pannovate\ElavonCreatorAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

class CurrencyController extends FOSRestController
{
    public function getCurrencyAction($id)
    {
        $currencyModel = $this->get('pannovate.ecmodelbundle.model.currency');
        $currency = $currencyModel->get($id);

        if($currency)
        {
            $view = $this->view($currency, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }   
    }

    public function getBaseCurrencyAction()
    {
        $currencyModel = $this->get('pannovate.ecmodelbundle.model.currency');
        $currentBaseCurrency = $currencyModel->getBaseCurrency();

        if($currentBaseCurrency)
        {
            $view = $this->view($currentBaseCurrency, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }
    }

    public function listAllAction(Request $request)
    {
        $currencyModel = $this->get('pannovate.ecmodelbundle.model.currency');
        $currencies = $currencyModel->readAll();

        if($currencies)
        {
            $view = $this->view($currencies, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view); 
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function listCurrenciesSelectAction()
    {
        $currencyModel = $this->get('pannovate.ecmodelbundle.model.currency');
        $currencies = $currencyModel->readForSelect();

        if($currencies)
        {
            $view = $this->view($currencies, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view);
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(array('currency', 'country'));

        return $context;
    }
}