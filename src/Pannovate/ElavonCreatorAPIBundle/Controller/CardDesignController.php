<?php

namespace Pannovate\ElavonCreatorAPIBundle\Controller;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\SecurityContextInterface;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class CardDesignController extends FOSRestController
{
    public function createAction(Request $request)
    {
        try
        {
            $designModel = $this->get('pannovate.ecmodelbundle.model.card_design');
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $design = $designModel->create($request->request->all(), $user);
            
            $url = $this->container->get('router')->generate(
                'pannovate_ecapi_design_get',
                array(
                    'id' => $design->getId()
                ),
                true // absolute
            );

            return $this->handleView(
                $this->view(
                    $design,
                    201,
                    array(
                        "Location" => $url
                    )
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function getDesignAction($id)
    {
        $designModel = $this->get('pannovate.ecmodelbundle.model.card_design');
        $design = $designModel->get($id);

        if($design)
        {
            $view = $this->view($design, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }   
    }

    public function getLastModifiedAction(Request $request)
    {
        $designModel = $this->get('pannovate.ecmodelbundle.model.card_design');
        $user = $this->get('security.token_storage')->getToken()->getUser();
        
        $design = $designModel->getLastModified($user);

        

        if($design)
        {
            $view = $this->view($design, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        } 
        
    }

    public function getRecentDesignsAction(Request $request)
    {
        $designModel = $this->get('pannovate.ecmodelbundle.model.card_design');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $designs = $designModel->getRecentDesigns($user);
        
        if($designs)
        {
            $view = $this->view($designs, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function listAllAction(Request $request)
    {
        $designModel = $this->get('pannovate.ecmodelbundle.model.card_design');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $designs = $designModel->readForUser($user);


        if($designs)
        {
            $view = $this->view($designs, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view); 
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }
    }

    public function deleteAction($id)
    {
        $designModel = $this->get('pannovate.ecmodelbundle.model.card_design');

        $design = $designModel->delete($id);

        $view = $this->view(null, 204);

        return $this->handleView($view);
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(array('card_design', 'user_basic_info', 'card_design_image_element'));

        return $context;
    }
}
