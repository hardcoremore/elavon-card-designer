<?php

namespace Pannovate\ElavonCreatorAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class ShippingAddressController extends FOSRestController
{
	public function createAction(Request $request)
    {
        try
        {
            $shippingAddressModel = $this->get('pannovate.ecmodelbundle.model.shipping_address');
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $shippingAddress = $shippingAddressModel->create($request->request->all(), $user);
            
            $url = $this->container->get('router')->generate(
                'pannovate_ecapi_shipping_address_get',
                array(
                    'id' => $shippingAddress->getId()
                ),
                true // absolute
            );

            return $this->handleView(
                $this->view(
                    $shippingAddress,
                    201,
                    array(
                        "Location" => $url
                    )
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function getShippingAddressAction($id)
    {
        $shippingAddressModel = $this->get('pannovate.ecmodelbundle.model.shipping_address');
        $shippingAddress = $shippingAddressModel->get($id);

        if($shippingAddress)
        {
            $view = $this->view($shippingAddress, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }   
    }

    public function listAllAction(Request $request)
    {
        $shippingAddressModel = $this->get('pannovate.ecmodelbundle.model.shipping_address');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $shippingAddresses = $shippingAddressModel->readForUser($user);

        if($shippingAddresses)
        {
            $view = $this->view($shippingAddresses, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view); 
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function updateAction(Request $request, $id)
    {
        try
        {
            $shippingAddressModel = $this->get('pannovate.ecmodelbundle.model.shipping_address');
            $shippingAddress = $shippingAddressModel->update($id, $request->request->all());

            return $this->handleView(
                $this->view(
                    $shippingAddress,
                    204
                )
            );    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function deleteAction($id)
    {
        $shippingAddressModel = $this->get('pannovate.ecmodelbundle.model.shipping_address');

        $shippingAddress = $shippingAddressModel->delete($id);

        $view = $this->view(null, 204);

        return $this->handleView($view);
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(array('shipping_address', 'country'));

        return $context;
    }
}