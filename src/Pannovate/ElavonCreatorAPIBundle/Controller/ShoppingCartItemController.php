<?php

namespace Pannovate\ElavonCreatorAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;
use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class ShoppingCartItemController extends FOSRestController
{
	public function createAction(Request $request)
    {
        try
        {
            $shoppingCartItemItemModel = $this->get('pannovate.ecmodelbundle.model.shopping_cart_item');
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $shoppingCartItem = $shoppingCartItemItemModel->create($request->request->all(), $user);

            $this->updateTotals();
            
            $url = $this->container->get('router')->generate(
                'pannovate_ecapi_shopping_cart_item_get',
                array(
                    'id' => $shoppingCartItem->getId()
                ),
                true // absolute
            );

            $view = $this->view(
                $shoppingCartItem,
                201,
                array(
                    "Location" => $url
                )
            );

            $view->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function getShoppingCartItemAction($id)
    {
        $shoppingCartItemItemModel = $this->get('pannovate.ecmodelbundle.model.shopping_cart_item');
        $shoppingCartItem = $shoppingCartItemItemModel->get($id);

        if($shoppingCartItem)
        {
            $view = $this->view($shoppingCartItem, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }   
    }

    public function listAllForCurrentUserAction(Request $request)
    {
        $shoppingCartModel = $this->get('pannovate.ecmodelbundle.model.shopping_cart');
        $shoppingCartItemItemModel = $this->get('pannovate.ecmodelbundle.model.shopping_cart_item');
        
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $shoppingCart = $shoppingCartModel->get($user->getShoppingCart()->getId());

        if($shoppingCart)
        {
            $view = $this->view($shoppingCart->getShoppingCartItems(), 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }
    }

    public function patchAction(Request $request, $id)
    {
        $shoppingCartItemItemModel = $this->get('pannovate.ecmodelbundle.model.shopping_cart_item');
        $shoppingCartItem = $shoppingCartItemItemModel->get($id);

        $user = $this->get('security.token_storage')->getToken()->getUser();

        if($shoppingCartItem)
        {
            if($shoppingCartItem->getShoppingCart()->getId() === $user->getShoppingCart()->getId())
            {
                $shoppingCartItem = $shoppingCartItemItemModel->patch($id, $request->request->all());
                $this->updateTotals();

                return $this->handleView($this->view(null, 204));
            }
            else
            {
                return $this->handleView($this->view("This shopping cart item does not belong you you!", 400));
            }   
        }
        else
        {
            return $this->handleView($this->view("Invalid shopping cart item id", 400));
        }        
    }

    public function deleteAction(Request $request, $id)
    {
        $shoppingCartItemItemModel = $this->get('pannovate.ecmodelbundle.model.shopping_cart_item');
        $shoppingCartModel = $this->get('pannovate.ecmodelbundle.model.shopping_cart');

        $shoppingCartItem = $shoppingCartItemItemModel->get($id);

        if($shoppingCartItem)
        {
            $shoppingCart = $shoppingCartModel->get($shoppingCartItem->getShoppingCart()->getId());
            $user = $this->get('security.token_storage')->getToken()->getUser();

            if($shoppingCart->getUser()->getId() !== $user->getId())
            {
                return $this->handleView($this->view("Invalid shopping cart id!", 400));
            }
            else
            {
                $shoppingCartItemItemModel->delete($id);

                $this->updateTotals();

                return $this->handleView($this->view(null, 204));
            }
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }
    }

    protected function updateTotals()
    {
        $shoppingCartModel = $this->get('pannovate.ecmodelbundle.model.shopping_cart');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $billingAddressModel = $this->get('pannovate.ecmodelbundle.model.billing_address');
        $shippingAddressModel = $this->get('pannovate.ecmodelbundle.model.shipping_address');

        $lastCreatedBillingAddress = $billingAddressModel->getLastCreatedAddressForUser($user);
        $lastCreatedShippingAddress = $shippingAddressModel->getLastCreatedAddressForUser($user);

        $shoppingCartModel->updateCartTotalPrice($user, $lastCreatedBillingAddress, $lastCreatedShippingAddress);
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(array('shopping_cart_item', 'shopping_cart_basic_info', 'user_basic_info', 'card_design_basic_info'));

        return $context;
    }
}