<?php

namespace Pannovate\ElavonCreatorAPIBundle\Controller;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class CardDesignOrderController extends FOSRestController
{
    public function createAction(Request $request)
    {
        try
        {
            $cardDesignOrderModel = $this->get('pannovate.ecmodelbundle.model.card_design_order');
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $cardDesignOrder = $cardDesignOrderModel->create($request->request->all(), $user);

            $url = $this->container->get('router')->generate(
                'pannovate_ecapi_card_design_order_get',
                array(
                    'id' => $cardDesignOrder->getId()
                ),
                true // absolute
            );

            $view = $this->view(
                $cardDesignOrder,
                201,
                array(
                    "Location" => $url
                )
            );

            $view->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);    
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function getCardDesignOrderAction($id)
    {
        $cardDesignOrderModel = $this->get('pannovate.ecmodelbundle.model.card_design_order');
        $cardDesignOrder = $cardDesignOrderModel->get($id);

        if($cardDesignOrder)
        {
            $view = $this->view($cardDesignOrder, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }   
    }

    public function getLastModifiedAction(Request $request)
    {
        $cardDesignOrderModel = $this->get('pannovate.ecmodelbundle.model.card_design_order');
        $user = $this->get('security.token_storage')->getToken()->getUser();
        
        $cardDesignOrder = $cardDesignOrderModel->getLastModified($user);

        

        if($cardDesignOrder)
        {
            $view = $this->view($cardDesignOrder, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        } 
        
    }

    public function getRecentCardDesignOrdersAction(Request $request)
    {
        $cardDesignOrderModel = $this->get('pannovate.ecmodelbundle.model.card_design_order');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $cardDesignOrders = $cardDesignOrderModel->getRecentDesigns($user);
        
        if($cardDesignOrders)
        {
            $view = $this->view($cardDesignOrders, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function listAllAction(Request $request)
    {
        $cardDesignOrderModel = $this->get('pannovate.ecmodelbundle.model.card_design_order');

        $cardDesignOrders = $cardDesignOrderModel->readAll();


        if($cardDesignOrders)
        {
            $view = $this->view($cardDesignOrders, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view); 
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(
            array(
                'card_design_order',
                'card_design_order_item',
                'user',
                'order_billing_address',
                'order_shipping_address',
                'order_card_design',
                'order_currency',
                'country'
            )
        );

        return $context;
    }
}
