<?php

namespace Pannovate\ElavonCreatorAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

use Pannovate\BaseAPIBundle\ValueObject\BadInputError;
use Pannovate\BaseAPIBundle\Error\ErrorCodes;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class ShoppingCartController extends FOSRestController
{
	public function createAction(Request $request)
    {
        try
        {
            $shoppingCartModel = $this->get('pannovate.ecmodelbundle.model.shopping_cart');
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $shoppingCart = $shoppingCartModel->create($request->request->all(), $user);
            
            $url = $this->container->get('router')->generate(
                'pannovate_elavon_ecapi_shopping_cart_get',
                array(
                    'id' => $shoppingCart->getId()
                ),
                true // absolute
            );

            $view = $this->view(
                $shoppingCart,
                201,
                array(
                    "Location" => $url
                )
            );

            $view->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        catch(InvalidFormDataException $error)
        {
            return $this->handleView($this->view($error->getErrorMessages(), 400));
        }
    }

    public function getShoppingCartAction($id)
    {
        $shoppingCartModel = $this->get('pannovate.ecmodelbundle.model.shopping_cart');
        $shoppingCart = $shoppingCartModel->get($id);

        if($shoppingCart)
        {
            $view = $this->view($shoppingCart, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleView($view);
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }   
    }

    public function listForCurrentUserAction(Request $request)
    {
        $shoppingCartModel = $this->get('pannovate.ecmodelbundle.model.shopping_cart');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $shoppingCartes = $shoppingCartModel->readForUser($user);

        if($shoppingCartes)
        {
            $view = $this->view($shoppingCartes, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view); 
        }
        else
        {
            return $this->handleView($this->view([], 200));
        }
    }

    public function patchAction(Request $request, $id)
    {
        return $this->updateCartTotals($request, $id);
    }

    public function updateCartTotals(Request $request, $id)
    {
        $shoppingCartModel = $this->get('pannovate.ecmodelbundle.model.shopping_cart');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $billingAddressModel = $this->get('pannovate.ecmodelbundle.model.billing_address');
        $shippingAddressModel = $this->get('pannovate.ecmodelbundle.model.shipping_address');

        $billingAddress = null;
        $shippingAddress = null;

        if($request->request->get('billingAddress'))
        {
            $billingAddress = $billingAddressModel->get($request->request->get('billingAddress'));
        }
        else
        {
            return $this->handleView($this->view("Invalid billing address", 400));
        }

        if($request->request->get('shippingSameAsBilling') === "true")
        {
            $shoppingCart = $shoppingCartModel->updateCartTotalPrice($user, $billingAddress, null, true);
        }
        else if($request->request->get('shippingAddress'))
        {
            $shippingAddress = $shippingAddressModel->get($request->request->get('shippingAddress'));
            $shoppingCart = $shoppingCartModel->updateCartTotalPrice($user, $billingAddress, $shippingAddress);
        }
        else
        {
            return $this->handleView($this->view("Invalid shipping address", 400));
        }

        return $this->handleView($this->view($shoppingCart, 200));
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(array('shopping_cart', 'user_basic_info'));

        return $context;
    }
}