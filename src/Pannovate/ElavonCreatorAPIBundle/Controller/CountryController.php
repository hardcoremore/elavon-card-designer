<?php

namespace Pannovate\ElavonCreatorAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

class CountryController extends FOSRestController
{
    public function listAllAction(Request $request)
    {
        $countryModel = $this->get('pannovate.ecmodelbundle.model.country');
        $countries = $countryModel->readAll();

        if($countries)
        {
            $view = $this->view($countries, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view); 
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }
    }

    public function listCountriesSelectAction()
    {
        $countryModel = $this->get('pannovate.ecmodelbundle.model.country');
        $countries = $countryModel->readForSelect();

        if($countries)
        {
            $view = $this->view($countries, 200)
                         ->setSerializationContext($this->createSerializationContext());

            return $this->handleview($view); 
        }
        else
        {
            return $this->handleView($this->view(null, 404));
        }
    }

    protected function createSerializationContext()
    {
        $context = $this->get('pannovate.baseapibundle.serialization_context');
        $context->setSerializeNull(true);
        $context->setGroups(array('country'));

        return $context;
    }
}