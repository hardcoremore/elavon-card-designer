<?php

namespace Pannovate\ElavonCreatorAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;

class ElavonFanfareAPIController extends FOSRestController
{
    public function getProgramsAction(Request $request)
    {
        $fanfareApiModel = $this->get('pannovate.ecmodelbundle.model.elavon_fanfare_api');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $programsResponse = $fanfareApiModel->getPrograms($user->getUsername());

        $view = $this->view($programsResponse, 200);

        return $this->handleView($view);
    }

    public function addCardSetAction(Request $request)
    {
        $fanfareApiModel = $this->get('pannovate.ecmodelbundle.model.elavon_fanfare_api');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $cardSetResponse = $fanfareApiModel->addCardSet($user->getUsername(), 823 , 'Promo', 100);

        $view = $this->view($cardSetResponse, 200);

        return $this->handleView($view);
    }

    public function getCardSetAction(Request $request)
    {
        $fanfareApiModel = $this->get('pannovate.ecmodelbundle.model.elavon_fanfare_api');
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $cardSetResponse = $fanfareApiModel->getCardSet($user->getUsername(), 822);

        $view = $this->view($cardSetResponse, 200);

        return $this->handleView($view);
    }
}
