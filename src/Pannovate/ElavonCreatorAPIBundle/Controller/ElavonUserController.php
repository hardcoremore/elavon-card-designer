<?php

namespace Pannovate\ElavonCreatorAPIBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Pannovate\BaseAPIBundle\Controller\UserController;

class ElavonUserController extends UserController
{
    protected function getSerializationGroups()
    {
        return array('user', 'elavon_user', 'country', 'currency', 'shopping_cart');
    }
}
