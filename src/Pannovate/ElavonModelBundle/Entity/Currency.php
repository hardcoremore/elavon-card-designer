<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * Currency
 *
 * @ORM\Table(name="currencies")
 * @ORM\Entity(repositoryClass="Pannovate\ElavonModelBundle\Entity\Repository\CurrencyRepository")
 *
 * @UniqueEntity(fields="code", message="Code already exists")
 */
class Currency
{
    public function __construct()
    {
        $this->isBase = false;
        $this->cardPrices = new ArrayCollection();
        $this->shoppingCarts = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=16)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=3)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="symbol", type="string", length=1)
     */
    private $symbol;

    /**
     * @var string
     *
     * @ORM\Column(name="rate", type="decimal", precision=20, scale=4)
     */
    private $rate;

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="currencies")
     * @ORM\JoinColumn(name="country", referencedColumnName="id")
     */
    private $country;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_base", type="boolean")
     */
    private $isBase;

    /**
    *
    * @ORM\OneToMany(targetEntity="CardPricing", mappedBy="currency")
    *
    */
    private $cardPrices;

    /**
    *
    * @ORM\OneToMany(targetEntity="ElavonUser", mappedBy="currency")
    *
    */
    private $users;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Currency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Currency
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set symbol
     *
     * @param string $symbol
     *
     * @return Currency
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;

        return $this;
    }

    /**
     * Get symbol
     *
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Set rate
     *
     * @param string $rate
     *
     * @return Currency
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set country
     *
     * @param integer $country
     *
     * @return Currency
     */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set is_base
     *
     * @param boolean $isBase
     * @return SiteUser
     */
    public function setIsBase($isBase)
    {
        if(gettype($isBase) === "string")
        {
            $this->isBase = ($isBase === 'true');
        }
        else
        {
            $this->isBase = (bool)$isBase;
        }

        return $this;
    }

    /**
     * Get isBase
     *
     * @return booelan
     */
    public function getIsBase()
    {
        return $this->isBase;
    }

    /**
     * Add cardPricing
     *
     * @param CardPricing $cardPricing
     *
     * @return CardPricing
     */
    public function addCardPricing(CardPricing $cardPricing)
    {
        $this->cardPrices[] = $cardPricing;

        return $this;
    }

    /**
     * Remove cardPricing
     *
     * @param CardPricing $cardPricing
     */
    public function removeCardPricing(CardPricing $cardPricing)
    {
        $this->cardPrices->removeElement($cardPricing);
    }

    /**
     * Get cardPrices
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCardPrices()
    {
        return $this->cardPrices;
    }

    /**
     * Add users
     *
     * @param ShoppingCart $user
     *
     * @return Currency
     */
    public function addUser(ElavonUser $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove users
     *
     * @param User $user
     */
    public function removeUser(ElavonUser $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}
