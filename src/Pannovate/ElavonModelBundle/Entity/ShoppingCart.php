<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use \Doctrine\Common\Collections\ArrayCollection;


/**
 * ShoppingCart
 *
 * @ORM\Table(name="shopping_cart")
 * @ORM\Entity(repositoryClass="Pannovate\ElavonModelBundle\Entity\Repository\ShoppingCartRepository")
 */
class ShoppingCart
{
    public function __construct()
    {
        $this->shoppingCartItems = new ArrayCollection();
        $this->cartPriceTotal = 0;
        $this->cartItemsTotalCount = 0;
        $this->cartItemsTotalPrice = 0;
        $this->shippingCharge = 0;
        $this->taxCharge = 0;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="ElavonUser", inversedBy="shoppingCart")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var decimal
     *
     * @ORM\Column(name="cart_items_total_price", type="decimal", precision=20, scale=2)
     */
    private $cartItemsTotalPrice;

    /**
     * @var decimal
     *
     * @ORM\Column(name="cart_price_total", type="decimal", precision=20, scale=2)
     */
    private $cartPriceTotal;

     /**
     * @var decimal
     *
     * @ORM\Column(name="shipping_charge", type="decimal", precision=20, scale=2)
     */
    private $shippingCharge;

    /**
     * @var decimal
     *
     * @ORM\Column(name="tax_charge", type="decimal", precision=20, scale=2)
     */
    private $taxCharge;

    /**
     * @var integer
     *
     * @ORM\Column(name="cart_items_total_count", type="smallint")
     */
    private $cartItemsTotalCount;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_modified_datetime", type="datetime", nullable=true)
     */
    private $lastModifiedDatetime;

    /**
    *
    * @ORM\OneToMany(targetEntity="ShoppingCartItem", mappedBy="shoppingCart")
    *
    */
    private $shoppingCartItems;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param ElavonUser $user
     *
     * @return ShoppingCart
     */
    public function setUser(ElavonUser $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return ElavonUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set cartItemsTotalPrice
     *
     * @param decimal $cartItemsTotalPrice
     *
     * @return ShoppingCart
     */
    public function setCartItemsTotalPrice($cartItemsTotalPrice)
    {
        $this->cartItemsTotalPrice = $cartItemsTotalPrice;

        return $this;
    }

    /**
     * Get cartItemsTotalPrice
     *
     * @return decimal
     */
    public function getCartItemsTotalPrice()
    {
        return $this->cartItemsTotalPrice;
    }

    /**
     * Set cartPriceTotal
     *
     * @param string $cartPriceTotal
     *
     * @return ShoppingCart
     */
    public function setCartPriceTotal($cartPriceTotal)
    {
        $this->cartPriceTotal = $cartPriceTotal;

        return $this;
    }

    /**
     * Get cartPriceTotal
     *
     * @return string
     */
    public function getCartPriceTotal()
    {
        return $this->cartPriceTotal;
    }

    /**
     * Set shippingCharge
     *
     * @param string $shippingCharge
     *
     * @return ShoppingCart
     */
    public function setShippingCharge($shippingCharge)
    {
        $this->shippingCharge = $shippingCharge;

        return $this;
    }

    /**
     * Get shippingCharge
     *
     * @return string
     */
    public function getShippingCharge()
    {
        return $this->shippingCharge;
    }

    /**
     * Set taxCharge
     *
     * @param string $taxCharge
     *
     * @return ShoppingCart
     */
    public function setTaxCharge($taxCharge)
    {
        $this->taxCharge = $taxCharge;

        return $this;
    }

    /**
     * Get taxCharge
     *
     * @return string
     */
    public function getTaxCharge()
    {
        return $this->taxCharge;
    }

    /**
     * Set cartItemsTotalCount
     *
     * @param integer $cartItemsTotalCount
     *
     * @return ShoppingCart
     */
    public function setCartItemsTotalCount($cartItemsTotalCount)
    {
        $this->cartItemsTotalCount = $cartItemsTotalCount;

        return $this;
    }

    /**
     * Get cartItemsTotalCount
     *
     * @return integer
     */
    public function getCartItemsTotalCount()
    {
        return $this->cartItemsTotalCount;
    }

    /**
     * Set lastModifiedDatetime
     *
     * @param \DateTime $lastModifiedDatetime
     *
     * @return ShoppingCart
     */
    public function setLastModifiedDatetime($lastModifiedDatetime)
    {
        $this->lastModifiedDatetime = $lastModifiedDatetime;

        return $this;
    }

    /**
     * Get lastModifiedDatetime
     *
     * @return \DateTime
     */
    public function getLastModifiedDatetime()
    {
        return $this->lastModifiedDatetime;
    }

    /**
     * Add shoppingCartItem
     *
     * @param ShoppingCartItem $shoppingCartItem
     *
     * @return ShoppingCartItem
     */
    public function addShoppingCartItem(ShoppingCartItem $shoppingCartItem)
    {
        $this->shoppingCartItemes[] = $shoppingCartItem;

        return $this;
    }

    /**
     * Remove shoppingCartItem
     *
     * @param ShoppingCartItem $shoppingCartItem
     */
    public function removeShoppingCartItem(ShoppingCartItem $shoppingCartItem)
    {
        $this->shoppingCartItemes->removeElement($shoppingCartItem);
    }

    /**
     * Get shoppingCartItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShoppingCartItems()
    {
        return $this->shoppingCartItems;
    }
}

