<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ShippingCharge
 *
 * @ORM\Table(name="shipping_charges", uniqueConstraints={@ORM\UniqueConstraint(name="shipping_charge_for_country", columns={"country_id"})})
 * @ORM\Entity
 *
 * @UniqueEntity(fields="country", message="Shipping charge already exists for that country")
 */
class ShippingCharge
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="Country", inversedBy="shippingCharge")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=20, scale=4)
     */
    private $amount;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Set country
    *
    * @param integer $country
    *
    * @return Currency
    */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return ShippingCharge
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }
}

