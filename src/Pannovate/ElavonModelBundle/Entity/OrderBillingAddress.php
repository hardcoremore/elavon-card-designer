<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use \Doctrine\Common\Collections\ArrayCollection;

/**
 * OrderBillingAddress
 *
 * @ORM\Table(name="order_billing_addresses")
 * @ORM\Entity(repositoryClass="Pannovate\ElavonModelBundle\Entity\Repository\OrderBillingAddressRepository")
 */
class OrderBillingAddress
{
    public function __construct()
    {
        $this->cardDesignOrders = new ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=64)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=32)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=32)
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=128)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=64)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=32)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=32)
     */
    private $postcode;

     /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="orderBillingAddresses")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=64)
     */
    private $phone;

    /**
    *
    * @ORM\OneToMany(targetEntity="CardDesignOrder", mappedBy="billingAddress")
    *
    */
    private $cardDesignOrders;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return OrderBillingAddress
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return OrderBillingAddress
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return OrderBillingAddress
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return OrderBillingAddress
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return OrderBillingAddress
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return OrderBillingAddress
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     *
     * @return OrderBillingAddress
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return OrderBillingAddress
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return OrderBillingAddress
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Add cardDesignOrder
     *
     * @param CardDesignOrder $cardDesignOrder
     *
     * @return User
     */
    public function addCardDesignOrder(CardDesignOrder $cardDesignOrder)
    {
        $this->cardDesignOrders[] = $cardDesignOrder;

        return $this;
    }

    /**
     * Remove cardDesignOrder
     *
     * @param Design $cardDesignOrder
     */
    public function removeCardDesignOrder(CardDesignOrder $cardDesignOrder)
    {
        $this->cardDesignOrders->removeElement($cardDesignOrder);
    }

    /**
     * Get cardDesignOrders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCardDesignOrders()
    {
        return $this->cardDesignOrders;
    }
}
