<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CardPricing
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Pannovate\ElavonModelBundle\Entity\Repository\CardPricingRepository")
 */
class CardPricing
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="minimum_cards", type="integer")
     */
    private $minimumCards;

    /**
     * @var integer
     *
     * @ORM\Column(name="maximum_cards", type="integer")
     */
    private $maximumCards;

    /**
     * @var decimal
     *
     * @ORM\Column(name="loyalty", type="decimal", precision=20, scale=4)
     */
    private $loyalty;

    /**
     * @var decimal
     *
     * @ORM\Column(name="gift", type="decimal", precision=20, scale=4)
     */
    private $gift;

    /**
     * @var decimal
     *
     * @ORM\Column(name="promo", type="decimal", precision=20, scale=4)
     */
    private $promo;

    /**
     * @ORM\ManyToOne(targetEntity="Currency", inversedBy="cardPrices")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set minimumCards
     *
     * @param integer $minimumCards
     *
     * @return CardPricing
     */
    public function setMinimumCards($minimumCards)
    {
        $this->minimumCards = $minimumCards;

        return $this;
    }

    /**
     * Get minimumCards
     *
     * @return integer
     */
    public function getMinimumCards()
    {
        return $this->minimumCards;
    }

    /**
     * Set maximumCards
     *
     * @param integer $maximumCards
     *
     * @return CardPricing
     */
    public function setMaximumCards($maximumCards)
    {
        $this->maximumCards = $maximumCards;

        return $this;
    }

    /**
     * Get maximumCards
     *
     * @return integer
     */
    public function getMaximumCards()
    {
        return $this->maximumCards;
    }

    /**
     * Set loyalty
     *
     * @param string $loyalty
     *
     * @return CardPricing
     */
    public function setLoyalty($loyalty)
    {
        $this->loyalty = $loyalty;

        return $this;
    }

    /**
     * Get loyalty
     *
     * @return string
     */
    public function getLoyalty()
    {
        return $this->loyalty;
    }

    /**
     * Set gift
     *
     * @param string $gift
     *
     * @return CardPricing
     */
    public function setGift($gift)
    {
        $this->gift = $gift;

        return $this;
    }

    /**
     * Get gift
     *
     * @return string
     */
    public function getGift()
    {
        return $this->gift;
    }

    /**
     * Set promo
     *
     * @param string $promo
     *
     * @return CardPricing
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * Get promo
     *
     * @return string
     */
    public function getPromo()
    {
        return $this->promo;
    }

     /**
    * Set currency
    *
    * @param integer $currency
    *
    * @return Currency
    */
    public function setCurrency(Currency $currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}

