<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CardDesignImageElement
 *
 * @ORM\Table(name="card_design_image_elements")
 * @ORM\Entity(repositoryClass="Pannovate\ElavonModelBundle\Entity\Repository\CardDesignImageElementRepository")
 */
class CardDesignImageElement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @ORM\ManyToOne(targetEntity="CardDesign", inversedBy="imageElements")
     * @ORM\JoinColumn(name="card_design", referencedColumnName="id")
     */
    private $cardDesign;

    /**
     * @var string
     *
     * @ORM\Column(name="original_file_name", type="string", length=255, nullable=true)
     */
    private $originalFileName;

    /**
     * @var string
     *
     * @ORM\Column(name="original_file_url", type="string", length=512, nullable=true)
     */
    private $originalFileUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="file_name", type="string", length=128)
     */
    private $fileName;

    /**
     * @var string
     *
     * @ORM\Column(name="file_path", type="string", length=255)
     */
    private $filePath;

    /**
     * @var string
     *
     * @ORM\Column(name="file_url", type="string", length=255)
     */
    private $fileUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="element_type", type="string", length=32)
     */
    private $elementType;

    /**
     * @var string
     *
     * @ORM\Column(name="card_side", type="string", length=1)
     */
    private $cardSide;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set originalFileName
     *
     * @param string $originalFileName
     *
     * @return CardDesignImageElement
     */
    public function setOriginalFileName($originalFileName)
    {
        $this->originalFileName = $originalFileName;

        return $this;
    }

    /**
     * Get originalFileName
     *
     * @return string
     */
    public function getOriginalFileName()
    {
        return $this->originalFileName;
    }

    /**
     * Set originalFileUrl
     *
     * @param string $originalFileUrl
     *
     * @return CardDesignImageElement
     */
    public function setOriginalFileUrl($originalFileUrl)
    {
        $this->originalFileUrl = $originalFileUrl;

        return $this;
    }

    /**
     * Get originalFileUrl
     *
     * @return string
     */
    public function getOriginalFileUrl()
    {
        return $this->originalFileUrl;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     *
     * @return CardDesignImageElement
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set filePath
     *
     * @param string $filePath
     *
     * @return CardDesignImageElement
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get filePath
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set fileUrl
     *
     * @param string $fileUrl
     *
     * @return CardDesignImageElement
     */
    public function setFileUrl($fileUrl)
    {
        $this->fileUrl = $fileUrl;

        return $this;
    }

    /**
     * Get fileUrl
     *
     * @return string
     */
    public function getFileUrl()
    {
        return $this->fileUrl;
    }

    /**
     * Set elementType
     *
     * @param string $elementType
     *
     * @return CardDesignImageElement
     */
    public function setElementType($elementType)
    {
        $this->elementType = $elementType;

        return $this;
    }

    /**
     * Get elementType
     *
     * @return string
     */
    public function getElementType()
    {
        return $this->elementType;
    }

    /**
     * Set cardSide
     *
     * @param string $cardSide
     *
     * @return CardDesignImageElement
     */
    public function setCardSide($cardSide)
    {
        $this->cardSide = $cardSide;

        return $this;
    }

    /**
     * Get cardSide
     *
     * @return string
     */
    public function getCardSide()
    {
        return $this->cardSide;
    }

    /**
     * Set cardDesign
     *
     * @param CardDesign $cardDesign
     *
     * @return CardDesignImageElement
     */
    public function setCardDesign(CardDesign $cardDesign = null)
    {
        $this->cardDesign = $cardDesign;

        return $this;
    }

    /**
     * Get cardDesign
     *
     * @return CardDesign
     */
    public function getCardDesign()
    {
        return $this->cardDesign;
    }
}
