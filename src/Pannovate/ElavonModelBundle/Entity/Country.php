<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;


/**
 * Country
 *
 * @ORM\Table(name="countries")
 * @ORM\Entity(repositoryClass="Pannovate\ElavonModelBundle\Entity\Repository\CountryRepository")
 */
class Country
{
    public function __construct()
    {
        $this->shippingAddresses = new ArrayCollection();
        $this->currencies = new ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=2)
     */
    private $code;

    /**
    *
    * @ORM\OneToMany(targetEntity="ShippingAddress", mappedBy="country")
    *
    */
    private $shippingAddresses;

    /**
    *
    * @ORM\OneToMany(targetEntity="OrderShippingAddress", mappedBy="country")
    *
    */
    private $orderShippingAddresses;

    /**
    *
    * @ORM\OneToMany(targetEntity="BillingAddress", mappedBy="country")
    *
    */
    private $billingAddresses;

    /**
    *
    * @ORM\OneToMany(targetEntity="OrderBillingAddress", mappedBy="country")
    *
    */
    private $orderBillingAddresses;

    /**
    *
    * @ORM\OneToMany(targetEntity="Currency", mappedBy="country")
    *
    */
    private $currencies;

    /**
    *
    * @ORM\OneToMany(targetEntity="ElavonUser", mappedBy="country")
    *
    */
    private $users;

    /**
    *
    * @ORM\OneToOne(targetEntity="Tax", mappedBy="country")
    *
    */
    private $tax;

    /**
    *
    * @ORM\OneToOne(targetEntity="ShippingCharge", mappedBy="country")
    *
    */
    private $shippingCharge;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add shippingAddress
     *
     * @param ShippingAddress $shippingAddress
     *
     * @return Country
     */
    public function addShippingAddress(ShippingAddress $shippingAddress)
    {
        $this->shippingAddresses[] = $shippingAddress;

        return $this;
    }

    /**
     * Remove shippingAddress
     *
     * @param ShippingAddress $shippingAddress
     */
    public function removeShippingAddress(ShippingAddress $shippingAddress)
    {
        $this->shippingAddresses->removeElement($shippingAddress);
    }

    /**
     * Get shippingAddresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShippingAddresses()
    {
        return $this->shippingAddresses;
    }

    /**
     * Add currency
     *
     * @param Currency $currency
     *
     * @return Currency
     */
    public function addCurrency(Currency $currency)
    {
        $this->currencies[] = $currency;

        return $this;
    }

    /**
     * Remove currency
     *
     * @param Currency $currency
     */
    public function removeCurrency(Currency $currency)
    {
        $this->currencies->removeElement($currency);
    }

    /**
     * Get currencies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }

    /**
     * Set tax
     *
     * @param Tax $tax
     *
     * @return Country
     */
    public function setTax(Tax $tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return Tax
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set shipping charge
     *
     * @param ShippingCharge $shippingCharge
     *
     * @return Country
     */
    public function setShippingCharge(ShippingCharge $shippingCharge)
    {
        $this->shippingCharge = $shippingCharge;

        return $this;
    }

    /**
     * Get shipping charge
     *
     * @return ShippingCharge
     */
    public function getShippingCharge()
    {
        return $this->shippingCharge;
    }
}
