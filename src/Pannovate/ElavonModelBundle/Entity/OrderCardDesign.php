<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderCardDesign
 *
 * @ORM\Table(name="order_card_designs")
 * @ORM\Entity(repositoryClass="Pannovate\ElavonModelBundle\Entity\Repository\OrderCardDesignRepository")
 */
class OrderCardDesign
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="front_design_image_data", type="text", nullable=true)
     */
    private $frontDesignImageData;

    /**
     * @var string
     *
     * @ORM\Column(name="front_image_name", type="string", length=255, nullable=true)
     */
    private $frontImageName;

     /**
     * @var string
     *
     * @ORM\Column(name="front_image_url", type="string", length=255, nullable=true)
     */
    private $frontImageUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="front_image_thumb_url", type="string", length=255, nullable=true)
     */
    private $frontImageThumbUrl;


    /**
     * @var string
     *
     * @ORM\Column(name="front_background_image_url", type="string", length=255, nullable=true)
     */
    private $frontBackgroundImageUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="back_design_image_data", type="text", nullable=true)
     */
    private $backDesignImageData;

    /**
     * @var string
     *
     * @ORM\Column(name="back_image_name", type="string", length=255, nullable=true)
     */
    private $backImageName;

     /**
     * @var string
     *
     * @ORM\Column(name="back_image_url", type="string", length=255, nullable=true)
     */
    private $backImageUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="back_image_thumb_url", type="string", length=255, nullable=true)
     */
    private $backImageThumbUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="back_background_image_url", type="string", length=255, nullable=true)
     */
    private $backBackgroundImageUrl;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return OrderCardDesign
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return OrderCardDesign
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set frontDesignImageData
     *
     * @param string $frontDesignImageData
     *
     * @return OrderCardDesign
     */
    public function setFrontDesignImageData($frontDesignImageData)
    {
        $this->frontDesignImageData = $frontDesignImageData;

        return $this;
    }

    /**
     * Get frontDesignImageData
     *
     * @return string
     */
    public function getFrontDesignImageData()
    {
        return $this->frontDesignImageData;
    }

    /**
     * Set frontImageName
     *
     * @param string $frontImageName
     *
     * @return OrderCardDesign
     */
    public function setFrontImageName($frontImageName)
    {
        $this->frontImageName = $frontImageName;

        return $this;
    }

    /**
     * Get frontImageName
     *
     * @return string
     */
    public function getFrontImageName()
    {
        return $this->frontImageName;
    }

    /**
     * Set frontImageUrl
     *
     * @param string $frontImageUrl
     *
     * @return OrderCardDesign
     */
    public function setFrontImageUrl($frontImageUrl)
    {
        $this->frontImageUrl = $frontImageUrl;

        return $this;
    }

    /**
     * Get frontImageUrl
     *
     * @return string
     */
    public function getFrontImageUrl()
    {
        return $this->frontImageUrl;
    }

    /**
     * Set frontImageThumbUrl
     *
     * @param string $frontImageThumbUrl
     *
     * @return OrderCardDesign
     */
    public function setFrontImageThumbUrl($frontImageThumbUrl)
    {
        $this->frontImageThumbUrl = $frontImageThumbUrl;

        return $this;
    }

    /**
     * Get frontImageThumbUrl
     *
     * @return string
     */
    public function getFrontImageThumbUrl()
    {
        return $this->frontImageThumbUrl;
    }

    /**
     * Set frontBackgroundImageUrl
     *
     * @param string $frontBackgroundImageUrl
     *
     * @return OrderCardDesign
     */
    public function setFrontBackgroundImageUrl($frontBackgroundImageUrl)
    {
        $this->frontBackgroundImageUrl = $frontBackgroundImageUrl;

        return $this;
    }

    /**
     * Get frontBackgroundImageUrl
     *
     * @return string
     */
    public function getFrontBackgroundImageUrl()
    {
        return $this->frontBackgroundImageUrl;
    }

    /**
     * Set backDesignImageData
     *
     * @param string $backDesignImageData
     *
     * @return OrderCardDesign
     */
    public function setBackDesignImageData($backDesignImageData)
    {
        $this->backDesignImageData = $backDesignImageData;

        return $this;
    }

    /**
     * Get backDesignImageData
     *
     * @return string
     */
    public function getBackDesignImageData()
    {
        return $this->backDesignImageData;
    }

    /**
     * Set backImageName
     *
     * @param string $backImageName
     *
     * @return OrderCardDesign
     */
    public function setBackImageName($backImageName)
    {
        $this->backImageName = $backImageName;

        return $this;
    }

    /**
     * Get backImageName
     *
     * @return string
     */
    public function getBackImageName()
    {
        return $this->backImageName;
    }

    /**
     * Set backImageUrl
     *
     * @param string $backImageUrl
     *
     * @return OrderCardDesign
     */
    public function setBackImageUrl($backImageUrl)
    {
        $this->backImageUrl = $backImageUrl;

        return $this;
    }

    /**
     * Get backImageUrl
     *
     * @return string
     */
    public function getBackImageUrl()
    {
        return $this->backImageUrl;
    }


    /**
     * Set backImageThumbUrl
     *
     * @param string $backImageThumbUrl
     *
     * @return OrderCardDesign
     */
    public function setBackImageThumbUrl($backImageThumbUrl)
    {
        $this->backImageThumbUrl = $backImageThumbUrl;

        return $this;
    }

    /**
     * Get backImageThumbUrl
     *
     * @return string
     */
    public function getBackImageThumbUrl()
    {
        return $this->backImageThumbUrl;
    }


    /**
     * Set backBackgroundImageUrl
     *
     * @param string $backBackgroundImageUrl
     *
     * @return OrderCardDesign
     */
    public function setBackBackgroundImageUrl($backBackgroundImageUrl)
    {
        $this->backBackgroundImageUrl = $backBackgroundImageUrl;

        return $this;
    }

    /**
     * Get backBackgroundImageUrl
     *
     * @return string
     */
    public function getBackBackgroundImageUrl()
    {
        return $this->backBackgroundImageUrl;
    }
}
