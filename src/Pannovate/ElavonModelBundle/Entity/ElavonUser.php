<?php

namespace Pannovate\ElavonModelBundle\Entity;   

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\EquatableInterface;

use Pannovate\BaseModelBundle\Entity\User as BaseUser;

use Doctrine\ORM\Mapping as ORM;

 /**
  * 
  * @ORM\Entity(repositoryClass="Pannovate\BaseModelBundle\Entity\Repository\UserRepository")
  *
  **/
class ElavonUser extends BaseUser
{

    public function __construct()
    {
        parent::__construct();

        $this->cardDesigns = new ArrayCollection();
        $this->cardDesignOrders = new ArrayCollection();
    }

    /**
    *
    * @ORM\OneToMany(targetEntity="CardDesign", mappedBy="createdBy")
    *
    */
    private $cardDesigns;

    /**
    *
    * @ORM\OneToMany(targetEntity="ShippingAddress", mappedBy="user")
    *
    */
    private $shippingAddresses;

    /**
    *
    * @ORM\OneToMany(targetEntity="BillingAddress", mappedBy="user")
    *
    */
    private $billingAddresses;

     /**
     * @ORM\ManyToOne(targetEntity="Currency", inversedBy="users")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=16)
     */
    private $city;

    /**
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="users")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
    *
    * @ORM\OneToOne(targetEntity="ShoppingCart", mappedBy="user")
    *
    */
    private $shoppingCart;

    /**
     * @var string
     *
     * @ORM\Column(name="programs", type="string", length=32)
     */
    private $programs;

    /**
    *
    * @ORM\OneToMany(targetEntity="CardDesignOrder", mappedBy="user")
    *
    */
    private $cardDesignOrders;

    /**
     * Add cardDesign
     *
     * @param CardDesign $cardDesign
     *
     * @return User
     */
    public function addCardDesign(CardDesign $cardDesign)
    {
        $this->cardDesigns[] = $cardDesign;

        return $this;
    }

    /**
     * Remove cardDesign
     *
     * @param Design $cardDesign
     */
    public function removeCardDesign(CardDesign $cardDesign)
    {
        $this->cardDesigns->removeElement($cardDesign);
    }

    /**
     * Get cardDesigns
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCardDesigns()
    {
        return $this->cardDesigns;
    }

    /**
     * Add shippingAddress
     *
     * @param ShippingAddress $shippingAddress
     *
     * @return ElavonUser
     */
    public function addShippingAddress(ShippingAddress $shippingAddress)
    {
        $this->shippingAddresses[] = $shippingAddress;

        return $this;
    }

    /**
     * Remove shippingAddress
     *
     * @param ShippingAddress $shippingAddress
     */
    public function removeShippingAddress(ShippingAddress $shippingAddress)
    {
        $this->shippingAddresses->removeElement($shippingAddress);
    }

    /**
     * Get shippingAddresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShippingAddresses()
    {
        return $this->shippingAddresses;
    }

    /**
    * Set currency
    *
    * @param integer $currency
    *
    * @return Currency
    */
    public function setCurrency(Currency $currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

     /**
     * Set shoppingCart
     *
     * @param ShoppingCart $shoppingCart
     *
     * @return ElavonUser
     */
    public function setShoppingCart(ShoppingCart $shoppingCart)
    {
        $this->shoppingCart = $shoppingCart;

        return $this;
    }

    /**
     * Get shoppingCart
     *
     * @return ShoppingCart
     */
    public function getShoppingCart()
    {
        return $this->shoppingCart;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return ShippingAddress
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
    * Set country
    *
    * @param integer $country
    *
    * @return country
    */
    public function setCountry(Country $country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set programs
     *
     * @param string $programs
     *
     */
    public function setPrograms($programs)
    {
        $this->programs = $programs;

        return $this;
    }

    /**
     * Get programs
     *
     * @return string
     */
    public function gePrograms()
    {
        return $this->programs;
    }

    /**
     * Add cardDesignOrder
     *
     * @param CardDesignOrder $cardDesignOrder
     *
     * @return User
     */
    public function addCardDesignOrder(CardDesignOrder $cardDesignOrder)
    {
        $this->cardDesignOrders[] = $cardDesignOrder;

        return $this;
    }

    /**
     * Remove cardDesignOrder
     *
     * @param Design $cardDesignOrder
     */
    public function removeCardDesignOrder(CardDesignOrder $cardDesignOrder)
    {
        $this->cardDesignOrders->removeElement($cardDesignOrder);
    }

    /**
     * Get cardDesignOrders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCardDesignOrders()
    {
        return $this->cardDesignOrders;
    }
}
