<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImageGalleryItems
 *
 * @ORM\Table(name="image_gallery_items")
 * @ORM\Entity(repositoryClass="Pannovate\ElavonModelBundle\Entity\Repository\ImageGalleryItemRepository")
 */
class ImageGalleryItem
{
    public function __construct()
    {
        $this->setIsActive(true);
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ImageGalleryCategory", inversedBy="imageItems")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="fileName", type="string", length=64)
     */
    private $fileName;

    /**
     * @var string
     *
     * @ORM\Column(name="filePath", type="string", length=255)
     */
    private $filePath;

    /**
     * @var string
     *
     * @ORM\Column(name="fileUrl", type="string", length=255)
     */
    private $fileUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="original_file_name", type="string", length=128)
     */
    private $originalFileName;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param ImageGalleryCategory $category
     *
     * @return ImageGalleryItems
     */
    public function setCategory(ImageGalleryCategory $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return ImageGalleryCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ImageGalleryItems
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set fileName
     *
     * @param string $fileName
     *
     * @return ImageGalleryItems
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * Set filePath
     *
     * @param string $filePath
     *
     * @return ImageGalleryItems
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;

        return $this;
    }

    /**
     * Get filePath
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * Set fileUrl
     *
     * @param string $fileUrl
     *
     * @return ImageGalleryItems
     */
    public function setFileUrl($fileUrl)
    {
        $this->fileUrl = $fileUrl;

        return $this;
    }

    /**
     * Get fileUrl
     *
     * @return string
     */
    public function getFileUrl()
    {
        return $this->fileUrl;
    }

     /**
     * Set originalFileName
     *
     * @param string $originalFileName
     *
     * @return ImageGalleryItems
     */
    public function setOriginalFilename($originalFileName)
    {
        $this->originalFileName = $originalFileName;

        return $this;
    }

    /**
     * Get originalFileName
     *
     * @return string
     */
    public function getOriginalFilename()
    {
        return $this->originalFileName;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return ImageGalleryItems
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }
}
