<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CardDesignOrderItem
 *
 * @ORM\Table(name="card_design_order_items")
 * @ORM\Entity(repositoryClass="Pannovate\ElavonModelBundle\Entity\Repository\CardDesignOrderItemRepository")
 */
class CardDesignOrderItem
{
    const STATUS_PENDING_APPROVAL = 'pending_approval';
    const STATUS_APPROVED = 'approved';
    const STATUS_REJECTED = 'rejected';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     * @ORM\ManyToOne(targetEntity="CardDesignOrder", inversedBy="cardDesignOrderItems")
     * @ORM\JoinColumn(name="card_design_order_id", referencedColumnName="id")
     */
    private $cardDesignOrder;

    /**
     * @ORM\OneToOne(targetEntity="OrderCardDesign", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="order_card_design_id", referencedColumnName="id")
     */
    private $cardDesign;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="smallint")
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="pricePerCard", type="decimal", precision=20, scale=2)
     */
    private $pricePerCard;

    /**
     * @var string
     *
     * @ORM\Column(name="totalPrice", type="decimal", precision=20, scale=2)
     */
    private $totalPrice;

    /**
     * @var array
     *
     * @ORM\Column(name="denomination", type="json_array")
     */
    private $denomination;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=16)
     */
    private $status;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cardDesignOrder
     *
     * @param CardDesignOrder $cardDesignOrder
     *
     * @return CardDesignOrderItem
     */
    public function setCardDesignOrder(CardDesignOrder $cardDesignOrder)
    {
        $this->cardDesignOrder = $cardDesignOrder;

        return $this;
    }

    /**
     * Get cardDesignOrder
     *
     * @return CardDesignOrder
     */
    public function getCardDesignOrder()
    {
        return $this->cardDesignOrder;
    }

    /**
     * Set cardDesign
     *
     * @param OrderCardDesign $cardDesign
     *
     * @return CardDesignOrderItem
     */
    public function setCardDesign(OrderCardDesign $cardDesign)
    {
        $this->cardDesign = $cardDesign;

        return $this;
    }

    /**
     * Get cardDesign
     *
     * @return OrderCardDesign
     */
    public function getCardDesign()
    {
        return $this->cardDesign;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return CardDesignOrderItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set pricePerCard
     *
     * @param string $pricePerCard
     *
     * @return CardDesignOrderItem
     */
    public function setPricePerCard($pricePerCard)
    {
        $this->pricePerCard = $pricePerCard;

        return $this;
    }

    /**
     * Get pricePerCard
     *
     * @return string
     */
    public function getPricePerCard()
    {
        return $this->pricePerCard;
    }

    /**
     * Set totalPrice
     *
     * @param string $totalPrice
     *
     * @return CardDesignOrderItem
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return string
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set denomination
     *
     * @param array $denomination
     *
     * @return CardDesignOrderItem
     */
    public function setDenomination($denomination)
    {
        $this->denomination = $denomination;

        return $this;
    }

    /**
     * Get denomination
     *
     * @return array
     */
    public function getDenomination()
    {
        return $this->denomination;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return CardDesignOrderItem
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }
}

