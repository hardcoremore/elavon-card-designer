<?php

namespace Pannovate\ElavonModelBundle\Entity;   

use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\EquatableInterface;

use Pannovate\BaseModelBundle\Entity\User as BaseUser;

use Doctrine\ORM\Mapping as ORM;

 /**
  * 
  * @ORM\Entity(repositoryClass="Pannovate\BaseModelBundle\Entity\Repository\UserRepository")
  *
  **/
class ElavonAdminPanelUser extends BaseUser
{

    public function __construct()
    {
        parent::__construct();
    }
}
