<?php

namespace Pannovate\ElavonModelBundle\Entity\Repository;

/**
 * CardPricingRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CardPricingRepository extends \Doctrine\ORM\EntityRepository
{
	public function listForCurrency($id)
	{
		return $this->createQueryBuilder('c')
						->innerJoin('c.currency', 'u', 'WITH', 'u = c.currency')
						->where('u.id = :id')
						->setParameter('id', $id)
						->getQuery()
						->getResult();
	}
}
