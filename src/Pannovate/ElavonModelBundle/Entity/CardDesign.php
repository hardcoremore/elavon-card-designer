<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CardDesign
 *
 * @ORM\Table(name="card_designs")
 * @ORM\Entity(repositoryClass="Pannovate\ElavonModelBundle\Entity\Repository\CardDesignRepository")
 */
class CardDesign
{

    public function __construct()
    {
        $this->setCreatedDatetime(new \DateTime());
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=16)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="front_design_image_data", type="text", nullable=true)
     */
    private $frontDesignImageData;

    /**
     * @var string
     *
     * @ORM\Column(name="front_image_name", type="string", length=255, nullable=true)
     */
    private $frontImageName;

     /**
     * @var string
     *
     * @ORM\Column(name="front_image_url", type="string", length=255, nullable=true)
     */
    private $frontImageUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="front_image_thumb_url", type="string", length=255, nullable=true)
     */
    private $frontImageThumbUrl;


    /**
     * @var string
     *
     * @ORM\Column(name="front_background_image_url", type="string", length=255, nullable=true)
     */
    private $frontBackgroundImageUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="back_design_image_data", type="text", nullable=true)
     */
    private $backDesignImageData;

    /**
     * @var string
     *
     * @ORM\Column(name="back_image_name", type="string", length=255, nullable=true)
     */
    private $backImageName;

     /**
     * @var string
     *
     * @ORM\Column(name="back_image_url", type="string", length=255, nullable=true)
     */
    private $backImageUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="back_image_thumb_url", type="string", length=255, nullable=true)
     */
    private $backImageThumbUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="back_background_image_url", type="string", length=255, nullable=true)
     */
    private $backBackgroundImageUrl;

    /**
     * @ORM\ManyToOne(targetEntity="ElavonUser", inversedBy="cardDesigns")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     */
    private $createdBy;

     /**
     * @var datetime
     *
     * @ORM\Column(name="created_datetime", type="datetime")
     */
    private $createdDatetime;

    /**
     * @var datetime
     *
     * @ORM\Column(name="last_modified_datetime", type="datetime", nullable=true)
     */
    private $lastModifiedDatetime;

    /**
    *
    * @ORM\OneToMany(targetEntity="CardDesignImageElement", mappedBy="cardDesign")
    *
    */
    private $imageElements;

    /**
    *
    * @ORM\OneToOne(targetEntity="ShoppingCartItem", mappedBy="cardDesign")
    *
    */
    private $shoppingCartItem;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return CardDesign
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CardDesign
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set frontDesignImageData
     *
     * @param string $frontDesignImageData
     *
     * @return CardDesign
     */
    public function setFrontDesignImageData($frontDesignImageData)
    {
        $this->frontDesignImageData = $frontDesignImageData;

        return $this;
    }

    /**
     * Get frontDesignImageData
     *
     * @return string
     */
    public function getFrontDesignImageData()
    {
        return $this->frontDesignImageData;
    }

    /**
     * Set frontImageName
     *
     * @param string $frontImageName
     *
     * @return CardDesign
     */
    public function setFrontImageName($frontImageName)
    {
        $this->frontImageName = $frontImageName;

        return $this;
    }

    /**
     * Get frontImageName
     *
     * @return string
     */
    public function getFrontImageName()
    {
        return $this->frontImageName;
    }

    /**
     * Set frontImageUrl
     *
     * @param string $frontImageUrl
     *
     * @return CardDesign
     */
    public function setFrontImageUrl($frontImageUrl)
    {
        $this->frontImageUrl = $frontImageUrl;

        return $this;
    }

    /**
     * Get frontImageUrl
     *
     * @return string
     */
    public function getFrontImageUrl()
    {
        return $this->frontImageUrl;
    }

    /**
     * Set frontImageThumbUrl
     *
     * @param string $frontImageThumbUrl
     *
     * @return CardDesign
     */
    public function setFrontImageThumbUrl($frontImageThumbUrl)
    {
        $this->frontImageThumbUrl = $frontImageThumbUrl;

        return $this;
    }

    /**
     * Get frontImageThumbUrl
     *
     * @return string
     */
    public function getFrontImageThumbUrl()
    {
        return $this->frontImageThumbUrl;
    }

    /**
     * Set frontBackgroundImageUrl
     *
     * @param string $frontBackgroundImageUrl
     *
     * @return CardDesign
     */
    public function setFrontBackgroundImageUrl($frontBackgroundImageUrl)
    {
        $this->frontBackgroundImageUrl = $frontBackgroundImageUrl;

        return $this;
    }

    /**
     * Get frontBackgroundImageUrl
     *
     * @return string
     */
    public function getFrontBackgroundImageUrl()
    {
        return $this->frontBackgroundImageUrl;
    }

    /**
     * Set backDesignImageData
     *
     * @param string $backDesignImageData
     *
     * @return CardDesign
     */
    public function setBackDesignImageData($backDesignImageData)
    {
        $this->backDesignImageData = $backDesignImageData;

        return $this;
    }

    /**
     * Get backDesignImageData
     *
     * @return string
     */
    public function getBackDesignImageData()
    {
        return $this->backDesignImageData;
    }

    /**
     * Set backImageName
     *
     * @param string $backImageName
     *
     * @return CardDesign
     */
    public function setBackImageName($backImageName)
    {
        $this->backImageName = $backImageName;

        return $this;
    }

    /**
     * Get backImageName
     *
     * @return string
     */
    public function getBackImageName()
    {
        return $this->backImageName;
    }

    /**
     * Set backImageUrl
     *
     * @param string $backImageUrl
     *
     * @return CardDesign
     */
    public function setBackImageUrl($backImageUrl)
    {
        $this->backImageUrl = $backImageUrl;

        return $this;
    }

    /**
     * Get backImageUrl
     *
     * @return string
     */
    public function getBackImageUrl()
    {
        return $this->backImageUrl;
    }


    /**
     * Set backImageThumbUrl
     *
     * @param string $backImageThumbUrl
     *
     * @return CardDesign
     */
    public function setBackImageThumbUrl($backImageThumbUrl)
    {
        $this->backImageThumbUrl = $backImageThumbUrl;

        return $this;
    }

    /**
     * Get backImageThumbUrl
     *
     * @return string
     */
    public function getBackImageThumbUrl()
    {
        return $this->backImageThumbUrl;
    }


    /**
     * Set backBackgroundImageUrl
     *
     * @param string $backBackgroundImageUrl
     *
     * @return CardDesign
     */
    public function setBackBackgroundImageUrl($backBackgroundImageUrl)
    {
        $this->backBackgroundImageUrl = $backBackgroundImageUrl;

        return $this;
    }

    /**
     * Get backBackgroundImageUrl
     *
     * @return string
     */
    public function getBackBackgroundImageUrl()
    {
        return $this->backBackgroundImageUrl;
    }

    /**
     * Set createdDatetime
     *
     * @param \DateTime $createdDatetime
     *
     * @return CardDesign
     */
    public function setCreatedDatetime($createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;

        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    /**
     * Set lastModifiedDatetime
     *
     * @param \DateTime $lastModifiedDatetime
     *
     * @return CardDesign
     */
    public function setLastModifiedDatetime($lastModifiedDatetime)
    {
        $this->lastModifiedDatetime = $lastModifiedDatetime;

        return $this;
    }

    /**
     * Get lastModifiedDatetime
     *
     * @return \DateTime
     */
    public function getLastModifiedDatetime()
    {
        return $this->lastModifiedDatetime;
    }

    /**
     * Set createdBy
     *
     * @param ElavonUser $createdBy
     *
     * @return CardDesign
     */
    public function setCreatedBy(ElavonUser $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return ElavonUser
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Add imageElement
     *
     * @param CardDesignImageElement $imageElement
     *
     * @return CardDesign
     */
    public function addImageElement(CardDesignImageElement $imageElement)
    {
        $this->imageElements[] = $imageElement;

        return $this;
    }

    /**
     * Remove imageElement
     *
     * @param CardDesignImageElement $imageElement
     */
    public function removeImageElement(CardDesignImageElement $imageElement)
    {
        $this->imageElements->removeElement($imageElement);
    }

    /**
     * Get imageElements
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImageElements()
    {
        return $this->imageElements;
    }

    /**
     * Set shoppingCartItem
     *
     * @param ShoppingCartItem $shoppingCartItem
     *
     * @return CardDesign
     */
    public function setShoppingCartItem(ShoppingCartItem $shoppingCartItem)
    {
        $this->shoppingCartItem = $shoppingCartItem;

        return $this;
    }

    /**
     * Set shoppingCartItem
     *
     * @return ShoppingCartItem
     */
    public function getShoppingCartItem()
    {
        return $this->shoppingCartItem;
    }
}
