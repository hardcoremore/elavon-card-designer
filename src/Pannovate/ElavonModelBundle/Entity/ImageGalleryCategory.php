<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImageGalleryCategory
 *
 * @ORM\Table(name="image_gallery_category")
 * @ORM\Entity
 */
class ImageGalleryCategory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32)
     */
    private $name;

   /**
    *
    * @ORM\OneToMany(targetEntity="ImageGalleryItem", mappedBy="category")
    *
    */
    private $imageItems;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ImageGalleryCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add imageItem
     *
     * @param ImageGalleryCategory $imageItem
     *
     * @return ImageGalleryCategory
     */
    public function addImageGalleryCategory(ImageGalleryCategory $imageItem)
    {
        $this->imageItems[] = $imageItem;

        return $this;
    }

    /**
     * Remove imageItem
     *
     * @param ImageGalleryCategory $imageItem
     */
    public function removeImageGalleryCategory(ImageGalleryCategory $imageItem)
    {
        $this->imageItems->removeElement($imageItem);
    }

    /**
     * Get imageItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaxes()
    {
        return $this->imageItems;
    }
}

