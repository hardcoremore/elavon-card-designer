<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * CardDesignOrder
 *
 * @ORM\Table(name="card_design_orders")
 * @ORM\Entity(repositoryClass="Pannovate\ElavonModelBundle\Entity\Repository\CardDesignOrderRepository")
 */
class CardDesignOrder
{
    const STATUS_NOT_PAYED = 'not_payed';
    const STATUS_PAYED = 'payed';
    const STATUS_PENDING_APPROVAL = 'pending_approval';
    const STATUS_APPROVED = 'approved';//processing
    const STATUS_REJECTED = 'rejected';
    //added By Bojan
    const STATUS_IN_PRODUCTION = 'in_production';
    const STATUS_PRODUCED = 'produced';

    public function __construct()
    {
        $this->cardDesignOrderItems = new ArrayCollection();
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ElavonUser", inversedBy="cardDesignOrders")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToOne(targetEntity="OrderCurrency", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

     /**
     * @ORM\ManyToOne(targetEntity="OrderBillingAddress", inversedBy="cardDesignOrders", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="billing_address_id", referencedColumnName="id")
     */
    private $billingAddress;

    /**
     * @ORM\ManyToOne(targetEntity="OrderShippingAddress", inversedBy="cardDesignOrders", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="shipping_address_id", referencedColumnName="id")
     */
    private $shippingAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="order_items_price_total", type="decimal", precision=20, scale=2)
     */
    private $orderItemsPriceTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_charge_price_total", type="decimal", precision=20, scale=2)
     */
    private $shippingChargePriceTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="tax_charge_price_total", type="decimal", precision=20, scale=2)
     */
    private $taxChargePriceTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="order_price_total", type="decimal", precision=20, scale=2)
     */
    private $orderPriceTotal;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_items_count", type="smallint")
     */
    private $orderItemsCount;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=16)
     */
    private $status;

    /**
    *
    * @ORM\OneToMany(targetEntity="CardDesignOrderItem", mappedBy="cardDesignOrder")
    *
    */
     private $cardDesignOrderItems;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param ElavonUser $user
     *
     * @return CardDesignOrder
     */
    public function setUser(ElavonUser $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return ElavonUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set currency
     *
     * @param OrderCurrency $currency
     *
     * @return CardDesignOrder
     */
    public function setCurrency(OrderCurrency $currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return OrderCurrency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set billingAddress
     *
     * @param OrderBillingAddress $billingAddress
     *
     * @return CardDesignOrder
     */
    public function setBillingAddress(OrderBillingAddress $billingAddress)
    {
        $this->billingAddress = $billingAddress;

        return $this;
    }

    /**
     * Get billingAddress
     *
     * @return OrderBillingAddress
     */
    public function getBillingAddress()
    {
        return $this->billingAddress;
    }

    /**
     * Set shippingAddress
     *
     * @param OrderShippingAddress $shippingAddress
     *
     * @return CardDesignOrder
     */
    public function setShippingAddress(OrderShippingAddress $shippingAddress)
    {
        $this->shippingAddress = $shippingAddress;

        return $this;
    }

    /**
     * Get shippingAddress
     *
     * @return OrderShippingAddress
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * Set orderItemsPriceTotal
     *
     * @param string $orderItemsPriceTotal
     *
     * @return CardDesignOrder
     */
    public function setOrderItemsPriceTotal($orderItemsPriceTotal)
    {
        $this->orderItemsPriceTotal = $orderItemsPriceTotal;

        return $this;
    }

    /**
     * Get orderItemsPriceTotal
     *
     * @return string
     */
    public function getOrderItemsPriceTotal()
    {
        return $this->orderItemsPriceTotal;
    }

    /**
     * Set shippingChargePriceTotal
     *
     * @param string $shippingChargePriceTotal
     *
     * @return CardDesignOrder
     */
    public function setShippingChargePriceTotal($shippingChargePriceTotal)
    {
        $this->shippingChargePriceTotal = $shippingChargePriceTotal;

        return $this;
    }

    /**
     * Get shippingChargePriceTotal
     *
     * @return string
     */
    public function getShippingChargePriceTotal()
    {
        return $this->shippingChargePriceTotal;
    }

    /**
     * Set taxChargePriceTotal
     *
     * @param string $taxChargePriceTotal
     *
     * @return CardDesignOrder
     */
    public function setTaxChargePriceTotal($taxChargePriceTotal)
    {
        $this->taxChargePriceTotal = $taxChargePriceTotal;

        return $this;
    }

    /**
     * Get taxChargePriceTotal
     *
     * @return string
     */
    public function getTaxChargePriceTotal()
    {
        return $this->taxChargePriceTotal;
    }

    /**
     * Set orderPriceTotal
     *
     * @param string $orderPriceTotal
     *
     * @return CardDesignOrder
     */
    public function setOrderPriceTotal($orderPriceTotal)
    {
        $this->orderPriceTotal = $orderPriceTotal;

        return $this;
    }

    /**
     * Get orderPriceTotal
     *
     * @return string
     */
    public function getOrderPriceTotal()
    {
        return $this->orderPriceTotal;
    }

    /**
     * Set orderItemsCount
     *
     * @param integer $orderItemsCount
     *
     * @return CardDesignOrder
     */
    public function setOrderItemsCount($orderItemsCount)
    {
        $this->orderItemsCount = $orderItemsCount;

        return $this;
    }

    /**
     * Get orderItemsCount
     *
     * @return integer
     */
    public function getOrderItemsCount()
    {
        return $this->orderItemsCount;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return CardDesignOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

     /**
     * Add cardDesignOrderItem
     *
     * @param CardDesignOrderItem $cardDesignOrderItem
     *
     * @return User
     */
    public function addCardDesignOrderItem(CardDesignOrderItem $cardDesignOrderItem)
    {
        $this->cardDesignOrderItems[] = $cardDesignOrderItem;

        return $this;
    }

    /**
     * Remove cardDesignOrderItem
     *
     * @param Design $cardDesignOrderItem
     */
    public function removeCardDesignOrderItem(CardDesignOrderItem $cardDesignOrderItem)
    {
        $this->cardDesignOrderItems->removeElement($cardDesignOrderItem);
    }

    /**
     * Get cardDesignOrderItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCardDesignOrderItems()
    {
        return $this->cardDesignOrderItems;
    }
}
