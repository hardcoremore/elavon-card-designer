<?php

namespace Pannovate\ElavonModelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ShoppingCartItem
 *
 * @ORM\Table(name="shopping_cart_items", uniqueConstraints={@ORM\UniqueConstraint(name="item_card_design", columns={"card_design_id"})})
 * @ORM\Entity(repositoryClass="Pannovate\ElavonModelBundle\Entity\Repository\ShoppingCartItemRepository")
 * 
 * @UniqueEntity(fields="cardDesign", message="Card design is already added to shopping cart")
 *
 */
class ShoppingCartItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ShoppingCart", inversedBy="shoppingCartItems")
     * @ORM\JoinColumn(name="shopping_cart", referencedColumnName="id")
     */
    private $shoppingCart;

    /**
     * @ORM\OneToOne(targetEntity="CardDesign", inversedBy="shoppingCartItem")
     * @ORM\JoinColumn(name="card_design_id", referencedColumnName="id")
     */
    private $cardDesign;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer")
     */
    private $quantity;

    /**
     * @var decimal
     *
     * @ORM\Column(name="price_per_card", type="decimal", precision=20, scale=2)
     */
    private $pricePerCard;

    /**
     * @var decimal
     *
     * @ORM\Column(name="total_price", type="decimal", precision=20, scale=2)
     */
    private $totalPrice;

    /**
     * @var array
     *
     * @ORM\Column(name="card_denomination", type="json_array", nullable=true)
     */
    private $cardDenomination;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shoppingCart
     *
     * @param integer $shoppingCart
     *
     * @return ShoppingCartItem
     */
    public function setShoppingCart($shoppingCart)
    {
        $this->shoppingCart = $shoppingCart;

        return $this;
    }

    /**
     * Get shoppingCart
     *
     * @return integer
     */
    public function getShoppingCart()
    {
        return $this->shoppingCart;
    }

    /**
     * Set cardDesign
     *
     * @param integer $cardDesign
     *
     * @return ShoppingCartItem
     */
    public function setCardDesign($cardDesign)
    {
        $this->cardDesign = $cardDesign;

        return $this;
    }

    /**
     * Get cardDesign
     *
     * @return integer
     */
    public function getCardDesign()
    {
        return $this->cardDesign;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return ShoppingCartItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set pricePerCard
     *
     * @param string $pricePerCard
     *
     * @return ShoppingCartItem
     */
    public function setPricePerCard($pricePerCard)
    {
        $this->pricePerCard = $pricePerCard;

        return $this;
    }

    /**
     * Get pricePerCard
     *
     * @return string
     */
    public function getPricePerCard()
    {
        return $this->pricePerCard;
    }

    /**
     * Set totalPrice
     *
     * @param string $totalPrice
     *
     * @return ShoppingCartItem
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return decimal
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set cardDenomination
     *
     * @param array $cardDenomination
     *
     * @return ShoppingCartItem
     */
    public function setCardDenomination($cardDenomination)
    {
        $this->cardDenomination = $cardDenomination;

        return $this;
    }

    /**
     * Get cardDenomination
     *
     * @return array
     */
    public function getCardDenomination()
    {
        return $this->cardDenomination;
    }
}

