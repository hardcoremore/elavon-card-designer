<?php

namespace Pannovate\ElavonModelBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CardDenominationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('quantity', 'text', array('label' => false))
                ->add('denomination', 'text', array('label' => false))
                ->getForm();
    }
}