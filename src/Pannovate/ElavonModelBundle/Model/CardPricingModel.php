<?php

namespace Pannovate\ElavonModelBundle\Model;

use Pannovate\BaseModelBundle\Base\Model;
use Pannovate\ElavonModelBundle\Model\CurrencyModel;

class CardPricingModel extends Model
{
	protected $currencyModel;

	public function setCurrencyModel(CurrencyModel $model)
	{
		$this->currencyModel = $model;
	}

	public function getCurrencyModel()
	{
		return $this->currencyModel;
	}

	public function listForCurrency($id)
    {
        $data = $this->entityManager
                     ->getRepository($this->entityName)
                     ->listForCurrency($id);
        
        return $data;
    }

    public function create($data, ...$arguments)
    {
    	$cardPrice = parent::create($data, $arguments);

    	$newCardData = $data;
    	$allCurrencies = $this->currencyModel->readAll();

		foreach($allCurrencies as $val) 
		{

			if($val->getId() !== (int)$data['currency']) {
				$newCardData['loyalty'] = $data['loyalty'] / $val->getRate();
				$newCardData['promo'] = $data['promo'] / $val->getRate();
				$newCardData['gift'] = $data['gift'] / $val->getRate();
				$newCardData['currency'] = $val->getId();
				parent::create($newCardData);
			}
		}

		return $cardPrice;
    }
}