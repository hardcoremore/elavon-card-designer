<?php

namespace Pannovate\ElavonModelBundle\Model;

use Pannovate\BaseModelBundle\Model\UserModel;
use Pannovate\ElavonModelBundle\Model\CurrencyModel;

class ElavonUserModel extends UserModel
{
    protected $currencyModel;
    protected $elavonFanfareAPIModel;

    public function setCurrencyModel(CurrencyModel $model)
    {
        $this->currencyModel = $model;
    }

    public function getCurrencyModel()
    {
        return $this->currencyModel;
    }

    public function setElavonFanfareAPIModel(ElavonFanfareAPIModel $model)
    {
        $this->elavonFanfareAPIModel = $model;
    }

    public function getElavonFanfareAPIModel()
    {
        return $this->elavonFanfareAPIModel;
    }

    protected function updateEntityBeforeCreate($entity)
    {
        parent::updateEntityBeforeCreate($entity);

        $baseCurrency = $this->getCurrencyModel()->getBaseCurrency();
        $entity->setCurrency($baseCurrency);

        $getPrograms = $this->getElavonFanfareAPIModel()->getPrograms($entity->getUsername());

        $fanfarePrograms = $getPrograms['programs'];

        if(isset($fanfarePrograms['program']))
        {
            $fanfarePrograms = $fanfarePrograms['program'];
        }

        $userPrograms = '';

        for($i = 0, $len = count($fanfarePrograms); $i < $len; $i++ )
        {
            $userPrograms .= $fanfarePrograms[$i]['type'] . ',';
        }

        $entity->setPrograms(rtrim($userPrograms, ','));
    }
}