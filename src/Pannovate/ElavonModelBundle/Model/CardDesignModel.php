<?php

namespace Pannovate\ElavonModelBundle\Model;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Pannovate\BaseModelBundle\Base\Model;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;
use Pannovate\BaseModelBundle\Services\FileUploadService;
use Pannovate\BaseModelBundle\Services\ImageHelperService;

use Pannovate\ElavonModelBundle\Entity\ElavonUser;
use Pannovate\ElavonModelBundle\Model\ShoppingCartItemModel;

class CardDesignModel extends Model
{
    protected $fileUploadService;
    protected $imageHelperService;
    protected $shoppingCartItemModel;

    public function setFileUploadService(FileUploadService $fileUpload)
    {
        $this->fileUploadService = $fileUpload;
    }

    public function getFileUploadService()
    {
        return $this->fileUploadService;
    }

    public function setImageHelperService(ImageHelperService $imageHelper)
    {
        $this->imageHelperService = $imageHelper;
    }

    public function getImageHelperService()
    {
        return $this->imageHelperService;
    }

    public function setShoppingCartItemModel(ShoppingCartItemModel $model)
    {
        $this->shoppingCartItemModel = $model;
    }

    public function getShoppingCartItemModel()
    {
        return $this->shoppingCartItemModel;
    }

    protected function updateEntityBeforeCreate($entity)
    {
        $entity->setCreatedBy($this->saveEntityArguments[0]);
    }

    protected function updateEntityBeforeUpdate($entity)
    {
        $entity->setLastModifiedDatetime(new \DateTime());
    }

    public function saveCardDesign(array $cardDesignData)
    {
        $filePath = $cardDesignData['file_path'];

        $design = $this->get($cardDesignData['id']);

        $thumbImageUrl = '';

        if($cardDesignData['card_side'] === 'front')
        {
            if($design->getFrontImageName())
            {
                $hashedFileName = $this->getFileUploadService()->saveUploadedFile($filePath, $design->getFrontImageName(), true);
            }
            else
            {
                $hashedFileName = $this->getFileUploadService()->saveUploadedFile($filePath, 'cardDesignFrontImage.png');

                // design image is created for the first time, update the image name and url
                $design->setFrontImageName($hashedFileName);
                $design->setFrontImageUrl($this->getFileUploadService()->getUrlFromFileName($hashedFileName, true));
            }
            
            $design->setFrontDesignImageData($cardDesignData['image_data']);

            // create front image thumb
            $thumbImageUrl = $this->createDesignThumbImage($design->getFrontImageName(), $design->getFrontImageThumbUrl());
            $design->setFrontImageThumbUrl($thumbImageUrl);
        }
        else
        {
            if($design->getBackImageName())
            {
                $hashedFileName = $this->getFileUploadService()->saveUploadedFile($filePath, $design->getBackImageName(), true);
            }
            else
            {
                $hashedFileName = $this->getFileUploadService()->saveUploadedFile($filePath, 'cardDesignBackImage.png');

                // design image is created for the first time, update the image name and url
                $design->setBackImageName($hashedFileName);
                $design->setBackImageUrl($this->getFileUploadService()->getUrlFromFileName($hashedFileName, true));
            }

            $design->setBackDesignImageData($cardDesignData['image_data']);

            // create back image thumb
            $thumbImageUrl = $this->createDesignThumbImage($design->getBackImageName(), $design->getBackImageThumbUrl());
            $design->setBackImageThumbUrl($thumbImageUrl);
        }


        $allObjects = json_decode($cardDesignData['image_data']);

        if($allObjects && property_exists($allObjects, 'objects') && is_array($allObjects->objects))
        {
            foreach($allObjects->objects AS $key => $val)
            {
                if($val->elementType === 'background-image')
                {
                    if($cardDesignData['card_side'] === 'front')
                    {
                        $design->setFrontBackgroundImageUrl(parse_url($val->src, PHP_URL_PATH));
                    }
                    else
                    {
                        $design->setBackBackgroundImageUrl(parse_url($val->src, PHP_URL_PATH));
                    }
                }
            }
        }

        $design->setLastModifiedDatetime(new \DateTime());

        $this->entityManager->flush();

        $fileUrlPath = $this->getFileUploadService()->getUrlFromFileName($hashedFileName);

        return $fileUrlPath;
    }

    public function createDesignThumbImage($designImageName, $currentThumbImageUrl = '')
    {
        $designImageFullPath = $this->getFileUploadService()->getPathFromFileName($designImageName) . '/' . $designImageName;
        $designImageProperties = $this->getImageHelperService()->readImageProperties($designImageFullPath);

        $thumbImageFullPath = '';
        $thumbImageFileName = '';

        if($currentThumbImageUrl && strlen($currentThumbImageUrl) > 0)
        {
            $thumbImageFileName = $this->getFileUploadService()->getFileNameFromUrl($currentThumbImageUrl);
            $thumbImageFullPath = $this->getFileUploadService()->getPathFromFileName($thumbImageFileName) . '/' . $thumbImageFileName;
            
        }
        else
        {
            $thumbImageFileName = $this->getFileUploadService()->getHashedFileName('thumb/'. $designImageFullPath);
            $thumbFileDirectoryPath = $this->getFileUploadService()->createFileFolder($thumbImageFileName);
            $thumbImageFullPath = $thumbFileDirectoryPath . '/' . $thumbImageFileName;
        }

        $imageCropParameters = [
            'x' => 0,
            'y' => 0,
            'rotate' => 0,
            'maxWidth' => 512
        ];

        $imageCropParameters['width'] = $designImageProperties['width'];
        $imageCropParameters['height'] = $designImageProperties['height'];

        

        $croppedImage = $this->getImageHelperService()->cropImage($designImageFullPath, $imageCropParameters, $designImageProperties);
        $this->getImageHelperService()->saveImage($croppedImage, $thumbImageFullPath, $designImageProperties);

        return $this->getFileUploadService()->getUrlFromFileName($thumbImageFileName, true);
    }

    public function getLastModified(ElavonUser $user)
    {
        $design = $this->entityManager
                       ->getRepository($this->entityName)
                       ->findOneBy(
                            array('createdBy' => $user->getId()),
                            array('lastModifiedDatetime' => "DESC")
                        );

        return $design;
    }

    public function getRecentDesigns(ElavonUser $user)
    {
        $data = $this->entityManager
                     ->getRepository($this->entityName)
                     ->getRecentDesigns($user->getId());
        
        return $data;
    }

    public function readForUser(ElavonUser $user)
    {
        $designs = $this->entityManager
                        ->getRepository($this->entityName)
                        ->findByCreatedBy($user);

        return $designs;
    }

    public function delete($id)
    {
        $em = $this->entityManager;
        $design = $this->get($id);

        // remove front design image
        if($design->getFrontImageName())
        {   
            $path = $this->getFileUploadService()->getPathFromFileName($design->getFrontImageName());

            if(file_exists($path . '/' . $design->getFrontImageName()))
            {
                unlink($path . '/' . $design->getFrontImageName());
            }
        }

        // remove back design image
        if($design->getBackImageName())
        {
            $path = $this->getFileUploadService()->getPathFromFileName($design->getBackImageName());

            if(file_exists($path . '/' . $design->getBackImageName()))
            {
                unlink($path . '/' . $design->getBackImageName());
            }
        }

        // remove front thumb image
        if($design->getFrontImageThumbUrl())
        {   
            $fileName = $this->getFileUploadService()->getFileNameFromUrl($design->getFrontImageThumbUrl());
            $path = $this->getFileUploadService()->getPathFromFileName($fileName);

            if(file_exists($path . '/' . $fileName))
            {
                unlink($path . '/' . $fileName);
            }
        }

        // remove back thumb image
        if($design->getBackImageThumbUrl())
        {   
            $fileName = $this->getFileUploadService()->getFileNameFromUrl($design->getBackImageThumbUrl());
            $path = $this->getFileUploadService()->getPathFromFileName($fileName);

            if(file_exists($path . '/' . $fileName))
            {
                unlink($path . '/' . $fileName);
            }
        }

        $imageElements = $design->getImageElements();

        foreach ($imageElements as $element) 
        {
            $elementFilePath = $this->getFileUploadService()->getPathFromFileName($element->getFileName());

            if(file_exists($elementFilePath . '/' . $element->getFileName()))
            {
                unlink($elementFilePath . '/' . $element->getFileName());
            }

            $design->removeImageElement($element);
            $this->entityManager->remove($element);
        }
            
        $shoppingCartItem = $this->getShoppingCartItemModel()->findByCardDesign($id);

        if($shoppingCartItem)
        {
            $this->getShoppingCartItemModel()->delete($shoppingCartItem->getId());
        }

        parent::delete($id);
    }
}
