<?php

namespace Pannovate\ElavonModelBundle\Model;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Pannovate\BaseModelBundle\Base\Model;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;
use Pannovate\BaseModelBundle\Services\FileUploadService;
use Pannovate\BaseModelBundle\Services\ImageHelperService;

use Pannovate\ElavonModelBundle\Entity\ElavonUser;
use Pannovate\ElavonModelBundle\Entity\ElavonAdminPanelUser;
use Pannovate\ElavonModelBundle\Model\ShoppingCartItemModel;


use Pannovate\ElavonModelBundle\Entity\OrderBillingAddress;
use Pannovate\ElavonModelBundle\Entity\OrderShippingAddress;
use Pannovate\ElavonModelBundle\Entity\OrderCardDesign;
use Pannovate\ElavonModelBundle\Entity\OrderCurrency;
use Pannovate\ElavonModelBundle\Entity\ShoppingCart;
use Pannovate\ElavonModelBundle\Entity\ShoppingCartItem;
use Pannovate\ElavonModelBundle\Entity\CardDesignOrder;
use Pannovate\ElavonModelBundle\Entity\CardDesignOrderItem;

class CardDesignOrderModel extends Model
{
    protected $cardDesignfileUploadService;
    protected $cardDesignOrderfileUploadService;
    protected $imageHelperService;
    protected $shoppingCartItemModel;
    protected $billingAddressModel;
    protected $shippingAddressModel;
    protected $shippingChargeModel;
    protected $taxModel;

    public function setCardDesignFileUploadService(FileUploadService $fileUpload)
    {
        $this->cardDesignfileUploadService = $fileUpload;
    }

    public function getCardDesignFileUploadService()
    {
        return $this->cardDesignfileUploadService;
    }

    public function setCardDesignOrderFileUploadService(FileUploadService $fileUpload)
    {
        $this->cardDesignOrderfileUploadService = $fileUpload;
    }

    public function getCardDesignOrderFileUploadService()
    {
        return $this->cardDesignOrderfileUploadService;
    }

    public function setImageHelperService(ImageHelperService $imageHelper)
    {
        $this->imageHelperService = $imageHelper;
    }

    public function getImageHelperService()
    {
        return $this->imageHelperService;
    }

    public function setShoppingCartItemModel(ShoppingCartItemModel $model)
    {
        $this->shoppingCartItemModel = $model;
    }

    public function getShoppingCartItemModel()
    {
        return $this->shoppingCartItemModel;
    }

    public function setBillingAddressModel(BillingAddressModel $model)
    {
        $this->billingAddressModel = $model;
    }

    public function getBillingAddressModel()
    {
        return $this->billingAddressModel;
    }

    public function setShippingAddressModel(ShippingAddressModel $model)
    {
        $this->shippingAddressModel = $model;
    }

    public function getShippingAddressModel()
    {
        return $this->shippingAddressModel;
    }

    public function setShippingChargeModel(ShippingChargeModel $model)
    {
        $this->shippingCharge = $model;
    }

    public function getShippingChargeModel()
    {
        return $this->shippingCharge;
    }

    public function setTaxModel(TaxModel $model)
    {
        $this->taxModel = $model;
    }

    public function getTaxModel()
    {
        return $this->taxModel;
    }

    protected function updateEntityBeforeUpdate($entity)
    {
        $entity->setLastModifiedDatetime(new \DateTime());
    }

    public function create($data, ...$arguments)
    {
        $this->saveEntityArguments = $arguments;

        $shippingSameAsBilling = isset($data['shippingSameAsBilling']) ? $data['shippingSameAsBilling'] : false;

        $billingAddress = $this->getBillingAddressModel()->get($data['billingAddress']);

        $shippingAddress = null;

        if(isset($data['shippingAddress']) && (int)$data['shippingAddress'] > 0)
        {
            $shippingAddress = $this->getShippingAddressModel()->get($data['shippingAddress']);
        }

        if(!$shippingAddress && $shippingSameAsBilling !== true)
        {
            throw new InvalidFormDataException("Invalid shipping address", 400);
        }

        $entity = new CardDesignOrder();

        $orderBillingAddress = new OrderBillingAddress();
        $orderBillingAddress->setCompany($billingAddress->getCompany());
        $orderBillingAddress->setFirstName($billingAddress->getFirstName());
        $orderBillingAddress->setLastName($billingAddress->getLastName());
        $orderBillingAddress->setEmail($billingAddress->getEmail());
        $orderBillingAddress->setAddress($billingAddress->getAddress());
        $orderBillingAddress->setCity($billingAddress->getCity());
        $orderBillingAddress->setPostcode($billingAddress->getPostcode());
        $orderBillingAddress->setCountry($billingAddress->getCountry());
        $orderBillingAddress->setPhone($billingAddress->getPhone());

        $this->entityManager->persist($orderBillingAddress);
        $this->entityManager->flush();

        if(!$shippingAddress)
        {
            $shippingAddress = $billingAddress;   
        }

        $orderShippingAddress = new OrderShippingAddress();
        $orderShippingAddress->setFirstName($shippingAddress->getFirstName());
        $orderShippingAddress->setLastName($shippingAddress->getLastName());
        $orderShippingAddress->setEmail($shippingAddress->getEmail());
        $orderShippingAddress->setAddress($shippingAddress->getAddress());
        $orderShippingAddress->setCity($shippingAddress->getCity());
        $orderShippingAddress->setPostcode($shippingAddress->getPostcode());
        $orderShippingAddress->setCountry($shippingAddress->getCountry());
        $orderShippingAddress->setPhone($shippingAddress->getPhone());

        $this->entityManager->persist($orderShippingAddress);

        $currency = $this->saveEntityArguments[0]->getCurrency();
        $shoppingCart = $this->saveEntityArguments[0]->getShoppingCart();


        $orderCurrency = new OrderCurrency();
        $orderCurrency->setName($currency->getName());
        $orderCurrency->setCode($currency->getCode());
        $orderCurrency->setSymbol($currency->getSymbol());
        $orderCurrency->setRate($currency->getRate());


        $entity->setUser($this->saveEntityArguments[0]);
        $entity->setCurrency($orderCurrency);
        $entity->setBillingAddress($orderBillingAddress);
        $entity->setShippingAddress($orderShippingAddress);


        $shoppingCartItems = $shoppingCart->getShoppingCartItems();

        $orderCardDesign;
        $cardDesignOrderItem;

        $itemsTotalPrice = 0;
        $itemsTotalCount = 0;

        $oldImageName = '';

        $newImageName = '';
        $newImageUrl = '';

        foreach ($shoppingCartItems as $item)
        {
            $itemsTotalPrice += $item->getTotalPrice();

            $orderCardDesign = new OrderCardDesign();
            $orderCardDesign->setType($item->getCardDesign()->getType());
            $orderCardDesign->setName($item->getCardDesign()->getName());

            $orderCardDesign->setFrontDesignImageData($item->getCardDesign()->getFrontDesignImageData());

            $oldImageName = $item->getCardDesign()->getFrontImageName();
            $newImageName = $this->copyImageFile($oldImageName);
            $newImageUrl = $this->getCardDesignOrderFileUploadService()->getUrlFromFileName($newImageName, true);
            $orderCardDesign->setFrontImageName($newImageName); // create new image
            $orderCardDesign->setFrontImageUrl($newImageUrl); // set new image url

            $oldImageName = $this->getCardDesignFileUploadService()->getFileNameFromUrl($item->getCardDesign()->getFrontImageThumbUrl());
            $newImageName = $this->copyImageFile($oldImageName);
            $newImageUrl = $this->getCardDesignOrderFileUploadService()->getUrlFromFileName($newImageName, true);
            $orderCardDesign->setFrontImageThumbUrl($newImageUrl); // set new image url

            if(strlen($item->getCardDesign()->getFrontBackgroundImageUrl()) > 0)
            {
                $oldImageName = $this->getCardDesignFileUploadService()->getFileNameFromUrl($item->getCardDesign()->getFrontBackgroundImageUrl());
                $newImageName = $this->copyImageFile($oldImageName);
                $newImageUrl = $this->getCardDesignOrderFileUploadService()->getUrlFromFileName($newImageName, true);
                $orderCardDesign->setFrontBackgroundImageUrl($newImageUrl); // set new image url
            }
            else
            {
                $orderCardDesign->setFrontBackgroundImageUrl(null); // set new image url   
            }


            $orderCardDesign->setBackDesignImageData($item->getCardDesign()->getBackDesignImageData());


            $oldImageName = $item->getCardDesign()->getBackImageName();
            $newImageName = $this->copyImageFile($oldImageName);
            $newImageUrl = $this->getCardDesignOrderFileUploadService()->getUrlFromFileName($newImageName, true);
            $orderCardDesign->setBackImageName($newImageName); // create new image
            $orderCardDesign->setBackImageUrl($newImageUrl); // set new image url

            $oldImageName = $this->getCardDesignFileUploadService()->getFileNameFromUrl($item->getCardDesign()->getBackImageThumbUrl());
            $newImageName = $this->copyImageFile($oldImageName);
            $newImageUrl = $this->getCardDesignOrderFileUploadService()->getUrlFromFileName($newImageName, true);
            $orderCardDesign->setBackImageThumbUrl($newImageUrl); // set new image url

            if(strlen($item->getCardDesign()->getBackBackgroundImageUrl()) > 0)
            {
                $oldImageName = $this->getCardDesignFileUploadService()->getFileNameFromUrl($item->getCardDesign()->getBackBackgroundImageUrl());
                $newImageName = $this->copyImageFile($oldImageName);
                $newImageUrl = $this->getCardDesignOrderFileUploadService()->getUrlFromFileName($newImageName, true);
                $orderCardDesign->setBackBackgroundImageUrl($newImageUrl);
            }
            else
            {
                $orderCardDesign->setBackBackgroundImageUrl(null);
            }
            

            $this->entityManager->persist($orderCardDesign);

            $cardDesignOrderItem = new CardDesignOrderItem();
            $cardDesignOrderItem->setCardDesignOrder($entity);
            $cardDesignOrderItem->setCardDesign($orderCardDesign);
            $cardDesignOrderItem->setQuantity($item->getQuantity());
            $cardDesignOrderItem->setPricePerCard($item->getPricePerCard());
            $cardDesignOrderItem->setTotalPrice($item->getTotalPrice());
            $cardDesignOrderItem->setDenomination($item->getCardDenomination());
            $cardDesignOrderItem->setStatus(CardDesignOrderItem::STATUS_PENDING_APPROVAL);

            $this->entityManager->persist($cardDesignOrderItem);
            $entity->addCardDesignOrderItem($cardDesignOrderItem);

            $this->entityManager->remove($item);

            $itemsTotalCount++;
        }


        $entity->setOrderItemsPriceTotal($itemsTotalPrice);

        $shippingCharge = $this->getShippingChargeModel()->readForCountry($orderShippingAddress->getCountry()->getId());
        $shippingChargeAmount = $shippingCharge->getAmount();

        $entity->setShippingChargePriceTotal($shippingChargeAmount);

        $taxCharge = $this->getTaxModel()->readForCountry($orderBillingAddress->getCountry()->getId());
        $taxChargePercent = $taxCharge->getAmount();

        $entity->setTaxChargePriceTotal($itemsTotalPrice * $taxChargePercent / 100);

        $entity->setOrderPriceTotal($itemsTotalPrice + $entity->getTaxChargePriceTotal() + $shippingChargeAmount);
        $entity->setOrderItemsCount($itemsTotalCount);
        $entity->setStatus(CardDesignOrder::STATUS_NOT_PAYED);


        $shoppingCart->setCartPriceTotal(0);
        $shoppingCart->setShippingCharge(0);
        $shoppingCart->setTaxCharge(0);
        $shoppingCart->setCartItemsTotalCount(0);
        $shoppingCart->setCartItemsTotalPrice(0);

        $this->entityManager->persist($entity);
        $this->entityManager->persist($shoppingCart);
        $this->entityManager->flush();


        return $entity;
    }

    public function getLastModified()
    {
        $design = $this->entityManager
                       ->getRepository($this->entityName)
                       ->findOneBy(
                            array('lastModifiedDatetime' => "DESC")
                        );

        return $design;
    }

    public function getRecentCardDesignOrders()
    {
        $data = $this->entityManager
                     ->getRepository($this->entityName)
                     ->getRecentCardDesignOrders();
        
        return $data;
    }

    public function readForUser(ElavonUser $user)
    {
        $designs = $this->entityManager
                        ->getRepository($this->entityName)
                        ->findByUser($user);

        return $designs;
    }

    protected function copyImageFile($oldImageName)
    {
        $oldImageFullPath = $this->getCardDesignFileUploadService()->getPathFromFileName($oldImageName) . '/' . $oldImageName;
        $hashedFileName = $this->getCardDesignOrderFileUploadService()->getHashedFileName($oldImageName);

        $newFileFolderDirectory = $this->getCardDesignOrderFileUploadService()->createFileFolder($hashedFileName);

        $oldImageProperties = $this->getImageHelperService()->readImageProperties($oldImageFullPath);
        $newImageResource = $this->getImageHelperService()->getImageResourceFromPath($oldImageFullPath, $oldImageProperties['type']);

        $newImageFullPath = $newFileFolderDirectory . '/' . $hashedFileName;

        $this->getImageHelperService()->saveImage($newImageResource, $newImageFullPath, $oldImageProperties);

        return $hashedFileName;
    }
}
