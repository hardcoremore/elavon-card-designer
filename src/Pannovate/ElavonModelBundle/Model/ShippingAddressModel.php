<?php

namespace Pannovate\ElavonModelBundle\Model;

use Pannovate\ElavonModelBundle\Entity\ElavonUser;
use Pannovate\BaseModelBundle\Base\Model;

class ShippingAddressModel extends Model
{
    protected function updateEntityBeforeCreate($entity)
    {
        $entity->setUser($this->saveEntityArguments[0]);
    }

    public function readAll()
    {
        $shippingAddresses = $this->entityManager
            ->getRepository($this->entityName)
            ->findAll();

        return $shippingAddresses;
    }

    public function readForUser(ElavonUser $user)
    {
        $shippingAddresses = $this->entityManager
            ->getRepository($this->entityName)
            ->findByUser($user);

        return $shippingAddresses;
    }

    public function getLastCreatedAddressForUser(ElavonUser $user)
    {
        $billingAddress = $this->entityManager
            ->getRepository($this->entityName)
            ->findBy(array('user' => $user->getId()), array('user' => 'DESC'), 1);

        if($billingAddress && count($billingAddress) > 0)
        {
            return $billingAddress[0];
        }

        return null;
    }
}
