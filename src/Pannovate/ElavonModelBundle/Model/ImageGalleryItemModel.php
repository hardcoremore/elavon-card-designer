<?php

namespace Pannovate\ElavonModelBundle\Model;

use Doctrine\ORM\EntityManager;

use Pannovate\BaseModelBundle\Base\Model;

use Pannovate\ElavonModelBundle\Entity\User;
use Pannovate\BaseModelBundle\Services\FileUploadService;

class ImageGalleryItemModel extends Model
{
    protected $fileUploadService;
    protected $cardDesignImageElementModel;

    public function setFileUploadService(FileUploadService $fileUploadService)
    {
        $this->fileUploadService = $fileUploadService;
    }

    public function saveGalleryImageItem(array $galleryImageItemData)
    {
        $filePath = $galleryImageItemData['file_path'];
        $fileName = $galleryImageItemData['file_name'];

        $hashedFileName = $this->fileUploadService->saveUploadedFile($filePath, $fileName);
        $fileUrlPath = $this->fileUploadService->getUrlFromFileName($hashedFileName);

        return $this->create([
            'name' => $galleryImageItemData['name'],
            'category' => $galleryImageItemData['category'],
            'originalFileName' => $fileName,
            'fileName' => $hashedFileName,
            'filePath' => $this->fileUploadService->getPathFromFileName($hashedFileName, true),
            'fileUrl' => $this->fileUploadService->getUrlFromFileName($hashedFileName, true)
        ]);
    }

    public function findByCategoryId($categoryId)
    {
        $albumImages = $this->entityManager
                       ->getRepository($this->entityName)
                       ->findByCategory($categoryId);
        
        return $albumImages;
    }
}
