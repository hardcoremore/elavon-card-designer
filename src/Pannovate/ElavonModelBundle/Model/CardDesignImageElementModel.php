<?php

namespace Pannovate\ElavonModelBundle\Model;

use Doctrine\ORM\EntityManager;

use Pannovate\BaseModelBundle\Base\Model;

use Pannovate\ElavonModelBundle\Entity\User;
use Pannovate\BaseModelBundle\Services\FileUploadService;

class CardDesignImageElementModel extends Model
{
    protected $fileUploadService;
    protected $cardDesignImageElementModel;

    public function setFileUploadService(FileUploadService $fileUploadService)
    {
        $this->fileUploadService = $fileUploadService;
    }

    public function saveCardDesignImageElement(array $cardDesignImageElementData)
    {
        $filePath = $cardDesignImageElementData['file_path'];
        $fileName = $cardDesignImageElementData['file_name'];

        $hashedFileName = $this->fileUploadService->saveUploadedFile($filePath, $fileName);
        $fileUrlPath = $this->fileUploadService->getUrlFromFileName($hashedFileName);

        $this->create([
            'cardDesign' => $cardDesignImageElementData['id'],
            'originalFileName' => $fileName,
            'fileName' => $hashedFileName,
            'filePath' => $this->fileUploadService->getPathFromFileName($hashedFileName, true),
            'fileUrl' => $this->fileUploadService->getUrlFromFileName($hashedFileName, true),
            'elementType' => $cardDesignImageElementData['element_type'],
            'cardSide' => substr($cardDesignImageElementData['card_side'], 0, 1)
        ]);

        return $fileUrlPath;
    }
}
