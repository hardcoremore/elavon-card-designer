<?php

namespace Pannovate\ElavonModelBundle\Model;

use Pannovate\BaseModelBundle\Base\Model;

class TaxModel extends Model
{
    public function readForCountry($countryId)
    {
        $tax = $this->entityManager
            ->getRepository($this->entityName)
            ->findOneByCountry($countryId);

        return $tax;
    }
}