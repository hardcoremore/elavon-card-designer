<?php

namespace Pannovate\ElavonModelBundle\Model;

use Pannovate\ElavonModelBundle\Entity\ElavonUser;
use Pannovate\BaseModelBundle\Base\Model;

class BillingAddressModel extends Model
{
    protected function updateEntityBeforeCreate($entity)
    {
        $entity->setUser($this->saveEntityArguments[0]);
    }

    public function readAll()
    {
        $billingAddresses = $this->entityManager
            ->getRepository($this->entityName)
            ->findAll();

        return $billingAddresses;
    }

    public function readForUser(ElavonUser $user)
    {
        $billingAddresses = $this->entityManager
            ->getRepository($this->entityName)
            ->findByUser($user);

        return $billingAddresses;
    }

    public function getLastCreatedAddressForUser(ElavonUser $user)
    {
        $billingAddress = $this->entityManager
            ->getRepository($this->entityName)
            ->findBy(array('user' => $user->getId()), array('user' => 'DESC'), 1);

        if($billingAddress && count($billingAddress) > 0)
        {
            return $billingAddress[0];
        }

        return null;
    }
}
