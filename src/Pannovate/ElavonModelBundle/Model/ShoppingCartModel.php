<?php

namespace Pannovate\ElavonModelBundle\Model;

use Pannovate\ElavonModelBundle\Entity\ElavonUser;
use Pannovate\ElavonModelBundle\Entity\BillingAddress;
use Pannovate\ElavonModelBundle\Entity\ShippingAddress;

use Pannovate\BaseModelBundle\Base\Model;
use Pannovate\ElavonModelBundle\Model\ShoppingCartItemModel;

class ShoppingCartModel extends Model
{
    protected $shoppingCartItemModel;
    protected $taxModel;
    protected $shippingChargeModel;

    public function setShoppingCartItemModel(ShoppingCartItemModel $model)
    {
        $this->shoppingCartItemModel = $model;
    }

    public function getShoppingCartItemModel()
    {
        return $this->shoppingCartItemModel;
    }

    public function setTaxModel(TaxModel $model)
    {
        $this->taxModel = $model;
    }

    public function getTaxModel()
    {
        return $this->taxModel;
    }

    public function setShippingChargeModel(ShippingChargeModel $model)
    {
        $this->shippingChargeModel = $model;
    }

    public function getShippingChargeModel()
    {
        return $this->shippingChargeModel;
    }

    protected function updateEntityBeforeCreate($entity)
    {
        $entity->setUser($this->saveEntityArguments[0]);
    }

    public function readAll()
    {
        $shoppingCarts = $this->entityManager
            ->getRepository($this->entityName)
            ->findAll();

        return $shoppingCarts;
    }

    public function readForUser(ElavonUser $user)
    {
        $shoppingCarts = $this->entityManager
            ->getRepository($this->entityName)
            ->findByUser($user);

        return $shoppingCarts;
    }

    public function updateCartTotalPrice(
        ElavonUser $user,
        BillingAddress $billingAddress = null,
        ShippingAddress $shippingAddress = null,
        $shippingSameAsBilling = false)
    {
        $itemsTotalPrice = $this->getShoppingCartItemModel()->getItemsTotalPriceForShoppingCart($user->getShoppingCart());
        $itemsTotalCount = $this->getShoppingCartItemModel()->getItemsTotalCount($user->getShoppingCart());

        $tax = null;
        $shippingCharge = null;

        $taxPercent = 0;
        $taxAmount = 0;
        $shippingChargeAmount = 0;
        $country = null;

        if($billingAddress)
        {
            $tax = $this->getTaxModel()->readForCountry($billingAddress->getCountry()->getId());
        }

        if($shippingSameAsBilling)
        {
            $shippingCharge = $this->getShippingChargeModel()->readForCountry($billingAddress->getCountry()->getId());
        }
        else if($shippingAddress)
        {
            $shippingCharge = $this->getShippingChargeModel()->readForCountry($shippingAddress->getCountry()->getId());
        }        

        if($tax)
        {
            $taxPercent = $tax->getAmount();
        }

        if($shippingCharge)
        {
            $shippingChargeAmount = $shippingCharge->getAmount();
        }

        $totalCartPrice = $itemsTotalPrice + $shippingChargeAmount;

        if($taxPercent > 0)
        {
            $taxAmount = $totalCartPrice * $taxPercent / 100;
            //number_format((float)$taxAmount, 2, '.', '');

            $totalCartPrice += $taxAmount;          
        }

        $user->getShoppingCart()->setTaxCharge($taxAmount);
        $user->getShoppingCart()->setShippingCharge($shippingChargeAmount);
        $user->getShoppingCart()->setCartItemsTotalCount($itemsTotalCount);
        $user->getShoppingCart()->setCartPriceTotal($totalCartPrice);
        $user->getShoppingCart()->setCartItemsTotalPrice($itemsTotalPrice);
        $user->getShoppingCart()->setLastModifiedDatetime(new \DateTime());

        $this->entityManager->flush();

        return $user->getShoppingCart();
    }
}
