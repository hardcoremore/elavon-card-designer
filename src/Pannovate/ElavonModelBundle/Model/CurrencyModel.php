<?php

namespace Pannovate\ElavonModelBundle\Model;

use Pannovate\BaseModelBundle\Base\Model;

use Pannovate\BaseModelBundle\Exception\Form\InvalidFormDataException;

class CurrencyModel extends Model
{
    public function patch($id, $data, ...$arguments)
    {
        if(is_array($data) && count($data) === 1 && array_key_exists("isBase", $data))
        {
            $currentBaseCurrency = $this->getBaseCurrency($id);
            $newBaseCurrency = $this->get($id);

            if($newBaseCurrency)
            {
                $allCurrencies = $this->readAll();
                $newRate;

                foreach($allCurrencies as $currency)
                {
                    if($currency->getId() ===  $newBaseCurrency->getId())
                    {
                        continue;
                    }
                    else
                    {
                        $newRate = 1 / $newBaseCurrency->getRate() * $currency->getRate();
                        $currency->setRate($newRate);
                        $this->entityManager->persist($currency);
                    }
                }

                $currentBaseCurrency->setIsBase(false);

                $newBaseCurrency->setRate(1);
                $newBaseCurrency->setIsBase(true);

                $this->entityManager->persist($newBaseCurrency);
                $this->entityManager->flush();
            }
            else
            {
                throw new InvalidFormDataException("Currency id is not valid", 400);
            }
        }    
        else
        {
            parent::patch($id, $data, $arguments);
        }
    }

    public function getBaseCurrency()
    {
        $data = $this->entityManager
                     ->getRepository($this->entityName)
                     ->findOneBy(array('isBase' => true));

        return $data;
    }
}