<?php

namespace Pannovate\ElavonModelBundle\Model;

use Pannovate\BaseAPIBundle\Services\CurlRequest;
use Pannovate\ElavonModelBundle\Services\ElavonFanfareAPIRequestBuilder;

class ElavonFanfareAPIModel
{

    protected $request;
    protected $requestBuilder;
    protected $options;
    protected $xml;

    public function __construct(CurlRequest $request, ElavonFanfareAPIRequestBuilder $requestBuilder, array $options)
    {
        $this->request = $request;
        $this->requestBuilder = $requestBuilder;
        $this->options = $options;
    }

    public function addCardSet($fanfareId, $programId, $cardType, $quantity, $amount = null)
    {
        $requestParams = [
            'xid'             => strtotime('now') . rand(1, 999),
            'serviceIdentity' => '99999999-9999-8888-0007-999999999999',
            'fanfareId'       => $fanfareId,
            'programId'       => $programId,
            'cardType'        => $cardType,
            'quantity'        => $quantity,
            'amount'          => $amount
        ];

        $requestData = $this->requestBuilder->buildAddCardSet($requestParams);
        $url = $this->options['base_url'] . '/addCardSet';
        $response = $this->request->post($url, $requestData, ['Content-Type: application/xml']);

        return $this->xmlToJson($response['body']);
    }

    public function getCardSet($fanfareId, $programId, $cardSetId = null)
    {
        $requestParams = [
            'serviceIdentity' => $this->options['service_identity'],
            'fanfareId' => $fanfareId,
            'programId' => $programId,
        ];

        if($cardSetId) {
            $requestParams['cardSetId'] = $cardSetId;
        }

        $requestData = $this->requestBuilder->buildGetCardSet($requestParams);
        $url  = $this->options['base_url'] . '/getCardSet';
        $response = $this->request->post($url, $requestData, ['Content-Type: application/xml']);

        return $this->xmlToJson($response['body']);
    }

    public function getPrograms($fanfareId)
    {
        $requestParams = [
            'serviceIdentity' => $this->options['service_identity'],
            'fanfareId' => $fanfareId,
        ];

        $requestData = $this->requestBuilder->buildGetPrograms($requestParams);
        $url  = $this->options['base_url'] . '/getPrograms';
        $response = $this->request->post($url, $requestData, ['Content-Type: application/xml']);

        return $this->xmlToJson($response['body']);
    }

    protected function xmlToJson($xmlString)
    {
        $xml = simplexml_load_string($xmlString);
        $jsonString = json_encode($xml);

        return json_decode($jsonString, TRUE);
    }
}
