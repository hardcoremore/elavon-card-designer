<?php

namespace Pannovate\ElavonModelBundle\Model;

use Pannovate\ElavonModelBundle\Entity\ShoppingCart;

use Pannovate\BaseModelBundle\Base\Model;

class ShoppingCartItemModel extends Model
{
    protected function updateEntityBeforeCreate($entity)
    {
        $entity->setShoppingCart($this->saveEntityArguments[0]->getShoppingCart());
        $entity->setTotalPrice($entity->getPricePerCard() * $entity->getQuantity());
    }

    public function readAll()
    {
        $shoppingCartItems = $this->entityManager
            ->getRepository($this->entityName)
            ->findAll();

        return $shoppingCartItems;
    }

    public function readForShoppingCart(ShoppingCart $shoppingCart)
    {
        $shoppingCartItems = $this->entityManager
            ->getRepository($this->entityName)
            ->findByShoppingCart($shoppingCart);

        return $shoppingCartItems;
    }

    public function findByCardDesign($cardDesignId)
    {
        $shoppingCartItem = $this->entityManager
            ->getRepository($this->entityName)
            ->findOneByCardDesign($cardDesignId);

        return $shoppingCartItem;
    }

    public function getItemsTotalPriceForShoppingCart(ShoppingCart $shoppingCart)
    {
        $shoppingCartItems = $this->readForShoppingCart($shoppingCart);

        $totalPrice = 0;

        foreach($shoppingCartItems as $item)
        {
            $totalPrice += $item->getTotalPrice();
        }

        return $totalPrice;
    }

    public function getItemsTotalCount(ShoppingCart $shoppingCart)
    {
        $shoppingCartItems = $this->readForShoppingCart($shoppingCart);

        $totalItemsCount = 0;

        foreach($shoppingCartItems as $item)
        {
            $totalItemsCount++;

            // @to-do count for card denominations
        }

        return $totalItemsCount;   
    }
}
