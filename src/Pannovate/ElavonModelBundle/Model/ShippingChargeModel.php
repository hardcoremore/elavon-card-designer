<?php

namespace Pannovate\ElavonModelBundle\Model;

use Pannovate\BaseModelBundle\Base\Model;

use Pannovate\ElavonModelBundle\Entity\Country;

class ShippingChargeModel extends Model
{
    public function readForCountry($countryId)
    {
        $shippingCharge = $this->entityManager
            ->getRepository($this->entityName)
            ->findOneByCountry($countryId);

        return $shippingCharge;
    }
}