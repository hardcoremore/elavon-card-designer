<?php

namespace Pannovate\ElavonModelBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ElavonModelExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('forms.yml');
        $loader->load('models.yml');

        $loader->load('forms/elavon_user.yml');
        $loader->load('forms/elavon_admin_panel_user.yml');
        $loader->load('forms/card_design.yml');
        $loader->load('forms/card_design_image_element.yml');
        $loader->load('forms/shipping_address.yml');
        $loader->load('forms/billing_address.yml');
        $loader->load('forms/currency.yml');
        $loader->load('forms/tax.yml');
        $loader->load('forms/shipping_charge.yml');
        $loader->load('forms/card_pricing.yml');
        $loader->load('forms/image_gallery_category.yml');
        $loader->load('forms/image_gallery_item.yml');
        $loader->load('forms/shopping_cart.yml');
        $loader->load('forms/shopping_cart_item.yml');
        $loader->load('forms/card_design_order.yml');
    }
}
