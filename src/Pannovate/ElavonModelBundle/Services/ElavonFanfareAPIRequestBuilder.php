<?php

namespace Pannovate\ElavonModelBundle\Services;

class ElavonFanfareAPIRequestBuilder
{
    protected $namespace;

    public function __construct($namespace)
    {
        $this->namespace = $namespace;
    }

    public function buildAddCardSet($data)
    {
        $xml = new \DOMDocument();
        $xmlRoot = $this->createXMLRoot($xml, 'AddCardSetRequest');
        $xmlRoot->appendChild($xml->createElement('xid', strtotime('now') . rand(1, 999)));
        $xmlRoot->appendChild($xml->createElement('serviceIdentity', $data['serviceIdentity']));
        $xmlRoot->appendChild($xml->createElement('fanfareId', $data['fanfareId']));
        $xmlRoot->appendChild($xml->createElement('programId', $data['programId']));
        $xmlRoot->appendChild($xml->createElement('cardType', $data['cardType']));
        $xmlRoot->appendChild($xml->createElement('quantity', $data['quantity']));
        $xmlRoot->appendChild($xml->createElement('amount', $data['amount']));
    }

    public function buildGetCardSet($data)
    {
        $xml = new \DOMDocument();
        $xmlRoot = $this->createXMLRoot($xml, 'GetCardSetRequest');
        $xmlRoot->appendChild($xml->createElement('xid', strtotime('now') . rand(1, 999)));
        $xmlRoot->appendChild($xml->createElement('serviceIdentity', $data['serviceIdentity']));
        $xmlRoot->appendChild($xml->createElement('fanfareId', $data['fanfareId']));
        $xmlRoot->appendChild($xml->createElement('programId', $data['programId']));

        if(isset($data['cardSetId']))
        {
            $xmlRoot->appendChild($xml->createElement('cardSetId', $data['cardSetId']));
        }

        return $xml->saveXML($xml->documentElement);
    }

    public function buildGetPrograms($data)
    {
        $xml = new \DOMDocument();
        $xmlRoot = $this->createXMLRoot($xml, 'ProgramListRequest');
        $xmlRoot->appendChild($xml->createElement('xid', strtotime('now') . rand(1, 999)));
        $xmlRoot->appendChild($xml->createElement('serviceIdentity', $data['serviceIdentity']));
        $xmlRoot->appendChild($xml->createElement('fanfareId', $data['fanfareId']));

        return $xml->saveXML($xml->documentElement);
    }

    protected function createXMLRoot(\DOMDocument $xml, $methodName)
    {
        $xmlRoot   = $xml->createElement($methodName);
        $xmlRoot->setAttribute('xmlns', $this->namespace);
        $xmlRoot = $xml->appendChild($xmlRoot);

        return $xmlRoot;
    }
}
