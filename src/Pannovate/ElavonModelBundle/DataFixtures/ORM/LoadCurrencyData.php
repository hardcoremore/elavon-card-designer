<?php

namespace Pannovate\ElavonModelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Pannovate\ElavonModelBundle\Entity\Currency;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCurrencyData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    private $currencies = array(
        'Pound',
        'Euro',
    );

    private $currencyCodes = array(
        'GBP',
        'EUR',
    );

    private $currencySymbols = array(
        '£',
        '€',
    );

     /**
     * @var ContainerInterface
     */
    private $container;

    public function getOrder()
    {
        return 4; // the order in which fixtures will be loaded
    }

    /**
     * @inheritDoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $currency = null;
        $numberOfCurrencies = count($this->currencies);

        for($i = 0; $i < $numberOfCurrencies; $i++)
        {
            $currency = new Currency();

            $currency->setName($this->currencies[$i]);
            $currency->setCode($this->currencyCodes[$i]);
            $currency->setSymbol($this->currencySymbols[$i]);

            if($i === 0)
            {
                $currency->setIsBase(true);
                $currency->setRate(1);
            }
            else
            {
                $currency->setIsBase(false);
                $currency->setRate(mt_rand(1, 200) / 1000);
            }

            $manager->persist($currency);
        }

        $manager->flush();
    }
}
