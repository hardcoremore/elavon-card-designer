<?php

namespace Pannovate\ElavonModelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Pannovate\ElavonModelBundle\Entity\ElavonAdminPanelUser;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadElavonAdminPanelUserData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    static $NUMBER_OF_USERS = 10;

     /**
     * @var ContainerInterface
     */
    private $container;

    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }

    /**
     * @inheritDoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = new ElavonAdminPanelUser();

        $user->setFirstName('Super');
        $user->setLastName('Admin');

        $user->setUsername('superadmin');
        $user->setEmail('super.admin@elavon.pannovate.com');

        $this->setUserPassword($user, 'superadmin');

        $user->setIsActive(true);

        $manager->persist($user);

        $manager->flush();
    }

    public function createUsername(ElavonAdminPanelUser $user)
    {
        $firstName = strtolower($user->getFirstName());
        $firstName = substr($firstName, 0, 1);

        return $firstName . strtolower($user->getLastName());
    }

    public function createEmail(ElavonAdminPanelUser $user, $index)
    {
        return strtolower($user->getFirstName()) . '.' . strtolower($user->getLastName()) . '-' . $index . "@email.com";
    }

    public function setUserPassword(ElavonAdminPanelUser $user, $password)
    {
        $passwordEncoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPassword = $passwordEncoder->encodePassword($password, $user->getSalt());

        $user->setPassword($encodedPassword);
    }
}
