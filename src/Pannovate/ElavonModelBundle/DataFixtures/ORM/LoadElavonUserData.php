<?php

namespace Pannovate\ElavonModelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Pannovate\ElavonModelBundle\Entity\ElavonUser;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{

    static $NUMBER_OF_USERS = 10;

     /**
     * @var ContainerInterface
     */
    private $container;

    private $firstNames = array(
        'Bayo',
        'Taiwo',
        'Wumi',
        'Lukman',
        'Vladimir',
        'Damir',
        'Vladimir',
        'Dragomir',
        'Andrija',
    );

    private $lastNames = array(
        'Okunowo',
        'Akinseye',
        'Ogheotuoma',
        'Ottun',
        'Trbovic',
        'Mehic',
        'Marinovic',
        'Krstic',
        'Bednarik'
    );

    protected $userTypes = array('email', 'facebook', 'google+', 'twitter');

    protected $genders = array('m', 'f');

    protected $city = array('Belgrade', 'Novi Sad', 'Madrid', 'Bristol', 'Zemun', 'London', 'Brussel', 'Barcelona', 'Birmingham', 'Anderleht');

    protected $programs = array('Loyalty', 'Promo', 'Gift');

    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }

    /**
     * @inheritDoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $user = null;

        $random = 0;

        $numberOfOptions = count($this->firstNames) - 1;
        
        for($i = 0; $i < self::$NUMBER_OF_USERS; $i++)
        {
            $user = new ElavonUser();

            $user->setFirstName($this->firstNames[mt_rand(0, $numberOfOptions)]);
            $user->setLastName($this->lastNames[mt_rand(0, $numberOfOptions)]);

            $user->setUsername($this->createUsername($user) . '-' . ($i+1));
            $user->setEmail($this->createEmail($user, $i+1));

            $user->setCity($this->city[$i]);
            $user->setPrograms($this->programs[mt_rand(0, 2)]);

            $this->setUserPassword($user, $user->getUsername());

            $user->setIsActive(true);

            $manager->persist($user);
        }

        $manager->flush();
    }

    public function createUsername(ElavonUser $user)
    {
        $firstName = strtolower($user->getFirstName());
        $firstName = substr($firstName, 0, 1);

        return $firstName . strtolower($user->getLastName());
    }

    public function createEmail(ElavonUser $user, $index)
    {
        return strtolower($user->getFirstName()) . '.' . strtolower($user->getLastName()) . '-' . $index . "@email.com";
    }

    public function setUserPassword(ElavonUser $user, $password)
    {
        $passwordEncoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPassword = $passwordEncoder->encodePassword($password, $user->getSalt());

        $user->setPassword($encodedPassword);
    }
}
