<?php

namespace Pannovate\ElavonModelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Pannovate\ElavonModelBundle\Entity\Country;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCountryData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    private $countries = array(
        'England',
        'Ireland',
        'France',
        'Spain',
        'Serbia',
        'Belgium',
        'Germany'
    );

    private $countryCodes = array(
        'UK',
        'IE',
        'FR',
        'ES',
        'RS',
        'BE',
        'DE'
    );

     /**
     * @var ContainerInterface
     */
    private $container;

    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }

    /**
     * @inheritDoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $country = null;
        $numberOfCountries = count($this->countries);

        for($i = 0; $i < $numberOfCountries; $i++)
        {
            $country = new Country();

            $country->setName($this->countries[$i]);
            $country->setCode($this->countryCodes[$i]);

            $manager->persist($country);
        }

        $manager->flush();
    }
}
