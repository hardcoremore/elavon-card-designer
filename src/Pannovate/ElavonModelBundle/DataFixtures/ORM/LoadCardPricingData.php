<?php

namespace Pannovate\ElavonModelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Pannovate\ElavonModelBundle\Entity\CardPricing;
use Pannovate\ElavonModelBundle\Entity\Currency;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadCardPricingData extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    private $minimum = array(1, 51, 101, 501, 1001);


    private $maximum = array(50, 100, 500, 1000, 2000);

    private $price = array(0.25, 0.5, 0.75, 1, 1.25);

     /**
     * @var ContainerInterface
     */
    private $container;

    public function getOrder()
    {
        return 5; // the order in which fixtures will be loaded
    }

    /**
     * @inheritDoc
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {   
        $numberOfOptions = count($this->price) - 1;
        $cardPricing = null;

        for($i = 0; $i < count($this->minimum)-1; $i++)
        {   
            $cardPricing = new CardPricing();

            $cardPricing->setMinimumCards($this->minimum[$i]);
            $cardPricing->setMaximumCards($this->maximum[$i]);
            $cardPricing->setCurrency($manager->getRepository('ElavonModelBundle:Currency')->find(mt_rand(1, 2)));
            $cardPricing->setLoyalty($this->price[mt_rand(0, $numberOfOptions)]);
            $cardPricing->setPromo($this->price[mt_rand(0, $numberOfOptions)]);
            $cardPricing->setGift($this->price[mt_rand(0, $numberOfOptions)]);


            $manager->persist($cardPricing);
        }

        $manager->flush();
    }
}
